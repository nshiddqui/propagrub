<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductIngredientsCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductIngredientsCategoriesTable Test Case
 */
class ProductIngredientsCategoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductIngredientsCategoriesTable
     */
    public $ProductIngredientsCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ProductIngredientsCategories',
        'app.Users',
        'app.MenuProducts',
        'app.Ingredients',
        'app.ProductIngredients',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductIngredientsCategories') ? [] : ['className' => ProductIngredientsCategoriesTable::class];
        $this->ProductIngredientsCategories = TableRegistry::getTableLocator()->get('ProductIngredientsCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductIngredientsCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
