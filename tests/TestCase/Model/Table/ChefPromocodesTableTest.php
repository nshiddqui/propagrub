<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChefPromocodesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChefPromocodesTable Test Case
 */
class ChefPromocodesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChefPromocodesTable
     */
    public $ChefPromocodes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ChefPromocodes',
        'app.Users',
        'app.PromoCodes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ChefPromocodes') ? [] : ['className' => ChefPromocodesTable::class];
        $this->ChefPromocodes = TableRegistry::getTableLocator()->get('ChefPromocodes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ChefPromocodes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
