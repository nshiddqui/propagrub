<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommunityMemberTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommunityMemberTable Test Case
 */
class CommunityMemberTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CommunityMemberTable
     */
    public $CommunityMember;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CommunityMember',
        'app.Communities',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CommunityMember') ? [] : ['className' => CommunityMemberTable::class];
        $this->CommunityMember = TableRegistry::getTableLocator()->get('CommunityMember', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommunityMember);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
