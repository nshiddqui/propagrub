<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportedCommunitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportedCommunitiesTable Test Case
 */
class ReportedCommunitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportedCommunitiesTable
     */
    public $ReportedCommunities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ReportedCommunities',
        'app.Communities',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ReportedCommunities') ? [] : ['className' => ReportedCommunitiesTable::class];
        $this->ReportedCommunities = TableRegistry::getTableLocator()->get('ReportedCommunities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportedCommunities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
