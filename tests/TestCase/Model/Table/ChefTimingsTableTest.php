<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChefTimingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChefTimingsTable Test Case
 */
class ChefTimingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChefTimingsTable
     */
    public $ChefTimings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ChefTimings',
        'app.Users',
        'app.Days',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ChefTimings') ? [] : ['className' => ChefTimingsTable::class];
        $this->ChefTimings = TableRegistry::getTableLocator()->get('ChefTimings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ChefTimings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
