<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserReviewsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserReviewsTable Test Case
 */
class UserReviewsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserReviewsTable
     */
    public $UserReviews;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserReviews',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserReviews') ? [] : ['className' => UserReviewsTable::class];
        $this->UserReviews = TableRegistry::getTableLocator()->get('UserReviews', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserReviews);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
