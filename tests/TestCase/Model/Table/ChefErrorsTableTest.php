<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChefErrorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChefErrorsTable Test Case
 */
class ChefErrorsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChefErrorsTable
     */
    public $ChefErrors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ChefErrors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ChefErrors') ? [] : ['className' => ChefErrorsTable::class];
        $this->ChefErrors = TableRegistry::getTableLocator()->get('ChefErrors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ChefErrors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
