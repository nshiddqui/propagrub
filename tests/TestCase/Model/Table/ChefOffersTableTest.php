<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChefOffersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChefOffersTable Test Case
 */
class ChefOffersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChefOffersTable
     */
    public $ChefOffers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ChefOffers',
        'app.Chefs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ChefOffers') ? [] : ['className' => ChefOffersTable::class];
        $this->ChefOffers = TableRegistry::getTableLocator()->get('ChefOffers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ChefOffers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
