<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChefServicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChefServicesTable Test Case
 */
class ChefServicesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChefServicesTable
     */
    public $ChefServices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ChefServices',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ChefServices') ? [] : ['className' => ChefServicesTable::class];
        $this->ChefServices = TableRegistry::getTableLocator()->get('ChefServices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ChefServices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
