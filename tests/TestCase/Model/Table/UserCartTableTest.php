<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserCartTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserCartTable Test Case
 */
class UserCartTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserCartTable
     */
    public $UserCart;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserCart',
        'app.Users',
        'app.MenuProducts',
        'app.Menus',
        'app.UserChefOrders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserCart') ? [] : ['className' => UserCartTable::class];
        $this->UserCart = TableRegistry::getTableLocator()->get('UserCart', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserCart);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
