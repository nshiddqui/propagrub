<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MenuProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MenuProductsTable Test Case
 */
class MenuProductsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MenuProductsTable
     */
    public $MenuProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MenuProducts',
        'app.Users',
        'app.Menus',
        'app.Categories',
        'app.MenuProductsImages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MenuProducts') ? [] : ['className' => MenuProductsTable::class];
        $this->MenuProducts = TableRegistry::getTableLocator()->get('MenuProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MenuProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
