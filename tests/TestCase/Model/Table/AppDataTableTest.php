<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AppDataTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AppDataTable Test Case
 */
class AppDataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AppDataTable
     */
    public $AppData;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AppData',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AppData') ? [] : ['className' => AppDataTable::class];
        $this->AppData = TableRegistry::getTableLocator()->get('AppData', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AppData);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
