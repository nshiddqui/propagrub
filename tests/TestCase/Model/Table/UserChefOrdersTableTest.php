<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserChefOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserChefOrdersTable Test Case
 */
class UserChefOrdersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserChefOrdersTable
     */
    public $UserChefOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserChefOrders',
        'app.Chefs',
        'app.UserCart',
        'app.Customers',
        'app.UserAddress',
        'app.UserCards',
        'app.Drivers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserChefOrders') ? [] : ['className' => UserChefOrdersTable::class];
        $this->UserChefOrders = TableRegistry::getTableLocator()->get('UserChefOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserChefOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
