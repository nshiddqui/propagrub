<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserOtpTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserOtpTable Test Case
 */
class UserOtpTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserOtpTable
     */
    public $UserOtp;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserOtp',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserOtp') ? [] : ['className' => UserOtpTable::class];
        $this->UserOtp = TableRegistry::getTableLocator()->get('UserOtp', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserOtp);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
