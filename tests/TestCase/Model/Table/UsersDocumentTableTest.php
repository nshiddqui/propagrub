<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersDocumentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersDocumentTable Test Case
 */
class UsersDocumentTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersDocumentTable
     */
    public $UsersDocument;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersDocument',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersDocument') ? [] : ['className' => UsersDocumentTable::class];
        $this->UsersDocument = TableRegistry::getTableLocator()->get('UsersDocument', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersDocument);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
