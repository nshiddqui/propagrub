<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WebsiteUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WebsiteUsersTable Test Case
 */
class WebsiteUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WebsiteUsersTable
     */
    public $WebsiteUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WebsiteUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WebsiteUsers') ? [] : ['className' => WebsiteUsersTable::class];
        $this->WebsiteUsers = TableRegistry::getTableLocator()->get('WebsiteUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WebsiteUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
