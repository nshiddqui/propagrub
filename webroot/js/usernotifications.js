$(document).ready(function () {
    $('#select-data-user').select2({
        width: '100%',
        placeholder: 'Select an User',
        ajax: {
            url: '/admin/api/v1/users/',
            dataType: 'json',
            delay: 250,
            cache: true
        }
    });
    $('#user-type').change();
    $('#user-type').on('change', function () {
        var val = $(this).val();
        if (val === 'custom') {
            $('#UserType').show();
        } else {
            $('#UserType').hide();
        }
    });
});