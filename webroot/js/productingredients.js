$(document).ready(function () {
    $('#menu-id').on('change', function () {
        var id = $(this).val();
        $.get('/api/v1/menu-categories', {
            menu_id: id
        }, function (res) {
            $('#category-id').html('');
            $.each(res.data, function (key, value) {
                $('#category-id').append('<option value="' + value.id + '">' + value.category_name + '</option>')
            });
            getProduct();
        })
    });

    function getProduct() {
        var id = $('#category-id').val();
        $.get('/api/v1/menu-products', {
            category_id: id
        }, function (res) {
            $('#product-id').html('');
            $.each(res.data, function (key, value) {
                $('#product-id').append('<option value="' + value.id + '">' + value.product_name + '</option>')
            });
        })
    }

    $('#category-id').on('change', function () {
        getProduct();
    });
});