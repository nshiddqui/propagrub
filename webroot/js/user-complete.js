$(document).ready(function () {
    if (jQuery().steps) {
        var autoNext = false;
        $("#chef-profile").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex) {
                var success = true;
                if (newIndex > currentIndex) {
                    $('#chef-profile-p-' + currentIndex + ' input').each(function () {
                        if (typeof $(this).attr('readonly') === 'undefined' && $(this).attr('type') !== 'hidden') {
                            success = $(this).is(':valid');
                            if (success === false) {
                                return false;
                            }
                        }
                    });
                    if ($('#email').prop('readonly') === false) {
                        if (autoNext) {
                            autoNext = false;
                        } else {
                            if (currentIndex == 1) { //I suppose 0 is the first step
                                success = false;
                                $.ajax({
                                    url: EMAIL_URL + '/' + $('#email').val(),
                                    success: function (response) {
                                        if (response.exists) {
                                            $.alert('This email is already taken. Please enter another email');
                                            success = false;
                                        } else {
                                            $.ajax({
                                                url: MOBILE_URL + '/' + $('#mobile').val(),
                                                success: function (response) {
                                                    if (response.exists) {
                                                        $.alert('This mobile number is already taken. Please enter another mobile number');
                                                        success = false;
                                                    } else {
                                                        autoNext = true;
                                                        $("#chef-profile").steps('next');
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    }
                    if (success === false) {
                        setTimeout(function () {
                            $('#user-complete')[0].reportValidity();
                        }, 30);
                    } else {
                        var formData = $('#user-complete').serialize();
                        $.ajax({
                            url: API_URL + '?validate=false',
                            type: 'POST',
                            data: formData
                        });
                    }
                }
                return success;
            },
            onInit: function () {
                /*
                 * Load if plugin not work
                 * loadElement();
                 */
                $('#chef-profile a[href="#next"]').text('save and Next');
            }
        });
    }
    $('#chef-detail-image').on('change', function () {
        if (this.files && this.files[0]) {
            $('label[for="chef-detail-image"] #thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('label[for="chef-detail-image"] #thumbnail-image').attr('src', $('label[for="chef-detail-image"] #thumbnail-image').attr('default-image'));
        }
    });
    // $('#image').on('change', function() {
    //     if (this.files && this.files[0]) {
    //         $('label[for="image"] #thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
    //     } else {
    //         $('label[for="image"] #thumbnail-image').attr('src', $('label[for="image"] #thumbnail-image').attr('default-image'));
    //     }
    // });
    $('a[href="#finish"]').on('click', function () {
        if ($('#user-complete')[0].reportValidity()) {
            $('#user-complete').submit();
        }
    });
    $('.every-day input#start-time').on('change', function () {
        $('.single-day input[start-time]').val($(this).val());
    });
    $('.every-day input#end-time').on('change', function () {
        $('.single-day input[end-time]').val($(this).val());
    });
    $('#chef-detail-sole-trader').on('click', function () {
        propCheckbox();
    });
    $('#chef-detail-application-option').on('click', function () {
        propApplicationCheckbox();
    });
    propCheckbox();
    propApplicationCheckbox();
    initializeSettingMap();


    $('.upload-input').on('change', function () {
        var image_src = $(this).parents('.dropzone').find('.thumbnail-image');
        if (this.files.length > 0) {
            const fsize = this.files.item(0).size;
            const file = Math.round((fsize / 1024));
            if (file > 2048) {
                $.alert("File too Big, please select a file less than 2mb");
                image_src.attr('src', image_src.attr('default-image'));
                $(this).val('');
                return false;
            }
        }
        if (this.files && this.files[0]) {
            image_src.attr('src', URL.createObjectURL(this.files[0]));
        } else {
            image_src.attr('src', image_src.attr('default-image'));
        }
    });
    $('#password').on('keyup, keydown', function () {
        if ($(this).val() != '') {
            $('#confirm-password').prop('required', true);
            $('#current-password').prop('required', true);
        } else {
            $('#confirm-password').prop('required', false);
            $('#current-password').prop('required', false);
        }
    });
});

function propCheckbox() {
    if ($('#chef-detail-sole-trader').prop('checked')) {
        $('.company_required').hide();
        $('.sole_trader_required').show();
        $('.company_required input').prop('required', false);
        $('.sole_trader_required input').prop('required', true);
    } else {
        $('.company_required').show();
        $('.sole_trader_required').hide();
        $('.company_required input').prop('required', true);
        $('.sole_trader_required input').prop('required', false);
    }
}

function propApplicationCheckbox() {
    if ($('#chef-detail-application-option').prop('checked')) {
        $('.application_option').show();
        $('.company_doc_required input').addClass('disbale-file');
        $('.company_doc_required input').val('');
        $('.company_doc_required input').prop('required', false);
        $('.application_option input').prop('required', true);
    } else {
        $('.application_option').hide();
        $('.company_doc_required input').prop('required', true);
        $('.company_doc_required input').removeClass('disbale-file');
        $('.application_option input').prop('required', false);
    }
    $('.wizard .content').animate({
        height: $('.body.current').outerHeight()
    }, 'slow');
}