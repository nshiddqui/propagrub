$(document).ready(function() {
    setTimeout(function() {
        loadElement();
    }, 30);
});
$(document).ajaxComplete(function() {
    loadElement();
});

function loadElement() {
    //Initialize Plugin
    if (jQuery().validate) {
        $("form").validate();
    }
    if (jQuery().select2) {
        $('select.select2').select2({ width: '100%' });
    }
    if (jQuery().SumoSelect) {
        $('select.sumoselect').SumoSelect({ search: true, placeholder: 'Select', csvDispCount: 1 });
    }
    if (jQuery().iCheck) {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
    }
    if (jQuery().datepicker) {
        $('input[datepicker]').each(function() {
            var format = 'yyyy-mm-dd';
            if (this.hasAttribute('format')) {
                format = $(this).attr('format');
            }
            $(this).datepicker({
                autoclose: true,
                format: format
            });
        })
    }
    if (jQuery().fancybox) {
        $('[fancybox]').fancybox();
    }
}