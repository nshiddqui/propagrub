$(document).ready(function () {
    $('.upload-input').on('change', function () {
        var image_src = $(this).parents('.dropzone').find('.thumbnail-image');
        if (this.files && this.files[0]) {
            image_src.attr('src', URL.createObjectURL(this.files[0]));
        } else {
            image_src.attr('src', image_src.attr('default-image'));
        }
    });
    $('form').on('submit', function (ev) {
        var mobile = $('#phone').val();
        if (mobile.length < 1) {
            ev.preventDefault();
            alert('Mobile is required');
        }
        $("#phone").val('+' + $("#phone").intlTelInput("getSelectedCountryData").dialCode + mobile);
    });
    if (jQuery().intlTelInput) {
        setTimeout(function () {
            $("#phone").intlTelInput({
                initialCountry: "auto",
                separateDialCode: true,
                onlyCountries: ['gb', 'in'],
                geoIpLookup: function (callback) {
                    $.get('https://ipinfo.io/' + IP_ADDRESS, {
                        token: '101bda4458f672'
                    }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/7.0.1/lib/libphonenumber/build/utils.js" // just for formatting/placeholders etc
            }).then(function () {
                var el = $('#phone');
                el.val(el.val().replace(' ', ''));
            });
        }, 30);
    }
});