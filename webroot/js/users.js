var goBack = [];
var initWidth = 20;
$(document).ready(function () {
    $('#hygiene-detail-btn').on('click', function () {
        var rating = $('#rating');
        if (rating.prop('required') && rating.val().trim() == '') {
            alert('Please enter hygiene rating ID');
            return;
        }
        if (rating.prop('required')) {
            $('#rating-div-text').hide();
            $('#rating-text').html(rating.val());
        } else {
            $('#rating-div-text').hide();
        }
        goBack.push('6');
        $('#stage-6').hide();
        $('#stage-7').show();
        progress(true);
    });
    $('#resturent-detail-btn').on('click', function () {
        var company_name = $('#resturant-name');
        var chef_cuisines = $('#chef-cuisines');
        var mobile = $('#mobile');
        var otp = $('#otp');
        var lat = $('#lat');
        if (company_name.val().trim() == '') {
            alert('Please enter restaurant name');
            return;
        }
        if (mobile.val().trim() == '') {
            alert('Please enter phone number');
            return;
        }
        if (otp.val().trim() == '') {
            alert('Please enter otp');
            return;
        }
        if (chef_cuisines.val().length == 0) {
            alert('Please enter cuisines');
            return;
        }
        if (lat.val().trim() == '') {
            alert('Please enter store address');
            return;
        }
        $.get(VERIFY_OTP_URL + '?mobile=' + $('#mobile').intlTelInput("getNumber").replaceAll(' ', '') + '&otp=' + otp.val().trim(), function (res) {
            if (res == 1) {
                $('#resturent-name-text').html(company_name.val());
                $('#mobile-number-text').html(mobile.val());
                $('#address-text').html($('#address').val());
                goBack.push('5');
                $('#stage-5').hide();
                $('#stage-6').show();
                progress(true);
            } else {
                alert('Invalid OTP');
            }
        });
    });
    $('#bussiness-deatil-btn').on('click', function () {
        var company_name = $('#company-name');
        var vat_number = $('#vat-number');
        if (company_name.prop('required') && company_name.val().trim() == '') {
            alert('Please enter registered company name');
            return;
        }
        if (vat_number.prop('required') && vat_number.val().trim() == '') {
            alert('Please enter VAT number');
            return;
        }
        goBack.push('4');
        $('#stage-4').hide();
        $('#stage-5').show();
        progress(true);
    });

    $('#delivery-fee-btn').on('click', function () {
        if ($('#delivery-fee').val() != '') {
            goBack.push('2-1');
            $('#stage-2-1').hide();
            $('#stage-3').show();
            progress(true);
        } else {
            alert('Please fill out fees field.')
        }
    });

    $('.service-type').on('click', function () {
        $('input[name="service_type"]').val($(this).attr('data-id'));
        $('#stage-1').hide();
        $('#stage-2').show();
        goBack.push(1);
        $('#go-back').show();
        progress(true);
    });
    $('.own-driver').on('click', function () {
        var val = $(this).attr('data-id');
        $('input[name="own_driver"]').val(val);
        $('#stage-2').hide();
        if (val == 1) {
            $('#delivery-fee').prop('required', false);
            $('#stage-3').show();
        } else {
            $('#delivery-fee').prop('required', true);
            $('#stage-2-1').show();
        }
        progress(true);
        goBack.push(2);
    });
    $('.pickup-available').on('click', function () {
        $('input[name="pickup_available"]').val($(this).attr('data-id'));
        $('#stage-3').hide();
        $('#stage-4').show();
        progress(true);
        goBack.push(3);
    });
    $('.login-box-body #mobilee').on('keyup', function () {
        var classes = '.login-box-body #email';
        if ($(this).val().length > 0) {
            $(classes).prop('readonly', true);
            $(classes).val('');
        } else {
            $(classes).prop('readonly', false);
        }
    });
    $('.login-box-body #email').on('keyup', function () {
        var classes = '.login-box-body #mobile';
        if ($(this).val().length > 0) {
            $(classes).prop('readonly', true);
            $(classes).val('');
        } else {
            $(classes).prop('readonly', false);
        }
    });
    $('form').on('submit', function (ev) {
        if ($('#email').length || $('#mobile').length) {
            var email = $('#email').val() || '';
            var mobile = $('#mobile').val() || '';
            if (email.length < 1 && mobile.length < 1) {
                ev.preventDefault();
                alert('Email or Mobile is required');
                setTimeout(function () {
                    $('.preloader').addClass('loaded');
                }, 32);
            } else {
                $("#mobile").val('+' + $("#mobile").intlTelInput("getSelectedCountryData").dialCode + mobile);
            }
        }
    });
    $('#email-continue').on('click', function (ev) {
        ev.preventDefault();
        var email_text = 'Continue with Email';
        var mobile_text = 'Continue with Mobile';
        if ($(this).text() === email_text) {
            $('#mobile-login').hide();
            $('#email-login').show();
            $('#email').prop('required', true);
            $('#mobile').prop('required', false);
            $(this).text(mobile_text);
        } else {
            $('#mobile-login').show();
            $('#email-login').hide();
            $('#email').prop('required', false);
            $('#mobile').prop('required', true);
            $(this).text(email_text);
        }
    });
    $("#mobile").on('keyup', function () {
        var existsVal = this.value;
        var number = existsVal.replace(/\D/g, '');
        if (number.length > 10) {
            number = number.substring(0, 11);
        }
        $(this).val(number);
    });
    $('.every-day input#start-time').on('change', function () {
        $('.single-day input[start-time]').val($(this).val());
        $('.single-day input[start-time]').change();
    });
    $('.every-day input#end-time').on('change', function () {
        $('.single-day input[end-time]').val($(this).val());
        $('.single-day input[end-time]').change();
    });
    $('.single-day input[end-time], .single-day input[start-time]').on('change', function () {
        var id = $(this).attr('id').split('-')[2];
        if ($('#chef-timings-' + id + '-start-time').val() != '' && $('#chef-timings-' + id + '-end-time').val() != '') {
            $('#chef-timings-' + id + '-notes').hide();
            $('#chef-timings-' + id + '-notes').prop('required', false);
        } else {
            $('#chef-timings-' + id + '-notes').show();
            $('#chef-timings-' + id + '-notes').prop('required', true);
        }
    });
    initialseIcheck();
    $('.single-day input[end-time], .single-day input[start-time]').change();
    $('#image').on('change', function () {
        if (this.files && this.files[0]) {
            $('label[for="image"] #thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('label[for="image"] #thumbnail-image').attr('src', $('label[for="image"] #thumbnail-image').attr('default-image'));
        }
    });
});

function checkTimingValidation(ev) {
    var id = $(ev).attr('id').split('-')[2];
    console.log(id);
    if ($('chef-timings-' + id + '-start-time').val() != '' && $('chef-timings-' + id + '-end-time').val() != '') {
        $('#chef-timings-' + id + '-notes').hide();
        $('#chef-timings-' + id + '-notes').prop('required', false);
    } else {
        $('#chef-timings-' + id + '-notes').show();
        $('#chef-timings-' + id + '-notes').prop('required', true);
    }
}

function sendOTP() {
    var mobile = $('#mobile').intlTelInput("getNumber").replaceAll(' ', '');
    $.get(SEND_OTP_URL + mobile, function () {
        $('.verify-otp-div').show();
    });
}

$(document).ajaxComplete(function () {
    initialseIcheck();
});

$(document).ready(function () {
    if (jQuery().inputmask) {
        $('#bank-detail-routing-number').inputmask({
            'mask': '99-99-99',
            'placeholder': '_',
            'removeMaskOnSubmit': true,
            'rightAlign': false
        });
    }
    if (jQuery().intlTelInput) {
        setTimeout(function () {
            $("#mobile").intlTelInput({
                initialCountry: "auto",
                separateDialCode: true,
                onlyCountries: ['gb', 'in'],
                geoIpLookup: function (callback) {
                    $.get('https://ipinfo.io/' + IP_ADDRESS, {
                        token: '101bda4458f672'
                    }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/7.0.1/lib/libphonenumber/build/utils.js" // just for formatting/placeholders etc
            }).then(function () {
                var el = $('#mobile');
                el.val(el.val().replace(' ', ''));
                initialseIcheck();
            });
        }, 30);
    }
});

function initialseIcheck() {
    $('input[name="sole_trader"]').on('ifChecked', function () {
        if ($(this).val() == 0) {
            $('#company-name-div').show();
            $('#company-name').prop('required', true);
        } else {
            $('#company-name-div').hide();
            $('#company-name').prop('required', false);
        }
    });

    $('input[name="vat_registered"]').on('ifChecked', function () {
        if ($(this).val() == 0) {
            $('#vat-number-div').hide();
            $('#vat-number').prop('required', false);
        } else {
            $('#vat-number-div').show();
            $('#vat-number').prop('required', true);
        }
    });
    $('input[name="is_hygiene"]').on('ifChecked', function () {
        if ($(this).val() == 0) {
            $('#rating-div').hide();
            $('#rating').prop('required', false);
        } else {
            $('#rating-div').show();
            $('#rating').prop('required', true);
        }
    });
}

// Constants

const SEGMENTED_CONTROL_BASE_SELECTOR = ".ios-segmented-control";
const SEGMENTED_CONTROL_INDIVIDUAL_SEGMENT_SELECTOR = ".ios-segmented-control .option input";
const SEGMENTED_CONTROL_BACKGROUND_PILL_SELECTOR = ".ios-segmented-control .selection";


// Main

document.addEventListener("DOMContentLoaded", setup);

// Body functions

function setup() {
    forEachElement(SEGMENTED_CONTROL_BASE_SELECTOR, elem => {
        elem.addEventListener("change", updatePillPosition);
    })
    window.addEventListener("resize", updatePillPosition); // Prevent pill from detaching from element when window resized. Becuase this is rare I haven't bothered with throttling the event
}

function updatePillPosition() {
    forEachElement(SEGMENTED_CONTROL_INDIVIDUAL_SEGMENT_SELECTOR, (elem, index) => {
        if (elem.checked) moveBackgroundPillToElement(elem, index);
    })
}

function moveBackgroundPillToElement(elem, index) {
    document.querySelector(SEGMENTED_CONTROL_BACKGROUND_PILL_SELECTOR).style.transform = "translateX(" + (elem.offsetWidth * index) + "px)";
}

// Helper functions

function forEachElement(className, fn) {
    Array.from(document.querySelectorAll(className)).forEach(fn);
}

function goBackRegister() {
    if (goBack) {
        var current_pos = goBack[goBack.length - 1];
        $('[id^=stage-]').hide();
        $('#stage-' + current_pos).show();
        if (goBack.length == 1) {
            $('#go-back').hide();
        }
        var index = goBack.indexOf(current_pos);
        if (index > -1) {
            goBack.splice(index, 1);
            console.log(goBack);
        }
        progress(false);
    }
}


/*
 * Google Map Initialize
 */
let marker;
let map;

function initialize() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: {
            lat: -33.8688,
            lng: 151.2195
        },
        zoom: 13,
        mapTypeId: "roadmap",
    });
    if ($('#lat').val().length && $('#lng').val().length) {
        let pos = new google.maps.LatLng($('#lat').val(), $('#lng').val());
        const bounds = new google.maps.LatLngBounds();
        bounds.extend(pos);
        placeMarker(pos, $('#address').val());
        map.fitBounds(bounds);
        var listener = google.maps.event.addListener(map, "idle", function () {
            if (map.getZoom() > 16)
                map.setZoom(16);
            google.maps.event.removeListener(listener);
        });
    }
    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
        document.getElementById("address"), {
        types: ["geocode"]
    }
    );
    autocomplete.addListener("place_changed", () => {
        const place = autocomplete.getPlace();
        // Clear out the old markers.
        // For each place, get the icon, name and location.
        const bounds = new google.maps.LatLngBounds();
        placeMarker(place.geometry.location, place.name);
        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }
        map.fitBounds(bounds);
    });
}

function placeMarker(location, address = null) {
    // Clear out the old markers.
    if (marker) {
        marker.setMap(null);
    }
    $('#lat').val(location.lat());
    $('#lng').val(location.lng());
    // Create a marker for each place.
    marker = new google.maps.Marker({
        map: map,
        icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
        },
        position: location,
        title: address,
        draggable: true,
    });
    google.maps.event.addListener(marker, 'dragend', function () {
        var markerPosition = marker.getPosition()
        $('#lat').val(markerPosition.lng());
        $('#lng').val(markerPosition.lat());
    });
}

function progress(add = true) {
    if (add) {
        initWidth = initWidth + 15;
    } else {
        initWidth = initWidth - 15;
    }
    $('.signup-total-progress').css('width', initWidth + '%');
}

function checkForm(id) {
    $('#' + id + ' input').each(function () {
        if (!this.checkValidity()) {
            alert('Please fill out the details.')
            return false;
        }
    });
    return true;
}

function editHygiene() {
    goBackRegister();
}

function editResturent() {
    goBackRegister();
    goBackRegister();
}