function changeMenuName(id) {
    var headerId = $('#menu-name-' + id);
    var actionIcon = headerId.find('img');
    headerText = headerId.find('span');
    if (actionIcon.hasClass('propa-pencil')) {
        actionIcon.attr('src', '//propagrub.com/chef/img/noun-check-1105964.png');
        actionIcon.removeClass('propa-pencil');
        actionIcon.addClass('propa-save');
        headerText.prop('contenteditable', true);
        headerText.focus();
    } else {
        actionIcon.attr('src', '//propagrub.com/chef/img/edit.png');
        actionIcon.removeClass('propa-save');
        actionIcon.addClass('propa-pencil');
        var text = headerText.text();
        $.ajax({
            url: CHANGE_MENU_NAME_URL + id,
            method: 'POST',
            data: {
                content: text
            },
            success: function() {
                headerText.prop('contenteditable', false);
            }
        });
    }
}

function changeMenuDescription(id) {
    var headerId = $('#menu-description-' + id);
    var actionIcon = headerId.find('img');
    headerText = headerId.find('span');
    if (actionIcon.hasClass('propa-pencil')) {
        actionIcon.attr('src', '//propagrub.com/chef/img/noun-check-1105964.png');
        actionIcon.removeClass('propa-pencil');
        actionIcon.addClass('propa-save');
        headerText.prop('contenteditable', true);
        headerText.focus();
    } else {
        actionIcon.attr('src', '//propagrub.com/chef/img/edit.png');
        actionIcon.removeClass('propa-save');
        actionIcon.addClass('propa-pencil');
        var text = headerText.text();
        $.ajax({
            url: CHANGE_MENU_DESCRIPTION_URL + id,
            method: 'POST',
            data: {
                content: text
            },
            success: function() {
                headerText.prop('contenteditable', false);
            }
        });
    }
}

function changeCategoryName(id) {
    var headerId = $('#category-name-' + id);
    var actionIcon = headerId.find('img');
    headerText = headerId.find('span');
    if (actionIcon.hasClass('propa-pencil')) {
        actionIcon.attr('src', '//propagrub.com/chef/img/noun-check-1105964.png');
        actionIcon.removeClass('propa-pencil');
        actionIcon.addClass('propa-save');
        headerText.prop('contenteditable', true);
        headerText.focus();
    } else {
        actionIcon.attr('src', '//propagrub.com/chef/img/edit.png');
        actionIcon.removeClass('propa-save');
        actionIcon.addClass('propa-pencil');
        var text = headerText.text();
        $.ajax({
            url: CHANGE_CATEGORY_NAME_URL + id,
            method: 'POST',
            data: {
                content: text
            },
            success: function() {
                headerText.prop('contenteditable', false);
            }
        });
    }
}

function changeCategoryDescription(id) {
    var headerId = $('#category-description-' + id);
    var actionIcon = headerId.find('img');
    headerText = headerId.find('span');
    if (actionIcon.hasClass('propa-pencil')) {
        actionIcon.attr('src', '//propagrub.com/chef/img/noun-check-1105964.png');
        actionIcon.removeClass('propa-pencil');
        actionIcon.addClass('propa-save');
        headerText.prop('contenteditable', true);
        headerText.focus();
    } else {
        actionIcon.attr('src', '//propagrub.com/chef/img/edit.png');
        actionIcon.removeClass('propa-save');
        actionIcon.addClass('propa-pencil');
        var text = headerText.text();
        $.ajax({
            url: CHANGE_CATEGORY_DESCRIPTION_URL + id,
            method: 'POST',
            data: {
                content: text
            },
            success: function() {
                headerText.prop('contenteditable', false);
            }
        });
    }
}

function addProduct(categoryID) {
    $.get(ADD_PRODUCT_URL + categoryID, function(res) {
        $('#action-modal').unbind();
        $('#menu-modal').find('.modal-body').html(res);
        $('#action-modal').on('click', function() {
            if ($('#menu-modal #modal-form').valid()) {
                $('#menu-modal #modal-form').submit();
            }
        });
        $('#menu-products-images-0-image').on('change', function() {
            if (this.files.length) {
                $('#product-image').css('background-image', "url('" + window.URL.createObjectURL(this.files[0]) + "')");
                $('#product-image').show();
                $('#product-image-empty').hide();
            } else {
                $('#product-image').hide();
                $('#product-image-empty').show();
            }
        });
        $('#menu-modal').find('.modal-title').html('New item');
        $('#action-modal').html('Add new item');
        $('#menu-modal').modal('show');
    });
}

function editProduct(productID) {
    $.get(EDIT_PRODUCT_URL + productID, function(res) {
        $('#action-modal').unbind();
        $('#menu-modal').find('.modal-body').html(res);
        $('#action-modal').on('click', function() {
            if ($('#menu-modal #modal-form').valid()) {
                $('#menu-modal #modal-form').submit();
            }
        });
        $('#menu-products-images-0-image').on('change', function() {
            if (this.files.length) {
                $('#product-image').css('background-image', "url('" + window.URL.createObjectURL(this.files[0]) + "')");
                $('#product-image').show();
                $('#product-image-empty').hide();
            } else {
                $('#product-image').hide();
                $('#product-image-empty').show();
            }
        });
        $('#menu-modal').find('.modal-title').html('Edit item');
        $('#action-modal').html('Save');
        $('#menu-modal').modal('show');
    });
}

function addCategory(menuID) {
    $('#add-category-modal').modal('show');
}

function manageOptions(categoryID, CatName) {
    $.get(MANAGE_OPTION_URL + categoryID, function(res) {
        $('#save-action-option-button').unbind();
        $('#manage-options-modal').find('.modal-body').html(res);
        $('#save-action-option-button').on('click', function() {
            if ($('#option-modal-form').valid()) {
                $('#option-modal-form').submit();
            }
        });
        $('#option-title').html(CatName);
        $('#manage-options-modal').modal('show');
    });
}

function editOption(categoryID, OptionID) {
    $.get(MANAGE_OPTION_URL + categoryID + '/' + OptionID, function(res) {
        $('#save-action-option-button').unbind();
        $('#manage-options-modal').find('.modal-body').html(res);
        $('#save-action-option-button').on('click', function() {
            if ($('#option-modal-form').valid()) {
                $('#option-modal-form').submit();
            }
        });
    });
}

function addIngredient() {
    var clone = $('#clone-row').clone();
    clone = clone.removeClass('d-none');
    clone = $(clone).attr('id', '');
    $('#clone-body').append(clone);
    $('.sumo-select-custom').last().SumoSelect({ search: true, placeholder: 'Select', csvDispCount: false });
}

function deleteIngredents(ev, id = 0) {
    if (id != 0) {
        $.get(DELETE_OPTION_URL + id);
    }
    $(ev).parents('tr').remove();

}

function addMenu() {
    $('#modal-form')[0].reset();
    $('#image').change();
    $('#add-modal').modal('show');
}

function menuDeletePrompt(id) {
    alert('click');
}

function sortOption() {
    $('#category-position-modal').modal('show');
}

$(document).ready(function() {
    $('#form-menu-upload').on('submit', function(ev) {
        if ($('#menu-file').val() == '') {
            if ($('#menu-file-url').val() == '') {
                ev.preventDefault();
                alert('Please upload menu or enter url');
            }
        }
    });
    if ($.fn.sortable) {
        $(".menu-sortable").sortable({
            connectWith: ".menu-product-item-info",
            handle: "i.drag-option",
            tolerance: "pointer",
            forcePlaceholderSize: false,
            update: function(event, ui) {
                var lstInboxID = $(this).sortable("toArray", { attribute: "product-id" });
                $.ajax({
                    url: SORTING_PRODUCT_URL,
                    method: 'POST',
                    data: {
                        content: lstInboxID
                    },
                    success: function() {}
                });
            }
        });
        $("#sortable").sortable({
            handle: "i.drag-option",
            tolerance: "pointer",
            forcePlaceholderSize: false
        });
    }
    $('#image').on('change', function() {
        if (this.files.length) {
            $('#product-image').css('background-image', "url('" + window.URL.createObjectURL(this.files[0]) + "')");
            $('#product-image').show();
            $('#product-image-empty').hide();
        } else {
            $('#product-image').hide();
            $('#product-image-empty').show();
        }
    });
    $('.menu-upload-custom').on('change', function() {
        if (this.files.length) {
            $(this).parents('form').submit();
        }
    });
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="popover"]').on('show.bs.popover', function(ev) {
        var id = ev.target.getAttribute('id');
        setTimeout(function() {
            $('.menu-delete-action').on('click', function() {
                $('#monu-delete-confirm').modal('show');
                $('#action-delete-menu-confirmed').unbind();
                $('#action-delete-menu-confirmed').on('click', function() {
                    $('#menu-delete-' + id).click();
                });
            });
            $('.menu-status-action').on('click', function() {
                $('#monu-status-confirm').modal('show');
                $('#action-status-menu-confirmed').unbind();
                $('#action-status-menu-confirmed').on('click', function() {
                    window.location.href = MENU_STATUS_URL + id;
                });
            });
        }, 30);
    });
    $('[category-toggle="popover"]').popover();
    $('[category-toggle="popover"]').on('show.bs.popover', function(ev) {
        var id = ev.target.getAttribute('id');
        setTimeout(function() {
            $('.change-position').on('click', sortOption);
            $('.category-delete-action').on('click', function() {
                $('#category-delete-confirm').modal('show');
                $('#action-delete-category-confirmed').unbind();
                $('#action-delete-category-confirmed').on('click', function() {
                    $('#category-delete-' + id).click();
                });
            });
        }, 30);
    });
    setTimeout(function() {
        initialise();
    }, 30);
});
$(document).ajaxComplete(function() {
    initialise();
});

function initialise() {
    $('#all-item').on('ifChecked', function() {
        $('input[id^="product-id"]').iCheck('check');
    });

    $('#all-item').on('ifUnchecked', function() {
        $('input[id^="product-id"]').iCheck('uncheck');
    });

    $('[name="is_allergens"]').on('ifChecked', function() {
        $(this).parents('form').find('[name="allergen_id[]"]')[0].sumo.disable();
    });
    $('[name="is_allergens"]').on('ifUnchecked', function() {
        console.log($(this).parents('form').find('[name="allergen_id[]"]'));
        $(this).parents('form').find('[name="allergen_id[]"]')[0].sumo.enable();
    });
}