$(document).ready(function() {
    $('#timing-2, #timing-3').on('ifChecked', function() {
        $('#timing-manage').show();
    });
    $('#timing-2, #timing-3').on('ifUnchecked', function() {
        $('#timing-manage').hide();
    });
    $('#timing-1').on('ifChecked', function() {
        $('#date-manage').show();
    });
    $('#timing-1').on('ifUnchecked', function() {
        $('#date-manage').hide();
    });
    $('[name="campaign"]').on('ifChecked', function() {
        $('.discount-labels').show();
    });
    $(document).ready(function() {
        $("#items-search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".tableFixHead tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $('#filter-category').on('change', function() {
            var value = $(this).val().toLowerCase();
            $(".tableFixHead tbody tr").filter(function() {
                console.log(value);
                console.log($(this).find('td p').text());
                $(this).toggle($(this).find('td p').text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
});

function revieOffer() {
    var discount = $('#discount').val();
    var minimum_spend_required = $('#minimum-spend-required').val();
    var timing = $('[name="timing"]:checked').val();
    var start_date = $('#start-date').val();
    var start_time = $('#start-time').val();
    var end_date = $('#end-date').val();
    var end_time = $('#end-time').val();
    var audience = $('[name="audience"]:checked').val();
    if (discount == '') {
        alert('Please select Discount');
        return;
    }
    if (minimum_spend_required == '') {
        alert('Please select Minimum Spend Required');
        return;
    }
    if (!timing) {
        alert('Please select Timing');
        return;
    }
    if (!audience) {
        alert('Please select Audience');
        return;
    }
    if (timing == '1') {
        var start_date = new Date();
        var end_date = new Date();
        end_date.setDate(end_date.getDate() + parseInt($('#days').val()));
        var timing_text = 'Start right now';
        $('#start-date').val(moment(start_date).format("YYYY-MM-DD"));
        $('#start-time').val(moment(start_date).format("hh:mm"));
        $('#end-date').val(moment(end_date).format("YYYY-MM-DD"));
        $('#end-time').val(moment(end_date).format("hh:mm"));
    } else if (timing == '2') {
        var timing_text = 'Schedule a one-off';
        var start_date = new Date($('#start-date').val() + ' ' + $('#start-time').val());
        var end_date = new Date($('#end-date').val() + ' ' + $('#end-time').val());
    } else {
        var timing_text = 'Schedule a regular offer';
        var start_date = new Date($('#start-date').val() + ' ' + $('#start-time').val());
        var end_date = new Date($('#end-date').val() + ' ' + $('#end-time').val());
    }

    if (audience == '1') {
        var audience_text = 'Everyone';
    } else if (audience == '2') {
        var audience_text = 'Propagrub Plus subscribers only';
    } else {
        var audience_text = 'New customers only';
    }
    $('#details-nav').removeClass('text-dark');
    $('#details-nav img').attr('src', '//propagrub.com/chef/img/1_disabled.png');
    $('#review-nav img').attr('src', '//propagrub.com/chef/img/2_enabled.png');
    $('#review-nav').addClass('text-dark');
    $('#review-details').show();
    $('.enter-offer').hide();
    $('#start-date-text').html(moment(start_date).format("HH:mm, DD MMMM YYYY"));
    $('#end-date-text').html(moment(end_date).format("HH:mm, DD MMMM YYYY"));
    $('#timing-text').html(timing_text);
    $('#offer-text').html(discount + '% off over £' + minimum_spend_required);
    $('#audience-text').html(audience_text);
    $('#finish-offer').show();
    $('#review-offer').hide();
}

function backOffer() {
    $('#finish-offer').hide();
    $('#review-offer').show();
    $('#review-details').hide();
    $('.enter-offer').show();
    $('#details-nav').addClass('text-dark');
    $('#details-nav img').attr('src', '//propagrub.com/chef/img/1_enabled.png');
    $('#review-nav img').attr('src', '//propagrub.com/chef/img/2_disabled.png');
    $('#review-nav').removeClass('text-dark');
}

function loadMenus() {
    $('#menu-products').modal('show');
}

function loadSelected() {
    $('#menu-products').modal('hide');
}