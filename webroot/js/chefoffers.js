$(document).ready(function () {
    $('#generate-code').on('click', function () {
        var code = 'PB';
        var code = code + makeid(6);
        $('#promo-code').val(code);
    });
    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                    charactersLength));
        }
        return result.toUpperCase();
    }
    $('#offer-exp-date').datetimepicker({
        format: 'YYYY-MM-DD hh:mm:ss'
    });
});