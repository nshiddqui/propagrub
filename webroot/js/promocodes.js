$(document).ready(function () {
    setTimeout(function () {
        $('#start-date').change();
    }, 23);
    $('#generate-code').on('click', function () {
        var code = 'PB';
        var code = code + makeid(6);
        $('#promocode').val(code);
    });
    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                    charactersLength));
        }
        return result.toUpperCase();
    }
    $('#start-date, #end-date').on('change', function () {
        var start_date = $('#start-date').val();
        var end_date = $('#end-date').val();
        if (start_date != '' && end_date != '') {
            date1 = new Date(start_date);
            date2 = new Date(end_date);
            // get total seconds between two dates
            var res = Math.abs(date1 - date2) / 1000;
            var Final_Result = Math.floor(res / 86400);
        } else {
            Final_Result = 0;
        }
        $('#total-days').val(Final_Result);
    });
});