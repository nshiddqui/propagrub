$(document).ready(function () {
    $('#menu-filter').on('change', function () {
        $('#dtMenuProducts').DataTable().draw();
    });
    $('#menu-id').on('change', function () {
        var id = $(this).val();
        $.get('/api/v1/menu-categories', {
            menu_id: id
        }, function (res) {
            $('#category-id').html('');
            $.each(res.data, function (key, value) {
                $('#category-id').append('<option value="' + value.id + '">' + value.category_name + '</option>')
            });
        })
    });
    $('#menu-products-images-0-image').on('change', function () {
        if (this.files && this.files[0]) {
            $('#thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('#thumbnail-image').attr('src', $('#thumbnail-image').attr('default-image'));
        }
    });
});