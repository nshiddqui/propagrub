$(document).ready(function () {
    $('#cuisine-image').on('change', function () {
        if (this.files && this.files[0]) {
            $('#thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('#thumbnail-image').attr('src', $('#thumbnail-image').attr('default-image'));
        }
    });
    $('form').on('submit', function (ev) {
        if ($('#cuisine-image').attr('need') == '1') {
            if ($('#cuisine-image')[0].files.length === 0) {
                ev.preventDefault();
                alert('Please upload image');
                setTimeout(function () {
                    $('.preloader').addClass('loaded');
                }, 30);
                return false;
            }
        }
    })
});