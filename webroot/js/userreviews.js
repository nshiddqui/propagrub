function loadReply(id) {
    $.get(REPLY_URL + id, function(res) {
        $('#save-button').unbind();
        $('#comment-modal').find('.modal-body').html(res);
        $('#save-button').on('click', function() {
            if ($('#modal-form').valid()) {
                $('#modal-form').submit();
            }
        });
        $('#comment-modal').modal('show');
    });
}

function loadReplyReview(id) {
    $.get(VIEW_REPLY_REVIEW_URL + id, function(res) {
        $('#review-reply-modal').find('.modal-body').html(res);
        $('#review-reply-modal').modal('show');
    });
}

function loadReview(id) {
    $.get(VIEW_REVIEW_URL + id, function(res) {
        $('#review-modal').find('.modal-body').html(res);
        $('#review-modal').modal('show');
    });
}

$(document).ready(function() {
    $('#filter-form select').on('change', function() {
        $('#filter-form').submit();
    })
    initialise();
})
$(document).ajaxComplete(function() {
    initialise();
});

function initialise() {
    $('#required-credit').on('ifChecked', function() {
        $('#credit-amount-here').show();
    });
    $('#required-credit').on('ifUnchecked', function() {
        $('#credit-amount-here').hide();
    });
}