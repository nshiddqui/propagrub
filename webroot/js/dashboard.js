$(document).ready(function () {

    $('.daterange').daterangepicker({
        opens: 'right',
        ranges: {
            'Per Day': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Per Week': [moment().subtract(6, 'days'), moment()],
            'Per Month': [moment().subtract(29, 'days'), moment()],
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
    },
            function (start, end) {
                var date_type = this.element[0].id;

                $("#start-" + date_type).val(start.format('YYYY-MM-DD'));
                $("#end-" + date_type).val(end.format('YYYY-MM-DD'));
                $('#' + date_type + ' span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $('#dashboard-from').submit();
            }
    );

});