<?php

use Cake\Core\Configure;

return [
    'HybridAuth' => [
        'providers' => [
            'Google' => [
                'enabled' => true,
                'keys' => [
                    'id' => '743519649982-qblfvad49f357r0b6b3oh6um67esfo9i.apps.googleusercontent.com',
                    'secret' => 'Fe8ZGvItIY8WZ_sWWmkDhkw-'
                ]
            ],
            'Facebook' => [
                'enabled' => true,
                'keys' => [
                    'id' => '384606575472810',
                    'secret' => '965606c11767c87745a6ceedc45cda39'
                ],
                'scope' => 'email'
            ]
        ],
        'debug_mode' => Configure::read('debug'),
        'debug_file' => LOGS . 'hybridauth.log',
    ]
];
