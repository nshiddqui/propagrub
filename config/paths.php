<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
/*
 * Use the DS to separate the directories in other defines
 */
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

/*
 * These defines should only be edited if you have cake installed in
 * a directory layout other than the way it is distributed.
 * When using custom settings be sure to use the DS and do not add a trailing DS.
 */

/*
 * The full path to the directory which holds "src", WITHOUT a trailing DS.
 */
define('ROOT', dirname(__DIR__));

/*
 * The actual directory name for the application directory. Normally
 * named 'src'.
 */
define('APP_DIR', 'src');

/*
 * Path to the application's directory.
 */
define('APP', ROOT . DS . APP_DIR . DS);

/*
 * Path to the config directory.
 */
define('CONFIG', ROOT . DS . 'config' . DS);

/*
 * File path to the webroot directory.
 *
 * To derive your webroot from your webserver change this to:
 *
 * `define('WWW_ROOT', rtrim($_SERVER['DOCUMENT_ROOT'], DS) . DS);`
 */
define('WWW_ROOT', ROOT . DS . 'webroot' . DS);

/*
 * Path to the tests directory.
 */
define('TESTS', ROOT . DS . 'tests' . DS);

/*
 * Path to the temporary files directory.
 */
define('TMP', ROOT . DS . 'tmp' . DS);

/*
 * Path to the logs directory.
 */
define('LOGS', ROOT . DS . 'logs' . DS);

/*
 * Path to the cache files directory. It can be shared between hosts in a multi-server setup.
 */
define('CACHE', TMP . 'cache' . DS);

/*
 * The absolute path to the "cake" directory, WITHOUT a trailing DS.
 *
 * CakePHP should always be installed with composer, so look there.
 */
define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . 'vendor' . DS . 'cakephp' . DS . 'cakephp');

/*
 * Path to the cake directory.
 */
define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
define('CAKE', CORE_PATH . 'src' . DS);


/* APi Root File */
define('API_ROOT', dirname(ROOT, 1)  . DS . 'propagrup_api' . DS);
define('WEB_API', 'https://propagrub.com/propagrup_api/');
define('API_DOC_ROOT', 'images/user_documents/');
define('API_PROFILE_IMAGE_ROOT', 'images/user_profile/');
define('API_MENU_ROOT', 'images/menu_images/');
define('API_PRODUCT_ROOT', 'images/products_images/');
define('API_GALLERY_ROOT', 'images/gallery/');
define('API_CUISINES_ROOT', 'images/cuisine/');
define('API_COMMUNITIES_ROOT', 'images/community/');
define('WEB_DOC', WEB_API . API_DOC_ROOT);
define('API_DOC', API_ROOT . API_DOC_ROOT);
define('WEB_PROFILE_IMAGE', WEB_API . API_PROFILE_IMAGE_ROOT);
define('API_PROFILE_IMAGE', API_ROOT . API_PROFILE_IMAGE_ROOT);
define('WEB_MENU', WEB_API . API_MENU_ROOT);
define('API_MENU', API_ROOT . API_MENU_ROOT);
define('WEB_PRODUCT', WEB_API . API_PRODUCT_ROOT);
define('API_PRODUCT', API_ROOT . API_PRODUCT_ROOT);
define('WEB_GALLERY', WEB_API . API_GALLERY_ROOT);
define('API_GALLERY', API_ROOT . API_GALLERY_ROOT);
define('WEB_CUISINES', WEB_API . API_CUISINES_ROOT);
define('API_CUISINES', API_ROOT . API_CUISINES_ROOT);
define('WEB_COMMUNITIES', WEB_API . API_COMMUNITIES_ROOT);
define('API_COMMUNITIES', API_ROOT . API_COMMUNITIES_ROOT);
define('GMAP_API', 'AIzaSyDaMTjANiWv7gcYvQR6fh1jClrHbZ9wdxE');
