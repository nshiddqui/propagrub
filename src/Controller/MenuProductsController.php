<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

class MenuProductsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $conditions = [
            'MenuProducts.user_id' => $this->Auth->user('id')
        ];
        if ($this->request->getQuery('menu_filter')) {
            $conditions['MenuProducts.menu_id'] = $this->request->getQuery('menu_filter');
            $this->set('menu_filter', $this->request->getQuery('menu_filter'));
        }
        $this->set('menu_filter', $this->request->getQuery('menu_filter'));
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('MenuProducts')
            ->queryOptions([
                'conditions' => [
                    $conditions
                ],
                'contain' => ['Menus', 'MenuCategories', 'ProductIngredients' => function ($q) {
                    return $q->select(['count' => $q->func()->count('*'), 'product_id']);
                }, 'MenuProductsImages' => function ($q) {
                    return $q->select(['image', 'menu_product_id']);
                }]
            ])
            ->options(['ajax' => ['data' => "%f%function(data){data.menu_filter = $('#menu-filter').val();}%f%"]])
            ->column('MenuProducts.id', ['label' => 'Sr. No.', 'width' => '30px'])
            ->column('MenuProductsImages', ['label' => 'Image', 'width' => '40px', 'database' => false])
            ->column('MenuProducts.product_name', ['label' => 'Product Name'])
            ->column('ProductIngredients', ['label' => 'Ingredients', 'database' => false])
            ->column('MenuProducts.product_price', ['label' => 'Price'])
            ->column('Menus.menu_name', ['label' => 'Menu'])
            ->column('MenuCategories.category_name', ['label' => 'Category'])
            ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '80px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    public function index($id = null)
    {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->MenuProducts, [
                'conditions' => [
                    $this->request->getQuery()
                ]
            ]);
            $this->set(compact('data'));
        } else {
            if (is_null($id)) {
                $menu_products = $this->MenuProducts->newEntity();
            } else {
                $menu_products = $this->MenuProducts->get($id, [
                    'contain' => ['MenuProductsImages'],
                ]);
                if ($menu_products->user_id !== $this->Auth->user('id')) {
                    $this->Flash->error(__('You are not authorized to edit this menu.'));

                    return $this->redirect(['action' => 'index']);
                }
            }
            $menu = $this->MenuProducts->Menus->find('list', [
                'conditions' => [
                    'user_id' => $this->Auth->user('id')
                ]
            ]);
            $menu_category = $this->MenuProducts->MenuCategories->find('list', [
                'conditions' => [
                    'user_id' => $this->Auth->user('id'),
                    'menu_id' => array_key_first($menu->toArray())
                ]
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $menu_products = $this->MenuProducts->patchEntity($menu_products, $this->getData());
                if ($this->MenuProducts->save($menu_products)) {
                    $this->Flash->success(__('The menu products has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The menu products could not be saved. Please, try again.'));
            }
            $this->set(compact('menu_products', 'menu', 'menu_category'));
            $this->DataTables->setViewVars('MenuProducts');
        }
    }

    public function add($menu_id, $category_id)
    {
        $menu_products = $this->MenuProducts->newEntity();
        if ($this->request->is('post')) {
            $menu_products = $this->MenuProducts->patchEntity($menu_products, $this->getData($menu_id, $category_id));
            if ($this->MenuProducts->save($menu_products)) {
                $this->Flash->success(__('The product has been added.'));
                if ($menu_id == 0) {
                    return $this->redirect($this->referer());
                }
                return $this->redirect('/menus/' . $menu_id);
            }
            $this->Flash->error(__('The product could not be add. Please, try again.'));
        }
        $this->viewBuilder()->setLayout(false);
        $this->loadModel('Allergens');
        $allergens = $this->Allergens->find('list');
        $this->set(compact('allergens', 'menu_products', 'menu_id'));
    }

    public function edit($id = null)
    {
        $menu_products = $this->MenuProducts->get($id, [
            'contain' => [
                'MenuProductsImages',
                'ProductIngredients' => function ($q) {
                    return $q->contain(['ProductIngredientsCategories' =>  [
                        'Ingredient' => function ($q) {
                            return $q->select(['list' => 'GROUP_CONCAT(`Ingredient`.`name`)', 'category_id']);
                        }
                    ]])->group('ProductIngredientsCategories.id');
                }
            ],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu_products = $this->MenuProducts->patchEntity($menu_products, $this->getData($menu_products->menu_id, $menu_products->category_id));
            if ($this->MenuProducts->save($menu_products)) {
                $this->Flash->success(__('The product has been updated.'));
                if ($menu_products->menu_id == 0) {
                    return $this->redirect($this->referer());
                }
                return $this->redirect('/menus/' . $menu_products->menu_id);
            }
            $this->Flash->error(__('The product could not be add. Please, try again.'));
        }
        // $this->viewBuilder()->setLayout(false);
        $this->loadModel('Allergens');
        $allergens = $this->Allergens->find('list');
        $this->loadModel('MenuCategories');
        $category = $this->MenuCategories->find('list', [
            'conditions' => [
                'MenuCategories.menu_id' => $menu_products->menu_id
            ]
        ]);
        $this->set(compact('allergens', 'menu_products', 'category'));
    }



    public function sorting()
    {
        $product_ids = $this->request->getData('content');
        foreach ($product_ids as $position => $id) {
            $menu_product = $this->MenuProducts->get($id);
            $menu_product->position = $position;
            $this->MenuProducts->save($menu_product);
        }
    }


    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->MenuProducts->get($id);
        if ($menu->user_id === $this->Auth->user('id')) {
            if ($this->MenuProducts->delete($menu)) {
                $this->Flash->success(__('The menu has been deleted.'));
            } else {
                $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getData($menu_id = null, $category_id = null)
    {
        $data = $this->request->getData();
        if (!empty($menu_id) || $menu_id == '0') {
            $data['menu_id'] = $menu_id;
        }
        if (empty($data['category_id']) && !empty($category_id)) {
            $data['category_id'] = $category_id;
        }
        $this->loadComponent('Fileupload');
        $data['user_id'] = $this->Auth->user('id');
        if (!empty($data['menu_products_images'][0]['image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_PRODUCT, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $_FILES['image'] = $data['menu_products_images'][0]['image'];
            $this->Fileupload->upload('image');
            $data['menu_products_images'][0]['image'] = $this->Fileupload->output('file_name');
            $data['menu_products_images'][0]['menu_id'] = $data['menu_id'];
            $data['menu_products_images'][0]['category_id'] = $data['category_id'];
        } else {
            unset($data['menu_products_images']);
        }
        $data['allergen_id'] = @implode(',', $data['allergen_id']);
        return $data;
    }
}
