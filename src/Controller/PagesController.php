<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Pages Controller
 *
 * @property \App\Model\Table\PagesTable $Pages
 *
 * @method \App\Model\Entity\Page[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PagesController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Authorized = true;
        $this->Auth->allow();
        parent::beforeFilter($event);
    }

    public function view($id = null) {
        $this->viewBuilder()->setLayout(false);
        $page = $this->Pages->get($id, [
            'contain' => [],
        ]);
        $this->set(compact('page'));
    }

}
