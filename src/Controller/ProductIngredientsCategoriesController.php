<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductIngredients Controller
 *
 * @property \App\Model\Table\ProductIngredientsTable $ProductIngredients
 *
 * @method \App\Model\Entity\ProductIngredient[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductIngredientsCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($cat_id, $id = null)
    {
        $options = $this->ProductIngredientsCategories->find('all', [
            'conditions' => [
                'ProductIngredientsCategories.product_category_id' => $cat_id
            ],
            'contain' => [
                'Ingredient' => function ($q) {
                    return $q->select(['list' => 'GROUP_CONCAT(`Ingredient`.`name`)', 'category_id']);
                }
            ],
            'group' => 'ProductIngredientsCategories.id'
        ]);
        if (is_null($id)) {
            $ingredients_category = $this->ProductIngredientsCategories->newEntity();
        } else {
            $ingredients_category = $this->ProductIngredientsCategories->get($id, [
                'contain' => [
                    'ProductIngredients',
                    'Ingredients'
                ]
            ]);
            if ($ingredients_category->user_id !== $this->Auth->user('id')) {
                $this->Flash->error(__('You are not authorized to edit this product ingredient category.'));

                return $this->redirect(['action' => 'index']);
            }
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['allergen_id'] = @implode(',', $data['allergen_id']);
            $data['user_id'] = $this->Auth->user('id');
            $data['product_category_id'] = $cat_id;
            $ingredients_category = $this->ProductIngredientsCategories->patchEntity($ingredients_category, $data);
            if ($this->ProductIngredientsCategories->save($ingredients_category)) {
                foreach ($data['product_ingredients_categories']['name'] as $key => $product_ingredients_category) {
                    if (empty($data['product_ingredients_categories']['name'][$key])) {
                        continue;
                    }
                    if (!empty($data['product_ingredients_categories']['id'][$key])) {
                        $ingredients_cat = $this->ProductIngredientsCategories->Ingredients->get($data['product_ingredients_categories']['id'][$key]);
                        $ingredients_cat->name = $data['product_ingredients_categories']['name'][$key];
                        $ingredients_cat->price = $data['product_ingredients_categories']['price'][$key];
                        $ingredients_cat->tax = $data['product_ingredients_categories']['tax'][$key];
                    } else {
                        $ingredients_cat = $this->ProductIngredientsCategories->Ingredients->newEntity([
                            'category_id' => $ingredients_category->id,
                            'name' => $data['product_ingredients_categories']['name'][$key],
                            'price' => $data['product_ingredients_categories']['price'][$key],
                            'tax' => $data['product_ingredients_categories']['tax'][$key],
                        ]);
                    }
                    $this->ProductIngredientsCategories->Ingredients->save($ingredients_cat);
                }

                $this->Flash->success(__('The product ingredient category has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The product ingredient category could not be saved. Please, try again.'));
        }
        $this->loadModel('MenuProducts');
        $products = $this->MenuProducts->find('all', [
            'conditions' => [
                'category_id' => $cat_id
            ],
            'contain' => ['ProductIngredients' =>  function ($q) use ($id) {
                return $q->where(['ProductIngredients.ingredients_category_id' => $id]);
            }]
        ]);
        $this->loadModel('Allergens');
        $allergens = $this->Allergens->find('list');
        $this->loadModel('MenuCategories');
        $cat = $this->MenuCategories->get($cat_id);
        $this->set(compact('ingredients_category', 'options', 'allergens', 'products', 'cat'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Ingredient id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $productIngredient = $this->ProductIngredientsCategories->Ingredients->get($id);
        if ($this->ProductIngredientsCategories->Ingredients->delete($productIngredient)) {
            $this->Flash->success(__('The product ingredient category has been deleted.'));
        } else {
            $this->Flash->error(__('The product ingredient category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getData()
    {
        $data = $this->request->getData();
        $data['user_id'] = $this->Auth->user('id');
        return $data;
    }
}
