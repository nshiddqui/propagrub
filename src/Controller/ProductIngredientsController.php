<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Exception\UnauthorizedException;

/**
 * ProductIngredients Controller
 *
 * @property \App\Model\Table\ProductIngredientsTable $ProductIngredients
 *
 * @method \App\Model\Entity\ProductIngredient[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductIngredientsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ProductIngredients')
                ->queryOptions([
                    'conditions' => [
                        'ProductIngredients.user_id' => $this->Auth->user('id')
                    ],
                    'contain' => [
                        'ProductIngredientsCategories'
                    ]
                ])
                ->column('ProductIngredients.id', ['label' => 'Sr. No.', 'width' => '30px'])
                ->column('ProductIngredients.name', ['label' => 'Ingredient Name'])
                ->column('ProductIngredientsCategories.ingredients_category_name', ['label' => 'Ingredient Category Name'])
                ->column('ProductIngredients.price', ['label' => 'Price'])
                ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '30px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($id = null) {
        if (is_null($id)) {
            $product_ingredient = $this->ProductIngredients->newEntity();
        } else {
            $product_ingredient = $this->ProductIngredients->get($id);
            if ($product_ingredient->user_id !== $this->Auth->user('id')) {
                $this->Flash->error(__('You are not authorized to edit this product ingredient.'));

                return $this->redirect(['action' => 'index']);
            }
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product_ingredient = $this->ProductIngredients->patchEntity($product_ingredient, $this->getData());
            if ($this->ProductIngredients->save($product_ingredient)) {
                $this->Flash->success(__('The product ingredient has been saved.'));
                if (is_null($id)) {
                    $product_ingredient->name = '';
                    $product_ingredient->price = '';
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error(__('The product ingredient could not be saved. Please, try again.'));
            }
        }
        $menu = $this->ProductIngredients->MenuProducts->Menus->find('list', [
            'conditions' => [
                'user_id' => $this->Auth->user('id')
            ]
        ]);
        $menu_category = $this->ProductIngredients->MenuProducts->MenuCategories->find('list', [
            'conditions' => [
                'menu_id' => array_key_first($menu->toArray())
            ]
        ]);
        $menu_product = $this->ProductIngredients->MenuProducts->find('list', [
            'conditions' => [
                'category_id' => array_key_first($menu_category->toArray())
            ]
        ]);
        $ingredients_category = $this->ProductIngredients->ProductIngredientsCategories->find('list', [
            'conditions' => [
                'user_id' => $this->Auth->user('id')
            ]
        ]);
        $this->set(compact('product_ingredient', 'menu', 'menu_category', 'menu_product', 'ingredients_category'));
        $this->DataTables->setViewVars('ProductIngredients');
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Ingredient id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $productIngredient = $this->ProductIngredients->get($id);
        if ($this->ProductIngredients->delete($productIngredient)) {
            $this->Flash->success(__('The product ingredient has been deleted.'));
        } else {
            $this->Flash->error(__('The product ingredient could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getData() {
        $data = $this->request->getData();
        $data['user_id'] = $this->Auth->user('id');
        $product = $this->ProductIngredients->MenuProducts->find('list', [
                    'conditions' => [
                        'user_id' => $this->Auth->user('id')
                    ]
                ])->toArray();
        $ingredients_category = $this->ProductIngredients->ProductIngredientsCategories->find('list', [
                    'conditions' => [
                        'user_id' => $this->Auth->user('id')
                    ]
                ])->toArray();
        if (!in_array($data['product_id'], array_keys($product))) {
            $this->Flash->error(__('You are not authorized to use this product.'));

            throw new UnauthorizedException(__('You are not allowed to access this page'));
        }

        if (!in_array($data['ingredients_category_id'], array_keys($ingredients_category))) {
            $this->Flash->error(__('You are not authorized to use this product ingredient category.'));

            throw new UnauthorizedException(__('You are not allowed to access this page'));
        }
        return $data;
    }

}
