<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Galleries Controller
 *
 * @property \App\Model\Table\GalleriesTable $Galleries
 *
 * @method \App\Model\Entity\Gallery[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GalleriesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        $gallery = $this->Galleries->newEntity();
        if ($this->request->is('post')) {
            $gallery = $this->Galleries->patchEntity($gallery, $this->getData());
            if ($this->Galleries->save($gallery)) {
                $this->Flash->success(__('The image has been uploaded.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The gallery image could not be uploaded. Please, try again.'));
        }
        $galleries = $this->paginate($this->Galleries);

        $this->set(compact('galleries', 'gallery'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Gallery id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $gallery = $this->Galleries->get($id);
        if ($this->Galleries->delete($gallery)) {
            $this->Flash->success(__('The gallery has been deleted.'));
        } else {
            $this->Flash->error(__('The gallery could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }

    private function getData() {
        $data = $this->request->getData();
        $this->loadComponent('Fileupload');
        $data['user_id'] = $this->Auth->user('id');
        $this->Fileupload->init(['upload_path' => API_GALLERY, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
        $this->Fileupload->upload('image');
        $data['image'] = $this->Fileupload->output('file_name');
        return $data;
    }

}
