<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * CommunityMember Controller
 *
 * @property \App\Model\Table\CommunityMemberTable $CommunityMember
 *
 * @method \App\Model\Entity\Community[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommunityMemberController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('CommunityMember')
            ->databaseColumn('CommunityMember.id')
            ->databaseColumn('Users.last_name', true)
            ->options(['ajax' => ['data' => "%f%function(data){data.id = getUrlParameter('id');}%f%"]])
            ->queryOptions([
                'conditions' => [
                    'CommunityMember.community_id' => $this->request->getQuery('id')
                ],
                'contain' => [
                    'Users'
                ]
            ])
            ->column('Users.image', ['label' => 'Image', 'width' => 100, 'class' => 'text-center'])
            ->column('Users.first_name', ['label' => 'Member Name'])
            ->column('Users.email', ['label' => 'Member Email'])
            ->column('Users.mobile', ['label' => 'Member Mobile'])
            ->column('Users.status', ['label' => 'Member Status'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->CommunityMember);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('CommunityMember');
        }
        $community = $this->CommunityMember->Communities->get($this->request->getQuery('id'));

        $this->set('community', $community);
    }



    /**
     * Delete method
     *
     * @param string|null $id Community id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $community = $this->CommunityMember->get($id);
        if ($this->CommunityMember->delete($community)) {
            $this->Flash->success(__('The community member has been deleted.'));
        } else {
            $this->Flash->error(__('The community member could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }
}
