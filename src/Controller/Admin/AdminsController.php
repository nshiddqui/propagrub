<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins
 *
 * @method \App\Model\Entity\Admin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdminsController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Auth->allow(['logout', 'login']);
        parent::beforeFilter($event);
    }

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Admins')
                ->databaseColumn('Admins.id')
                ->queryOptions([
                    'conditions' => [
                        'Admins.is_super' => 0
                    ]
                ])
                ->column('Admins.image', ['label' => 'Image'])
                ->column('Admins.name', ['label' => 'Admin Name'])
                ->column('Admins.email', ['label' => 'Email'])
                ->column('Admins.status', ['label' => 'Status'])
                ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->Auth->user('is_super') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Admins);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Admins');
        }
    }

    public function login() {
        if ($this->Auth->user()) {
            return $this->redirect(['controller' => 'dashboard']);
        } else if ($auth = $this->Cookie->read('Auth')) {
            if (isset($auth['id']) && !empty($auth['id'])) {
                $this->Auth->setUser($auth);
                return $this->redirect(['controller' => 'dashboard']);
            }
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $user['role'] = 1;
                $user['is_complete'] = true;
                if ($user['status'] === true) {
                    $this->Auth->setUser($user);
                    if ($this->request->is('api')) {
                        $this->Api->login($user);
                    }
                    return $this->redirect(['controller' => 'dashboard']);
                } else {
                    $this->Flash->error(__('This admin is not activated, please contact our administrator.'));
                }
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
    }

    public function logout() {
        $this->Cookie->delete('Auth');
        return $this->redirect($this->Auth->logout());
    }

    public function chnageProfile($id = null) {
        $condition = false;
        if (is_null($id)) {
            $admin = $this->Admins->get($this->Auth->user('id'), [
                'contain' => [],
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $condition = true;
            }
        } else if ($id === 'add') {
            if ($this->Auth->user('is_super') != '1') {
                throw new UnauthorizedException(__('You are not alowed to access this page'));
            }
            $admin = $this->Admins->newEntity();
            if ($this->request->is('post')) {
                $condition = true;
            }
        } else {
            $admin = $this->Admins->get($id, [
                'contain' => [],
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $condition = true;
            }
        }
        if ($condition) {
            $admin = $this->Admins->patchEntity($admin, $this->getAdminData());
            if ($this->Admins->save($admin)) {
                if ($id === $this->Auth->user('id')) {
                    $data = $admin->toArray();
                    $data['role'] = 1;
                    $this->Auth->setUser($data);
                }
                $this->Flash->success(__('The profile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The profile could not be saved. Please, try again.'));
        }
        $this->set(compact('admin'));
    }

    private function getAdminData() {
        $data = $this->request->getData();
        if (empty($data['password'])) {
            unset($data['password']);
        }
        if (!empty($data['image']['tmp_name'])) {
            $this->loadComponent('Fileupload');
            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif']]);
            $this->Fileupload->upload('image');
            $data['image'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['image']);
        }
        return $data;
    }

    public function active($id, $code) {
        $this->request->allowMethod(['post']);
        $this->loadModel('Admins');
        $users = $this->Admins->get($id);
        $users->status = $code;
        if ($this->Admins->save($users)) {
            $this->Flash->success(__('The admin has been ' . ($code == '1' ? 'Activate' : 'Deactivated') . '.'));
        } else {
            $this->Flash->error(__('The admin could not be  ' . ($code == '1' ? 'Activate' : 'Deactivated') . '. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }

}
