<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

class ChefErrorsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ChefErrors')
            ->databaseColumn('ChefErrors.id')
            ->databaseColumn('ReportedByUser.last_name', true)
            ->databaseColumn('ReportedToUser.last_name', true)
            ->databaseColumn('ReportedToUser.id')
            ->queryOptions([
                'contain' => [
                    'ReportedByUser',
                    'ReportedToUser'
                ]
            ])
            ->column('ReportedByUser.first_name', ['label' => 'Reported By'])
            ->column('ReportedToUser.first_name', ['label' => 'Reported To'])
            ->column('ReportedToUser.image', ['label' => 'Reported Image', 'width' => 100, 'class' => 'text-center'])
            ->column('ChefErrors.reported_options', ['label' => 'Reason'])
            ->column('ChefErrors.reported_detail', ['label' => 'Description'])
            ->column('ChefErrors.created_at', ['label' => 'Reported Date'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->ChefErrors);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('ChefErrors');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Promo Code id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Users');
        $users = $this->Users->get($id);
        $users->is_deleted = 1;
        if ($this->Users->save($users)) {
            $this->Flash->success(__('The chef has been deleted.'));
        } else {
            $this->Flash->error(__('The chef could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
