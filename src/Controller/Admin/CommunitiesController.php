<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * Communities Controller
 *
 * @property \App\Model\Table\CommunitiesTable $Communities
 *
 * @method \App\Model\Entity\Community[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommunitiesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Communities')
            ->databaseColumn('Communities.id')
            ->databaseColumn('Users.last_name', true)
            ->queryOptions([
                'contain' => [
                    'Users',
                    'CommunityMember' => function ($q) {
                        return $q->select(['count' => $q->func()->count('*'), 'community_id'])->group('community_id');
                    },
                ]
            ])
            ->column('Communities.community_image', ['label' => 'Community Image', 'width' => 100, 'class' => 'text-center'])
            ->column('Communities.community_name', ['label' => 'Community Name'])
            ->column('Communities.is_verified', ['label' => 'Is Verified'])
            ->column('CommunityMember', ['label' => 'Total Members', 'database' => false])
            ->column('Users.first_name', ['label' => 'Created By'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Communities);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Communities');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Community id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $community = $this->Communities->get($id, [
            'contain' => ['CommunityMember', 'CommunityPostComment', 'CommunityPosts'],
        ]);

        $this->set('community', $community);
    }


    /**
     * Edit method
     *
     * @param string|null $id Community id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        if (is_null($id)) {
            $community = $this->Communities->newEntity();
        } else {
            $community = $this->Communities->get($id, [
                'contain' => [],
            ]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!empty($data['community_image']['tmp_name'])) {
                $this->loadComponent('Fileupload');
                $this->Fileupload->init(['upload_path' => API_COMMUNITIES, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif']]);
                $this->Fileupload->upload('community_image');
                $data['community_image'] = $this->Fileupload->output('file_name');
            } else {
                unset($data['community_image']);
            }
            $community = $this->Communities->patchEntity($community, $data);
            if ($this->Communities->save($community)) {
                $this->Flash->success(__('The community has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community could not be saved. Please, try again.'));
        }
        $this->set(compact('community'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Community id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $community = $this->Communities->get($id);
        if ($this->Communities->delete($community)) {
            $this->Flash->success(__('The community has been deleted.'));
        } else {
            $this->Flash->error(__('The community could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }
}
