<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Core\Configure;
use Cake\Http\Client;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
    }

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Users')
            ->queryOptions([
                'conditions' => [
                    'Users.role' => '2'
                ],
                'join' => [
                    [
                        'table' => 'chef_timings',
                        'alias' => 'ChefTimings',
                        'conditions' => [
                            'ChefTimings.user_id = Users.id'
                        ]
                    ]
                ],
                'group' => ['Users.id'],
                'contain' => ['ChefDetails']
            ])
            ->databaseColumn('Users.id')
            ->databaseColumn('Users.gender')
            ->databaseColumn('ChefDetails.resturant_name')
            ->databaseColumn('ChefDetails.chef_cuisines_ids')
            ->databaseColumn('ChefDetails.estimated_delivery_time')
            ->databaseColumn('ChefDetails.order_min_amount')
            ->databaseColumn('ChefDetails.packaging_charges')
            ->databaseColumn('ChefDetails.short_description')
            ->databaseColumn('ChefDetails.address')
            ->databaseColumn('ChefDetails.image')
            ->databaseColumn('ChefDetails.service_type')
            ->databaseColumn('ChefDetails.geofence_radius')
            ->databaseColumn('ChefDetails.company_name')
            ->databaseColumn('ChefDetails.company_number')
            ->databaseColumn('ChefDetails.sole_trader')
            ->databaseColumn('ChefDetails.council_registration')
            ->databaseColumn('ChefDetails.level_certificate')
            ->databaseColumn('ChefTimings.id')
            ->databaseColumn('Users.last_name', true)
            ->column('Users.image', ['label' => 'Image'])
            ->column('Users.first_name', ['label' => 'Chef Name'])
            ->column('Users.email', ['label' => 'Email'])
            ->column('Users.mobile', ['label' => 'Mobile'])
            ->column('Percentage', ['label' => 'Mobile', 'database' => false])
            ->column('Users.status', ['label' => 'Status'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);

        $this->DataTables->createConfig('Customer')
            ->queryOptions([
                'conditions' => [
                    'Users.role' => '4'
                ]
            ])
            ->table('Users')
            ->databaseColumn('Users.id')
            ->databaseColumn('Users.last_name', true)
            ->column('Users.image', ['label' => 'Image'])
            ->column('Users.first_name', ['label' => 'Chef Name'])
            ->column('Users.email', ['label' => 'Email'])
            ->column('Users.mobile', ['label' => 'Mobile'])
            ->column('Users.status', ['label' => 'Status'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);

        $this->DataTables->createConfig('Driver')
            ->queryOptions([
                'conditions' => [
                    'Users.role' => '3'
                ],
                'contain' => ['UsersDocument', 'BankDetails'],
                'group' => ['Users.id']
            ])
            ->table('Users')
            ->databaseColumn('Users.id')
            ->databaseColumn('UsersDocument.id')
            ->databaseColumn('BankDetails.id')
            ->databaseColumn('Users.last_name', true)
            ->column('Users.image', ['label' => 'Image'])
            ->column('Users.first_name', ['label' => 'Driver Name'])
            ->column('Users.email', ['label' => 'Email'])
            ->column('Users.mobile', ['label' => 'Mobile'])
            ->column('Users.id', ['label' => 'View Documents', 'database' => false])
            ->column('Users.status', ['label' => 'Status'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);

        $this->DataTables->createConfig('Documents')
            ->queryOptions([
                'conditions' => [
                    'Users.id' => $this->request->getQuery('id')
                ],
                'contain' => ['Users']
            ])
            ->options(['ajax' => ['data' => "%f%function(data){data.id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);}%f%"]])
            ->table('UsersDocument')
            ->databaseColumn('UsersDocument.id')
            ->databaseColumn('UsersDocument.user_id')
            ->databaseColumn('UsersDocument.message')
            ->column('image', ['label' => 'Image', 'database' => false])
            ->column('UsersDocument.doc_type', ['label' => 'Document Type'])
            ->column('UsersDocument.doc_number', ['label' => 'Document Number'])
            ->column('UsersDocument.doc_name', ['label' => 'Document Number'])
            ->column('UsersDocument.file', ['label' => 'Download Document', 'searchable' => false, 'class' => 'text-center'])
            ->column('UsersDocument.status', ['label' => 'Status'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $role = [
                2 => 'Chef',
                3 => 'Driver',
                4 => 'Customer'
            ];
            $this->viewVars = [];
            $conditions = [
                'role in' => [2, 3, 4]
            ];
            if ($this->request->getQuery('q')) {
                $conditions['or'] = [
                    'first_name LIKE' => "%" . $this->request->getQuery('q') . "%",
                    'last_Name LIKE' => "%" . $this->request->getQuery('q') . "%"
                ];
            }
            $datas = $this->Users->find('all', ['conditions' => $conditions, 'fields' => [
                'first_name', 'last_name', 'role', 'id'
            ]]);
            $response = ['results' => []];
            foreach ($datas as $data) {
                array_push($response['results'], ['id' => $data->id, 'text' => $data->full_name . '( ' . $role[$data->role] . ' )']);
            }
            $this->set($response);
        } else {
            $this->DataTables->setViewVars('Users');
        }
    }

    public function driver() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Users);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Driver');
        }
    }

    public function customer() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Users);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Customer');
        }
    }

    public function dashboard() {
    }

    public function bankDetails($id) {
        if ($this->Users->BankDetails->exists(['user_id' => $id])) {
            $this->Users->BankDetails->setPrimaryKey('user_id');
            $bank_detail = $this->Users->BankDetails->get($id);
        } else {
            $bank_detail = $this->Users->BankDetails->newEntity();
        }
        if ($this->request->getQuery('status')) {
            $bank_detail->status = $this->request->getQuery('status');
            $bank_detail->message = $this->request->getQuery('message');
            if ($this->Users->BankDetails->save($bank_detail)) {
                $this->Flash->success(__('The bank deatils has been ' . ($bank_detail->status == '1' ? 'Approved' : 'Declined') . '.'));
            } else {
                $this->Flash->error(__('The bank deatils could not be  ' . ($bank_detail->status == '1' ? 'Approved' : 'Declined') . '. Please, try again.'));
            }
            return $this->redirect(['action' => 'bankDetails', $id]);
        }
        $this->set(compact('bank_detail'));
    }

    public function complete($id) {
        $this->loadModel('DropDown');
        $user = $this->Users->get($id, [
            'contain' => ['UsersDocument', 'ChefDetails', 'ChefTimings', 'BankDetails']
        ]);
        $service_type = $this->Users->ChefDetails->ChefServices->find('list');
        $weeks = $this->Users->ChefTimings->Weeks->find('list');
        $chef_cuisines = $this->Users->ChefDetails->ChefCuisines->find('list');
        $this->set(compact('user', 'service_type', 'weeks', 'chef_cuisines'));
    }

    public function active($id, $code) {
        $this->request->allowMethod(['post']);
        $this->loadModel('Users');
        $users = $this->Users->get($id);
        $users->status = $code;
        if ($this->Users->save($users)) {
            $this->Flash->success(__('The user has been ' . ($code == '1' ? 'Activate' : 'Deactivated') . '.'));
        } else {
            $this->Flash->error(__('The user could not be  ' . ($code == '1' ? 'Activate' : 'Deactivated') . '. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }

    public function documents($id) {
        if ($this->request->getQuery('id')) {
            $user_document = $this->Users->UsersDocument->get($this->request->getQuery('id'));
            $user_document->status = $this->request->getQuery('status');
            $user_document->message = $this->request->getQuery('message');
            if ($this->Users->UsersDocument->save($user_document)) {
                $this->Flash->success(__('The document has been ' . ($user_document->status == '1' ? 'Approved' : 'Declined') . '.'));
                return $this->redirect(['action' => 'documents', $id]);
            } else {
                $this->Flash->error(__('The document could not be  ' . ($user_document->status == '1' ? 'Approved' : 'Declined') . '. Please, try again.'));
            }
        }
        $this->DataTables->setViewVars('Documents');
    }
}
