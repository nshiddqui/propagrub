<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Routing\Router;
use FroalaEditor\Image;

/**
 * Pages Controller
 *
 * @property \App\Model\Table\PagesTable $Pages
 *
 * @method \App\Model\Entity\Page[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PagesController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->removeAuthorized = true;
        $this->Auth->allow('view');
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function index($id = null) {
        $page = $this->Pages->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $page = $this->Pages->patchEntity($page, $this->request->getData());
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('The page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The page could not be saved. Please, try again.'));
        }
        $this->set(compact('page'));
    }

    public function view($id = null) {
        $this->viewBuilder()->setLayout(false);
        $page = $this->Pages->get($id, [
            'contain' => [],
        ]);
        $this->set(compact('page'));
    }

    public function uploadImage() {
        try {
            $response = Image::upload('/uploads/');
            echo stripslashes(json_encode($response));
        } catch (Exception $e) {
            http_response_code(404);
        }
        die;
    }

}
