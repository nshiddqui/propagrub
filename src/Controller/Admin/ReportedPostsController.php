<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

class ReportedPostsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ReportedPosts')
            ->databaseColumn('ReportedPosts.id')
            ->databaseColumn('ReportedByUser.last_name', true)
            ->databaseColumn('CommunityPosts.id')
            ->queryOptions([
                'contain' => [
                    'ReportedByUser',
                    'CommunityPosts'
                ]
            ])
            ->column('ReportedByUser.first_name', ['label' => 'Reported By'])
            ->column('ReportedByUser.image', ['label' => 'Reported Image', 'width' => 100, 'class' => 'text-center'])
            ->column('CommunityPosts.post_title', ['label' => 'Post Title'])
            ->column('CommunityPosts.post_detail', ['label' => 'Post Description'])
            ->column('ReportedPosts.reported_reason', ['label' => 'Reason'])
            ->column('ReportedPosts.comments', ['label' => 'Description'])
            ->column('ReportedPosts.created_at', ['label' => 'Reported Date'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->ReportedPosts);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('ReportedPosts');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Promo Code id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('CommunityPosts');
        $post = $this->CommunityPosts->get($id);
        if ($this->CommunityPosts->delete($post)) {
            $this->Flash->success(__('The feed been deleted.'));
        } else {
            $this->Flash->error(__('The feed could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
