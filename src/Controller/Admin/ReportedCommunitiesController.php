<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

class ReportedCommunitiesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ReportedCommunities')
            ->databaseColumn('ReportedCommunities.id')
            ->databaseColumn('Users.last_name', true)
            ->databaseColumn('Communities.id')
            ->queryOptions([
                'contain' => [
                    'Communities',
                    'Users'
                ]
            ])
            ->column('Users.first_name', ['label' => 'Reported By'])
            ->column('Communities.community_image', ['label' => 'Community Image', 'width' => 100, 'class' => 'text-center'])
            ->column('Communities.community_name', ['label' => 'Community Name'])
            ->column('ReportedCommunities.reported_reason', ['label' => 'Reason'])
            ->column('ReportedCommunities.comments', ['label' => 'Description'])
            ->column('ReportedCommunities.created_at', ['label' => 'Reported Date'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->ReportedCommunities);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('ReportedCommunities');
        }
    }
}
