<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * ChefCuisines Controller
 *
 * @property \App\Model\Table\PromoCodesTable $PromoCodes
 *
 * @method \App\Model\Entity\PromoCode[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChefCuisinesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ChefCuisines')
                ->column('ChefCuisines.id', ['label' => '#', 'width' => '30px'])
                ->column('ChefCuisines.cuisine_image', ['label' => 'Image', 'width' => '80px', 'class' => 'text-center'])
                ->column('ChefCuisines.cuisine_name', ['label' => 'Name'])
                ->column('action', ['label' => 'Action', 'database' => false, 'width' => '90px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($id = null) {
        if (is_null($id)) {
            $chef_cuisines = $this->ChefCuisines->newEntity();
        } else {
            $chef_cuisines = $this->ChefCuisines->get($id, [
                'contain' => [],
            ]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chef_cuisines = $this->ChefCuisines->patchEntity($chef_cuisines, $this->getData());
            if ($this->ChefCuisines->save($chef_cuisines)) {
                $this->Flash->success(__('The cuisines has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cuisines could not be saved. Please, try again.'));
        }
        $this->set(compact('chef_cuisines'));
        $this->DataTables->setViewVars('ChefCuisines');
    }

    /**
     * Delete method
     *
     * @param string|null $id Promo Code id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $chef_cuisines = $this->ChefCuisines->get($id);
        if ($this->ChefCuisines->delete($chef_cuisines)) {
            $this->Flash->success(__('The cuisines has been deleted.'));
        } else {
            $this->Flash->error(__('The cuisines could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getData() {
        $data = $this->request->getData();
        $this->loadComponent('Fileupload');
        if (!empty($data['cuisine_image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_CUISINES, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $this->Fileupload->upload('cuisine_image');
            $data['cuisine_image'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['cuisine_image']);
        }
        return $data;
    }

}
