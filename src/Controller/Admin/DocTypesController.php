<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * DocTypes Controller
 *
 * @property \App\Model\Table\PromoCodesTable $PromoCodes
 *
 * @method \App\Model\Entity\PromoCode[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocTypesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('DocTypes')
                ->queryOptions([
                    'contain' => ['UserRole']
                ])
                ->column('DocTypes.id', ['label' => '#'])
                ->column('DocTypes.doc_type', ['label' => 'Document Type'])
                ->column('UserRole.name', ['label' => 'User Role'])
                ->column('DocTypes.is_required', ['label' => 'Is Required'])
                ->column('action', ['label' => 'Action', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($id = null) {
        if (is_null($id)) {
            $doc_types = $this->DocTypes->newEntity();
        } else {
            $doc_types = $this->DocTypes->get($id, [
                'contain' => [],
            ]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $doc_types = $this->DocTypes->patchEntity($doc_types, $this->request->getData());
            if ($this->DocTypes->save($doc_types)) {
                $this->Flash->success(__('The document type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The document type could not be saved. Please, try again.'));
        }
        $user_role = $this->DocTypes->UserRole->find('list');
        $this->set(compact('doc_types', 'user_role'));
        $this->DataTables->setViewVars('DocTypes');
    }

    /**
     * Delete method
     *
     * @param string|null $id Promo Code id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $promoCode = $this->DocTypes->get($id);
        if ($this->DocTypes->delete($promoCode)) {
            $this->Flash->success(__('The document type has been deleted.'));
        } else {
            $this->Flash->error(__('The document type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
