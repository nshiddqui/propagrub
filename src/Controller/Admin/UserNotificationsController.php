<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Client;

class UserNotificationsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadModel('DropDown');
        $send_by = $this->DropDown->getNotificationSender();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('UserNotifications')
            ->databaseColumn('UserNotifications.id')
            ->queryOptions([
                'contain' => [
                    'Users'
                ]
            ])
            ->databaseColumn('UserNotifications.to_user_id')
            ->databaseColumn('Users.last_name')
            ->column('Users.first_name', ['label' => 'Send To', 'seachable' => false])
            ->column('UserNotifications.notification_title', ['label' => 'Title'])
            ->column('UserNotifications.notification_text', ['label' => 'Message'])
            ->column('UserNotifications.created_at', ['label' => 'Send Date'])
            ->column('action', ['label' => 'Action', 'database' => false]);
        $this->set(compact('send_by'));
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->UserNotifications);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('UserNotifications');
        }
    }

    public function send() {
        $notification = $this->UserNotifications->newEntity();
        if ($this->request->is('post')) {
            $tokens = [];
            $data = $this->request->getData();
            $this->UserNotifications->Users->setDisplayField('push_token');
            if ($data['user_type'] === 'custom') {
                $user_ids = array_filter($data['to_user_id']);
                foreach ($user_ids as $user_id) {
                    $data['to_user_id'] = $user_id;
                    $data['from_user_id'] = $this->Auth->user('id');
                    $data['notification_type'] = 123;
                    $notification = $this->UserNotifications->newEntity();
                    $notification = $this->UserNotifications->patchEntity($notification, $data);
                    $this->UserNotifications->save($notification);
                }
                $tokens = $this->UserNotifications->Users->find('list', [
                    'conditions' => [
                        'id in ' => $user_ids,
                        'push_token !=' => null
                    ]
                ])->toArray();
            } else {
                $role = [
                    'chef' => 2,
                    'driver' => 3,
                    'customer' => 4
                ];
                $data['to_user_id'] = $data['user_type'];
                $data['from_user_id'] = $this->Auth->user('id');
                $data['notification_type'] = 123;
                $notification = $this->UserNotifications->newEntity();
                $notification = $this->UserNotifications->patchEntity($notification, $data);
                $this->UserNotifications->save($notification);
                $conditions = [
                    'push_token !=' => null
                ];
                if ($data['user_type'] !== 'all') {
                    $conditions['role'] = $role[$data['user_type']];
                }
                $tokens = $this->UserNotifications->Users->find('list', [
                    'conditions' => $conditions
                ])->toArray();
            }
            if (!empty($tokens)) {
                try {
                    $client = new Client();
                    $client->post('https://www.ijyaweb.com/elevateit_api/api/sendNotificationFromWeb', [
                        'push_token' => array_values(array_filter($tokens)),
                        'title' => $data['title'],
                        'body' => $data['detail'],
                        'type' => 'chat',
                        'from_user_id' => $this->Auth->user('id'),
                        'to_user_type' => $data['user_type'],
                        'to_user_id' => array_filter($data['to_user_id'])
                    ]);
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    die;
                }
            }
            $this->Flash->success('Notification Sent Successfully');
            $this->redirect(['action' => 'index']);
        }
        $this->set(compact('notification'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Promo Code id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $notification = $this->UserNotifications->get($id);
        if ($this->UserNotifications->delete($notification)) {
            $this->Flash->success(__('The notification has been deleted.'));
        } else {
            $this->Flash->error(__('The notification could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
