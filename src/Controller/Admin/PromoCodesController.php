<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * PromoCodes Controller
 *
 * @property \App\Model\Table\PromoCodesTable $PromoCodes
 *
 * @method \App\Model\Entity\PromoCode[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PromoCodesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('PromoCodes')
                ->queryOptions([
                    'contain' => [
                        'ChefPromocodes' => function ($q) {
                            return $q->select(['count' => $q->func()->count('*'), 'promo_code_id']);
                        }
                    ]
                ])
                ->column('PromoCodes.id', ['label' => '#'])
                ->column('PromoCodes.promocode', ['label' => 'Promocode'])
                ->column('PromoCodes.start_date', ['label' => 'Start Date'])
                ->column('PromoCodes.end_date', ['label' => 'End Date'])
                ->column('PromoCodes.total_user', ['label' => 'Total Users'])
                ->column('ChefPromocodes', ['label' => 'Applied Users', 'database' => false])
                ->column('PromoCodes.created', ['label' => 'Created Date']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($id = null) {
        if (is_null($id)) {
            $promoCode = $this->PromoCodes->newEntity();
        } else {
            $promoCode = $this->PromoCodes->get($id, [
                'contain' => [],
            ]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $promoCode = $this->PromoCodes->patchEntity($promoCode, $this->request->getData());
            if ($this->PromoCodes->save($promoCode)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $this->set(compact('promoCode'));
        $this->DataTables->setViewVars('PromoCodes');
    }

    /**
     * Delete method
     *
     * @param string|null $id Promo Code id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $promoCode = $this->PromoCodes->get($id);
        if ($this->PromoCodes->delete($promoCode)) {
            $this->Flash->success(__('The promo code has been deleted.'));
        } else {
            $this->Flash->error(__('The promo code could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
