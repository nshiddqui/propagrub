<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * WebsiteUsers Controller
 *
 * @property \App\Model\Table\PromoCodesTable $PromoCodes
 *
 * @method \App\Model\Entity\PromoCode[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WebsiteUsersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('WebsiteUsers')
                ->column('WebsiteUsers.id', ['label' => '#','searchable' => false])
                ->column('WebsiteUsers.first_name', ['label' => 'First Name'])
                ->column('WebsiteUsers.last_name', ['label' => 'Last Name'])
                ->column('WebsiteUsers.email', ['label' => 'Email'])
                ->column('WebsiteUsers.phone', ['label' => 'Phone Number'])
                ->column('WebsiteUsers.User_type', ['label' => 'User Type'])
                ->column('WebsiteUsers.created', ['label' => 'Created']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($id = null) {
        $this->DataTables->setViewVars('WebsiteUsers');
    }

}
