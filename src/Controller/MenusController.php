<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

class MenusController extends AppController
{

    public function index()
    {
        $new_menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {
            $new_menu = $this->Menus->patchEntity($new_menu, $this->getData());
            if ($this->Menus->save($new_menu)) {
                $this->Flash->success(__('The product has been added.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The product could not be add. Please, try again.'));
        }
        $menus = $this->Menus->find('all', [
            'conditions' => [
                'Menus.user_id' => $this->Auth->user('id')
            ],
            'contain' => [
                'MenuCategories'
            ]
        ]);
        $this->set(compact('menus', 'new_menu'));
    }

    public function view($id = null)
    {
        $menu_category = $this->Menus->MenuCategories->newEntity();
        if ($this->request->is('post')) {
            $menu = $this->Menus->MenuCategories->patchEntity($menu_category, $this->request->getData() + ['user_id' => $this->Auth->user('id'), 'menu_id' => $id]);
            if ($this->Menus->MenuCategories->save($menu_category)) {
                $this->Flash->success(__('The category has been added.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The category could not be add. Please, try again.'));
        }
        $menu = $this->Menus->get($id, [
            'conditions' => [
                'Menus.user_id' => $this->Auth->user('id')
            ],
            'contain' => [
                'MenuCategories' => [
                    'MenuProducts' => [
                        'MenuProductsImages'
                    ]
                ]
            ]
        ]);
        $this->set(compact('menu', 'menu_category'));
    }

    public function getMenu($id)
    {
        $data = $this->Menus->get($id);
        $this->set(compact('data'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menus->get($id);
        if ($menu->user_id === $this->Auth->user('id')) {
            if ($this->Menus->delete($menu)) {
                $this->Flash->success(__('The menu has been deleted.'));
            } else {
                $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeName($id = null)
    {
        $this->request->allowMethod(['post']);
        $menu = $this->Menus->get($id);
        if ($menu->user_id === $this->Auth->user('id')) {
            $menu->menu_name = $this->request->getData('content');
            $this->Menus->save($menu);
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }
    }

    public function changeStatus($id = null)
    {
        $menu = $this->Menus->get($id);
        $this->Menus->updateAll(['status' => 0], ['user_id' => $this->Auth->user('id')]);
        if ($menu->user_id === $this->Auth->user('id')) {
            $menu->status = 1;
            $this->Menus->save($menu);
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function uploadMenu($id)
    {
        $this->request->allowMethod(['post']);
        $menu = $this->Menus->get($id);
        if ($menu->user_id === $this->Auth->user('id')) {
            $data = $this->request->getData();
            if (!empty($data['menu_image_upload']['tmp_name'])) {
                $this->loadComponent('Fileupload');
                $this->Fileupload->init(['upload_path' => API_MENU, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
                $this->Fileupload->upload('menu_image_upload');
                $menu->image = $this->Fileupload->output('file_name');
                $this->Menus->save($menu);
            }
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }
        return $this->redirect($this->referer());
    }

    public function changeDescription($id = null)
    {
        $this->request->allowMethod(['post']);
        $menu = $this->Menus->get($id);
        if ($menu->user_id === $this->Auth->user('id')) {
            $menu->menu_description = $this->request->getData('content');
            $this->Menus->save($menu);
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }
    }

    private function getData()
    {
        $data = $this->request->getData();
        $this->loadComponent('Fileupload');
        $data['user_id'] = $this->Auth->user('id');
        if (!empty($data['image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_MENU, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $this->Fileupload->upload('image');
            $data['image'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['image']);
        }
        if (!empty($data['pdf']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_DOC, 'allowed_types' => ['pdf'], 'encrypt_name' => true]);
            $this->Fileupload->upload('pdf');
            $data['pdf'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['pdf']);
        }
        return $data;
    }

    public function upload()
    {
        $this->loadModel('ChefDetails');
        $this->ChefDetails->setPrimaryKey('user_id');
        $user = $this->ChefDetails->get($this->Auth->user('id'));
        if (!empty($user->menu_file)) {
            return $this->render('uploaded');
        }
        if ($this->request->is('post')) {
            $this->loadComponent('Fileupload');
            $data = $this->request->getData();
            if (!empty($data['menu_file']['tmp_name'])) {
                $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files', 'allowed_types' => ['pdf'], 'encrypt_name' => true]);
                $this->Fileupload->upload('menu_file');
                $data['menu_file'] = Router::url('/files/' . $this->Fileupload->output('file_name'), true);
            } else {
                $data['menu_file'] = $data['menu_file_url'];
            }
            if (!empty($data['alchohol_license_file']['tmp_name'])) {
                $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files', 'allowed_types' => ['pdf'], 'encrypt_name' => true]);
                $this->Fileupload->upload('alchohol_license_file');
                $data['alchohol_license_file'] = Router::url('/files/' . $this->Fileupload->output('file_name'), true);
            } else {
                unset($data['alchohol_license_file']);
            }
            $user = $this->ChefDetails->patchEntity($user, $data);
            if ($this->ChefDetails->save($user)) {
                // $this->Flash->success('Your menu has been uploaded, we will response you soon.');
                return $this->redirect(['controller' => 'Users', 'action' => 'onboarding']);
            }
        }
    }

    public function sippingPropducts()
    {
        $menu_categories = $this->Menus->MenuCategories->find('all', [
            'conditions' => [
                'MenuCategories.user_id' => $this->Auth->user('id'),
                'MenuCategories.menu_id' => 0
            ],
            'contain' => [
                'MenuProducts' => [
                    'MenuProductsImages'
                ]
            ]
        ]);
        if ($menu_categories->count() === 0) {
            $menu_categories = $this->Menus->MenuCategories->newEntity([
                'user_id' => $this->Auth->user('id'),
                'menu_id' => 0,
                'category_name' => 'Shipping Products'
            ]);
            $this->Menus->MenuCategories->save($menu_categories);
            $menu_categories = $this->Menus->MenuCategories->find('all', [
                'conditions' => [
                    'MenuCategories.user_id' => $this->Auth->user('id'),
                    'MenuCategories.menu_id' => 0
                ],
                'contain' => [
                    'MenuProducts' => [
                        'MenuProductsImages'
                    ]
                ]
            ]);
        }
        $menu_categories = $menu_categories->first();
        $this->set(compact('menu_categories'));
    }
}
