<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Core\Configure;
use Cake\Http\Client;

/**
 * UserChefOrders Controller
 *
 * @property \App\Model\Table\UserChefOrdersTable $UserChefOrders
 *
 * @method \App\Model\Entity\UserChefOrder[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserChefOrdersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->Auth->allow('view');
        $this->loadModel('DropDown');
        $this->set('order_status', $this->DropDown->getOrderStatus());
        $this->set('order_type', $this->DropDown->getOrderType());
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('UserChefOrders')
                ->queryOptions([
                    'conditions' => [
                        'UserChefOrders.chef_id' => $this->Auth->user('id'),
                        'UserChefOrders.status' => $this->request->getQuery('order_status')
                    ],
                    'contain' => [
                        'Customers',
                        'UserAddress'
                    ]
                ])
                ->databaseColumn('Customers.last_name', true)
                ->databaseColumn('UserAddress.house_no', true)
                ->databaseColumn('UserAddress.city', true)
                ->databaseColumn('UserAddress.state', true)
                ->databaseColumn('UserAddress.country', true)
                ->databaseColumn('UserAddress.postal_code', true)
                ->options(['ajax' => ['data' => "%f%function(data){data.order_status = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);}%f%"]])
                ->column('UserChefOrders.id', ['label' => 'Sr. No.', 'width' => '30px'])
                ->column('UserChefOrders.order_no', ['label' => 'Order No'])
                ->column('UserChefOrders.order_type', ['label' => 'Order Type'])
                ->column('Customers.first_name', ['label' => 'User Name'])
                ->column('UserAddress.street_address', ['label' => 'User Address'])
                ->column('Customers.mobile', ['label' => 'User Phone'])
                ->column('UserChefOrders.total_cost', ['label' => 'Total Cost'])
                ->column('details', ['label' => 'Details', 'database' => false, 'width' => '80px', 'align' => 'center', 'class' => 'text-center']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    public function index($id) {
        $this->set('current_order_status', $this->DropDown->getOrderStatus($id));
        $this->DataTables->setViewVars('UserChefOrders');
    }

    public function view($id) {
        $order = $this->UserChefOrders->get($id, [
            'contain' => [
                'Customers',
                'Drivers',
                'UserAddress',
                'UserCart' => ['MenuProducts' => [
                        'ProductIngredients'
                    ]
                ]
            ]
        ]);
        Configure::write('CakePdf.download', true);
        Configure::write('CakePdf.filename', "{$order->order_no}.pdf");
        $this->set(compact('order'));
    }

    public function dispatcher($status = null) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->UserChefOrders->get($this->request->getData('id'), [
                'conditions' => [
                    'chef_id' => $this->Auth->user('id'),
                ]
            ]);
            $order = $this->UserChefOrders->patchEntity($order, $this->request->getData());
            if ($this->UserChefOrders->save($order)) {
                $this->Flash->success(__('The order has been assigned to driver.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('The order could not assigned to driver. Please, try again.'));
            }
        }
        $this->loadModel('DropDown');
        $this->loadModel('Users');
        $this->DropDown->addOrderStatus('1', 'New');
        $order_status = $this->DropDown->getOrderStatus();
        $order_type = $this->DropDown->getOrderType();
        foreach ($order_status as $status_id => $order) {
            $orders[$order] = $this->UserChefOrders->find('all', [
                'conditions' => [
                    'UserChefOrders.status' => $status_id,
                    'UserChefOrders.chef_id' => $this->Auth->user('id'),
                ],
                'contain' => [
                    'UserAddress',
                    'Customers',
                    'UserCart' => ['MenuProducts']
                ]
            ]);
            if ($this->request->is('api')) {
                if ($status_id != $status) {
                    $orders[$order] = $orders[$order]->count();
                }
            }
        }
        if (is_null($status)) {
            $status = '1';
        }
        $drivers = $this->Users->find('list', [
            'conditions' => [
                'role' => 3
            ]
        ]);
        $this->set(compact('orders', 'status', 'order_status', 'drivers', 'order_type'));
    }

    public function status($id, $status) {
        $this->request->allowMethod(['post']);
        $order = $this->UserChefOrders->get($id, [
            'conditions' => [
                'chef_id' => $this->Auth->user('id'),
            ]
        ]);
        if ($status == '2') {
            $userDetails = $this->UserChefOrders->Chefs->find('all', [
                        'fields' => ['auth_token', 'first_name', 'last_name'],
                        'conidtions' => [
                            'id' => $this->Auth->user('id')
                        ]
                    ])->first();
            $client = new Client();
            $response = $client->get(WEB_API . 'api/getNearbyDrivers', [
                        'auth_token' => $userDetails->auth_token
                    ])->getJson();
            if ($response['status'] === 200 && !empty($response['data'])) {
                $push_tokens = [];
                foreach ($response['data'] as $data) {
                    if (!empty($data['push_token'])) {
                        $push_tokens[$data['id']] = $data['push_token'];
                    }
                }
                if (!empty($push_tokens)) {
                    $client->post(WEB_API . 'api/sendNotificationFromWeb', [
                        'from_user_id' => $this->Auth->user('id'),
                        'push_token' => implode(',', array_values($push_tokens)),
                        'title' => 'New Order Request',
                        'body' => "{$userDetails->full_name} has sent you a request for new order. Tap to view",
                        'type' => 'driver_new_order',
                        'order_id' => $order->id,
                        'to_user_id' => array_keys($push_tokens),
                        'to_user_type' => 'Driver',
                        'auth_token' => $userDetails->auth_token
                    ])->getJson();
                } else {
                    $this->Flash->error(__('No driver available to accept this order. Please, try again later.'));
                    return $this->redirect($this->referer());
                }
            } else {
                $this->Flash->error(__('No driver available to accept this order. Please, try again later.'));
                return $this->redirect($this->referer());
            }
        }
        $order->status = $status;
        if ($this->UserChefOrders->save($order)) {
            $this->Flash->success(__('The order status has been changed.'));
        } else {
            $this->Flash->error(__('The order status has not be changed. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }

}
