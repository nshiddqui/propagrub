<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * ChefOffers Controller
 *
 * @property \App\Model\Table\ChefOffersTable $ChefOffers
 *
 * @method \App\Model\Entity\ChefOffer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChefOffersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadModel('DropDown');
        $offer_type = $this->DropDown->getOfferType();
        $this->set(compact('offer_type'));
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ChefOffers')
                ->queryOptions([
                    'conditions' => [
                        'ChefOffers.chef_id' => $this->Auth->user('id')
                    ]
                ])
                ->column('ChefOffers.id', ['label' => 'Sr. No.', 'width' => '30px'])
                ->column('ChefOffers.offer_title', ['label' => 'Offer Title'])
                ->column('ChefOffers.offer_value', ['label' => 'Offer Value'])
                ->column('ChefOffers.offer_type', ['label' => 'Offer Type'])
                ->column('ChefOffers.min_amount', ['label' => 'Min Amount'])
                ->column('ChefOffers.offer_exp_date', ['label' => 'Expiry Date'])
                ->column('ChefOffers.promo_code', ['label' => 'Promo Code'])
                ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '120px', 'align' => 'center']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    public function index($id = null) {
        if (is_null($id)) {
            $chef_offer = $this->ChefOffers->newEntity();
        } else {
            $chef_offer = $this->ChefOffers->get($id, [
                'contain' => [],
            ]);
            if ($chef_offer->chef_id !== $this->Auth->user('id')) {
                $this->Flash->error(__('You are not authorized to edit this offer.'));

                return $this->redirect(['action' => 'index']);
            }
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chef_offer = $this->ChefOffers->patchEntity($chef_offer, $this->getData());
            if ($this->ChefOffers->save($chef_offer)) {
                $this->Flash->success(__('The offer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The offer could not be saved. Please, try again.'));
        }
        $this->set(compact('chef_offer'));
        $this->DataTables->setViewVars('ChefOffers');
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $chef_offer = $this->ChefOffers->get($id);
        if ($chef_offer->chef_id === $this->Auth->user('id')) {
            if ($this->ChefOffers->delete($chef_offer)) {
                $this->Flash->success(__('The offer has been deleted.'));
            } else {
                $this->Flash->error(__('The offer could not be deleted. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('You are not authorized to delete this offer.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getData() {
        $data = $this->request->getData();
        $data['chef_id'] = $this->Auth->user('id');
        return $data;
    }

}
