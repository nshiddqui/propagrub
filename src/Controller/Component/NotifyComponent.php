<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Twilio\Rest\Client;
use SendGrid\Mail\Mail;
use Cake\Mailer\Email;

class NotifyComponent extends Component
{
    /*
     * Twillo Credentials
     */

    public $sid = 'AC252a078d4d4a5a66f66398d7a5f510a2';
    public $token = 'dadc5ea677ccec1a96949a094c2e00b8';

    /*
     * Sendgrid API KEY
     */
    public $skey = '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';

    public function initialize(array $config)
    {
        $this->Twilio = new Client($this->sid, $this->token);
        $this->Controller = $this->_registry->getController();
        parent::initialize($config);
    }

    public function sendOTP($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        /*
         * Generate & Send OTP
         */
        $otp = rand(100000, 999999);
        $this->Controller->loadModel('UserRole');
        $this->Controller->UserOtp->setPrimaryKey('send_to');
        $UserRole = $this->Controller->UserRole->newEntity();
        $UserRole->otp = $otp;
        if (!empty($user->mobile)) {
            $UserRole->send_to = $user->mobile;
        }
        $this->Controller->UserRole->save($UserRole);
    }

    public function sendOTPMobile($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        /*
         * Generate & Send OTP
         */
        $otp = rand(100000, 999999);
        $this->Controller->loadModel('UserOtp');
        $this->Controller->UserOtp->setPrimaryKey('send_to');
        $UserOtp = $this->Controller->UserOtp->newEntity();
        $UserOtp->otp = $otp;
        $UserOtp->send_to = $user->mobile;
        $this->Controller->UserOtp->save($UserOtp);

        try {
            $this->Twilio->messages->create(
                $user->mobile,
                [
                    "from" => "+19143152050",
                    "messagingServiceSid" => "MGbcdf331f5313198589d874b069f62f81",
                    "body" => "Your OTP to Login / Register Propagrub is {$otp} It will be valid for 3 minutes."
                ]
            );
        } catch (\Twilio\Exceptions\RestException $e) {
            $this->Controller->Flash->error(__('This mobile number not provisioned in sandbox environment.'));
            return false;
        }
        return true;
    }

    public function sendOTPEmail($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        /*
         * Generate & Send OTP
         */
        $otp = rand(100000, 999999);
        $this->Controller->loadModel('UserOtp');
        $this->Controller->UserOtp->setPrimaryKey('send_to');
        $UserOtp = $this->Controller->UserOtp->newEntity();
        $UserOtp->otp = $otp;
        $UserOtp->send_to = $user->email;
        $this->Controller->UserOtp->save($UserOtp);

        try {
            $email = new Email('default');
            $email->setFrom("no-reply@mail.propagrub.com", "PropaGrub");
            $email->setSubject("OTP Verification From PropaGrub");
            $email->setTo($user->email);
            $email->send("Your OTP to Login / Register Propagrub is {$otp} It will be valid for 3 minutes.");
        } catch (\Mailgun\Connection\Exceptions\MissingRequiredParameters $e) {
            $this->Controller->Flash->error(__('This email id is not provisioned in sandbox environment.'));
            return false;
        }
        return true;
    }

    public function sendWelcomeEmail($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        if (empty($user->email)) {
            return false;
        }
        try {
            $email = new Email('default');
            $email->setFrom("no-reply@mail.propagrub.com", "PropaGrub");
            $email->setSubject("Welcome to PropaGrub");
            $email->viewBuilder()->setTemplate('welcome');
            $email->setEmailFormat('html');
            $email->setViewVars(compact('user'));
            $email->setTo($user->email);
            $email->send();
        } catch (\Mailgun\Connection\Exceptions\MissingRequiredParameters $e) {
            $this->Controller->Flash->error(__('This email id is not provisioned in sandbox environment.'));
            return false;
        }
        return true;
    }

    public function sendFeedbackEmail($data)
    {
        try {
            $email = new Email('default');
            $email->setFrom("no-reply@mail.propagrub.com", "PropaGrub");
            $email->setSubject("Review from PropaGrub");
            $email->viewBuilder()->setTemplate('feedback');
            $email->setEmailFormat('html');
            $email->setViewVars(compact('data'));
            $email->setTo('support@propagrub.com');
            $email->send();
        } catch (\Mailgun\Connection\Exceptions\MissingRequiredParameters $e) {
            $this->Controller->Flash->error(__('This email id is not provisioned in sandbox environment.'));
            return false;
        }
        return true;
    }

    public function sendAgreementEmail($data)
    {
        try {
            $email = new Email('default');
            $email->setFrom("no-reply@mail.propagrub.com", "PropaGrub");
            $email->setSubject("Your agreement with PropaGrub");
            $email->viewBuilder()->setTemplate('agreement');
            $email->setEmailFormat('html');
            $email->setViewVars(compact('data'));
            $email->setTo($data->email);
            $email->send();
        } catch (\Mailgun\Connection\Exceptions\MissingRequiredParameters $e) {
            $this->Controller->Flash->error(__('This email id is not provisioned in sandbox environment.'));
            return false;
        }
        return true;
    }

    public function sendUploadingtEmail($data)
    {
        try {
            $email = new Email('default');
            $email->setFrom("no-reply@mail.propagrub.com", "PropaGrub");
            $email->setSubject("PropaGrub Identity and Document Verification");
            $email->viewBuilder()->setTemplate('upload_file');
            $email->setEmailFormat('html');
            $email->setViewVars(compact('data'));
            $email->setTo($data->email);
            $email->send();
        } catch (\Mailgun\Connection\Exceptions\MissingRequiredParameters $e) {
            $this->Controller->Flash->error(__('This email id is not provisioned in sandbox environment.'));
            return false;
        }
        return true;
    }
}
