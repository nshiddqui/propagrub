<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Exception\UnauthorizedException;

class MenuCategoriesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $conditions = [
            'MenuCategories.user_id' => $this->Auth->user('id')
        ];
        if ($this->request->getQuery('menu_filter')) {
            $conditions['MenuCategories.menu_id'] = $this->request->getQuery('menu_filter');
            $this->set('menu_filter', $this->request->getQuery('menu_filter'));
        }
        $this->set('menu_filter', $this->request->getQuery('menu_filter'));
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('MenuCategories')
            ->queryOptions([
                'conditions' => [
                    $conditions
                ],
                'contain' => ['Menus']
            ])
            ->options(['ajax' => ['data' => "%f%function(data){data.menu_filter = $('#menu-filter').val();}%f%"]])
            ->column('MenuCategories.id', ['label' => 'Sr. No.', 'width' => '30px'])
            ->column('Menus.menu_name', ['label' => 'Menu Name'])
            ->column('MenuCategories.category_name', ['label' => 'Category Name'])
            ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '30px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    public function index($id = null)
    {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->MenuCategories, [
                'conditions' => [
                    $this->request->getQuery()
                ]
            ]);
            $this->set(compact('data'));
        } else {
            if (is_null($id)) {
                $menu_category = $this->MenuCategories->newEntity();
            } else {
                $menu_category = $this->MenuCategories->get($id, [
                    'contain' => ['Menus'],
                ]);
                if ($menu_category->user_id !== $this->Auth->user('id')) {
                    $this->Flash->error(__('You are not authorized to edit this menu category.'));

                    return $this->redirect(['action' => 'index']);
                }
            }
            $menu = $this->MenuCategories->Menus->find('list', [
                'conditions' => [
                    'Menus.user_id' => $this->Auth->user('id')
                ]
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $menu_category = $this->MenuCategories->patchEntity($menu_category, $this->getData($menu->toArray()));
                if ($this->MenuCategories->save($menu_category)) {
                    $this->Flash->success(__('The menu category has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The menu category could not be saved. Please, try again.'));
            }
            $this->set(compact('menu_category', 'menu'));
            $this->DataTables->setViewVars('MenuCategories');
        }
    }

    public function changeName($id = null)
    {
        $this->request->allowMethod(['post']);
        $menu_category = $this->MenuCategories->get($id);
        if ($menu_category->user_id === $this->Auth->user('id')) {
            $menu_category->category_name = $this->request->getData('content');
            $this->MenuCategories->save($menu_category);
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }
    }

    public function changeDescription($id = null)
    {
        $this->request->allowMethod(['post']);
        $menu_category = $this->MenuCategories->get($id);
        if ($menu_category->user_id === $this->Auth->user('id')) {
            $menu_category->category_description = $this->request->getData('content');
            $this->MenuCategories->save($menu_category);
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu.'));
        }
        $this->set(['data' => $menu_category]);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menu_category = $this->MenuCategories->get($id);
        if ($menu_category->user_id === $this->Auth->user('id')) {
            if ($this->MenuCategories->delete($menu_category)) {
                $this->Flash->success(__('The menu category has been deleted.'));
            } else {
                $this->Flash->error(__('The menu category could not be deleted. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('You are not authorized to delete this menu category.'));
        }

        return $this->redirect($this->referer());
    }

    public function sorting()
    {
        $product_ids = $this->request->getData('category_id');
        foreach ($product_ids as $position => $id) {
            $menu_product = $this->MenuCategories->get($id);
            $menu_product->position = $position;
            $this->MenuCategories->save($menu_product);
        }
        return $this->redirect($this->referer());
    }

    private function getData($menu)
    {
        $data = $this->request->getData();
        $data['user_id'] = $this->Auth->user('id');
        if (!in_array($data['menu_id'], array_keys($menu))) {
            $this->Flash->error(__('You are not authorized to use this menu.'));

            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        return $data;
    }
}
