<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * UserReviews Controller
 *
 * @property \App\Model\Table\UserReviewsTable $UserReviews
 *
 * @method \App\Model\Entity\UserReview[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserReviewsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadModel('Menus');

        $menu = $this->Menus->find('list', [
            'conditions' => [
                'Menus.user_id' => $this->Auth->user('id')
            ]
        ]);

        if ($menu->count()) {
            $menu_id = $this->request->getQuery('menu_id', array_keys($menu->toArray(), $menu->first())[0]);
            $duration = $this->request->getQuery('duration', '7');

            $userReviews = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id'),
                    'UserReviews.menu_id' => $menu_id,
                    'DATE(UserReviews.created_at) >' => date('Y-m-d', strtotime('- ' . $duration  . ' days')),
                ]
            ]);

            $rating = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id')
                ],
                'fields' => [
                    'rating' => 'AVG(UserReviews.rating)'
                ]
            ])->first();

            $total_review = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id')
                ]
            ])->count();

            $total_1_review = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id'),
                    'UserReviews.rating' => 1
                ]
            ])->count();
            $total_2_review = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id'),
                    'UserReviews.rating' => 2
                ]
            ])->count();
            $total_3_review = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id'),
                    'UserReviews.rating' => 3
                ]
            ])->count();
            $total_4_review = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id'),
                    'UserReviews.rating' => 4
                ]
            ])->count();
            $total_5_review = $this->UserReviews->find('all', [
                'conditions' => [
                    'UserReviews.rating_to' => $this->Auth->user('id'),
                    'UserReviews.rating' => 5
                ]
            ])->count();

            if ($total_1_review != 0) {
                $total_1_review = ($total_1_review / $total_review) * 100;
            }
            if ($total_2_review != 0) {
                $total_2_review = ($total_2_review / $total_review) * 100;
            }
            if ($total_3_review != 0) {
                $total_3_review = ($total_3_review / $total_review) * 100;
            }
            if ($total_4_review != 0) {
                $total_4_review = ($total_4_review / $total_review) * 100;
            }
            if ($total_5_review != 0) {
                $total_5_review = ($total_5_review / $total_review) * 100;
            }

            $this->set(compact('userReviews', 'menu', 'rating', 'duration', 'menu_id', 'total_review', 'total_5_review', 'total_4_review', 'total_3_review', 'total_2_review', 'total_1_review'));
        } else {
            $this->set(compact('menu'));
        }
    }

    public function sendReply($id)
    {
        $userReview = $this->UserReviews->get($id, [
            'conditions' => [
                'UserReviews.rating_to' => $this->Auth->user('id'),
            ],
            'contain' => [
                'UserChefOrders'
            ]
        ]);

        if ($this->request->is(['post', 'put'])) {
            $userReview = $this->UserReviews->patchEntity($userReview, $this->request->getData());
            if ($this->UserReviews->save($userReview)) {
                $this->Flash->success(__('The reply has been sent.'));
            }
            return $this->redirect(['action' => 'index']);
        }

        $rating = $this->UserReviews->find('all', [
            'conditions' => [
                'UserReviews.rating_to' => $this->Auth->user('id')
            ],
            'fields' => [
                'rating' => 'AVG(UserReviews.rating)'
            ]
        ])->first();

        $this->set(compact('userReview', 'rating'));
    }

    function viewReply($id)
    {
        $userReview = $this->UserReviews->get($id, [
            'conditions' => [
                'UserReviews.rating_to' => $this->Auth->user('id'),
            ],
            'contain' => [
                'UserChefOrders'
            ]
        ]);
        $this->set(compact('userReview'));
    }

    /**
     * View method
     *
     * @param string|null $id User Review id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userReview = $this->UserReviews->get($id, [
            'conditions' => [
                'UserReviews.rating_to' => $this->Auth->user('id'),
            ],
            'contain' => [
                'UserChefOrders'
            ]
        ]);

        $rating = $this->UserReviews->find('all', [
            'conditions' => [
                'UserReviews.rating_to' => $this->Auth->user('id')
            ],
            'fields' => [
                'rating' => 'AVG(UserReviews.rating)'
            ]
        ])->first();

        $this->set(compact('userReview', 'rating'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userReview = $this->UserReviews->newEntity();
        if ($this->request->is('post')) {
            $userReview = $this->UserReviews->patchEntity($userReview, $this->request->getData());
            if ($this->UserReviews->save($userReview)) {
                $this->Flash->success(__('The user review has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user review could not be saved. Please, try again.'));
        }
        $this->set(compact('userReview'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Review id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userReview = $this->UserReviews->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userReview = $this->UserReviews->patchEntity($userReview, $this->request->getData());
            if ($this->UserReviews->save($userReview)) {
                $this->Flash->success(__('The user review has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user review could not be saved. Please, try again.'));
        }
        $this->set(compact('userReview'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Review id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userReview = $this->UserReviews->get($id);
        if ($this->UserReviews->delete($userReview)) {
            $this->Flash->success(__('The user review has been deleted.'));
        } else {
            $this->Flash->error(__('The user review could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
