<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');

        $this->Cookie->setConfig([
            'expires' => '+1 year',
        ]);

        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginRedirect' => '/dashboard',
            'authError' => 'Your session is expired, please login again.',
            'authenticate' => [
                'HybridAuth',
                'Form' => [
                    'userModel' => 'Admins',
                    'fields' => ['username' => 'email']
                ],
            ],
            'storage' => 'Session'
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
        $this->request->addDetector('api', ['param' => 'api', 'value' => 'api']);
        //load api component
        if ($this->request->is('api')) {
            $this->loadComponent('Api');
        }
        parent::beforeFilter($event);
        if ($this->Auth->user()) {
            $this->set('authUser', $this->Auth->user());
            if ($this->Auth->user('role') === 1) {
                if ($this->request->getParam('prefix') !== 'admin') {
                    $this->redirect(['controller' => 'Users', 'action' => 'dashboard', 'prefix' => 'admin']);
                }
                $this->viewBuilder()->setLayoutPath('admin');
                $this->loadModel('Pages');
                $this->set('page_lists', $this->Pages->find('list'));
            } else {
                if ($this->request->getParam('prefix') === 'admin') {
                    $this->redirect(['controller' => 'Users', 'action' => 'dashboard', 'prefix' => false]);
                }
                $this->isVerify();
                $user = $this->Auth->user();
                /*
                if (strtotime($user['subscription_expired']) < strtotime('now')) {
                    $this->viewBuilder()->setLayout('no_action');
                }
                */
                $this->loadModel('DropDown');
                $this->set('order_status', $this->DropDown->getOrderStatus());
            }
        } else {
            if (empty($this->request->getAttribute('params')['_ext'])) {
                $this->viewBuilder()->setLayout('no_sidebar');
            }
        }
        $this->set('params', $this->request->getAttribute('params'));
        $this->set('ip_address', $this->request->clientIp());
    }

    public function isVerify()
    {
        if (!isset($this->Authorized)) {
            $params = $this->request->getAttribute('params');
            $user = $this->Auth->user();
            if ($user['is_complete'] == false) {
                if (($params['controller'] === 'Users' && $params['action'] === 'initRegister') === false) {
                    $this->redirect(['controller' => 'Users', 'action' => 'initRegister']);
                }
            } else if ($user['status'] == false) {
                $allow_functions = [
                    'Users' => [
                        'onboarding',
                        'timings',
                        'banks',
                        'agreement',
                    ],
                    'Menus' => 'upload'
                ];
                $condition = true;
                foreach ($allow_functions as $controller => $action) {
                    if (is_array($action)) {
                        foreach ($action as $action_1) {
                            if ($params['controller'] === $controller && $params['action'] === $action_1) {
                                $condition = false;
                                break;
                            }
                        }
                        if ($condition == false) {
                            break;
                        }
                    } else {
                        if ($params['controller'] === $controller && $params['action'] === $action) {
                            $condition = false;
                            break;
                        }
                    }
                }
                if ($condition) {
                    $this->redirect(['controller' => 'Users', 'action' => 'onboarding']);
                }
            }
            /*
            if (($params['controller'] === 'UserSubscriptions' && $params['action'] === 'index') === false) {
                $user = $this->Auth->user();
                if (strtotime($user['subscription_expired']) < strtotime('now')) {
                    $this->redirect(['controller' => 'UserSubscriptions', 'action' => 'index']);
                }
            }
            */
        }
    }
}
