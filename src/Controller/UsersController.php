<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Utility\Security;
use stdClass;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->Auth->allow(['logout', 'login', 'register', 'verify', 'resend', 'authenticated', 'endpoint', 'checkEmail', 'forgotPassword', 'accept']);
        parent::beforeFilter($event);
    }

    public function login($role_id = 2)
    {
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        } else if ($auth = $this->Cookie->read('Auth')) {
            if (isset($auth['id']) && !empty($auth['id'])) {
                $user = $this->Users->find('all', [
                    'conditions' => [
                        'id' => $auth['id']
                    ]
                ]);
                if ($user->count()) {
                    $user = $user->first();
                    $userData = $user->toArray();
                    $this->Auth->setUser($userData);
                    $this->Cookie->write('Auth', $userData);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Cookie->delete('Auth');
                }
            }
        }
        if ($this->request->is('post')) {
            $email = $this->request->getData('email');
            $password = $this->request->getData('password');
            if (empty($email) || empty($password)) {
                $this->Flash->error(__('Email and Password does not exists'));
                return $this->redirect($this->referer());
            }

            $user = $this->Users->find('all', [
                'conditions' => [
                    'role' => $role_id,
                    'email' => $email
                ]
            ]);
            if ($user->count()) {
                $user = $user->first();
                if ($user->password == md5($password)) {
                    $userData = $user->toArray();
                    if ($userData['status'] === false && $userData['is_complete'] == true) {
                        $this->Flash->error(__('Your profile is not approved yet. For more information please <a href="mailto:nazim27294@gmail.com">contact us</a>'));
                    }
                    $this->Auth->setUser($userData);
                    $this->Cookie->write('Auth', $userData);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('Invalid Password for this account.'));
                    return $this->redirect($this->referer());
                }
            } else {
                $this->Flash->error(__('Provided user does not exists. Please register yourself.'));
                return $this->redirect(['action' => 'register', $email]);
            }
        }
    }

    public function register($email = null, $role_id = 2)
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['role'] = $role_id;
            $data['password'] = md5($data['password']);
            $data['auth_token'] = md5(time());
            $data['username'] = str_replace('@', '', $data['username']);
            if (isset($data['image']) && !empty($data['image']['tmp_name'])) {
                $this->loadComponent('Fileupload');
                $this->Fileupload->init(['upload_path' => API_PROFILE_IMAGE, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true, 'file_name' => $this->Auth->user('id') . '.png']);
                $_FILES['thumbnail'] = $data['image'];
                $this->Fileupload->upload('thumbnail');
                $data['image'] = $this->Fileupload->output('file_name');
            } else {
                if (isset($data['image'])) {
                    unset($data['image']);
                }
            }
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $user = $this->Users->find('all', [
                    'conditions' => [
                        'role' => $role_id,
                        'email' => $user->email
                    ]
                ]);
                $user = $user->first();
                $userData = $user->toArray();
                $this->Auth->setUser($userData);
                $this->Cookie->write('Auth', $userData);
                return $this->redirect(['action' => 'initRegister']);
            }
            if (isset($user->getErrors()['username'])) {
                $user->setErrors(['username' => false], true);
                $this->Flash->error(__("User with username '{$user->username}' is already taken. Please enter a unique username."));
            } else if (isset($user->getErrors()['email'])) {
                $user->setErrors(['email' => false], true);
                $this->Flash->error(__("User with email '{$data['email']}' is already taken. Please enter a unique email."));
            } else {
                $this->Flash->error(__('User with similar details already exists.'));
            }
        }
        $user->email = $email;
        $this->viewBuilder()->setLayout('adminator_public');
        $this->set(compact('user'));
    }


    public function initRegister()
    {
        $chef_cuisines = $this->Users->ChefDetails->ChefCuisines->find('list');
        $this->viewBuilder()->setLayout('adminator_public');
        if ($this->request->is('post')) {
            $user = $this->Users->ChefDetails->newEntity();
            $data = $this->request->getData();
            $data['user_id'] = $this->Auth->user('id');
            $data['chef_cuisines_ids'] = implode(',', $data['chef_cuisines_ids']);
            $user = $this->Users->ChefDetails->patchEntity($user, $data);
            if ($this->Users->ChefDetails->save($user)) {
                $user = $this->Users->get($this->Auth->user('id'));
                $user->is_complete = 1;
                $this->Users->save($user);
                $userData = $user->toArray();
                $this->Auth->setUser($userData);
                $this->Cookie->write('Auth', $userData);
            }
            $this->redirect(['action' => 'onboarding']);
        }
        $this->set(compact('chef_cuisines'));
    }

    public function onboarding()
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UsersDocument', 'ChefDetails', 'ChefTimings', 'BankDetails']
        ]);
        $this->viewBuilder()->setLayout('adminator_no_action');
        $this->set(compact('user'));
    }

    public function timings()
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['ChefTimings']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->redirect(['action' => 'onboarding']);
            }
        }
        $this->viewBuilder()->setLayout('adminator_no_action');
        $weeks = $this->Users->ChefTimings->Weeks->find('list');
        $this->set(compact('user', 'weeks'));
    }

    public function verifyOtp()
    {
        $this->loadModel('UserOtp');
        $data = $this->UserOtp->find('all', [
            'conditions' => [
                'send_to' => '+' . $this->request->getQuery('mobile'),
                'otp' => $this->request->getQuery('otp')
            ]
        ]);
        if ($data->count()) {
            echo 1;
        }
        die;
    }

    public function banks($verify = false)
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['BankDetails', 'ChefDetails']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                if ($verify == 'first') {
                    $user->key = base64_encode($this->Auth->user('id'));
                    $this->loadComponent('Notify');
                    $this->Notify->sendAgreementEmail($user);
                }
                $this->redirect(['action' => 'onboarding']);
            }
        }
        $this->viewBuilder()->setLayout('adminator_no_action');
        $this->set(compact('user'));
    }

    public function agreement()
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UsersDocument', 'ChefDetails', 'ChefTimings', 'BankDetails']
        ]);
        $this->viewBuilder()->setLayout('adminator_no_action');
        $this->set(compact('user'));
    }

    public function accept($id)
    {
        $id = base64_decode($id);
        $user = $this->Users->get($id);
        $user = $this->Users->patchEntity($user, ['is_verified' => true]);
        if ($this->Users->save($user)) {
            $userData = $user->toArray();
            $this->Auth->setUser($userData);
            $this->Cookie->write('Auth', $userData);
            $this->redirect(['action' => 'onboarding']);
        }
    }

    public function launch()
    {
        $user = $this->Users->get($this->Auth->user('id'));
        if ($user->is_verified == true) {
            $user = $this->Users->patchEntity($user, ['status' => true]);
            if ($this->Users->save($user)) {
                $userData = $user->toArray();
                $this->Auth->setUser($userData);
                $this->Cookie->write('Auth', $userData);
                return $this->redirect('/dashboard');
            }
        }
        $this->redirect($this->referer());
    }


    public function forgotPassword()
    {
        if ($this->request->is('post')) {
            $userData = $this->Users->find('all', [
                'conditions' => [
                    'email' => $this->request->getData('email')
                ]
            ]);
            if ($userData->count()) {
                $user = $userData->first();
                $client = new Client();
                $client->post(WEB_API . 'api/forgotPassword', [
                    'email' => $user->email,
                    'role_id' => $user->role
                ]);
                $this->Flash->success('Propagrub Password Reset');
                $this->redirect('/');
            } else {
                $this->Flash->error(__('Provided user does not exists. Please register yourself.'));
                return $this->redirect(['action' => 'register', $this->request->getData('email')]);
            }
        }
    }

    public function resend()
    {
        $this->loadComponent('Notify');
        $email = $this->getRequest()->getSession()->read('otp-email');
        $id = $this->getRequest()->getSession()->read('otp-user-id');
        if (!empty($email)) {
            $this->Notify->sendOTPEmail($id);
        } else {
            $this->Notify->sendOTPMobile($id);
        }
        $this->Flash->success(__('OTP Resent on your device. please verify your otp'));
        $this->redirect($this->referer());
    }

    public function sendOtp($mobile)
    {
        $this->loadComponent('Notify');
        $data = new stdClass();
        $data->mobile = '+' . $mobile;
        $this->Notify->sendOTPMobile($data);
        die;
    }


    public function complete()
    {
        $this->loadModel('DropDown');
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UsersDocument', 'ChefDetails', 'ChefTimings', 'BankDetails']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->getData($user), [
                'validate' => $this->request->getQuery('validate') ? false : true
            ]);
            if ($this->Users->save($user)) {
                $user = $this->Users->get($this->Auth->user('id'));
                $this->Auth->setUser($user->toArray());
                if ($user->status != '1') {
                    $this->Flash->error(__('Thanks for submitting your details, admin will approve your profile and you\'ll notified'));
                }
                return $this->redirect('/dashboard');
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $service_type = $this->Users->ChefDetails->ChefServices->find('list');
        $weeks = $this->Users->ChefTimings->Weeks->find('list');
        $chef_cuisines = $this->Users->ChefDetails->ChefCuisines->find('list');
        $this->set(compact('user', 'service_type', 'weeks', 'chef_cuisines'));
    }

    public function logout()
    {
        $this->Cookie->delete('Auth');
        return $this->redirect($this->Auth->logout());
    }

    public function getData($existData = null)
    {
        $data = $this->request->getData();
        if (!empty($data['password'])) {
            if ($existData->password == md5($data['current_password'])) {
                $data['password'] = md5($data['password']);
            } else {
                $this->Flash->error('Invalid Current Password.');
                unset($data['password']);
            }
        } else {
            unset($data['password']);
        }
        $this->loadComponent('Fileupload');
        if (!empty($data['chef_detail']['image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_PROFILE_IMAGE, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $_FILES['thumbnail'] = $data['chef_detail']['image'];
            $this->Fileupload->upload('thumbnail');
            $data['chef_detail']['image'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['chef_detail']['image']);
        }
        if (!empty($data['chef_detail']['council_registration']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_DOC, 'allowed_types' => '*', 'encrypt_name' => true]);
            $_FILES['thumbnail'] = $data['chef_detail']['council_registration'];
            $this->Fileupload->upload('thumbnail');
            $data['chef_detail']['council_registration'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['chef_detail']['council_registration']);
        }

        if (!empty($data['chef_detail']['level_certificate']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_DOC, 'allowed_types' => '*', 'encrypt_name' => true]);
            $_FILES['thumbnail'] = $data['chef_detail']['level_certificate'];
            $this->Fileupload->upload('thumbnail');
            $data['chef_detail']['level_certificate'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['chef_detail']['level_certificate']);
        }

        if (!empty($data['chef_detail']['application_document']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_DOC, 'allowed_types' => '*', 'encrypt_name' => true]);
            $_FILES['thumbnail'] = $data['chef_detail']['application_document'];
            $this->Fileupload->upload('thumbnail');
            $data['chef_detail']['application_document'] = $this->Fileupload->output('file_name');
        } else {
            unset($data['chef_detail']['application_document']);
        }

        if (isset($data['image']) && !empty($data['image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => API_PROFILE_IMAGE, 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true, 'file_name' => $this->Auth->user('id') . '.png']);
            $_FILES['thumbnail'] = $data['image'];
            $this->Fileupload->upload('thumbnail');
            $data['image'] = $this->Fileupload->output('file_name');
        } else {
            if (isset($data['image'])) {
                unset($data['image']);
            }
        }
        $data['latitude'] = $data['chef_detail']['lat'];
        $data['longitude'] = $data['chef_detail']['lng'];
        $data['chef_detail']['chef_cuisines_ids'] = implode(',', $data['chef_detail']['chef_cuisines_ids']);
        if (empty($data['password'])) {
            unset($data['password']);
        }
        if ($existData->username !== $data['username']) {
            $data['user_name_modification']  = date('Y-m-d h:i:s');
        }

        return $data;
    }

    public function dashboard()
    {
        $this->loadModel('UserChefOrders');
        $this->loadModel('MenuProducts');
        $condition = [];
        if ($this->request->getQuery('start_date') && $this->request->getQuery('end_date')) {
            $condition['created_at >='] = $this->request->getQuery('start_date');
            $condition['created_at <='] = $this->request->getQuery('end_date');
            $this->set('start_date', $this->request->getQuery('start_date'));
            $this->set('end_date', $this->request->getQuery('end_date'));
        };
        $total_products = $this->MenuProducts->find('all', [
            'conditions' => $condition + [
                'user_id' => $this->Auth->user('id')
            ]
        ])->count();
        $this->set(compact('total_products'));
    }

    public function endpoint()
    {
        $this->request->session()->start();
        \Hybrid_Endpoint::process();
    }

    public function authenticated()
    {
        $user = $this->Auth->identify();
        if ($user) {
            $userExists = $this->Users->exists([
                'role' => '2',
                'social_login_id' => $user->identifier
            ]);
            if (($userExists || $this->request->is('post')) === false) {
                $this->set(compact('user'));
                return;
            }
            if ($this->request->is('post')) {
                $usernameExist = $this->Users->exists([
                    'role' => '2',
                    'username' => $this->request->getData('username')
                ]);
                if ($usernameExist) {
                    $this->set(compact('user'));
                    $this->Flash->error('Provided username already exists.');
                    return;
                }
            }
            $loginType = ['Facebook' => '4', 'Google' => '3'];
            $client = new Client();
            $response = $client->post(WEB_API . 'api/userSignUp', [
                'first_name' => $user->firstName,
                'username' => $this->request->getData('username'),
                'last_name' => $user->lastName,
                'role' => '2',
                'login_type' => $loginType[Configure::read('provider')],
                'latitude' => '51.5287718',
                'longitude' => '-0.241681',
                'device_type' => 'Web',
                'social_login_id' => $user->identifier,
                'image_url' => $user->photoURL,
            ])->getJson();
            if ($response['status'] === 200) {
                $this->Auth->setUser($response['data']);
                $this->Flash->success('Login successful');

                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error('Invalid failed');
            }
        } else {
            $this->Flash->error('Invalid failed');
        }

        return $this->redirect($this->referer());
    }

    public function bankDetails()
    {
        if ($this->Users->BankDetails->exists(['user_id' => $this->Auth->user('id')])) {
            $this->Users->BankDetails->setPrimaryKey('user_id');
            $bank_detail = $this->Users->BankDetails->get($this->Auth->user('id'));
        } else {
            $bank_detail = $this->Users->BankDetails->newEntity();
        }
        if ($this->request->is(['post', 'put'])) {
            $authToken = $this->Users->find('all', [
                'fields' => ['auth_token'],
                'conidtions' => [
                    'id' => $this->Auth->user('id')
                ]
            ])->first()->auth_token;
            $data = $this->request->getData();
            $data['payment_email'] = $this->Auth->user('email');
            $data['phone'] = $this->Auth->user('mobile');
            $data['dob'] = $this->Auth->user('dob');
            $data['user_id'] = $this->Auth->user('id');
            $client = new Client();
            $chefDetails = $this->Users->ChefDetails->find('all', [
                'conditions' => [
                    'user_id' => $this->Auth->user('id')
                ]
            ])->first();
            $file_rel = [
                'doc_front_id' => 'front_file',
                'doc_rear_id' => 'back_file',
                'address_front_doc_id' => 'front_proof_file',
                'address_rear_doc_id' => 'back_proof_file'
            ];
            foreach ($file_rel as $doc_key => $doc_value) {
                $response = $client->post(WEB_API . 'api/uploadStripeDocument', [
                    'auth_token' => $authToken,
                    'role' => 2,
                    'file_name' => $chefDetails->{$doc_value},
                    'user_id' => $this->Auth->user('id')
                ])->getJson();
                if ($response['status'] !== 200) {
                    $this->set(compact('bank_detail'));
                    $this->Flash->error('Invalid file format, please try again.');
                    return;
                }
                $data[$doc_value] = $response['data'];
                $data[$doc_key] = $response['data'];
            }
            $response = $client->post(WEB_API . 'api/addBankAccounnt', [
                'account_holder_name' => $data['holder_name'],
                'account_number' => $data['account_number'],
                'sort_code' => $data['sort_code']
            ])->getJson();
            if (empty($response['data'])) {
                $this->Flash->error('Invalid bank details, please try again.');
                return;
            }
            $data['external_account'] = $response['data']['id'];
            $full_name = explode('@', $data['holder_name']);
            $response = $client->post(WEB_API . 'api/createAccount', [
                'email' => $data['payment_email'],
                'front_file' => $data['doc_front_id'],
                'back_file' => $data['doc_rear_id'],
                'front_proof_file' => $data['address_front_doc_id'],
                'back_proof_file' => $data['address_rear_doc_id'],
                'first_name' => $full_name[0],
                'last_name' => isset($full_name[1]) ? $full_name[1] : '',
                'city' => $data['city'],
                'line1' => $data['address'],
                'postal_code' => $data['post_code'],
                'dob' => date("d-m-Y", strtotime($data['date_of_birth'])),
                'phone_number' => $data['phone'],
            ])->getJson();
            if (empty($response) || empty($response['data'])) {
                $this->set(compact('bank_detail'));
                $this->Flash->error('The provided details are invalid, please check once your details.');
                return;
            }
            $data['stripe_bank_id'] = $response['data']['id'];
            $response = $client->post(WEB_API . 'api/linkExternalAccount', [
                'account_id' => $data['stripe_bank_id'],
                'external_account' => $data['external_account'],
            ])->getJson();
            if (empty($response) || empty($response['data'])) {
                $this->set(compact('bank_detail'));
                $this->Flash->error('Unable to link your bank account, please try again later.');
                return;
            }
            $data['stripe_account_id'] = $response['data']['id'];
            $data['dob'] = $data['date_of_birth'];
            $data['routing_number'] = $data['sort_code'];
            $$bank_detail = $this->Users->BankDetails->patchEntity($bank_detail, $data);
            if ($this->Users->BankDetails->save($bank_detail)) {
                $this->Flash->success('Your bank account has been updated.');
                return $this->redirect($this->referer());
            }
            $this->Flash->error('Unable to save your bank details. please try again later.');
        }
        $this->set(compact('bank_detail'));
    }

    public function checkEmail($email)
    {
        $this->set('exists', $this->Users->exists([
            'email' => $email,
            'not' => [
                'email' => $this->Auth->user('email')
            ]
        ]));
    }
    public function checkMobile($mobile)
    {
        $this->set('exists', $this->Users->exists([
            'mobile' => $mobile,
            'not' => [
                'mobile' => $this->Auth->user('mobile')
            ]
        ]));
    }

    public function sales()
    {
    }

    public function liveOrders()
    {
    }

    public function invoices()
    {
    }

    public function advertisements()
    {
    }

    public function collection()
    {
        $this->loadModel('ChefDeliveries');
        $collection = $this->ChefDeliveries->find('all', [
            'conditions' => [
                'user_id' => $this->Auth->user('id')
            ]
        ]);
        if ($collection->count() == 0) {
            $collection = $this->ChefDeliveries->newEntity();
        } else {
            $collection = $collection->first();
        }
        if ($this->request->is(['post', 'put'])) {
            $collection->user_id = $this->Auth->user('id');
            $collection = $this->ChefDeliveries->patchEntity($collection, $this->request->getData());
            if ($this->ChefDeliveries->save($collection)) {
                $this->Flash->success('Updated Successful.');
                return $this->redirect($this->referer());
            }
            $this->Flash->error('Unable to updated, please check your details.');
        }
        $this->set(compact('collection'));
    }
}
