<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * UserSubscriptions Controller
 *
 * @property \App\Model\Table\UserSubscriptions $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserSubscriptionsController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->loadModel('DropDown');
        $this->loadModel('UserCards');
        parent::beforeFilter($event);
    }

    public function index() {
        $this->Flash->error('We are currently running in beta version. We\'ll be live soon.');
        return $this->redirect(['controller'  => 'dashboard', 'action' => 'index']);
        $subscript_plan = $this->DropDown->getSubscriptionAmount();
        if ($this->UserCards->exists(['user_id' => $this->Auth->user('id')])) {
            $this->UserCards->setPrimaryKey('user_id');
            $card_detail = $this->UserCards->get($this->Auth->user('id'), ['contain' => [
                'Users' => [
                    'UserSubscriptions',
                    'PromoCodes'
                ]
            ]]);
        } else {
            $card_detail = $this->UserCards->newEntity();
        }
        if ($this->request->is(['post', 'put'])) {
            $data = $user_card_data = $this->getData($card_detail);
            if ($data) {
                unset($user_card_data['user']);
                $card_detail = $this->UserCards->patchEntity($card_detail, $user_card_data);
                if ($this->UserCards->save($card_detail)) {
                    if ($this->UserSubscriptions->exists(['user_id' => $this->Auth->user('id')])) {
                        $this->UserSubscriptions->setPrimaryKey('user_id');
                        $subscript = $this->UserSubscriptions->get($this->Auth->user('id'));
                    } else {
                        $subscript = $this->UserSubscriptions->newEntity();
                    }
                    $subscript = $this->UserSubscriptions->patchEntity($subscript, $data['user']['user_subscription']);
                    $this->UserSubscriptions->save($subscript);
                    $this->Flash->success('Your subscriptions has been updated.');
                    return $this->redirect($this->referer());
                }
                $this->Flash->error('Unable to update subscription. please try again later.');
            } else {
                return $this->redirect($this->referer());
            }
        }
        $this->set(compact('card_detail', 'subscript_plan'));
    }

    public function getData($existData = []) {
        $data = $this->request->getData();
        $this->loadModel('Users');
        $userData = $this->Users->get($this->Auth->user('id'));
        $data['user_id'] = $this->Auth->user('id');
        $data['user']['user_subscription']['user_id'] = $this->Auth->user('id');
        $dbData = [
            'card_holder_name',
            'card_expiry_month',
            'card_expiry_year'
        ];
        $exp_date = null;
        $check_account = false;
        foreach ($dbData as $col) {
            if ($data[$col] != $existData->{$col}) {
                $check_account = true;
                break;
            }
        }
        if (!empty($data['user']['promo_code']['promocode']) && $data['user']['promo_code']['promocode'] != $existData['user']['promo_code']->promocode) {
            $this->loadModel('PromoCodes');
            if ($this->PromoCodes->exists(['promocode' => $data['user']['promo_code']['promocode'], 'end_date >=' => date('Y-m-d'), 'start_date <=' => date('Y-m-d')])) {
                $this->PromoCodes->setPrimaryKey('promocode');
                $promocodes = $this->PromoCodes->get($data['user']['promo_code']['promocode']);
                $used_user = $this->ChefPromocodes->find('all', [
                    'conditions' => [
                        'ChefPromocodes.promo_code_id' => $promocodes->id
                    ]
                ])->count();
                if ($used_user > $promocodes->total_user) {
                    $exp_date = $promocodes->end_date;
                    $user = $this->Users->patchEntity($userData, ['promo_code_id' => $promocodes->id]);
                    $this->Users->save($user);
                    $chef_promocodes = $this->ChefPromocodes->patchEntity(['user_id' => $this->Auth->user('id'), 'promo_code_id' => $promocodes->id]);
                    $this->ChefPromocodes->save($chef_promocodes);
                    $authUser = $this->Auth->user();
                    $authUser['promo_code_id'] = $promocodes->id;
                    $this->Auth->setUser($authUser);
                } else {
                    $this->Flash->error('Promocode already in used, please use another promocode');
                }
            } else {
                $this->Flash->error('Promocode is expired or not valid, please use another promocode');
            }
        }
        if ($check_account || ($data['user']['user_subscription']['subscrriber_plan'] != @$existData['user']['user_subscription']->subscrriber_plan)) {
            $stripe = new \Stripe\StripeClient('sk_test_P9M2KP1z8CrWiGm2Nod9JpLE008HSBDZ8W');
            if ($check_account || empty(@$existData['user']['user_subscription']->stripe_customer_id)) {
                try {
                    $token = $stripe->tokens->create([
                        'card' => [
                            'number' => (int) $data['card_number'],
                            'exp_month' => 3,
                            'exp_year' => (int) $data['card_expiry_year'],
                            'cvc' => (int) $data['card_cvv'],
                        ],
                    ]);
                } catch (Exception $e) {
                    $this->Flash->error('Invalid card details. please try again later.');
                    return false;
                }
            }
            \Stripe\Stripe::setApiKey('sk_test_P9M2KP1z8CrWiGm2Nod9JpLE008HSBDZ8W');
            try {
                if ($check_account || empty(@$existData['user']['user_subscription']->stripe_customer_id)) {
                    if (empty(@$existData['user']['user_subscription']->stripe_customer_id)) {
                        $customerId = \Stripe\Customer::create(array(
                            'email' => $this->Auth->user('email'),
                            'source' => $token->id
                        ))->id;
                    } else {
                        $customerId = $stripe->customers->update(
                            $existData['user']['user_subscription']->stripe_customer_id,
                            [
                                'source' => $token->id
                            ]
                        )->id;
                    }
                } else {
                    $customerId = $existData['user']['user_subscription']->stripe_customer_id;
                }
            } catch (Exception $e) {
                $this->Flash->error('Unable to create subscription. please try again later.');
                return false;
            }
            $priceCents = round($this->DropDown->getSubscriptionDetails($data['user']['user_subscription']['subscrriber_plan'])['price'] * 100);
            try {
                $plan = \Stripe\Plan::create(array(
                    "product" => [
                        "name" => $this->DropDown->getSubscriptionAmount($data['user']['user_subscription']['subscrriber_plan'])
                    ],
                    "amount" => $priceCents,
                    "currency" => 'INR',
                    "interval" => $this->DropDown->getSubscriptionDetails($data['user']['user_subscription']['subscrriber_plan'])['interval'],
                    "interval_count" => 1
                ));
            } catch (Exception $e) {
                $this->Flash->error('Unable to create plan. please try again later.');
                return false;
            }
            try {
                //                if (empty(@$existData['user']['user_subscription']->stripe_subscription_id)) {
                $sub_data = array(
                    "customer" => $customerId,
                    "items" => array(
                        array(
                            "plan" => $plan,
                        ),
                    ),
                );
                if (!empty($exp_date)) {
                    $sub_data['trial_end'] = strtotime($exp_date);
                }
                $subscription = \Stripe\Subscription::create($sub_data);
                //                } else {
                //                    $this->Flash->error('Unable to subscribe to plan. please cancle your previous subscription.');
                //                    return false;
                //                    $sub_data = array(
                //                        "items" => array(
                //                            array(
                //                                "plan" => $plan,
                //                            ),
                //                        ),
                //                    );
                //                    if (!empty($exp_date)) {
                //                        $sub_data['trial_end'] = strtotime($exp_date);
                //                    }
                //                    $subscription = $stripe->subscriptions->update($existData['user']['user_subscription']->stripe_subscription_id, $sub_data);
                //                }
            } catch (Exception $e) {
                $this->Flash->error('Unable to subscribe to plan. please try again later.');
                return false;
            }
            $subsData = $subscription->jsonSerialize();
            if ($subsData['status'] == 'active') {
                $data['user']['user_subscription']['stripe_subscription_id'] = $subsData['id'];
                $data['user']['user_subscription']['stripe_customer_id'] = $subsData['customer'];
                if (!empty($subsData['plan'])) {
                    $data['user']['user_subscription']['stripe_plan_id'] = $subsData['plan']['id'];
                    $data['user']['user_subscription']['plan_amount'] = ($subsData['plan']['amount'] / 100);
                    $data['user']['user_subscription']['plan_amount_currency'] = $subsData['plan']['currency'];
                    $data['user']['user_subscription']['plan_interval'] = $subsData['plan']['interval'];
                    $data['user']['user_subscription']['plan_interval_count'] = $subsData['plan']['interval_count'];
                }
                $data['user']['user_subscription']['payer_email'] = $this->Auth->user('email');
                $data['user']['user_subscription']['plan_period_start'] = date("Y-m-d H:i:s", $subsData['current_period_start']);
                $data['user']['user_subscription']['plan_period_end'] = date("Y-m-d H:i:s", $subsData['current_period_end']);
                $data['user']['user_subscription']['status'] = $subsData['status'];
                if (empty($exp_date)) {
                    $exp_date = date('Y-m-d', strtotime('+1 ' . $this->DropDown->getSubscriptionDetails($data['user']['user_subscription']['subscrriber_plan'])['interval']));
                }
            } else {
                $this->Flash->error('Subscription activation failed. please try again later.');
                return false;
            }
        }

        if (!empty($exp_date)) {
            $authUser = $this->Auth->user();
            $authUser['subscription_expired'] = $exp_date;
            $this->Auth->setUser($authUser);
            $user = $this->Users->patchEntity($userData, ['subscription_expired' => $exp_date]);
            $this->Users->save($user);
        }
        return $data;
    }
}
