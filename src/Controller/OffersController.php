<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Offers Controller
 *
 * @property \App\Model\Table\OffersTable $Offers
 *
 * @method \App\Model\Entity\Offer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OffersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $offers = $this->Offers->find('all', [
            'conidtions' => [
                'Offers.user_id' => $this->Auth->user('id')
            ]
        ]);

        $this->set(compact('offers'));
    }

    /**
     * View method
     *
     * @param string|null $id Offer id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $offer = $this->Offers->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set('offer', $offer);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $offer = $this->Offers->newEntity();
        if ($this->request->is('post')) {
            $offer = $this->Offers->patchEntity($offer, $this->request->getData());
            if ($this->Offers->save($offer)) {
                $this->Flash->success(__('The offer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The offer could not be saved. Please, try again.'));
        }
        $users = $this->Offers->Users->find('list', ['limit' => 200]);
        $this->set(compact('offer', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Offer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $offer = $this->Offers->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $offer = $this->Offers->patchEntity($offer, $this->request->getData());
            if ($this->Offers->save($offer)) {
                $this->Flash->success(__('The offer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The offer could not be saved. Please, try again.'));
        }
        $users = $this->Offers->Users->find('list', ['limit' => 200]);
        $this->set(compact('offer', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Offer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $offer = $this->Offers->get($id);
        if ($this->Offers->delete($offer)) {
            $this->Flash->success(__('The offer has been deleted.'));
        } else {
            $this->Flash->error(__('The offer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function basketTotal()
    {
        $offer = $this->Offers->newEntity();
        if ($this->request->is('post')) {
            $offer = $this->Offers->patchEntity($offer, $this->request->getData() + ['user_id' => $this->Auth->user('id'), 'offer_type' => $this->request->getData('timing')]);
            if ($this->Offers->save($offer)) {
                $this->Flash->success(__('The offer has been created.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The offer could not be created. Please, try again.'));
        }
        $this->set(compact('offer'));
    }

    public function specificItems()
    {
        $offer = $this->Offers->newEntity();
        if ($this->request->is('post')) {
            $offer = $this->Offers->patchEntity($offer, $this->request->getData() + ['user_id' => $this->Auth->user('id'), 'offer_type' => $this->request->getData('timing')]);
            if ($this->Offers->save($offer)) {
                $this->Flash->success(__('The offer has been created.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The offer could not be created. Please, try again.'));
        }
        $this->loadModel('MenuProducts');
        $items = $this->MenuProducts->find('all', [
            'fields' => [
                'MenuProducts.id',
                'MenuProducts.product_name',
                'MenuCategories.category_name',
                'MenuProducts.product_price'
            ],
            'contain' => [
                'MenuCategories'
            ],
            'conditions' => [
                'MenuProducts.user_id' => $this->Auth->user('id')
            ]
        ])->toArray();
        $this->MenuProducts->MenuCategories->setPrimaryKey('category_name');
        $categories = $this->MenuProducts->MenuCategories->find('list', [
            'conditions' => [
                'MenuCategories.user_id' => $this->Auth->user('id')
            ]
        ]);
        $this->set(compact('offer', 'items', 'categories'));
    }
}
