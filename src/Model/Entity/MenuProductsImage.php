<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MenuProductsImage Entity
 *
 * @property int $id
 * @property int $menu_id
 * @property int $category_id
 * @property int $menu_product_id
 * @property int $image
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\Menu $menu
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\MenuProduct $menu_product
 */
class MenuProductsImage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'menu_id' => true,
        'category_id' => true,
        'menu_product_id' => true,
        'image' => true,
        'created_at' => true,
        'modified_at' => true,
        'menu' => true,
        'category' => true,
        'menu_product' => true,
    ];
}
