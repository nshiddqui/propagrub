<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserReview Entity
 *
 * @property int $id
 * @property string $rating_to
 * @property string $rating_by
 * @property float $rating
 * @property string $review
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $modified_at
 */
class UserReview extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'rating_to' => true,
        'rating_by' => true,
        'rating' => true,
        'review' => true,
        'created_at' => true,
        'modified_at' => true,
        'review_reply' => true,
    ];
}
