<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReportedPost Entity
 *
 * @property int $id
 * @property string $post_id
 * @property string $reported_by
 * @property string $reported_reason
 * @property string $comments
 * @property \Cake\I18n\FrozenTime $created_at
 *
 * @property \App\Model\Entity\Post $post
 */
class ReportedPost extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'post_id' => true,
        'reported_by' => true,
        'reported_reason' => true,
        'comments' => true,
        'created_at' => true,
        'post' => true,
    ];
}
