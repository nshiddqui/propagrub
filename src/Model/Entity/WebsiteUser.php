<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WebsiteUser Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $email
 * @property string $last_name
 * @property string $User_type
 * @property string $phone
 * @property \Cake\I18n\FrozenTime $created
 */
class WebsiteUser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'email' => true,
        'last_name' => true,
        'User_type' => true,
        'phone' => true,
        'created' => true,
    ];
}
