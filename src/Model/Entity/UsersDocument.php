<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UsersDocument Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $doc_type
 * @property string $doc_number
 * @property string $doc_name
 * @property string $file
 * @property string $expiry_month
 * @property string $expiry_year
 * @property \Cake\I18n\FrozenTime $created
 * @property string $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UsersDocument extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'doc_type' => true,
        'doc_number' => true,
        'doc_name' => true,
        'file' => true,
        'expiry_month' => true,
        'expiry_year' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
    ];
}
