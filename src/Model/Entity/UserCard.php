<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserCard Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $card_holder_name
 * @property string $card_expiry_month
 * @property string $card_expiry_year
 * @property string $card_number
 * @property string $card_cvv
 * @property \Cake\I18n\FrozenTime $created
 * @property string $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserCard extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'card_holder_name' => true,
        'card_expiry_month' => true,
        'card_expiry_year' => true,
        'card_number' => true,
        'card_cvv' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
    ];
}
