<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserOtp Entity
 *
 * @property int $id
 * @property string $send_to
 * @property int $otp
 * @property \Cake\I18n\FrozenTime $send_at
 */
class UserOtp extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'send_to' => true,
        'otp' => true,
        'send_at' => true,
    ];
}
