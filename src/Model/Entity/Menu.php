<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Menu Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $menu_name
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\MenuCategory[] $menu_categories
 * @property \App\Model\Entity\MenuProduct[] $menu_products
 * @property \App\Model\Entity\MenuProductsImage[] $menu_products_images
 */
class Menu extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'menu_name' => true,
        'menu_description' => true,
        'image' => true,
        'created_at' => true,
        'modified_at' => true,
        'pdf' => true,
        'user' => true,
        'menu_categories' => true,
        'menu_products' => true,
        'menu_products_images' => true,
        'estimated_preparation_time' => true,
        'position' => true
    ];
}
