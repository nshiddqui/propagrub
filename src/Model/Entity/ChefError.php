<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChefError Entity
 *
 * @property int $id
 * @property string $reported_to
 * @property string $reported_by
 * @property string $reported_options
 * @property string $reported_detail
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $modified_at
 */
class ChefError extends Entity {
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'reported_to' => true,
        'reported_by' => true,
        'reported_to_user' => true,
        'reported_by_user' => true,
        'reported_options' => true,
        'reported_detail' => true,
        'created_at' => true,
        'modified_at' => true,
    ];
}
