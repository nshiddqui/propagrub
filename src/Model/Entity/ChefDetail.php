<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChefDetail Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $resturant_name
 * @property string $estimated_delivery_time
 * @property string $order_min_amount
 * @property string $packaging_charges
 * @property string $short_description
 * @property string $address
 * @property string $image
 * @property int $service_type
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 */
class ChefDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'resturant_name' => true,
        'chef_cuisines_ids' => true,
        'estimated_delivery_time' => true,
        'order_min_amount' => true,
        'packaging_charges' => true,
        'short_description' => true,
        'address' => true,
        'lat' => true,
        'lng' => true,
        'image' => true,
        'service_type' => true,
        'geofence_radius' => true,
        'status' => true,
        'company_name' => true,
        'company_number' => true,
        'council_registration' => true,
        'level_certificate' => true,
        'created_at' => true,
        'modified_at' => true,
        'sole_trader' => true,
        'sole_trader_name' => true,
        'front_file' => true,
        'back_file' => true,
        'front_proof_file' => true,
        'back_proof_file' => true,
        'application_document' => true,
        'application_number' => true,
        'application_option' => true,
        'user' => true,
        'own_driver' => true,
        'pickup_available' => true,
        'delivery_fee' => true,
        'alchohol_license_file' => true,
        'menu_file' => true,
        'is_hygiene' => true,
        'rating' => true,
        'vat_registered' => true,
        'vat_number' => true,
        'mobile_number' => true,
    ];
}
