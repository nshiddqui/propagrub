<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChefCuisine Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $cuisine_name
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 */
class ChefCuisine extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'cuisine_name' => true,
        'cuisine_image' => true,
    ];
}
