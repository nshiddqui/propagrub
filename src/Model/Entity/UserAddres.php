<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserAddres Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $house_no
 * @property string $street_address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $postal_code
 * @property bool $is_primary
 * @property string $latitude
 * @property string $longitude
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserAddres extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'house_no' => true,
        'street_address' => true,
        'city' => true,
        'state' => true,
        'country' => true,
        'postal_code' => true,
        'is_primary' => true,
        'latitude' => true,
        'longitude' => true,
        'created_at' => true,
        'modified' => true,
        'user' => true,
    ];

    protected function _getFullAddress() {
        return $this->house_no . ', ' . $this->street_address . ', ' . $this->city . ' ' . $this->state . ', ' . $this->country . '-' . $this->postal_code;
    }

}
