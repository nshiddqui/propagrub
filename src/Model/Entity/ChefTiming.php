<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChefTiming Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $cuisine_name
 * @property string $week_id
 * @property string $start_time
 * @property string $end_time
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Day $day
 */
class ChefTiming extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'cuisine_name' => true,
        'notes' => true,
        'week_id' => true,
        'start_time' => true,
        'end_time' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
        'day' => true,
    ];
}
