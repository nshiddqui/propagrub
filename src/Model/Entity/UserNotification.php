<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserNotification Entity
 *
 * @property int $id
 * @property int $to_user_id
 * @property int $from_user_id
 * @property int $notification_type
 * @property string $notification_title
 * @property string $notification_text
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\ToUser $to_user
 * @property \App\Model\Entity\FromUser $from_user
 */
class UserNotification extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'to_user_id' => true,
        'from_user_id' => true,
        'notification_type' => true,
        'notification_title' => true,
        'notification_text' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
        'from_user' => true,
    ];
}
