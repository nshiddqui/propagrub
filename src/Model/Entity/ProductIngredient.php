<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductIngredient Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int $ingredients_category_id
 * @property int $user_id
 * @property string $name
 * @property string $price
 * @property bool $mandatory
 * @property int $min_quantity
 * @property int $max_quantity
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\MenuProduct $menu_product
 * @property \App\Model\Entity\User $user
 */
class ProductIngredient extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'ingredients_category_id' => true,
        'user_id' => true,
        'created_at' => true,
        'modified_at' => true,
        'product' => true,
        'user' => true,
        'product_ingredients_categories' => true
    ];
}
