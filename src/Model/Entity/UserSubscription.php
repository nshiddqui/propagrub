<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserSubscription Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $payment_method
 * @property string $stripe_subscription_id
 * @property string $stripe_customer_id
 * @property string $stripe_plan_id
 * @property float $plan_amount
 * @property string $plan_amount_currency
 * @property string $plan_interval
 * @property int $plan_interval_count
 * @property string $payer_email
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $plan_period_start
 * @property \Cake\I18n\FrozenTime $plan_period_end
 * @property string $status
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\StripeSubscription $stripe_subscription
 * @property \App\Model\Entity\StripeCustomer $stripe_customer
 * @property \App\Model\Entity\StripePlan $stripe_plan
 */
class UserSubscription extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'payment_method' => true,
        'stripe_subscription_id' => true,
        'stripe_customer_id' => true,
        'stripe_plan_id' => true,
        'plan_amount' => true,
        'plan_amount_currency' => true,
        'plan_interval' => true,
        'plan_interval_count' => true,
        'payer_email' => true,
        'created' => true,
        'plan_period_start' => true,
        'plan_period_end' => true,
        'status' => true,
        'user' => true,
        'stripe_subscription' => true,
        'stripe_customer' => true,
        'stripe_plan' => true,
        'subscrriber_plan' => true
    ];

}
