<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Offer Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $discount
 * @property float $min_amount
 * @property \Cake\I18n\FrozenDate|null $start_date
 * @property string|null $start_time
 * @property \Cake\I18n\FrozenDate|null $end_date
 * @property string|null $end_time
 * @property int $audience_type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $is_regular
 *
 * @property \App\Model\Entity\User $user
 */
class Offer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'discount' => true,
        'minimum_spend_required' => true,
        'start_date' => true,
        'start_time' => true,
        'end_date' => true,
        'end_time' => true,
        'audience' => true,
        'created' => true,
        'modified' => true,
        'is_regular' => true,
        'user' => true,
        'offer_type' => true,
    ];
}
