<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MenuCategory Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $menu_id
 * @property string $category_name
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Menu $menu
 */
class MenuCategory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'menu_id' => true,
        'category_name' => true,
        'category_description' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
        'menu' => true,
    ];
}
