<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $role
 * @property string $first_name
 * @property string $last_name
 * @property string|null $email
 * @property string|null $mobile
 * @property int|null $gender
 * @property string|null $image
 * @property string|null $dob
 * @property \Cake\I18n\FrozenTime|null $last_login
 * @property string|null $push_token
 * @property string|null $auth_token
 * @property bool $is_verified
 * @property bool $status
 * @property int|null $login_type
 * @property string|null $social_login_id
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $device_type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\SocialLogin $social_login
 * @property \App\Model\Entity\BankDetail[] $bank_details
 * @property \App\Model\Entity\ChefCuisine[] $chef_cuisines
 * @property \App\Model\Entity\ChefDetail[] $chef_details
 * @property \App\Model\Entity\ChefTiming[] $chef_timings
 * @property \App\Model\Entity\DriverVehicle[] $driver_vehicles
 * @property \App\Model\Entity\MenuCategory[] $menu_categories
 * @property \App\Model\Entity\MenuProduct[] $menu_products
 * @property \App\Model\Entity\Menu[] $menus
 * @property \App\Model\Entity\ProductIngredient[] $product_ingredients
 * @property \App\Model\Entity\UserAddres[] $user_address
 * @property \App\Model\Entity\UserCard[] $user_cards
 * @property \App\Model\Entity\UsersDocument[] $users_document
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'role' => true,
        'first_name' => true,
        'last_name' => true,
        'password' => true,
        'username' => true,
        'email' => true,
        'mobile' => true,
        'gender' => true,
        'image' => true,
        'dob' => true,
        'last_login' => true,
        'push_token' => true,
        'auth_token' => true,
        'is_verified' => true,
        'subscription_expired' => true,
        'status' => true,
        'login_type' => true,
        'social_login_id' => true,
        'is_complete' => true,
        'latitude' => true,
        'longitude' => true,
        'device_type' => true,
        'created' => true,
        'modified' => true,
        'social_login' => true,
        'bank_details' => true,
        'chef_cuisines' => true,
        'chef_detail' => true,
        'chef_timings' => true,
        'driver_vehicles' => true,
        'menu_categories' => true,
        'menu_products' => true,
        'menus' => true,
        'product_ingredients' => true,
        'user_address' => true,
        'user_cards' => true,
        'users_document' => true,
        'bank_detail' => true,
        'user_subscription' => true,
        'register_method' => true,
        'is_deleted' => true,
        'user_name_modification' => true
    ];

    protected function _getFullName()
    {
        return $this->first_name . '  ' . $this->last_name;
    }
}
