<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserChefOrder Entity
 *
 * @property int $id
 * @property int $chef_id
 * @property string $order_no
 * @property int $order_type
 * @property string $transection_id
 * @property int $customer_id
 * @property int $address_id
 * @property int $card_id
 * @property string|null $total_cost
 * @property string $service_fee
 * @property string $delivery_charges
 * @property string $promo_code
 * @property string $discount
 * @property int $status
 * @property int $driver_id
 * @property \Cake\I18n\FrozenTime $deliver_date
 * @property int|null $rating
 * @property string $review
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $chef
 * @property \App\Model\Entity\UserCart $user_cart
 * @property \App\Model\Entity\User $customer
 * @property \App\Model\Entity\UserAddres $user_addres
 * @property \App\Model\Entity\UserCard $user_card
 * @property \App\Model\Entity\User $driver
 */
class UserChefOrder extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'otp' => true,
        'chef_id' => true,
        'order_no' => true,
        'order_type' => true,
        'transection_id' => true,
        'customer_id' => true,
        'address_id' => true,
        'card_id' => true,
        'total_cost' => true,
        'service_fee' => true,
        'delivery_charges' => true,
        'promo_code' => true,
        'discount' => true,
        'status' => true,
        'driver_id' => true,
        'deliver_date' => true,
        'rating' => true,
        'review' => true,
        'created_at' => true,
        'modified_at' => true,
        'chef' => true,
        'user_cart' => true,
        'customer' => true,
        'user_addres' => true,
        'user_card' => true,
        'driver' => true,
    ];

}
