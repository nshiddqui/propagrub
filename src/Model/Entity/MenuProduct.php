<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MenuProduct Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $menu_id
 * @property int $category_id
 * @property string $product_name
 * @property string $product_price
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Menu $menu
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\MenuProductsImage[] $menu_products_images
 */
class MenuProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'menu_id' => true,
        'category_id' => true,
        'position' => true,
        'product_name' => true,
        'product_price' => true,
        'product_tax' => true,
        'allergen_id' => true,
        'is_allergens' => true,
        'product_detail' => true,
        'internal_name' => true,
        'is_age_restricted' => true,
        'image' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
        'menu' => true,
        'category' => true,
        'menu_products_images' => true,
        'is_shippable' => true,
        'estimated_preparation_time' => true,
        'shipping_charges' => true
    ];
}
