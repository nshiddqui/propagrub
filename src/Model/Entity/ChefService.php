<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChefService Entity
 *
 * @property int $id
 * @property string $service_name
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 */
class ChefService extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'service_name' => true,
        'created_at' => true,
        'modified_at' => true,
    ];
}
