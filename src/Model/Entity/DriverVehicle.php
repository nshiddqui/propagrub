<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DriverVehicle Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $vehicle_type
 * @property string $manufecturer_name
 * @property string $model_name
 * @property string $manufacturer_year
 * @property string $vehicle_plate_number
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 */
class DriverVehicle extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'vehicle_type' => true,
        'manufecturer_name' => true,
        'model_name' => true,
        'manufacturer_year' => true,
        'vehicle_plate_number' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
    ];
}
