<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BankDetail Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $bank_name
 * @property string $bank_location
 * @property string $account_number
 * @property string $holder_name
 * @property string $payment_email
 * @property string $routing_number
 * @property string $phone
 * @property string $id_number
 * @property string $dob
 * @property string $address
 * @property string $stret_address
 * @property string $city
 * @property string $post_code
 * @property string $address_rear_doc_id
 * @property string $address_front_doc_id
 * @property string $doc_front_id
 * @property string $doc_rear_id
 * @property string $stripe_account_id
 * @property string $stripe_bank_id
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $modified_at
 *
 * @property \App\Model\Entity\User $user
 */
class BankDetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'bank_name' => true,
        'bank_location' => true,
        'account_number' => true,
        'holder_name' => true,
        'payment_email' => true,
        'routing_number' => true,
        'phone' => true,
        'id_number' => true,
        'dob' => true,
        'address' => true,
        'stret_address' => true,
        'city' => true,
        'post_code' => true,
        'address_rear_doc_id' => true,
        'address_front_doc_id' => true,
        'doc_front_id' => true,
        'doc_rear_id' => true,
        'stripe_account_id' => true,
        'stripe_bank_id' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
    ];
}
