<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserCart Entity
 *
 * @property int $id
 * @property int|null $order_id
 * @property int $user_id
 * @property int $product_id
 * @property int $menu_id
 * @property int $chef_id
 * @property string $ingredients_ids
 * @property int $quantity
 * @property string $notes
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $modified_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\MenuProduct $menu_product
 * @property \App\Model\Entity\Menu $menu
 * @property \App\Model\Entity\UserChefOrder[] $user_chef_orders
 */
class UserCart extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_id' => true,
        'user_id' => true,
        'product_id' => true,
        'menu_id' => true,
        'chef_id' => true,
        'ingredients_ids' => true,
        'quantity' => true,
        'notes' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
        'menu_product' => true,
        'menu' => true,
        'user_chef_orders' => true,
    ];
}
