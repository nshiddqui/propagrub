<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChefOffer Entity
 *
 * @property int $id
 * @property string $chef_id
 * @property string $offer_value
 * @property string $offer_type
 * @property \Cake\I18n\FrozenTime $offer_exp_date
 * @property string $promo_code
 * @property string $offer_title
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property string $min_amount
 *
 * @property \App\Model\Entity\Chef $chef
 */
class ChefOffer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'chef_id' => true,
        'offer_value' => true,
        'offer_type' => true,
        'offer_exp_date' => true,
        'promo_code' => true,
        'offer_title' => true,
        'created_at' => true,
        'updated_at' => true,
        'min_amount' => true,
        'chef' => true,
    ];
}
