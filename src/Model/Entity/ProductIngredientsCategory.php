<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductIngredientsCategory Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $product_id
 * @property int $product_category_id
 * @property string $ingredients_category_name
 * @property string|null $ingredients_category_detail
 * @property int|null $is_allergens
 * @property int|null $allergen_id
 * @property int|null $more_chosen
 * @property int $created_at
 * @property string $choose_range
 * @property int $start_range
 * @property int $end_range
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\MenuProduct $menu_product
 * @property \App\Model\Entity\Ingredient[] $ingredients
 * @property \App\Model\Entity\ProductIngredient[] $product_ingredients
 */
class ProductIngredientsCategory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'product_id' => true,
        'product_category_id' => true,
        'ingredients_category_name' => true,
        'ingredients_category_detail' => true,
        'is_allergens' => true,
        'allergen_id' => true,
        'more_chosen' => true,
        'created_at' => true,
        'choose_range' => true,
        'start_range' => true,
        'end_range' => true,
        'is_required' => true,
        'user' => true,
        'menu_product' => true,
        'ingredients' => true,
        'product_ingredients' => true,
    ];
}
