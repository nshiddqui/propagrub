<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Community Entity
 *
 * @property int $id
 * @property string $community_name
 * @property string $community_image
 * @property int $is_verified
 * @property string $created_by
 * @property \Cake\I18n\FrozenTime $created_at
 *
 * @property \App\Model\Entity\CommunityMember[] $community_member
 * @property \App\Model\Entity\CommunityPostComment[] $community_post_comment
 * @property \App\Model\Entity\CommunityPost[] $community_posts
 */
class Community extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'community_name' => true,
        'community_image' => true,
        'is_verified' => true,
        'created_by' => true,
        'created_at' => true,
        'community_member' => true,
        'community_post_comment' => true,
        'community_posts' => true,
    ];
}
