<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocType Entity
 *
 * @property int $id
 * @property string $doc_type
 * @property string $user_role
 * @property int $is_required
 * @property \Cake\I18n\FrozenTime $created_at
 */
class DocType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'doc_type' => true,
        'user_role' => true,
        'is_required' => true,
        'created_at' => true,
    ];
}
