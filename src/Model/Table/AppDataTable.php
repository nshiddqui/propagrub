<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AppData Model
 *
 * @method \App\Model\Entity\AppData get($primaryKey, $options = [])
 * @method \App\Model\Entity\AppData newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AppData[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AppData|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppData saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppData patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AppData[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AppData findOrCreate($search, callable $callback = null, $options = [])
 */
class AppDataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('app_data');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('terms_of_service')
            ->maxLength('terms_of_service', 255)
            ->requirePresence('terms_of_service', 'create')
            ->notEmptyString('terms_of_service');

        $validator
            ->scalar('privacy_policy')
            ->maxLength('privacy_policy', 255)
            ->requirePresence('privacy_policy', 'create')
            ->notEmptyString('privacy_policy');

        $validator
            ->scalar('app_icon_url')
            ->maxLength('app_icon_url', 255)
            ->requirePresence('app_icon_url', 'create')
            ->notEmptyString('app_icon_url');

        return $validator;
    }
}
