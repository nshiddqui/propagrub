<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class DropDownTable extends Table {

    public $notification_sender = [
        'all' => 'All',
        'chef' => 'Chef',
        'customer' => 'Customer',
        'driver' => 'Driver',
        'custom' => 'Custom'
    ];
    public $estimate_time = [
        '15' => '15 Minutes',
        '30' => '30 Minutes',
        '45' => '45 Minutes',
        '60' => '1 Hours',
        '75' => '1 Hours 15 Minutes',
        '90' => '1 Hours 30 Minutes',
        '105' => '1 Hours 45 Minutes',
        '120' => '2 Hours'
    ];
    public $geofence_radius = [
        '1' => '1 Mile',
        '2' => '2 Miles',
        '5' => '5 Miles',
        '10' => '10 Miles',
        '15' => '15 Miles',
        '20' => '20 Miles'
    ];
    public $order_status = [
        '1' => 'Pending',
        '2' => 'Approved',
        '3' => 'Processing',
        '4' => 'Ready To Pick',
        '5' => 'On Going',
        '6' => 'Completed',
        '7' => 'Rejected',
        '8' => 'Cancelled'
    ];
    public $order_type = [
        '1' => 'Pickup',
        '2' => 'Delivery'
    ];
    public $offer_type = [
        'amount' => 'Amount',
        'percentage' => 'Percentage'
    ];
    public $subscription_amount = [
        '1' => 'Weekly Subscription (£ 25)',
        '2' => 'Monthly Subscription (£ 85)',
        '3' => 'Yearly Subscription (£ 950)'
    ];
    public $subscription_details = [
        '1' => array(
            'name' => 'Weekly Subscription',
            'price' => 25,
            'interval' => 'week'
        ),
        '2' => array(
            'name' => 'Monthly Subscription',
            'price' => 85,
            'interval' => 'month'
        ),
        '3' => array(
            'name' => 'Yearly Subscription',
            'price' => 950,
            'interval' => 'year'
        )
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable(false);
    }

    public function getEstimateTime($key = null) {
        if (is_null($key)) {
            return $this->estimate_time;
        } else {
            return $this->estimate_time[$key];
        }
    }

    public function addEstimateTime($key, $value) {
        $this->estimate_time[$key] = $value;
    }

    public function removeEstimateTime($value) {
        if ($key = array_search($value, $this->estimate_time)) {
            unset($this->estimate_time[$key]);
        }
    }

    public function getGeofenceRadius($key = null) {
        if (is_null($key)) {
            return $this->geofence_radius;
        } else {
            return $this->geofence_radius[$key];
        }
    }

    public function addGeofenceRadius($key, $value) {
        $this->geofence_radius[$key] = $value;
    }

    public function removeGeofenceRadius($value) {
        if ($key = array_search($value, $this->geofence_radius)) {
            unset($this->geofence_radius[$key]);
        }
    }

    public function getOrderStatus($key = null) {
        if (is_null($key)) {
            return $this->order_status;
        } else {
            return $this->order_status[$key];
        }
    }

    public function addOrderStatus($key, $value) {
        $this->order_status[$key] = $value;
    }

    public function removeOrderStatus($value) {
        if ($key = array_search($value, $this->order_status)) {
            unset($this->order_status[$key]);
        }
    }

    public function getOrderType($key = null) {
        if (is_null($key)) {
            return $this->order_type;
        } else {
            return $this->order_type[$key];
        }
    }

    public function addOrderType($key, $value) {
        $this->order_type[$key] = $value;
    }

    public function removeOrderType($value) {
        if ($key = array_search($value, $this->order_type)) {
            unset($this->order_type[$key]);
        }
    }

    public function getNotificationSender($key = null) {
        if (is_null($key)) {
            return $this->notification_sender;
        } else {
            return $this->notification_sender[$key];
        }
    }

    public function addNotificationSender($key, $value) {
        $this->notification_sender[$key] = $value;
    }

    public function removeNotificationSender($value) {
        if ($key = array_search($value, $this->notification_sender)) {
            unset($this->notification_sender[$key]);
        }
    }

    public function getOfferType($key = null) {
        if (is_null($key)) {
            return $this->offer_type;
        } else {
            return $this->offer_type[$key];
        }
    }

    public function addOfferType($key, $value) {
        $this->offer_type[$key] = $value;
    }

    public function removeOfferType($value) {
        if ($key = array_search($value, $this->offer_type)) {
            unset($this->offer_type[$key]);
        }
    }

    public function getSubscriptionAmount($key = null) {
        if (is_null($key)) {
            return $this->subscription_amount;
        } else {
            return $this->subscription_amount[$key];
        }
    }

    public function addSubscriptionAmount($key, $value) {
        $this->subscription_amount[$key] = $value;
    }

    public function removeSubscriptionAmount($value) {
        if ($key = array_search($value, $this->subscription_amount)) {
            unset($this->subscription_amount[$key]);
        }
    }

    public function getSubscriptionDetails($key = null) {
        if (is_null($key)) {
            return $this->subscription_details;
        } else {
            return $this->subscription_details[$key];
        }
    }

    public function addSubscriptionDetails($key, $value) {
        $this->subscription_details[$key] = $value;
    }

    public function removeSubscriptionDetails($value) {
        if ($key = array_search($value, $this->subscription_details)) {
            unset($this->subscription_details[$key]);
        }
    }

}
