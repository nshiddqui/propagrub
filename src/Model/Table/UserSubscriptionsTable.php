<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserSubscriptions Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\StripeSubscriptionsTable&\Cake\ORM\Association\BelongsTo $StripeSubscriptions
 * @property \App\Model\Table\StripeCustomersTable&\Cake\ORM\Association\BelongsTo $StripeCustomers
 * @property \App\Model\Table\StripePlansTable&\Cake\ORM\Association\BelongsTo $StripePlans
 *
 * @method \App\Model\Entity\UserSubscription get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserSubscription newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserSubscription[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscription|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSubscription saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSubscription patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscription[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscription findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserSubscriptionsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_subscriptions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('payment_method')
                ->notEmptyString('payment_method');

        $validator
                ->numeric('plan_amount')
                ->requirePresence('plan_amount', 'create')
                ->notEmptyString('plan_amount');

        $validator
                ->scalar('plan_amount_currency')
                ->maxLength('plan_amount_currency', 10)
                ->requirePresence('plan_amount_currency', 'create')
                ->notEmptyString('plan_amount_currency');

        $validator
                ->scalar('plan_interval')
                ->maxLength('plan_interval', 10)
                ->requirePresence('plan_interval', 'create')
                ->notEmptyString('plan_interval');

        $validator
                ->requirePresence('plan_interval_count', 'create')
                ->notEmptyString('plan_interval_count');

        $validator
                ->scalar('payer_email')
                ->maxLength('payer_email', 50)
                ->requirePresence('payer_email', 'create')
                ->notEmptyString('payer_email');

        $validator
                ->dateTime('plan_period_start')
                ->requirePresence('plan_period_start', 'create')
                ->notEmptyDateTime('plan_period_start');

        $validator
                ->dateTime('plan_period_end')
                ->requirePresence('plan_period_end', 'create')
                ->notEmptyDateTime('plan_period_end');

        $validator
                ->scalar('status')
                ->maxLength('status', 50)
                ->requirePresence('status', 'create')
                ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

}
