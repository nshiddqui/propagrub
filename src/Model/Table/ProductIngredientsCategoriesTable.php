<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductIngredientsCategories Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property &\Cake\ORM\Association\BelongsTo $Products
 * @property &\Cake\ORM\Association\BelongsTo $ProductCategories
 * @property &\Cake\ORM\Association\BelongsTo $Allergens
 *
 * @method \App\Model\Entity\ProductIngredientsCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProductIngredientsCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProductIngredientsCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProductIngredientsCategory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductIngredientsCategory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductIngredientsCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProductIngredientsCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProductIngredientsCategory findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductIngredientsCategoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('product_ingredients_categories');
        $this->setDisplayField('ingredients_category_name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('MenuCategories', [
            'foreignKey' => 'product_category_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Ingredients', [
            'foreignKey' => 'category_id',
        ]);
        $this->hasOne('Ingredient', [
            'className' => 'Ingredients',
            'foreignKey' => 'category_id',
        ]);
        $this->hasMany('ProductIngredients', [
            'foreignKey' => 'ingredients_category_id'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always'
                ]
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('ingredients_category_name')
            ->maxLength('ingredients_category_name', 255)
            ->requirePresence('ingredients_category_name', 'create')
            ->notEmptyString('ingredients_category_name');

        $validator
            ->scalar('ingredients_category_detail')
            ->allowEmptyString('ingredients_category_detail');

        $validator
            ->integer('is_allergens')
            ->allowEmptyString('is_allergens');

        $validator
            ->integer('more_chosen')
            ->allowEmptyString('more_chosen');

        $validator
            ->integer('created_at')
            ->notEmptyString('created_at');

        $validator
            ->scalar('choose_range')
            ->maxLength('choose_range', 20)
            ->allowEmptyString('choose_range');

        $validator
            ->integer('start_range')
            ->allowEmptyString('start_range');

        $validator
            ->integer('end_range')
            ->allowEmptyString('end_range');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['product_category_id'], 'MenuCategories'));

        return $rules;
    }
}
