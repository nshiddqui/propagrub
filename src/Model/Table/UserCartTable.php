<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserCart Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Orders
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property &\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\MenusTable&\Cake\ORM\Association\BelongsTo $Menus
 * @property &\Cake\ORM\Association\BelongsTo $Chefs
 *
 * @method \App\Model\Entity\UserCart get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserCart newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserCart[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserCart|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserCart saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserCart patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserCart[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserCart findOrCreate($search, callable $callback = null, $options = [])
 */
class UserCartTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_cart');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('MenuProducts', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Menus', [
            'foreignKey' => 'menu_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'chef_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('ingredients_ids')
                ->maxLength('ingredients_ids', 255)
                ->requirePresence('ingredients_ids', 'create')
                ->notEmptyString('ingredients_ids');

        $validator
                ->integer('quantity')
                ->requirePresence('quantity', 'create')
                ->notEmptyString('quantity');

        $validator
                ->scalar('notes')
                ->maxLength('notes', 255)
                ->requirePresence('notes', 'create')
                ->notEmptyString('notes');

        $validator
                ->dateTime('created_at')
                ->notEmptyDateTime('created_at');

        $validator
                ->dateTime('modified_at')
                ->requirePresence('modified_at', 'create')
                ->notEmptyDateTime('modified_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['product_id'], 'MenuProducts'));
        $rules->add($rules->existsIn(['menu_id'], 'Menus'));
        $rules->add($rules->existsIn(['chef_id'], 'Users'));

        return $rules;
    }

}
