<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserChefOrders Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Chefs
 * @property &\Cake\ORM\Association\BelongsTo $Transections
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Customers
 * @property &\Cake\ORM\Association\BelongsTo $Addresses
 * @property &\Cake\ORM\Association\BelongsTo $Cards
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Drivers
 *
 * @method \App\Model\Entity\UserChefOrder get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserChefOrder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserChefOrder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserChefOrder|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserChefOrder saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserChefOrder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserChefOrder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserChefOrder findOrCreate($search, callable $callback = null, $options = [])
 */
class UserChefOrdersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_chef_orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Chefs', [
            'className' => 'Users',
            'foreignKey' => 'chef_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'className' => 'Users',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('UserAddress', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('UserCards', [
            'foreignKey' => 'card_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Drivers', [
            'className' => 'Users',
            'foreignKey' => 'driver_id',
            'joinType' => 'LEFT',
        ]);
        $this->hasMany('UserCart', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('order_no')
                ->maxLength('order_no', 255)
                ->requirePresence('order_no', 'create')
                ->notEmptyString('order_no');

        $validator
                ->requirePresence('order_type', 'create')
                ->notEmptyString('order_type');

        $validator
                ->scalar('total_cost')
                ->maxLength('total_cost', 255)
                ->allowEmptyString('total_cost');

        $validator
                ->scalar('service_fee')
                ->maxLength('service_fee', 255)
                ->requirePresence('service_fee', 'create')
                ->notEmptyString('service_fee');

        $validator
                ->scalar('delivery_charges')
                ->maxLength('delivery_charges', 255)
                ->requirePresence('delivery_charges', 'create')
                ->notEmptyString('delivery_charges');

        $validator
                ->scalar('promo_code')
                ->maxLength('promo_code', 255)
                ->requirePresence('promo_code', 'create')
                ->notEmptyString('promo_code');

        $validator
                ->scalar('discount')
                ->maxLength('discount', 255)
                ->requirePresence('discount', 'create')
                ->notEmptyString('discount');

        $validator
                ->integer('status')
                ->requirePresence('status', 'create')
                ->notEmptyString('status');

        $validator
                ->dateTime('deliver_date')
                ->requirePresence('deliver_date', 'create')
                ->notEmptyDateTime('deliver_date');

        $validator
                ->integer('rating')
                ->allowEmptyString('rating');

        $validator
                ->scalar('review')
                ->maxLength('review', 255)
                ->requirePresence('review', 'create')
                ->notEmptyString('review');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['chef_id'], 'Chefs'));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        $rules->add($rules->existsIn(['address_id'], 'UserAddress'));
        $rules->add($rules->existsIn(['card_id'], 'UserCards'));
        $rules->add($rules->existsIn(['driver_id'], 'Drivers'));

        return $rules;
    }

}
