<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChefCuisines Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ChefCuisine get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChefCuisine newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChefCuisine[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChefCuisine|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefCuisine saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefCuisine patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChefCuisine[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChefCuisine findOrCreate($search, callable $callback = null, $options = [])
 */
class ChefCuisinesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('chef_cuisines');
        $this->setDisplayField('cuisine_name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('cuisine_name')
                ->maxLength('cuisine_name', 255)
                ->requirePresence('cuisine_name', 'create')
                ->notEmptyString('cuisine_name');

        $validator
                ->dateTime('cuisine_image')
                ->maxLength('cuisine_image', 255)
                ->requirePresence('cuisine_image', 'create')
                ->notEmptyString('cuisine_image');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        return $rules;
    }

}
