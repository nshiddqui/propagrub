<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserCards Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserCard get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserCard newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserCard[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserCard|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserCard saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserCard patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserCard[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserCard findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserCardsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_cards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('card_holder_name')
            ->maxLength('card_holder_name', 255)
            ->requirePresence('card_holder_name', 'create')
            ->notEmptyString('card_holder_name');

        $validator
            ->scalar('card_expiry_month')
            ->maxLength('card_expiry_month', 255)
            ->requirePresence('card_expiry_month', 'create')
            ->notEmptyString('card_expiry_month');

        $validator
            ->scalar('card_expiry_year')
            ->maxLength('card_expiry_year', 255)
            ->requirePresence('card_expiry_year', 'create')
            ->notEmptyString('card_expiry_year');

        $validator
            ->scalar('card_number')
            ->maxLength('card_number', 255)
            ->requirePresence('card_number', 'create')
            ->notEmptyString('card_number');

        $validator
            ->scalar('card_cvv')
            ->maxLength('card_cvv', 255)
            ->requirePresence('card_cvv', 'create')
            ->notEmptyString('card_cvv');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
