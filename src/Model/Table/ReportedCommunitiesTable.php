<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReportedCommunities Model
 *
 * @property \App\Model\Table\CommunitiesTable&\Cake\ORM\Association\BelongsTo $Communities
 *
 * @method \App\Model\Entity\ReportedCommunity get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportedCommunity newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportedCommunity[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportedCommunity|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportedCommunity saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportedCommunity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportedCommunity[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportedCommunity findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportedCommunitiesTable extends Table {
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('reported_communities');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Communities', [
            'foreignKey' => 'community_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'reported_by',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('reported_by')
            ->maxLength('reported_by', 255)
            ->requirePresence('reported_by', 'create')
            ->notEmptyString('reported_by');

        $validator
            ->scalar('reported_reason')
            ->requirePresence('reported_reason', 'create')
            ->notEmptyString('reported_reason');

        $validator
            ->scalar('comments')
            ->requirePresence('comments', 'create')
            ->notEmptyString('comments');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['community_id'], 'Communities'));

        return $rules;
    }
}
