<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChefErrors Model
 *
 * @method \App\Model\Entity\ChefError get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChefError newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChefError[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChefError|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefError saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefError patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChefError[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChefError findOrCreate($search, callable $callback = null, $options = [])
 */
class ChefErrorsTable extends Table {
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('chef_errors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');


        $this->belongsTo('ReportedByUser', [
            'className' => 'Users',
            'foreignKey' => 'reported_by',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ReportedToUser', [
            'className' => 'Users',
            'foreignKey' => 'reported_to',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('reported_to')
            ->maxLength('reported_to', 255)
            ->requirePresence('reported_to', 'create')
            ->notEmptyString('reported_to');

        $validator
            ->scalar('reported_by')
            ->maxLength('reported_by', 255)
            ->requirePresence('reported_by', 'create')
            ->notEmptyString('reported_by');

        $validator
            ->scalar('reported_options')
            ->maxLength('reported_options', 255)
            ->requirePresence('reported_options', 'create')
            ->notEmptyString('reported_options');

        $validator
            ->scalar('reported_detail')
            ->maxLength('reported_detail', 3000)
            ->requirePresence('reported_detail', 'create')
            ->notEmptyString('reported_detail');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->dateTime('modified_at')
            ->notEmptyDateTime('modified_at');

        return $validator;
    }
}
