<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\SocialLoginsTable&\Cake\ORM\Association\BelongsTo $SocialLogins
 * @property \App\Model\Table\BankDetailsTable&\Cake\ORM\Association\HasMany $BankDetails
 * @property \App\Model\Table\ChefCuisinesTable&\Cake\ORM\Association\HasMany $ChefCuisines
 * @property \App\Model\Table\ChefDetailsTable&\Cake\ORM\Association\HasMany $ChefDetails
 * @property \App\Model\Table\ChefTimingsTable&\Cake\ORM\Association\HasMany $ChefTimings
 * @property \App\Model\Table\DriverVehiclesTable&\Cake\ORM\Association\HasMany $DriverVehicles
 * @property \App\Model\Table\MenuCategoriesTable&\Cake\ORM\Association\HasMany $MenuCategories
 * @property \App\Model\Table\MenuProductsTable&\Cake\ORM\Association\HasMany $MenuProducts
 * @property \App\Model\Table\MenusTable&\Cake\ORM\Association\HasMany $Menus
 * @property \App\Model\Table\ProductIngredientsTable&\Cake\ORM\Association\HasMany $ProductIngredients
 * @property \App\Model\Table\UserAddressTable&\Cake\ORM\Association\HasMany $UserAddress
 * @property \App\Model\Table\UserCardsTable&\Cake\ORM\Association\HasMany $UserCards
 * @property \App\Model\Table\UsersDocumentTable&\Cake\ORM\Association\HasMany $UsersDocument
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('full_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');


        $this->belongsTo('UserRole', [
            'foreignKey' => 'role',
        ]);
        $this->belongsTo('PromoCodes', [
            'foreignKey' => 'promo_code_id',
        ]);

        $this->hasMany('BankDetails', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('ChefCuisines', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('ChefDetails', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('ChefTimings', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('DriverVehicles', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('MenuCategories', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('MenuProducts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Menus', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('ProductIngredients', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('UserAddress', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('UserCards', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('UsersDocument', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('UserSubscriptions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('BankDetails', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('role')
            ->requirePresence('role', 'create')
            ->notEmptyString('role');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->email('email')
            ->add('email', 'custom', [
                'rule' => function ($value, $context) {
                    $condition = [
                        'email' => $value
                    ];
                    if (isset($context['data']['id'])) {
                        $condition['role'] = $this->get($context['data']['id'])->role;
                        $condition['id !='] = $context['data']['id'];
                    } else {
                        $condition['role'] = $context['data']['role'];
                    }
                    return $this->exists($condition) ? 'Email should be unique' : true;
                },
                'message' => 'Email should be unique'
            ])
            ->allowEmptyString('email');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 20)
            ->add('mobile', 'custom', [
                'rule' => function ($value, $context) {
                    $condition = [
                        'mobile' => $value
                    ];
                    if (isset($context['data']['id'])) {
                        $condition['role'] = $this->get($context['data']['id'])->role;
                        $condition['id !='] = $context['data']['id'];
                    } else {
                        $condition['role'] = $context['data']['role'];
                    }
                    return $this->exists($condition) ? 'Mobile number should be unique' : true;
                },
                'message' => 'Mobile number should be unique'
            ])
            ->allowEmptyString('mobile');
        $validator
            ->integer('gender')
            ->notEmptyString('gender');
        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->add('username', 'custom', [
                'rule' => function ($value, $context) {
                    return preg_match('/^[a-z\d_.]{4,20}$/i', $value) ? true : 'Invalide Username';
                },
                'message' => 'Invalide Username'
            ])
            ->notEmptyString('username');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->allowEmptyFile('image');

        $validator
            ->scalar('dob')
            ->maxLength('dob', 255)
            ->allowEmptyString('dob');

        $validator
            ->dateTime('last_login')
            ->allowEmptyDateTime('last_login');

        $validator
            ->scalar('push_token')
            ->maxLength('push_token', 255)
            ->allowEmptyString('push_token');

        $validator
            ->scalar('auth_token')
            ->maxLength('auth_token', 255)
            ->allowEmptyString('auth_token');

        $validator
            ->boolean('is_verified')
            ->notEmptyString('is_verified');

        $validator
            ->boolean('is_complete')
            ->notEmptyString('is_complete');

        $validator
            ->boolean('status')
            ->notEmptyString('status');

        $validator
            ->integer('login_type')
            ->allowEmptyString('login_type');

        $validator
            ->scalar('latitude')
            ->maxLength('latitude', 255)
            ->allowEmptyString('latitude');

        $validator
            ->scalar('longitude')
            ->maxLength('longitude', 255)
            ->allowEmptyString('longitude');

        $validator
            ->scalar('device_type')
            ->maxLength('device_type', 255)
            ->allowEmptyString('device_type');

        $validator
            ->date('subscription_expired')
            ->allowEmptyDate('subscription_expired');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['role'], 'UserRole'));
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }

    public function find($type = 'all', $options = []) {
        $options['conditions'][$this->getAlias() . '.is_deleted'] = false;
        return parent::find($type, $options);
    }
}
