<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChefTimings Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property &\Cake\ORM\Association\BelongsTo $Weeks
 *
 * @method \App\Model\Entity\ChefTiming get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChefTiming newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChefTiming[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChefTiming|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefTiming saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefTiming patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChefTiming[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChefTiming findOrCreate($search, callable $callback = null, $options = [])
 */
class ChefTimingsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('chef_timings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Weeks', [
            'foreignKey' => 'week_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('cuisine_name')
                ->maxLength('cuisine_name', 255)
                ->allowEmptyString('cuisine_name');

        $validator
                ->scalar('start_time')
                ->maxLength('start_time', 255)
                ->requirePresence('start_time', 'create')
                ->allowEmptyString('start_time');

        $validator
                ->scalar('end_time')
                ->maxLength('end_time', 255)
                ->requirePresence('end_time', 'create')
                ->allowEmptyString('end_time');

        $validator
                ->dateTime('created_at')
                ->allowEmptyString('created_at');

        $validator
                ->dateTime('modified_at')
                ->allowEmptyString('modified_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['week_id'], 'Weeks'));

        return $rules;
    }

}
