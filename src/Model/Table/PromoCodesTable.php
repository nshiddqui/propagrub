<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PromoCodes Model
 *
 * @property \App\Model\Table\RolesTable&\Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\PromoCode get($primaryKey, $options = [])
 * @method \App\Model\Entity\PromoCode newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PromoCode[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PromoCode|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PromoCode saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PromoCode patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PromoCode[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PromoCode findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PromoCodesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('promo_codes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserRole', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ChefPromocodes', [
            'foreignKey' => 'promo_code_id',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('promocode')
                ->maxLength('promocode', 255)
                ->requirePresence('promocode', 'create')
                ->notEmptyString('promocode')
                ->add('promocode', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->date('start_date')
                ->requirePresence('start_date', 'create')
                ->notEmptyDate('start_date');

        $validator
                ->integer('total_user')
                ->notEmptyString('total_user');

        $validator
                ->date('end_date')
                ->requirePresence('end_date', 'create')
                ->notEmptyDate('end_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['promocode']));
        $rules->add($rules->existsIn(['role_id'], 'UserRole'));

        return $rules;
    }

}
