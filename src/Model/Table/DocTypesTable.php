<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DocTypes Model
 *
 * @method \App\Model\Entity\DocType get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocType findOrCreate($search, callable $callback = null, $options = [])
 */
class DocTypesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('doc_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->belongsTo('UserRole', [
            'propertyName' => 'userRoles',
            'foreignKey' => 'user_role',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('doc_type')
                ->maxLength('doc_type', 255)
                ->requirePresence('doc_type', 'create')
                ->notEmptyString('doc_type');

        $validator
                ->scalar('user_role')
                ->maxLength('user_role', 255)
                ->requirePresence('user_role', 'create')
                ->notEmptyString('user_role');

        $validator
                ->integer('is_required')
                ->requirePresence('is_required', 'create')
                ->notEmptyString('is_required');

        $validator
                ->dateTime('created_at')
                ->notEmptyDateTime('created_at');

        return $validator;
    }

}
