<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChefOffers Model
 *
 * @property \App\Model\Table\ChefsTable&\Cake\ORM\Association\BelongsTo $Chefs
 *
 * @method \App\Model\Entity\ChefOffer get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChefOffer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChefOffer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChefOffer|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefOffer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefOffer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChefOffer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChefOffer findOrCreate($search, callable $callback = null, $options = [])
 */
class ChefOffersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('chef_offers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'chef_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('offer_value')
                ->maxLength('offer_value', 255)
                ->requirePresence('offer_value', 'create')
                ->notEmptyString('offer_value');

        $validator
                ->scalar('offer_type')
                ->maxLength('offer_type', 255)
                ->requirePresence('offer_type', 'create')
                ->notEmptyString('offer_type');

        $validator
                ->dateTime('offer_exp_date')
                ->requirePresence('offer_exp_date', 'create')
                ->notEmptyDateTime('offer_exp_date');

        $validator
                ->scalar('promo_code')
                ->maxLength('promo_code', 255)
                ->minLength('promo_code', 6)
                ->requirePresence('promo_code', 'create')
                ->notEmptyString('promo_code');

        $validator
                ->scalar('offer_title')
                ->maxLength('offer_title', 255)
                ->requirePresence('offer_title', 'create')
                ->notEmptyString('offer_title');


        $validator
                ->scalar('min_amount')
                ->maxLength('min_amount', 255)
                ->requirePresence('min_amount', 'create')
                ->notEmptyString('min_amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['chef_id'], 'Users'));

        return $rules;
    }

}
