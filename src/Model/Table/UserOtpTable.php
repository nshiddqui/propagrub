<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserOtp Model
 *
 * @method \App\Model\Entity\UserOtp get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserOtp newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserOtp[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserOtp|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserOtp saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserOtp patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserOtp[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserOtp findOrCreate($search, callable $callback = null, $options = [])
 */
class UserOtpTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_otp');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('send_to')
            ->maxLength('send_to', 255)
            ->requirePresence('send_to', 'create')
            ->notEmptyString('send_to');

        $validator
            ->integer('otp')
            ->requirePresence('otp', 'create')
            ->notEmptyString('otp');

        $validator
            ->dateTime('send_at')
            ->notEmptyDateTime('send_at');

        return $validator;
    }
}
