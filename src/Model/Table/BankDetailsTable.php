<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BankDetails Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property &\Cake\ORM\Association\BelongsTo $AddressRearDocs
 * @property &\Cake\ORM\Association\BelongsTo $AddressFrontDocs
 * @property &\Cake\ORM\Association\BelongsTo $DocFronts
 * @property &\Cake\ORM\Association\BelongsTo $DocRears
 * @property &\Cake\ORM\Association\BelongsTo $StripeAccounts
 * @property &\Cake\ORM\Association\BelongsTo $StripeBanks
 *
 * @method \App\Model\Entity\BankDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\BankDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BankDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BankDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BankDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BankDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BankDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BankDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class BankDetailsTable extends Table
{

        /**
         * Initialize method
         *
         * @param array $config The configuration for the Table.
         * @return void
         */
        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('bank_details');
                $this->setDisplayField('id');
                $this->setPrimaryKey('id');

                $this->belongsTo('Users', [
                        'foreignKey' => 'user_id',
                        'joinType' => 'INNER',
                ]);
        }

        /**
         * Default validation rules.
         *
         * @param \Cake\Validation\Validator $validator Validator instance.
         * @return \Cake\Validation\Validator
         */
        public function validationDefault(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmptyString('id', null, 'create');

                $validator
                        ->scalar('account_number')
                        ->maxLength('account_number', 255)
                        ->requirePresence('account_number', 'create')
                        ->notEmptyString('account_number');

                // $validator
                //         ->scalar('holder_name')
                //         ->maxLength('holder_name', 255)
                //         ->requirePresence('holder_name', 'create')
                //         ->notEmptyString('holder_name');

                // $validator
                //         ->scalar('payment_email')
                //         ->maxLength('payment_email', 255)
                //         ->requirePresence('payment_email', 'create')
                //         ->notEmptyString('payment_email');

                // $validator
                //         ->scalar('routing_number')
                //         ->maxLength('routing_number', 255)
                //         ->requirePresence('routing_number', 'create')
                //         ->notEmptyString('routing_number');

                // $validator
                //         ->scalar('phone')
                //         ->maxLength('phone', 255)
                //         ->requirePresence('phone', 'create')
                //         ->notEmptyString('phone');


                // $validator
                //         ->scalar('dob')
                //         ->maxLength('dob', 255)
                //         ->requirePresence('dob', 'create')
                //         ->notEmptyString('dob');

                // $validator
                //         ->scalar('address')
                //         ->maxLength('address', 255)
                //         ->requirePresence('address', 'create')
                //         ->notEmptyString('address');


                // $validator
                //         ->scalar('city')
                //         ->maxLength('city', 255)
                //         ->requirePresence('city', 'create')
                //         ->notEmptyString('city');

                // $validator
                //         ->scalar('post_code')
                //         ->maxLength('post_code', 255)
                //         ->requirePresence('post_code', 'create')
                //         ->notEmptyString('post_code');


                return $validator;
        }

        /**
         * Returns a rules checker object that will be used for validating
         * application integrity.
         *
         * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
         * @return \Cake\ORM\RulesChecker
         */
        public function buildRules(RulesChecker $rules)
        {
                $rules->add($rules->existsIn(['user_id'], 'Users'));

                return $rules;
        }
}
