<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserReviews Model
 *
 * @method \App\Model\Entity\UserReview get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserReview newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserReview[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserReview|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserReview saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserReview patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserReview[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserReview findOrCreate($search, callable $callback = null, $options = [])
 */
class UserReviewsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_reviews');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('UserChefOrders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('rating_to')
            ->maxLength('rating_to', 255)
            ->requirePresence('rating_to', 'create')
            ->notEmptyString('rating_to');

        $validator
            ->scalar('rating_by')
            ->maxLength('rating_by', 255)
            ->requirePresence('rating_by', 'create')
            ->notEmptyString('rating_by');

        $validator
            ->numeric('rating')
            ->notEmptyString('rating');

        $validator
            ->scalar('review')
            ->maxLength('review', 3000)
            ->requirePresence('review', 'create')
            ->notEmptyString('review');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->dateTime('modified_at')
            ->notEmptyDateTime('modified_at');

        return $validator;
    }
}
