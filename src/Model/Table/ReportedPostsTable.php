<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReportedPosts Model
 *
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\BelongsTo $Posts
 *
 * @method \App\Model\Entity\ReportedPost get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportedPost newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportedPost[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportedPost|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportedPost saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportedPost patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportedPost[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportedPost findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportedPostsTable extends Table {
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('reported_posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('CommunityPosts', [
            'foreignKey' => 'post_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ReportedByUser', [
            'className' => 'Users',
            'foreignKey' => 'reported_by',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('reported_by')
            ->maxLength('reported_by', 255)
            ->requirePresence('reported_by', 'create')
            ->notEmptyString('reported_by');

        $validator
            ->scalar('reported_reason')
            ->requirePresence('reported_reason', 'create')
            ->notEmptyString('reported_reason');

        $validator
            ->scalar('comments')
            ->requirePresence('comments', 'create')
            ->notEmptyString('comments');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['post_id'], 'Posts'));

        return $rules;
    }
}
