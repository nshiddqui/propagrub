<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChefDetails Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ChefDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChefDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChefDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChefDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChefDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChefDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class ChefDetailsTable extends Table
{

        /**
         * Initialize method
         *
         * @param array $config The configuration for the Table.
         * @return void
         */
        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('chef_details');
                $this->setDisplayField('id');
                $this->setPrimaryKey('id');

                $this->belongsTo('Users', [
                        'foreignKey' => 'user_id',
                        'joinType' => 'INNER',
                ]);
                $this->belongsTo('ChefServices', [
                        'foreignKey' => 'service_type',
                        'joinType' => 'INNER',
                ]);

                $this->belongsTo('ChefCuisines', [
                        'foreignKey' => 'chef_cuisines_ids',
                        'joinType' => 'LEFT',
                ]);

                $this->addBehavior('Timestamp', [
                        'events' => [
                                'Model.beforeSave' => [
                                        'created_at' => 'new',
                                        'modified_at' => 'always'
                                ]
                        ]
                ]);
        }

        /**
         * Default validation rules.
         *
         * @param \Cake\Validation\Validator $validator Validator instance.
         * @return \Cake\Validation\Validator
         */
        public function validationDefault(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmptyString('id', null, 'create');

                $validator
                        ->scalar('resturant_name')
                        ->maxLength('resturant_name', 255)
                        ->requirePresence('resturant_name', 'create')
                        ->notEmptyString('resturant_name');



                $validator
                        ->scalar('chef_cuisines_ids')
                        ->allowEmptyString('chef_cuisines_ids');


                $validator
                        ->scalar('address')
                        ->maxLength('address', 255)
                        ->requirePresence('address', 'create')
                        ->notEmptyString('address');

                $validator
                        ->scalar('image')
                        ->maxLength('image', 255)
                        ->notEmptyFile('image');

                $validator
                        ->integer('service_type')
                        ->requirePresence('service_type', 'create')
                        ->notEmptyString('service_type');

                $validator
                        ->integer('status')
                        ->notEmptyString('status');

                return $validator;
        }

        /**
         * Returns a rules checker object that will be used for validating
         * application integrity.
         *
         * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
         * @return \Cake\ORM\RulesChecker
         */
        public function buildRules(RulesChecker $rules)
        {
                $rules->add($rules->existsIn(['user_id'], 'Users'));

                return $rules;
        }
}
