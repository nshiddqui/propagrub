<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserNotifications Model
 *
 * @property \App\Model\Table\ToUsersTable&\Cake\ORM\Association\BelongsTo $ToUsers
 * @property \App\Model\Table\FromUsersTable&\Cake\ORM\Association\BelongsTo $FromUsers
 *
 * @method \App\Model\Entity\UserNotification get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserNotification newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserNotification[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserNotification|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserNotification saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserNotification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserNotification[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserNotification findOrCreate($search, callable $callback = null, $options = [])
 */
class UserNotificationsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_notifications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'to_user_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('FromUsers', [
            'foreignKey' => 'from_user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->integer('notification_type')
                ->requirePresence('notification_type', 'create')
                ->notEmptyString('notification_type');

        $validator
                ->scalar('notification_title')
                ->maxLength('notification_title', 255)
                ->requirePresence('notification_title', 'create')
                ->notEmptyString('notification_title');

        $validator
                ->scalar('notification_text')
                ->maxLength('notification_text', 255)
                ->requirePresence('notification_text', 'create')
                ->notEmptyString('notification_text');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {

        return $rules;
    }

}
