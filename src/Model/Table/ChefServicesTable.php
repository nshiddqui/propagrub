<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChefServices Model
 *
 * @method \App\Model\Entity\ChefService get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChefService newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChefService[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChefService|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefService saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChefService patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChefService[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChefService findOrCreate($search, callable $callback = null, $options = [])
 */
class ChefServicesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('chef_services');
        $this->setDisplayField('service_name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('service_name')
            ->maxLength('service_name', 255)
            ->requirePresence('service_name', 'create')
            ->notEmptyString('service_name');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->scalar('modified_at')
            ->maxLength('modified_at', 255)
            ->requirePresence('modified_at', 'create')
            ->notEmptyString('modified_at');

        return $validator;
    }
}
