<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Communities Model
 *
 * @property \App\Model\Table\CommunityMemberTable&\Cake\ORM\Association\HasMany $CommunityMember
 * @property \App\Model\Table\CommunityPostCommentTable&\Cake\ORM\Association\HasMany $CommunityPostComment
 * @property \App\Model\Table\CommunityPostsTable&\Cake\ORM\Association\HasMany $CommunityPosts
 *
 * @method \App\Model\Entity\Community get($primaryKey, $options = [])
 * @method \App\Model\Entity\Community newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Community[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Community|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Community saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Community patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Community[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Community findOrCreate($search, callable $callback = null, $options = [])
 */
class CommunitiesTable extends Table {
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('communities');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('CommunityMember', [
            'foreignKey' => 'community_id',
        ]);
        $this->hasMany('CommunityPostComment', [
            'foreignKey' => 'community_id',
        ]);
        $this->hasMany('CommunityPosts', [
            'foreignKey' => 'community_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'created_by',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('community_name')
            ->maxLength('community_name', 255)
            ->requirePresence('community_name', 'create')
            ->notEmptyString('community_name');

        $validator
            ->scalar('community_image')
            ->maxLength('community_image', 255)
            ->requirePresence('community_image', 'create')
            ->notEmptyFile('community_image');

        $validator
            ->integer('is_verified')
            ->notEmptyString('is_verified');

        $validator
            ->scalar('created_by')
            ->maxLength('created_by', 255)
            ->requirePresence('created_by', 'create')
            ->notEmptyString('created_by');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        return $validator;
    }
}
