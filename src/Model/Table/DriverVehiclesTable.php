<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DriverVehicles Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DriverVehicle get($primaryKey, $options = [])
 * @method \App\Model\Entity\DriverVehicle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DriverVehicle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DriverVehicle|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DriverVehicle saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DriverVehicle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DriverVehicle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DriverVehicle findOrCreate($search, callable $callback = null, $options = [])
 */
class DriverVehiclesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('driver_vehicles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('vehicle_type')
            ->maxLength('vehicle_type', 255)
            ->requirePresence('vehicle_type', 'create')
            ->notEmptyString('vehicle_type');

        $validator
            ->scalar('manufecturer_name')
            ->maxLength('manufecturer_name', 255)
            ->requirePresence('manufecturer_name', 'create')
            ->notEmptyString('manufecturer_name');

        $validator
            ->scalar('model_name')
            ->maxLength('model_name', 255)
            ->requirePresence('model_name', 'create')
            ->notEmptyString('model_name');

        $validator
            ->scalar('manufacturer_year')
            ->maxLength('manufacturer_year', 255)
            ->requirePresence('manufacturer_year', 'create')
            ->notEmptyString('manufacturer_year');

        $validator
            ->scalar('vehicle_plate_number')
            ->maxLength('vehicle_plate_number', 255)
            ->requirePresence('vehicle_plate_number', 'create')
            ->notEmptyString('vehicle_plate_number');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->scalar('modified_at')
            ->maxLength('modified_at', 255)
            ->requirePresence('modified_at', 'create')
            ->notEmptyString('modified_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
