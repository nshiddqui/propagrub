<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductIngredients Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Products
 * @property &\Cake\ORM\Association\BelongsTo $IngredientsCategories
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ProductIngredient get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProductIngredient newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProductIngredient[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProductIngredient|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductIngredient saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductIngredient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProductIngredient[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProductIngredient findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductIngredientsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('product_ingredients');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('MenuProducts', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ProductIngredientsCategories', [
            'foreignKey' => 'ingredients_category_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always'
                ]
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'MenuProducts'));
        $rules->add($rules->existsIn(['ingredients_category_id'], 'ProductIngredientsCategories'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
