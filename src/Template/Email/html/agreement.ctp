<html>

<head>
    <title>Here's your signed agreement</title>
</head>

<body>
    <div style="font-family:Arial">
        <div><img src="https://propagrub.com/chef/img/propagrub_aggrement.png" height="160px" style="display:block;margin-top:20px;margin-bottom:20px;margin-left:auto;margin-right:auto;background-color:#ffffff" width="160px">
        </div>
        <div style="padding:20px 0px 20px 0px;background-color:#d51b81;font-size:28px;color:#ffffff;text-align:center;line-height:1.2em;font-weight:bold"><label>
                Here's your signed agreement</label>
        </div>
        <div style="margin:20px 20px 10px 20px;background-color:#ffffff">
            <p>Dear <?= $data->full_name ?>,</p>
            <p>Here's a copy of your signed agreement for our records. Keep it safe, in case you need to refer to it later.</p>
            <br>
            <p><strong>Partner</strong></p>
            <p><?= $data['chef_detail']->resturant_name ?></p>
            <br>
            <p><strong>Start Date</strong></p>
            <p><?= $data->created->format('D M d h:i:s Y') ?></p>
            <br>
            <p><strong>Services</strong></p>
            <p>
            </p>
            <table style="border-collapse:collapse;width:100%">
                <tbody>
                    <tr>
                        <th style="border:1px solid #ddd;padding:8px;padding-top:5px;padding-bottom:5px;font-weight:normal;text-align:left;background-color:#d51b81;color:white">Site</th>
                        <th style="border:1px solid #ddd;padding:8px;padding-top:5px;padding-bottom:5px;font-weight:normal;text-align:left;background-color:#d51b81;color:white">Operating Model</th>
                        <th style="border:1px solid #ddd;padding:8px;padding-top:5px;padding-bottom:5px;font-weight:normal;text-align:left;background-color:#d51b81;color:white">Range</th>
                        <th style="border:1px solid #ddd;padding:8px;padding-top:5px;padding-bottom:5px;font-weight:normal;text-align:left;background-color:#d51b81;color:white">Commission (exc. VAT)</th>
                    </tr>
                    <tr>
                        <td style="border:1px solid #ddd;padding:8px"><?= $data['chef_detail']->resturant_name ?>
                        </td>
                        <td style="border:1px solid #ddd;padding:8px">Basic
                        </td>
                        <td style="border:1px solid #ddd;padding:8px"><span id="m_6923596073192523301m_6337215760114050256m_8832234949149737006j_id0:emailTemplate:j_id3:j_id4:j_id7:j_id8:serviceRepeat:0:j_id23">All order values</span>
                        </td>
                        <td style="border:1px solid #ddd;padding:8px">14.00%
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid #ddd;padding:8px"><?= $data['chef_detail']->resturant_name ?>
                        </td>
                        <td style="border:1px solid #ddd;padding:8px">Pick Up
                        </td>
                        <td style="border:1px solid #ddd;padding:8px"><span id="m_6923596073192523301m_6337215760114050256m_8832234949149737006j_id0:emailTemplate:j_id3:j_id4:j_id7:j_id8:serviceRepeat:1:j_id23">All order values</span>
                        </td>
                        <td style="border:1px solid #ddd;padding:8px">12.00%
                        </td>
                    </tr>
                </tbody>
            </table>
            <p></p>
            <br>
            <p><strong>Products and Joining Fees</strong></p>
            <p>
            </p>
            <table style="border-collapse:collapse;width:100%">
                <tbody>
                    <tr>
                        <th style="border:1px solid #ddd;padding:8px;padding-top:5px;padding-bottom:5px;font-weight:normal;text-align:left;background-color:#d51b81;color:white">Site</th>
                        <th style="border:1px solid #ddd;padding:8px;padding-top:5px;padding-bottom:5px;font-weight:normal;text-align:left;background-color:#d51b81;color:white">Product</th>
                        <th style="border:1px solid #ddd;padding:8px;padding-top:5px;padding-bottom:5px;font-weight:normal;text-align:left;background-color:#d51b81;color:white">Price (excl. VAT)</th>
                    </tr>
                    <tr>
                        <td style="border:1px solid #ddd;padding:8px"><?= $data['chef_detail']->resturant_name ?>
                        </td>
                        <td style="border:1px solid #ddd;padding:8px">Tablet
                        </td>
                        <td style="border:1px solid #ddd;padding:8px">
                            GBP &nbsp;200.00
                        </td>
                    </tr>
                </tbody>
            </table>
            <p></p>
            <br>
            <p>We're delighted you've chosen to partner with Propagrub</p>
            <br>
            <p style="text-align: center;margin-bottom:30px"><strong>I have read all instructions and guidelines i here accept that</strong></p>
            <p style="text-align: center;"><?= $this->Html->link('I Agree', ['controller' => 'Users', 'action' => 'accept', $data->key], ['fullBase' => true, 'web' => true, 'style' => 'color: #fff;text-align: center;vertical-align: middle;user-select: none;border: 1px solid transparent;padding: 1.375rem 5.75rem;font-size: .875rem;line-height: 1.5;border-radius: 0.25rem;background-color: #d51b81 !important;border-color: #d51b81 !important;text-decoration: none;']) ?></p>
            <br>
            <p>All the best,</p>
            <p>Team Propagrub</p>
        </div>
    </div>
</body>

</html>