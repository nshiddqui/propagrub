<html>

<body>
    <table bgcolor="#f3f5f5" border="0" cellpadding="0" cellspacing="0" id="m_2058366628898028845m_177328286126362610m_-5494059004568167701backgroundtable" role="presentation" style="min-width:650px" width="100%">
        <tbody>
            <tr>
                <td align="center" id="m_2058366628898028845m_177328286126362610m_-5494059004568167701skeleton" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" role="presentation" style="min-width:650px;width:650px;max-width:650px" width="650">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="padding:0"><a href="https://propagrub.com/chef" style="border:none" target="_blank"><img alt="" height="183" src="https://propagrub.com/chef/img/verifier.png" style="display:block;border:none;font-family:HelveticaNeue,helvetica,arial,sans-serif;font-size:14px;line-height:1.3em;color:rgb(0,204,188);text-decoration:none;font-weight:normal;text-align:center;max-width:650px;width:650px;height:183px;max-height:183px" width="650"></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table bgcolor="#d51b81" border="0" cellpadding="0" cellspacing="0" role="presentation" style="min-width:650px;width:650px" width="650">
                                        <tbody>
                                            <tr>
                                                <td align="left" style="padding:0 40px">
                                                    <div style="font-family:HelveticaNeue,helvetica,arial,sans-serif;font-size:28px;line-height:1.2em;color:#ffffff;text-align:left;font-weight:bold;text-decoration:none;margin:24px 0 16px 0;padding:0">
                                                        <div style="text-align:center">We need to verify your identity</div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" role="presentation" style="min-width:650px;width:650px" width="650">
                                        <tbody>
                                            <tr>
                                                <td align="left" style="padding:14px 40px">
                                                    <div style="font-family:HelveticaNeue,helvetica,arial,sans-serif;font-size:16px;line-height:1.4em;color:#585c5c;text-align:left;font-weight:normal;text-decoration:none;margin:0;padding:0">
                                                        <p>Hi <?= $data->full_name ?>,<br>
                                                            <br>
                                                            Thanks for choosing Propagrub as your delivery partner.<br>
                                                            <br>
                                                            Before we can get your business up and running with us, we need to verify who you are.<br>
                                                            <br>
                                                            Please complete your information using the link below, with our verification partner Onfido. <strong><a href="https://onfido.com/company/about/" style="color:#d51b81;text-decoration:none" target="_blank">Learn more about Onfido</a></strong>.<br>
                                                            <br>
                                                            Thanks&nbsp;<br>
                                                            <br>
                                                            <strong>Restaurant Verification Team</strong><br>
                                                            &nbsp;
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" role="presentation" style="min-width:650px;width:650px" width="650">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="padding:0px 25px 32px 25px">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" width="200">
                                                        <tbody>
                                                            <tr>
                                                                <td bgcolor="#d51b81" style="border-radius:3px;padding:16px;color:#ffffff;text-align:center;font-weight:bold;text-decoration:none"><a href="https://onfido.Propagrub.com/apex/Onfido_VerificationPage?applicantId=" style="font-family:HelveticaNeue,helvetica,arial,sans-serif;font-size:16px;line-height:1.1em;color:#ffffff;text-align:center;font-weight:bold;text-decoration:none;margin:0;padding:0;display:block;word-wrap:break-word" target="_blank">Verify me</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" role="presentation" style="min-width:650px;width:650px" width="650">
                                        <tbody>
                                            <tr>
                                                <td align="left" style="padding:14px 40px">
                                                    <div style="font-family:HelveticaNeue,helvetica,arial,sans-serif;font-size:16px;line-height:1.4em;color:#585c5c;text-align:left;font-weight:normal;text-decoration:none;margin:0;padding:0">
                                                        <p>Your personal data will be processed in accordance with our <strong><a href="https://restaurants.Propagrub.com/en-gb/privacy" style="text-decoration:none;color:#d51b81" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://restaurants.Propagrub.com/en-gb/privacy&amp;source=gmail&amp;ust=1642337095282000&amp;usg=AOvVaw0ehwxTjo2Js-N5qAdqePej">Restaurant Privacy Policy</a></strong>.<br>
                                                            <br>
                                                            By proceeding with the identity verification process&nbsp;you acknowledge that you have read and consent to the terms of the <strong><a href="https://restaurants.Propagrub.com/en-gb/privacy" style="color:#d51b81;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://restaurants.Propagrub.com/en-gb/privacy&amp;source=gmail&amp;ust=1642337095282000&amp;usg=AOvVaw0ehwxTjo2Js-N5qAdqePej">Restaurant Privacy Policy</a></strong> and the information within this email, which explains how we use and protect information about you, relating to your application and partner services provided by Propagrub.
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="min-width:650px;width:650px" width="650">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="padding:0 25px 0 25px">
                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" style="padding:32px 0 32px 0">
                                                                    <p style="font-family:arial,helvetica,sans-serif;font-size:9px;line-height:12px;color:#585c5c;text-align:center;font-weight:normal;text-decoration:none;Margin:12px 0 0 0;padding:0"><a href="http:///?utm_source=Propagrub_crm&amp;utm_medium=email&amp;utm_campaign=terms" style="color:#585c5c;text-decoration:none;font-weight:normal;line-height:1.3em;border-bottom:1px solid #585c5c" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http:///?utm_source%3DPropagrub_crm%26utm_medium%3Demail%26utm_campaign%3Dterms&amp;source=gmail&amp;ust=1642337095282000&amp;usg=AOvVaw0qBhS3QmyuFWQr-PC-no72"></a></p>
                                                                    <div style="font-family:arial,helvetica,sans-serif;font-size:9px;line-height:12px;color:#585c5c;text-align:center;font-weight:normal;text-decoration:none;Margin:12px 0 0 0;padding:0">Service provided by <a href="https://onfido.com/company/about/" style="color:#oocdbc;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://onfido.com/company/about/&amp;source=gmail&amp;ust=1642337095282000&amp;usg=AOvVaw2JS1YCnMDVXjOkY-oPGute">Onfido</a></div>
                                                                    <p style="font-family:arial,helvetica,sans-serif;font-size:9px;line-height:12px;color:#585c5c;text-align:center;font-weight:normal;text-decoration:none;Margin:12px 0 0 0;padding:0">Propagrub, The River Building, <a href="https://www.google.com/maps/search/1+Cousin+Lane,+London,+EC4R+3TE?entry=gmail&amp;source=g">1 Cousin Lane, London, EC4R 3TE</a></p>
                                                                    <p style="font-family:arial,helvetica,sans-serif;font-size:9px;line-height:12px;color:#585c5c;text-align:center;font-weight:normal;text-decoration:none;Margin:12px 0 0 0;padding:0">© 2021 Propagrub</p>
                                                                    <p role="list" style="font-family:arial,helvetica,sans-serif;font-size:9px;line-height:12px;color:#585c5c;text-align:center;font-weight:normal;text-decoration:none;Margin:24px 0 0 0;padding:0"><a href="https://restaurants.Propagrub.com/en-gb/privacy" role="listitem" style="color:#585c5c;text-decoration:none;font-weight:normal;line-height:1.3em;border-bottom:1px solid #585c5c" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://restaurants.Propagrub.com/en-gb/privacy&amp;source=gmail&amp;ust=1642337095282000&amp;usg=AOvVaw0ehwxTjo2Js-N5qAdqePej">Privacy</a> <a href="http://help.Propagrub.com/en" role="listitem" style="color:#585c5c;text-decoration:none;font-weight:normal;line-height:1.3em;border-bottom:1px solid #585c5c" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://help.Propagrub.com/en&amp;source=gmail&amp;ust=1642337095282000&amp;usg=AOvVaw2AqcZhQPN-gZRAxteFXgCN">Help Centre</a></p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>