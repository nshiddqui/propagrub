<table style="height: 330px; width: 100%; border-collapse: collapse; border-style: none;" border="0">
    <tbody>
        <tr style="height: 70px;">
            <th style="width: 100; height: 70px; border-style: none; background-color: #ca2389; text-align: center;" scope="row"><img src="https://propagrub.com//images//logo1.png" alt="logo" width="200" height="47" /></th>
        </tr>
        <tr style="height: 200px;">
            <td style="width: 100; height: 200px; vertical-align: top;">
                <p>Hi <?= $user->username ?>,</p>
                <p>Welcome to PropaGrub. Your account has been created successfully.</p>
            </td>
        </tr>
        <tr style="height: 60px;">
            <td style="width: 100%; height: 60px; background-color: #ce3695; text-align: center; vertical-align: middle;">
                <p><a href="https://www.instagram.com/propagrub_com/"><img src="https://propagrub.com//images/instagram.png" alt="" width="20" height="20" /></a>.&nbsp; &nbsp; <a href="https://www.linkedin.com/company/propagrub"><img src="https://propagrub.com//images/linkedin.png" alt="" width="20" height="20" /></a>.&nbsp; &nbsp; <a href="https://twitter.com/Propagrub_com"><img src="https://propagrub.com//images/twitter.png" alt="" width="20" height="20" /></a></p>
                <p><span style="color: #ffffff;"><a style="color: #ffffff;" href="https://propagrub.com/index.php/privacy-policy/" target="_blank">Privacy Policy</a> | <a style="color: #ffffff;" href="https://propagrub.com/index.php/terms-conditions/" target="_blank">Terms &amp; Conditions</a> | </span><a href="https://propagrub.com/index.php/contact/" target="_blank"><span style="color: #ffffff;">Contact Us</span></a></p>
                <p><span style="color: #ffffff;">All Right Reserved &copy; 2021 <a href="https://propagrub.com/" target="_blank"><span style="color: #ffffff;">PropaGrub</span></a></span></p>
            </td>
        </tr>
    </tbody>
</table>