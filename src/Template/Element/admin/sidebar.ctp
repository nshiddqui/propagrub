<?= $this->Html->script('https://unpkg.com/@popperjs/core@2') ?>
<?= $this->Html->script('https://unpkg.com/tippy.js@6') ?>
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <?= $this->Html->image('user.png', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
        </div>
        <div class="pull-left info">
            <p><?= array_key_exists('name', $authUser) ? $authUser['name'] : $authUser['first_name'] . ' ' . $authUser['last_name'] ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><?= $this->Html->link('<i class="fa fa-dashboard"></i><span>Dashboard</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="View your dashboard summery."></i>', ['controller' => 'dashboard'], ['escape' => false]) ?></li>
        <?php if ($authUser['role'] === 2) { ?>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Dispatcher</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="When you receive a new order it will show here, make sure to approve the steps one by one from New to Completed."></i>', ['controller' => 'user_chef_orders', 'action' => 'dispatcher'], ['escape' => false]) ?></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i> <span>Order List</span>
                    <span class="pull-right-container">
                        <i class="fa fa-info-circle pull-right info-text"></i>
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php foreach ($order_status as $sidebar_order_id => $sidebar_order) { ?>
                        <li><?= $this->Html->link("<i class='fa fa-circle-o'></i>{$sidebar_order} Order List", ['controller' => 'user_chef_orders', 'action' => 'index', $sidebar_order_id], ['escape' => false]) ?></li>
                    <?php } ?>
                </ul>
            </li>
            <li><?= $this->Html->link('<i class="fa fa-bars"></i><span>Menus Management</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Create your menu by adding a name and photo. Upload your PDF menu. "></i>', ['controller' => 'menus'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Menu Categories Management</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Select your menu and add a category for example Lunch or Diner "></i>', ['controller' => 'menu_categories'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Menu Products Management</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Create products for example Cheeseburger and add them to your menu category. Select a price and write about / what is in your dish. "></i>', ['controller' => 'menu_products'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Products Ad-ons Management</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Add additional ingredients for customers to add as extras, for example: cheese, onion, bacon, lettuce. "></i>', ['controller' => 'product_ingredients'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Products Ad-ons Categories Management</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Set % or £ discounts for your menu"></i>', ['controller' => 'product_ingredients_categories'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Offers Management</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Update your: contact details, chef store information and documents."></i>', ['controller' => 'chef_offers'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Chef Details</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Enter your bank details to get paid!"></i>', ['controller' => 'users', 'action' => 'complete'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Bank Details</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Chose your subscription plan"></i>', ['controller' => 'users', 'action' => 'bank_details'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Subscription Management</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Upload your best images for customers to view and share! "></i>', ['controller' => 'user_subscriptions', 'action' => 'index'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Galleries</span><i class="fa fa-info-circle pull-right info-text" data-tippy-content="Having trouble? Contact the Propagrub Team for support and help!"></i>', ['controller' => 'galleries'], ['escape' => false]) ?></li>
        <?php } ?>
        <?php if ($authUser['role'] === 1) { ?>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Chef Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'users'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Customer Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'users', 'action' => 'customer'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Driver Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'users', 'action' => 'driver'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Notification Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'UserNotifications'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Promocode Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'PromoCodes'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Document Type Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'DocTypes'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Cuisines Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'ChefCuisines'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Website Users Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'WebsiteUsers'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Communities Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'Communities', 'action' => 'index'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Reported Communities</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'ReportedCommunities', 'action' => 'index'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Reported Users</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'ChefErrors', 'action' => 'index'], ['escape' => false]) ?></li>
            <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Reported Feeds</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'ReportedPosts', 'action' => 'index'], ['escape' => false]) ?></li>
            <?php if ($authUser['is_super'] === 1) { ?>
                <li><?= $this->Html->link('<i class="fa fa-users"></i><span>Admin Management</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'admins'], ['escape' => false]) ?></li>
            <?php } ?>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i> <span>Page Managements</span>
                    <span class="pull-right-container">
                        <i class="fa fa-info-circle pull-right info-text"></i>
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php foreach ($page_lists as $sidebar_page_id => $sidebar_page) { ?>
                        <li><?= $this->Html->link("<i class='fa fa-circle-o'></i>{$sidebar_page} Page", ['controller' => 'pages', 'action' => 'index', $sidebar_page_id], ['escape' => false]) ?></li>
                    <?php } ?>
                </ul>
            </li>
            <li><?= $this->Html->link('<i class="fa fa-users"></i><span>Change Profile</span><i class="fa fa-info-circle pull-right info-text"></i>', ['controller' => 'admins', 'action' => 'chnageProfile'], ['escape' => false]) ?></li>
        <?php } ?>
        <li><?= $this->Html->link('<i class="fa fa-list-alt"></i><span>Help</span><i class="fa fa-info-circle pull-right info-text"></i>', 'mailto:support@propagrub.com', ['escape' => false, 'web' => true]) ?></li>
    </ul>
</section>
<script>
    // With the above scripts loaded, you can call `tippy()` with a CSS
    // selector and a `content` prop:
    tippy('.info-text');
</script>