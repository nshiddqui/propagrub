<?= $this->Html->image('beta.png', ['class' => 'beta-version']) ?>
<!-- Logo -->
<a href="/" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><?= $this->Html->image('logo-small.png') ?></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><?= $this->Html->image('logo.png') ?></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <?php if (isset($authUser) && $authUser['is_complete'] == true) { ?>
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
    <?php } ?>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Logout -->
            <li>
                <?= $this->Form->postLink('Save and Exit <i class="fa fa-sign-out"></i>', ['controller' => 'users', 'action' => 'logout'], ['escape' => false, 'confirm' => 'Are you sure want to Save and Exit?']) ?>
            </li>
        </ul>
    </div>
</nav>