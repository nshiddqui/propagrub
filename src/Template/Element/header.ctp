<!-- ### $Topbar ### -->
<div class="header navbar">
    <div class="header-container">
        <ul class="nav-left">
            <li>
                <a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);">
                    <i class="ti-menu"></i>
                </a>
            </li>
        </ul>
        <ul class="nav-right">
            <li>
                <?= $this->Html->link('<i class="ti-power-off"></i>', ['controller' => 'Users', 'action' => 'logout'], ['class' => 'px-4', 'escape' => false]) ?>
            </li>
        </ul>
    </div>
</div>