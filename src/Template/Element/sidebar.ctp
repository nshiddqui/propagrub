<!-- #Left Sidebar ==================== -->
<div class="sidebar">
    <div class="sidebar-inner">
        <!-- ### $Sidebar Header ### -->
        <div class="sidebar-logo">
            <div class="peers ai-c fxw-nw">
                <div class="peer peer-greed">
                    <a class="sidebar-link td-n" href="#">
                        <div class="peers ai-c fxw-nw">
                            <?= $this->Html->image('login_logo.png', ['class' => 'w-75 m-auto py-3']) ?>
                        </div>
                    </a>
                </div>
                <div class="peer">
                    <div class="mobile-toggle sidebar-toggle">
                        <a href="" class="td-n">
                            <i class="ti-arrow-circle-left"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- ### $Sidebar Menu ### -->
        <ul class="sidebar-menu scrollable pos-r">
            <li class="nav-item mT-30 actived">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-blue-500 menu-home"></i> </span> <span class="title">Home</span>', ['controller' => 'Users', 'action' => 'dashboard'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-brown-500 menu-rating"></i> </span> <span class="title">Ratings and reviews</span>', ['controller' => 'UserReviews', 'action' => 'index'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-blue-500 menu-sales"></i> </span> <span class="title">Sales</span>', ['controller' => 'Users', 'action' => 'sales'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-deep-orange-500 menu-live-orders"></i> </span> <span class="title">Live orders</span>', ['controller' => 'userChefOrders', 'action' => 'dispatcher'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-deep-purple-500 menu-menus"></i> </span> <span class="title">Menus</span>', ['controller' => 'Menus', 'action' => 'index'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-deep-purple-500 menu-shipping"></i> </span> <span class="title">Shipping Products</span>', ['controller' => 'Menus', 'action' => 'sippingPropducts'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-deep-purple-500 menu-collection"></i> </span> <span class="title">Delivery / Collection</span>', ['controller' => 'Users', 'action' => 'collection'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <!-- <li class="nav-item">
                <a class="sidebar-link" href="#">
                    <span class="icon-holder">
                        <i class="c-indigo-500 menu-team"></i>
                    </span>
                    <span class="title">Team</span>
                </a>
            </li> -->
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-light-blue-500 menu-invoice"></i> </span> <span class="title">Invoices</span>', ['controller' => 'Users', 'action' => 'invoices'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="c-orange-500 menu-marketer"></i>
                    </span>
                    <span class="title">Marketer</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <?= $this->Html->link('Offers', ['controller' => 'Offers', 'action' => 'index'], ['escape' => false, 'class' => 'sidebar-link']) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('Adverts',  ['controller' => 'Users', 'action' => 'advertisements'], ['escape' => false, 'class' => 'sidebar-link']) ?>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <?= $this->Html->link('<span class="icon-holder"> <i class="c-pink-500 menu-feedback"></i> </span> <span class="title">Feedback</span>', ['controller' => 'Feedbacks', 'action' => 'index'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
            <li class="nav-item profile-menu">
                <?php
                if ($authUser['image'] && !empty($authUser['image'])) {
                    $userImage = $this->Html->image($authUser['image'], ['style' => 'height:35px;width:35px;border-radius:100%;', 'profile' => WEB_PROFILE_IMAGE]);
                } else {
                    $userImage = '<i class="c-pink-500 menu-user"></i>';
                }
                ?>
                <?= $this->Html->link('<span class="icon-holder">  ' . $userImage . '</span> <span class="title">' . $authUser['first_name'] . ' ' . $authUser['last_name'] . ' </span> ' . $this->Html->image('icon_setting.png', ['style' => 'height:18px;margin-left:5px;']), ['controller' => 'Users', 'action' => 'complete'], ['escape' => false, 'class' => 'sidebar-link']) ?>
            </li>
        </ul>
    </div>
</div>