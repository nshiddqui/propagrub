<?php
$this->assign('title', 'Create offer');
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->css('https://cdn.jsdelivr.net/npm/icheck@1.0.2/skins/square/blue.css', ['block' => true]);
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/jquery.sumoselect.min.js', ['block' => true]);
$this->Html->script('https://cdn.jsdelivr.net/npm/icheck@1.0.2/icheck.min.js', ['block' => true]);
?>
<?= $this->Form->create() ?>
<div class="row gap-20 pos-r">
    <div class="w-100">
        <div class="row gap-30 menu-container">
            <h5 class="col-12 text-primary"><?= $this->Html->link($this->Html->image('btn_back.png', ['style' => 'height: 15px;']) . ' Marketer', ['controller' => 'Offers', 'action' => 'index'], ['style' => 'color: #da2485;', 'escape' => false]) ?></h5>
            <h1 class="col-12 text-dark font-weight-bold">Create offer</h1>
            <div class="col-12 my-3">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-12 my-3">
                                <div class="layers bgc-white p-20 rounded">
                                    <div class="layer w-100 p-5">
                                        <p class="d-inline-block lead text-dark" id="details-nav"><?= $this->Html->image('1_enabled.png', ['style' => 'height:20px']) ?> Details</p>
                                        <p class="ml-5 d-inline-block lead" id="review-nav"><?= $this->Html->image('2_disabled.png', ['style' => 'height:20px']) ?> Review and confirm</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-3 enter-offer">
                                <div class="layers bgc-white p-20 rounded">
                                    <h3 class="col-12 text-dark font-weight-bold">Which items do you want to include?</h3>
                                    <p class="col-12">The discount only applies to the item - not to anything added to it, like extra cheese.</p>
                                    <div class="col-12">
                                        <?= $this->Html->link('Add items', 'javascript:loadMenus()', ['web' => true, 'class' => 'btn btn-default add-item-icon']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-3 enter-offer">
                                <div class="layers bgc-white p-20 rounded">
                                    <h3 class="col-12 text-dark font-weight-bold">Which campaign do you want to choose?</h3>
                                    <p class="col-12">Tailor the way we advertise your offer to customers.</p>
                                    <div class="col-12">
                                        <div class="form-group ">
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('campaign', ['1' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Flash deal</h6>
                                                        <p class="mb-0">Sell off extra stock when you're about to close</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('campaign', ['2' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Order more, save more</h6>
                                                        <p class="mb-0">Attract larger orders from groups and families</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('campaign', ['3' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Resturant picks</h6>
                                                        <p class="mb-0">Promote new menu items or special dishes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('campaign', ['4' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Free item</h6>
                                                        <p class="mb-0">Allow customers to choose a free item</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 discount-labels" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $this->Form->control('discount', ['options' => [5 => '5%', 10 => '10%', 15 => '15%', 20 => '20%', 40 => '40%', 60 => '60%', 70 => '70%'], 'empty' => 'select']) ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $this->Form->control('minimum_spend_required', ['options' => [0 => '£0.00', 5.00 => '£5.00', 10.00 => '£10.00', 20.00 => '£20.00', 40.00 => '£40.00', 60.00 => '£60.00', 80.00 => '£80.00', 100.00 => '£100.00'], 'empty' => 'select']) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="col-12 discount-labels" style="display: none;">You'll cover the cost of the discount.<?= $this->Html->link('Find out more', 'https://propagrub.com', ['escape' => false]) ?></p>
                                </div>
                            </div>
                            <div class="col-12 my-3 enter-offer">
                                <div class="layers bgc-white p-20 rounded">
                                    <h3 class="col-12 text-dark font-weight-bold">Timing</h3>
                                    <div class="col-12">
                                        <div class="form-group ">
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('timing', ['1' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Start right now</h6>
                                                        <p class="mb-0">e.g. 20% off for the rest of the day</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('timing', ['2' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Schedule a one-off</h6>
                                                        <p class="mb-0">e.g. 20% off for the holidays</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('timing', ['3' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Schedule a regular offer</h6>
                                                        <p class="mb-0">e.g. 20% from 15:00 to 16:00 on weekdays</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12" style="display: none;" id="timing-manage">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $this->Form->control('start_date', ['type' => 'date"']) ?>
                                                <?= $this->Form->control('start_time', ['type' => 'time"']) ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $this->Form->control('end_date', ['type' => 'date"']) ?>
                                                <?= $this->Form->control('end_time', ['type' => 'time"']) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12" style="display: none;" id="date-manage">
                                        <?= $this->Form->control('days', ['options' => [1 => '1 day', 2 => '2 days', 3 => '3 days', 5 => '5 days', 10 => '10 days'], 'label' => 'Select Duration']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-3 enter-offer">
                                <div class="layers bgc-white p-20 rounded">
                                    <h3 class="col-12 text-dark font-weight-bold">Audience</h3>
                                    <div class="col-12">
                                        <div class="form-group ">
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('audience', ['1' => '1'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Everyone</h6>
                                                        <p class="mb-0">All customers - great for boosting order numbers</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('audience', ['2' => '2'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">Propagrub Plus subscribers only</h6>
                                                        <p class="mb-0">Our most loyal group - they order twice as often and spend twice as much</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
                                                <div class="row">
                                                    <div class="col-1 text-right mt-2">
                                                        <?= $this->Form->radio('audience', ['3' => '3'], ['label' => false, 'hiddenField' => false]) ?>
                                                    </div>
                                                    <div class="col-11 pl-0">
                                                        <h6 class="mb-0">New customers only</h6>
                                                        <p class="mb-0">Anyone who hasn't ordered from your resturant before</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-3" id="review-details" style="display: none;">
                                <div class="layers bgc-white p-20 rounded">
                                    <h3 class="col-12 text-dark font-weight-bold">Review your offer</h3>
                                    <div class="col-12">
                                        <table>
                                            <tr>
                                                <td style="width: 200px; font-size: 16px; padding-bottom: 5px;">Offer</td>
                                                <td style="font-size: 16px; padding-bottom: 5px;color:#343a40;" id="offer-text">20% off over £15.00</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 200px; font-size: 16px; padding-bottom: 5px;">Timing</td>
                                                <td style="font-size: 16px; padding-bottom: 5px;color:#343a40;" id="timing-text">Schedule a one off</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 200px; font-size: 16px; padding-bottom: 5px;">Start Date</td>
                                                <td style="font-size: 16px; padding-bottom: 5px;color:#343a40;" id="start-date-text">10:00, 11 November 2021</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 200px; font-size: 16px; padding-bottom: 5px;">End Date</td>
                                                <td style="font-size: 16px; padding-bottom: 5px;color:#343a40;" id="end-date-text">23:59, 11 November 2021</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 200px; font-size: 16px; padding-bottom: 5px;">Audience</td>
                                                <td style="font-size: 16px; padding-bottom: 5px;color:#343a40;" id="audience-text">Eevryone</td>
                                            </tr>
                                        </table>
                                        <hr>
                                    </div>
                                    <h3 class="col-12 text-dark font-weight-bold">What customers see</h3>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $this->Html->image('img1.png') ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $this->Html->image('img2.png') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div id="review-offer">
                            <?= $this->Html->link('Next: Review your offer', 'javascript:revieOffer()', ['escape' => false, 'class' => 'btn btn-primary', 'web' => true]) ?>
                        </div>
                        <div id="finish-offer" style="display: none;">
                            <?= $this->Form->submit('Schedule your offer now', ['class' => 'btn btn-primary']) ?>
                            <?= $this->Html->link('Back', 'javascript:backOffer()', ['escape' => false, 'class' => 'btn btn-default bd mt-2', 'web' => true, 'style' => 'width:176px']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<!-- Modal -->
<div class="modal fade" id="menu-products" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Add items</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body text-dark">
                <div class="row">
                    <p class="col-12">Select up to 10 items. Selected items must be over £3.00</p>
                    <div class="col-md-6">
                        <?= $this->Form->control('items_search', ['label' => false, 'placeholder' => 'Search an items']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('filter_category', ['label' => false, 'options' => $categories, 'empty' => 'Filter by category']) ?>
                    </div>
                    <div class="col-12">
                        <table class="table tableFixHead">
                            <thead>
                                <tr>
                                    <th style="width: 23px;">&nbsp;</th>
                                    <th style="width: 343px;">Item</th>
                                    <th style="width: 100px;">Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($items as $item) { ?>
                                    <tr>
                                        <td style="width: 23px;"><?= $this->Form->checkbox('items_id', [$item->id => $item->id], ['label' => false, 'hiddenField' => false]) ?></td>
                                        <td style="width: 343px;">
                                            <?= $item->product_name ?>
                                            <p class="mb-0 text-secondary"><?= $item['menu_category']->category_name ?></p>
                                        </td>
                                        <td style="width: 100px;">£<?= $item->product_price ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="loadSelected()">Add selected items</button>
            </div>
        </div>
    </div>
</div>