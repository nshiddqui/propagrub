<?php
$this->assign('title', 'Marketer');
?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h1 class="col-12 text-dark font-weight-bold">Marketer</h1>
            <div class="col-12">
                <div class="layers bd bgc-white p-10 rounded">
                    <div class="layer w-100 row">
                        <h3 class="col-12 text-dark my-3">Create a new offer</h3>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="layers bd bgc-white p-20 rounded" style="cursor: pointer;" onclick="window.location.href = '<?= $this->Url->build(['action' => 'basketTotal']) ?>'">
                                        <div class="layer w-100 row">
                                            <div class="col-md-2 px-0 text-center">
                                                <?= $this->Html->image('menu.png', ['style' => 'height: 50px;']) ?>
                                            </div>
                                            <div class="col-md-8">
                                                <p class="mb-0 text-truncate text-dark menu-product-info">Basket total</p>
                                                <p class="mb-0 text-truncate menu-product-info">E.g 25% off the entire menu</p>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <?= $this->Html->image('btn_forward.png', ['style' => 'height: 25px; margin-top: 15px;']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="layers bd bgc-white p-20 rounded" style="cursor: pointer;" onclick="window.location.href = '<?= $this->Url->build(['action' => 'specificItems']) ?>'">
                                        <div class="layer w-100 row">
                                            <div class="col-md-2 px-0 text-center">
                                                <?= $this->Html->image('icon_2.png', ['style' => 'height: 50px;']) ?>
                                            </div>
                                            <div class="col-md-8">
                                                <p class="mb-0 text-truncate text-dark menu-product-info">Specific items</p>
                                                <p class="mb-0 text-truncate menu-product-info">E.g 25% off a chicken burger</p>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <?= $this->Html->image('btn_forward.png', ['style' => 'height: 25px; margin-top: 15px;']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="col-12 text-dark my-3">All offers</h3>
                        <?php if ($offers->count()) { ?>
                            <div class="col-12">
                                <table class="table">
                                    <tr>
                                        <th>Status</th>
                                        <th>Offer Details</th>
                                        <th>Start date</th>
                                        <th>End date</th>
                                    </tr>
                                    <?php foreach ($offers as $offer) { ?>
                                        <tr>
                                            <td><?= $offer->status == '1' ? 'Active' : 'Closed' ?></td>
                                            <td><?= $offer->timing == '1' ? ($offer->timing == '2' ? 'Schedule a one-off' : 'Start right now') : 'Schedule a regular offer' ?></td>
                                            <td><?= $offer->start_date . ' ' . $offer->start_time ?></td>
                                            <td><?= $offer->end_date . ' ' . $offer->end_time ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        <?php } else { ?>
                            <div class="w-100 text-center my-3">
                                <?= $this->Html->image('no_search_data.png', ['style' => 'height:200px']) ?>
                                <h3>You haven't created any offers yet.</h3>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>