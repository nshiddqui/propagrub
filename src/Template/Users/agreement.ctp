<?php
$this->assign('title', 'Agreement');
?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h1 class="col-12 text-dark font-weight-bold">Your agreement</h1>
            <div class="col-12 my-3">
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <div class="row">
                            <div class="col-12 text-center">
                                <?= $this->Html->image('plus-welcome.png') ?>
                            </div>
                            <h3 class="col-12 text-center text-dark mt-1">Welcome to Propagrub, <?= $user['chef_detail']->resturant_name ?></h3>
                            <p class="col-12 text-center">We can't wait for customers to try your food.</p>
                            <hr class="col-12">
                            <h5 class="col-12 text-dark mt-1 font-weight-bold">Next steps:</h5>
                            <p class="col-12 mt-4"><?= $this->Html->image('mail-1.png', ['style' => 'height:48px', 'class' => 'mr-3']) ?> We have sent you an agreement email. Please accept that agreement to proceed.</p>
                            <?= $this->Html->link(' Back to Home', ['controller' => 'Users', 'action' => 'onboarding'], ['class' => 'btn btn-primary mt-4']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>