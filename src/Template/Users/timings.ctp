<?= $this->Form->create($user) ?>
<div class="container mt-3">
    <div class="row">
        <h2 class="col-12 text-dark">Settings</h2>
        <div class="col-12 my-4">
            <div class="row">
                <div class="col-md-8">
                    <div class="mx-3 box-outer mt-3 px-3 py-3">
                        <h2 class="right-box-header">Set your opening hours</h2>
                        <div class="col-md-12">
                            <div class="row every-day">
                                <div class="col-md-4">Every Day</div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <?= $this->Form->control('start_time', ['label' => false, 'type' => 'time"']) ?>
                                        </div>
                                        <div class="col-md-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-md-5">
                                            <?= $this->Form->control('end_time', ['label' => false, 'type' => 'time"']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <?php
                            $weekKey = 0;
                            foreach ($weeks as $week_id => $week) {
                            ?>
                                <div class="row single-day">
                                    <div class="col-md-4 font-weight-bold text-dark"><?= $week ?></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <?= $this->Form->hidden("chef_timings.{$weekKey}.id") ?>
                                                <?= $this->Form->hidden("chef_timings.{$weekKey}.week_id", ['value' => $week_id]) ?>
                                                <?= $this->Form->control("chef_timings.{$weekKey}.start_time", ['label' => false, 'type' => 'time""', 'start-time' => true]) ?>
                                            </div>
                                            <div class="col-md-2">
                                                <label>To</label>
                                            </div>
                                            <div class="col-md-5">
                                                <?= $this->Form->control("chef_timings.{$weekKey}.end_time", ['label' => false, 'type' => 'time""', 'end-time' => true]) ?>
                                            </div>
                                            <div class="col-12">
                                                <?= $this->Form->control("chef_timings.{$weekKey}.notes", ['label' => false, 'required' => true, 'placeholder' => 'Enter notes']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                $weekKey++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-right-outer mx-3 mt-3 px-3 py-3">
                        <?= $this->Form->submit('Set opening hours', ['class' => 'btn btn-primary w-100']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>