<?php
$this->assign('title', ' Bank Details');
?>
<?= $this->Html->css('https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css', ['block' => 'css']) ?>
<?= $this->Html->script('https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js', ['block' => 'script']) ?>
<?= $this->Html->css('bank-details', ['block' => 'css']) ?>
<?= $this->Html->script('bank-details', ['block' => 'script']) ?>
<?= $this->Form->create($bank_detail, ['type' => 'file']) ?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <h4>Bank Details</h4>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->Form->hidden('id') ?>
                        <?= $this->Form->control('holder_name', ['value' => (!empty($bank_detail->holder_name) ? $bank_detail->holder_name : $authUser['first_name'] . ' ' . $authUser['last_name']), 'required' => true]) ?>
                        <?= $this->Form->control('sort_code', ['value' => $bank_detail->routing_number, 'required' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('account_number', ['required' => true]) ?>
                    </div>
                </div>
                <h4>Address Info</h4>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->control('address', ['label' => 'Address Line1', 'required' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('city', ['required' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('post_code', ['label' => 'Postal Code', 'required' => true]) ?>
                    </div>
                </div>
                <?php if (empty($bank_detail->stripe_account_id)) { ?>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?= $this->Form->submit('Save', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>