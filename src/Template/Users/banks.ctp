<?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/jquery.inputmask.bundle.min.js', ['block' => true]) ?>
<?= $this->Form->create($user) ?>
<div class="container mt-3">
    <div class="row">
        <h2 class="col-12 text-dark">Settings</h2>
        <div class="col-12 my-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="mx-3 box-outer mt-3 px-3 py-3">
                        <h2 class="right-box-header col-12">Add your bank details</h2>
                        <p class="col-12">This is where we'll send your payments.</p>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $this->Form->hidden("bank_detail.id") ?>
                                    <?= $this->Form->control('bank_detail.account_number', ['required' => true, 'maxlength' => 8]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->control('bank_detail.routing_number', ['required' => true, 'label' => 'Sort code']) ?>
                                </div>
                                <hr class="col-11">
                                <div class="col-12 text-right">
                                    <?= $this->Form->submit('Add  details', ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<div style="text-align:center;">
    <?= $this->Html->image('powered.png', ['style' => 'height:40px']) ?>
</div>