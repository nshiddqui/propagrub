<?= $this->Html->css('https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css', ['block' => 'css']) ?>
<?= $this->Html->script('https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js', ['block' => 'script']) ?>
<div class="card login-card">
    <div class="row no-gutters">
        <div class="col-md-5">
            <?= $this->Html->image('login.jpg', ['alt' => 'login', 'class' => 'login-card-img']) ?>
            <div class="bg-opacity-login">
            </div>
            <?= $this->Html->image('img_logo.gif', ['alt' => 'login', 'class' => 'absolute-center']) ?>
            <div class="bg-text-login">
                <h5>Overview</h5>
                <p>✔ £1 per day membership to run your own kitchen</p>
                <p>✔ We will upload your menu for you</p>
                <p>✔ We will contact you upon launch</p>
                <p>✔ Use Propagrub’s driver network or your own</p>
                <p>✔ Marketing and promotion of your business </p>
                <p>✔ Attract loyal customers, send notifications to your followers</p>
                <p>✔ 10% per order + discretionary driver tip</p>

                <h5>To Sign up</h5>
                <p>1. Tell us about your restaurant and business.</p>
                <p>2. Upload your menu.</p>
                <p>3. Start taking orders.</p>
            </div>
            <?= $this->Html->image('beta.png', ['class' => 'beta-version']) ?>
        </div>
        <div class="col-md-7">
            <div class="card-body register-box-body">
                <div class="brand-wrapper">
                    <?= $this->Html->image('login_logo.png', ['alt' => 'logo', 'class' => 'logo']) ?>
                </div>
                <?= $this->Form->create() ?>
                <?= $this->Flash->render() ?>
                <h3>Hi <b><?= $user->displayName ?></b>,</h3>
                <br>
                <div class="text-center">
                    <?= $this->Html->image($user->photoURL, ['alt' => 'logo', 'class' => 'logo']) ?>
                </div>
                <br>
                <div class="form-group has-feedback">
                    <?= $this->Form->control('username', ['placeholder' => 'Username', 'label' => false]) ?>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-12">
                        <?= $this->Form->button('Register', ['class' => 'btn btn-danger btn-block btn-flat']) ?>
                    </div>
                    <!-- /.col -->
                </div>
                <?= $this->Form->end() ?>
                <div class="link-extra">
                    <?= $this->Html->link('Privay Policy', 'https://propagrub.com/privacy-policy/', ['target' => '_BLANK']) ?>&nbsp;&nbsp;|&nbsp;
                    <?= $this->Html->link('Terms & Conditions', 'https://propagrub.com/terms-conditions/', ['target' => '_BLANK']) ?>&nbsp;&nbsp;|&nbsp;
                    <?= $this->Html->link('FAQ\'s', 'https://propagrub.com/chefsfaq/', ['target' => '_BLANK']) ?>
                </div>
            </div>
        </div>
    </div>
</div>