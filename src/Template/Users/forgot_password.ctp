<div class="card login-card">
    <div class="row no-gutters">
        <div class="col-md-5">
            <?= $this->Html->image('login.jpg', ['alt' => 'login', 'class' => 'login-card-img']) ?>
            <div class="bg-opacity-login">
            </div>
            <?= $this->Html->image('img_logo.gif', ['alt' => 'login', 'class' => 'absolute-center']) ?>
            <div class="bg-text-login">
                <h5>Overview</h5>
                <p>✔ £1 per day membership to run your own kitchen</p>
                <p>✔ We will upload your menu for you</p>
                <p>✔ We will contact you upon launch</p>
                <p>✔ Use Propagrub’s driver network or your own</p>
                <p>✔ Marketing and promotion of your business </p>
                <p>✔ Attract loyal customers, send notifications to your followers</p>
                <p>✔ 10% per order + discretionary driver tip</p>

                <h5>To Sign up</h5>
                <p>1. Tell us about your restaurant and business.</p>
                <p>2. Upload your menu.</p>
                <p>3. Start taking orders.</p>
            </div>
            <?= $this->Html->image('beta.png', ['class' => 'beta-version']) ?>
        </div>
        <div class="col-md-7">
            <div class="card-body register-box-body">
                <div class="brand-wrapper">
                    <?= $this->Html->image('login_logo.png', ['alt' => 'logo', 'class' => 'logo']) ?>
                </div>
                <p class="login-card-description">Please enter your email to retrive your password</p>
                <?= $this->Form->create(null) ?>
                <?= $this->Flash->render() ?>
                <div class="form-group has-feedback">
                    <?= $this->Form->control('email', ['placeholder' => 'Email', 'label' => false, 'class' => 'form-control']) ?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <hr />
                <?= $this->Form->submit('Submit', ['class' => 'btn btn-danger btn-block btn-flat mb-2']) ?>
                <?= $this->Html->link('Login', ['controller' => 'users', 'action' => 'login'], ['class' => 'btn btn-block mb-4 border']) ?>
                <?= $this->Form->end() ?>
                <div class="link-extra">
                    <?= $this->Html->link('Privay Policy', 'https://propagrub.com/privacy-policy/', ['target' => '_BLANK']) ?>&nbsp;&nbsp;|&nbsp;
                    <?= $this->Html->link('Terms & Conditions', 'https://propagrub.com/terms-conditions/', ['target' => '_BLANK']) ?>&nbsp;&nbsp;|&nbsp;
                    <?= $this->Html->link('FAQ\'s', 'https://propagrub.com/chefsfaq/', ['target' => '_BLANK']) ?>
                </div>
            </div>
            <!-- /.login-box-body -->
        </div>
    </div>
</div>