<?php
$this->Html->css('https://cdn.jsdelivr.net/npm/icheck@1.0.2/skins/square/blue.css', ['block' => true]);
$this->Html->script('https://cdn.jsdelivr.net/npm/icheck@1.0.2/icheck.min.js', ['block' => true]);
$this->Html->script('https://maps.googleapis.com/maps/api/js?key=' . GMAP_API . '&libraries=places&callback=initialize', ['block' => 'script']);
?>

<?= $this->Form->create($collection) ?>
<div class="container mt-3">
    <div class="row">
        <h2 class="col-12 text-dark">Delivery / Collection</h2>
        <div class="col-12 my-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="mx-3 box-outer mt-3 px-3 py-3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <?= $this->Form->control('type', ['options' => [0 => ' Delivery'], 'label' => false, 'type' => 'radio', 'hiddenField' => false, 'required' => true]) ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?= $this->Form->control('type', ['options' => [1 => ' Collection'], 'label' => false, 'type' => 'radio', 'hiddenField' => false, 'required' => true]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="delievery-div" style="display: none;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mt-3 price-input-product" style="position: relative;">
                                            <div class="input-group-text bgc-white bd" style="left: 0;">£</div>
                                            <?= $this->Form->control('charges', ['label' => 'Delivery Charges (per Km)', 'type' => 'number']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="collection-div" style="display: none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $this->Form->control('start_time', ['label' => 'Start Time', 'type' => 'time"']) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $this->Form->control('end_time', ['label' => 'End Time', 'type' => 'time"']) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $this->Form->hidden('lat', ['id' => 'lat']) ?>
                                        <?= $this->Form->hidden('lng', ['id' => 'lng']) ?>
                                        <?= $this->Form->control('address', ['label' => 'Address']) ?>
                                        <div id="map"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <?= $this->Form->submit('Update', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->Html->scriptStart(['block' => true]); ?>
$(document).ready(function() {
$('input[name="type"]').on('ifChecked', function() {
$('#delievery-div, #collection-div, #shipping-div').hide();
$('#delievery-div input, #collection-div input').prop('required', false);
switch ($(this).val()) {
case '0':
$('#delievery-div').show();
$('#delievery-div input').prop('required', true);
break;
case '1':
$('#collection-div').show();
$('#collection-div input').prop('required', true);
break;
default:
$('#shipping-div').show();

}
});
$('input[name="type"]:checked').trigger('ifChecked');
});
<?= $this->Html->scriptEnd(); ?>