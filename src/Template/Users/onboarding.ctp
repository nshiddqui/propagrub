<?php
$onboarding_status = 0;
?>
<div class="home-bg-half"></div>
<div class="container mt-3">
    <div class="row">
        <h1 class="col-12 text-white">Welcome to Propagrub</h1>
        <div class="col-12 my-4">
            <div class="row">
                <div class="col-md-7">
                    <div class="mx-3 box-outer  mt-3">
                        <div class="box-outer-part">
                            <span class="box-first-part">
                                <?= $this->Html->image('noun-check-green.png', ['style' => 'height: fit-content;']) ?>
                            </span>
                            <div class="colspan-all">
                                <p class="main-box-title">Sign up</p>
                                <p class="main-box-content">Completed</p>
                            </div>
                        </div>
                        <div class="box-outer-part" onclick="window.location.href='<?= $this->Url->build(['controller' => 'Menus', 'action' => 'upload'])  ?>'">
                            <span class="box-first-part">
                                <?php if (!empty($user['chef_detail']->menu_file)) {
                                    $onboarding_status = 1;
                                ?>
                                    <?= $this->Html->image('noun-check-green.png', ['style' => 'height: fit-content;']) ?>
                                <?php } else { ?>
                                    <?= $this->Html->image('menu-main-icon.png', ['style' => 'height: fit-content;']) ?>
                                <?php } ?>
                            </span>
                            <div class="colspan-all">
                                <p class="main-box-title">Add your menu</p>
                                <p class="main-box-content">Link to your menu or upload a file</p>
                            </div>
                            <?= $this->Html->image('btn_forward.png', ['style' => 'height:20px;margin-left: auto;']) ?>
                        </div>
                        <div class="box-outer-part" onclick="<?= ($onboarding_status == 1) ? 'window.location.href=\'' . $this->Url->build(['controller' => 'Users', 'action' => 'timings']) . '\'' : '' ?>">
                            <span class="box-first-part">
                                <?php if (!empty($user['chef_timings'])) {
                                    $onboarding_status = 2;
                                ?>
                                    <?= $this->Html->image('noun-check-green.png', ['style' => 'height: fit-content;']) ?>
                                <?php } else { ?>
                                    <?= $this->Html->image('schedule-main-icon.png', ['style' => 'height: fit-content;']) ?>
                                <?php } ?>
                            </span>
                            <div class="colspan-all">
                                <p class="main-box-title">Set your opening hours</p>
                                <p class="main-box-content">Let us know when you're happy to take orders</p>
                            </div>
                            <?= $this->Html->image('btn_forward.png', ['style' => 'height:20px;margin-left: auto;']) ?>
                        </div>
                        <div class="box-outer-part" onclick="<?= ($onboarding_status == 2) ? 'window.location.href=\'' . $this->Url->build(['controller' => 'Users', 'action' => 'banks', (empty($user['bank_detail']) ? 'first' : '')]) . '\'' : '' ?>">
                            <span class="box-first-part">
                                <?php if (!empty($user['bank_detail'])) {
                                    $onboarding_status = 3;
                                ?>
                                    <?= $this->Html->image('noun-check-green.png', ['style' => 'height: fit-content;']) ?>
                                <?php } else { ?>
                                    <?= $this->Html->image('payment-fill-icon.png', ['style' => 'height: fit-content;']) ?>
                                <?php } ?>
                            </span>
                            <div class="colspan-all">
                                <p class="main-box-title">Let us know where you'd like to be paid</p>
                                <p class="main-box-content">Add your bank account details</p>
                            </div>
                            <?= $this->Html->image('btn_forward.png', ['style' => 'height:20px;margin-left: auto;']) ?>
                        </div>
                        <div class="box-outer-part" onclick="<?= ($onboarding_status == 3) ? 'window.location.href=\'' . $this->Url->build(['controller' => 'Users', 'action' => 'agreement']) . '\'' : '' ?>">
                            <span class="box-first-part">
                                <?php if ($user->is_verified == false) { ?>
                                    <?php if ($onboarding_status == 3) {  ?>
                                        <span class="propagrub-preloader-form"></span>
                                    <?php } else { ?>
                                        <?= $this->Html->image('agreement-icon.png', ['style' => 'height: fit-content;']) ?>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?= $this->Html->image('noun-check-green.png', ['style' => 'height: fit-content;']) ?>
                                <?php } ?>
                            </span>
                            <div class="colspan-all">
                                <p class="main-box-title">Accept your agreement</p>
                                <p class="main-box-content">Check your email - we've sent you a message</p>
                            </div>
                            <?= $this->Html->image('btn_forward.png', ['style' => 'height:20px;margin-left: auto;']) ?>
                        </div>
                        <div class="box-outer-part" onclick="<?= ($user->is_verified == true) ? 'window.location.href=\'' . $this->Url->build(['controller' => 'Users', 'action' => 'launch']) . '\'' : '' ?>">
                            <span class="box-first-part">
                                <?= $this->Html->image('plus-priority-delivery.png', ['style' => 'height: fit-content;']) ?>
                            </span>
                            <div class="colspan-all">
                                <p class="main-box-title">Launch on Propagrub</p>
                                <p class="main-box-content">Set up your device and get ready to open</p>
                            </div>
                            <?php if ($user->is_verified == false) { ?>
                                <?= $this->Html->image('icon_lock.png', ['style' => 'height:20px;margin-left: auto;']) ?>
                            <?php } else { ?>
                                <?= $this->Html->image('btn_forward.png', ['style' => 'height:20px;margin-left: auto;']) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="box-right-outer mx-3 mt-3">
                        <h2 class="right-box-header"> A taste of what we offer</h2>
                        <div class="right-box-inner">
                            <span class="right-box-inner-first">
                                <?= $this->Html->image('rider-surge.png', ['style' => 'height:48px;']) ?>
                            </span>
                            <div class="colspan-all">
                                <p class="right-box-inner-herder">Hassle-free deliveries</p>
                                <p class="right-box-inner-description">
                                    We get your food to your customers, fast, hot and in perfect condition.
                                </p>
                            </div>
                        </div>
                        <div class="right-box-inner">
                            <span class="right-box-inner-first">
                                <?= $this->Html->image('plus-for-group-lib-light.png', ['style' => 'height:48px;']) ?>
                            </span>
                            <div class="colspan-all">
                                <p class="right-box-inner-herder">Promotion to 1000s of customers</p>
                                <p class="right-box-inner-description">
                                    We'll help you attract attention with targeted adverts, offers and marketing.
                                </p>
                            </div>
                        </div>
                        <div class="right-box-inner">
                            <span class="right-box-inner-first">
                                <?= $this->Html->image('laptop.png', ['style' => 'height:48px;']) ?>
                            </span>
                            <div class="colspan-all">
                                <p class="right-box-inner-herder">Insights that power your growth</p>
                                <p class="right-box-inner-description">
                                    Use our benchmarked data to track how you're performing against local competitors and get to know your customers.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>