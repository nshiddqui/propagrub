<?php
$this->assign('title', 'Dashboard');
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/jquery.sumoselect.min.js', ['block' => true]);
?>
<?= $this->Html->script('//code.jquery.com/ui/1.13.0-rc.3/jquery-ui.min.js', ['block' => true]) ?>
<div class="container">
    <div class="row masonry pos-r" style="position: relative; height: 1825.2px;">
        <div class="masonry-sizer col-md-6"></div>
        <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
            <div class="row menu-container">
                <h1 class="col-12 text-dark font-weight-bold">Hi, <?= $authUser['first_name'] . ' ' . $authUser['last_name'] ?></h1>
                <div class="col-12 my-3 d-none">
                    <div class="row">
                        <div class="col-md-3">
                            <?= $this->Form->control('test', ['options' => ['Test1', 'Test2'], 'label' => false]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->control('test', ['options' => ['Test1', 'Test2'], 'label' => false]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->control('test', ['options' => ['Test1', 'Test2'], 'label' => false]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->control('test', ['options' => ['Test1', 'Test2'], 'label' => false]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 my-3">
                    <div class="layers bgc-white p-20 rounded">
                        <div class="row w-100">
                            <div class="col-md-2 text-center pr-0 mr-0">
                                <?= $this->Html->image('propa_pay.png', ['style' => 'height:100px']) ?>
                            </div>
                            <div class="col-md-8 pl-0 ml-0">
                                <h3 class="text-dark font-weight-bold my-4">Propagrub's Cryptocurrency is launching soon!</h3>
                            </div>
                            <div class="col-md-2 mt-4">
                                <?= $this->Html->link('Find out more', 'https://propagrub.com', ['class' => 'btn btn-primary', 'web' => true]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>