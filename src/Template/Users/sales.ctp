<div class="container">
    <div class="row masonry pos-r" style="position: relative; height: 1825.2px;">
        <div class="masonry-sizer col-md-6"></div>
        <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
            <div class="row menu-container">
                <h1 class="col-12 text-dark font-weight-bold">Sales</h1>
                <div class="w-100 text-center my-3">
                    <?= $this->Html->image('no_search_data.png', ['style' => 'height:200px']) ?>
                    <h3>You don't have any sales yet</h3>
                </div>
            </div>
        </div>
    </div>
</div>