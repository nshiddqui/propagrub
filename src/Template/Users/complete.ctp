<?php
$this->assign('title', 'Chef Registration Details');
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/jquery.sumoselect.min.js', ['block' => true]);
?>
<?= $this->Html->script('https://unpkg.com/@popperjs/core@2') ?>
<?= $this->Html->script('https://unpkg.com/tippy.js@6') ?>
<script>
    var API_URL = '<?= $this->Url->build(['controller' => 'api', 'action' => 'v1', 'users', 'complete']) ?>';
    var EMAIL_URL = '<?= $this->Url->build(['controller' => 'api', 'action' => 'v1', 'users', 'checkEmail']) ?>';
    var MOBILE_URL = '<?= $this->Url->build(['controller' => 'api', 'action' => 'v1', 'users', 'checkEmail']) ?>';
</script>
<?= $this->Html->css('https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css', ['block' => 'css']) ?>
<?= $this->Html->script('https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js', ['block' => 'script']) ?>
<?= $this->Html->component('jquery-step/jquery.steps', 'css', ['block' => 'css']) ?>
<?= $this->Html->component('jquery-step/jquery.steps', 'script', ['block' => 'script']) ?>
<?= $this->Html->script('https://maps.googleapis.com/maps/api/js?key=' . GMAP_API . '&libraries=places&callback=initializeSettingMap', ['block' => 'script']); ?>
<?= $this->Html->css('user-complete', ['block' => 'css']) ?>
<?= $this->Html->script('user-complete', ['block' => 'script']) ?>
<?= $this->Form->create($user, ['type' => 'file', 'id' => 'user-complete']) ?>
<div class="d-none">
    <?php foreach ($service_type as $service_id => $service) { ?>
        <?= $this->Form->control('chef_detail.service_type', ['value' => $service_id, 'label' => false]) ?>
    <?php } ?>
</div>
<div class="container-fluid">
    <h4 class="c-grey-900 mT-10 mB-30">Settings</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <h4 class="c-grey-900 mB-20">Contact Person Details</h4>
                <hr class="w-100">
                <div class="row">
                    <div class="col-12 text-center">
                        <label for="image" style="text-align: center;width: fit-content;border-radius: 100%;">
                            <?= $this->Html->image((!empty($user->image) ? $user->image . '?' . microtime(true) : 'not-found.png'), ['class' => 'thumbnail-image', 'style' => 'border-radius: 100%;', 'profile' => WEB_PROFILE_IMAGE, 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                        </label>
                        <div class="d-none">
                            <?= $this->Form->control('image', ['type' => 'file', 'label' => false, 'accept' => 'image/*', 'required' => false]) ?>
                        </div>
                        <p class="thumbnail-paragraph">[Note: Image dimension between 250x250 and 512x512]</p>
                    </div>
                    <hr class="w-100">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-4"><label for="first-name">Contact Person Name <span class="text-danger">*</span></label></div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $this->Form->control('first_name', ['label' => false]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $this->Form->control('last_name', ['label' => false]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4"><label for="email">Username <span class="text-danger">*</span> </label></div>
                            <div class="col-md-8">
                                <?= $this->Form->control('username', ['label' => false, 'required' => true, 'readonly' => $this->Time->wasWithinLast(7, $user->user_name_modification), 'onkeyup' => "this.value = this.value.replace(/[^a-zA-Z0-9_.]/g, '')"]) ?>
                                <div class="w-100 text-danger" style="margin-top: -15px;margin-bottom: 15px;">
                                    (Last Modification: <?= $this->Time->timeAgoInWords($user->user_name_modification) ?>)
                                </div>
                            </div>
                            <div class="col-md-4"><label for="email">Email Address <span class="text-danger">*</span></label></div>
                            <div class="col-md-8"><?= $this->Form->control('email', ['label' => false, 'required' => true]) ?></div>
                            <div class="col-md-4"><label for="mobile">Contact Number <span class="text-danger">*</span></label></div>
                            <div class="col-md-8">
                                <?= $this->Form->control('mobile', ['label' => false, 'required' => true]) ?>
                            </div>
                            <div class="col-md-4"><label>Gender <span class="text-danger">*</span></label></div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-2">
                                        <?= $this->Form->control('gender', ['type' => 'radio', 'options' => [1 => ' Male'], 'label' => false, 'hiddenField' => false]) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $this->Form->control('gender', ['type' => 'radio', 'options' => [2 => ' Female'], 'label' => false, 'hiddenField' => false]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <h4 class="c-grey-900 mB-20">Restaurant Details</h4>
                <hr class="w-100">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="resturant-name">Chef / Restaurant Name <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->control('chef_detail.id') ?>
                                <?= $this->Form->control('chef_detail.resturant_name', ['label' => false]) ?>
                            </div>
                            <div class="col-md-4">
                                <label for="resturant-name">Select Cuisines <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->control('chef_detail.chef_cuisines_ids', ['label' => false, 'options' => $chef_cuisines, 'value' => explode(',', $user['chef_detail']->chef_cuisines_ids), 'multiple' => true, 'required' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <label for="order-min-amount">Order Min Amount <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <i class="usd icon">£</i>
                                <?= $this->Form->control('chef_detail.order_min_amount', ['label' => false, 'type' => 'number']) ?>
                            </div>
                            <div class="col-md-4">
                                <label for="short-description">Short Description <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->control('chef_detail.short_description', ['label' => false, 'type' => 'textarea']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Select your store Address <span class="text-danger">*</span></label>
                                <?= $this->Form->hidden('chef_detail.lat', ['id' => 'lat']) ?>
                                <?= $this->Form->hidden('chef_detail.lng', ['id' => 'lng']) ?>
                                <?= $this->Form->control('chef_detail.address', ['label' => false]) ?>
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <h4 class="c-grey-900 mB-20">Restaurant Timings</h4>
                <hr class="w-100">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row every-day">
                            <div class="col-md-4">Every Day</div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-5">
                                        <?= $this->Form->control('start_time', ['label' => false, 'type' => 'time"']) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <label>To</label>
                                    </div>
                                    <div class="col-md-5">
                                        <?= $this->Form->control('end_time', ['label' => false, 'type' => 'time"']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <?php
                        $weekKey = 0;
                        foreach ($weeks as $week_id => $week) {
                        ?>
                            <div class="row single-day">
                                <div class="col-md-4 font-weight-bold text-dark"><?= $week ?></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <?= $this->Form->hidden("chef_timings.{$weekKey}.id") ?>
                                            <?= $this->Form->hidden("chef_timings.{$weekKey}.week_id", ['value' => $week_id]) ?>
                                            <?= $this->Form->control("chef_timings.{$weekKey}.start_time", ['label' => false, 'type' => 'time""', 'start-time' => true]) ?>
                                        </div>
                                        <div class="col-md-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-md-5">
                                            <?= $this->Form->control("chef_timings.{$weekKey}.end_time", ['label' => false, 'type' => 'time""', 'end-time' => true]) ?>
                                        </div>
                                        <div class="col-12">
                                            <?= $this->Form->control("chef_timings.{$weekKey}.notes", ['label' => false, 'required' => true, 'placeholder' => 'Enter notes']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            $weekKey++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <h4 class="c-grey-900 mB-20">Change Password</h4>
                <hr class="w-100">
                <div class="row">
                    <div class="col-md-4"><label>Current Password</label></div>
                    <div class="col-md-8">
                        <?= $this->Form->control('current_password', ['type' => 'password', 'label' => false, 'value' => '', 'placeholder' => 'Current Password']) ?>
                    </div>
                    <div class="col-md-4"><label>New Password</label></div>
                    <div class="col-md-8">
                        <?= $this->Form->control('password', ['type' => 'password', 'label' => false, 'value' => '', 'minlength' => 6, 'placeholder' => 'New Password']) ?>
                    </div>
                    <div class="col-md-4"><label>Confirm Password</label></div>
                    <div class="col-md-8">
                        <?= $this->Form->control('Confirm_password', ['type' => 'password', 'label' => false, 'value' => '', 'placeholder' => 'Confirm Password']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20 text-center">
                <?= $this->Form->submit('Update', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<script>
    setTimeout(function() {
        tippy('.info-text', {
            content: 'Lorem ipsum dolor sit amet!',
        });
    }, 1400);

    /*
     * Google Map Initialize
     */

    function initializeSettingMap() {
        map = new google.maps.Map(document.getElementById("map"), {
            center: {
                lat: -33.8688,
                lng: 151.2195
            },
            zoom: 13,
            mapTypeId: "roadmap",
        });
        if ($('#lat').val().length && $('#lng').val().length) {
            let pos = new google.maps.LatLng($('#lat').val(), $('#lng').val());
            const bounds = new google.maps.LatLngBounds();
            bounds.extend(pos);
            placeMarker(pos, $('#chef-detail-address').val());
            map.fitBounds(bounds);
            var listener = google.maps.event.addListener(map, "idle", function() {
                if (map.getZoom() > 16)
                    map.setZoom(16);
                google.maps.event.removeListener(listener);
            });
        }
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById("chef-detail-address"), {
                types: ["geocode"]
            }
        );
        autocomplete.addListener("place_changed", () => {
            const place = autocomplete.getPlace();
            // Clear out the old markers.
            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();
            placeMarker(place.geometry.location, place.name);
            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            map.fitBounds(bounds);
        });
    }

    function placeMarker(location, address = null) {
        // Clear out the old markers.
        if (marker) {
            marker.setMap(null);
        }
        $('#lat').val(location.lat());
        $('#lng').val(location.lng());
        // Create a marker for each place.
        marker = new google.maps.Marker({
            map: map,
            icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
            },
            position: location,
            title: address,
            draggable: true,
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            var markerPosition = marker.getPosition()
            $('#lat').val(markerPosition.lng());
            $('#lng').val(markerPosition.lat());
        });
    }
    var password = document.getElementById("password"),
        confirm_password = document.getElementById("confirm-password");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>