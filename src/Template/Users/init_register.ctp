<?php
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/jquery.sumoselect.min.js', ['block' => true]);
$this->Html->css('https://cdn.jsdelivr.net/npm/icheck@1.0.2/skins/square/blue.css', ['block' => true]);
$this->Html->script('https://cdn.jsdelivr.net/npm/icheck@1.0.2/icheck.min.js', ['block' => true]);
$this->Html->script('https://maps.googleapis.com/maps/api/js?key=' . GMAP_API . '&libraries=places&callback=initialize', ['block' => 'script']);
$this->Html->css('https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css', ['block' => true]);
$this->Html->script('https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js', ['block' => 'script']);
$this->Html->scriptStart(['block' => true]);
echo "var SEND_OTP_URL = '" . $this->Url->build('/api/v1/users/send-otp/') . "';";
echo "var VERIFY_OTP_URL = '" . $this->Url->build('/api/v1/users/verify-otp/') . "';";
$this->Html->scriptEnd();
?>
<style>
    #mainContent {
        position: absolute;
        left: 0;
        top: 0;
    }
</style>

<div class="main-body-register">
    <div class="bg-blur"></div>
    <div class="register-modal-propagrub">
        <div class="layout-heading">
            <?= $this->Html->image('btn_back.png', ['onclick' => 'goBackRegister()', 'style' => 'display:none;position: absolute; left: 20px; width: 30px;cursor:pointer', 'id' => 'go-back']) ?>
            <?= $this->Html->image('login_logo.png', ['style' => 'height: 80%;']) ?>
        </div>
        <div class="layout-body">
            <?= $this->Form->create() ?>
            <?= $this->Form->hidden('service_type') ?>
            <?= $this->Form->hidden('own_driver') ?>
            <?= $this->Form->hidden('pickup_available') ?>
            <div id="stage-1">
                <div class="text-center my-3">
                    <?= $this->Html->image('diversity.png') ?>
                </div>
                <div class="row text-center">
                    <h3 class="col-12">Good to meet you, <?= $authUser['first_name'] ?></h3>
                    <p class="col-12 text-center">
                        Before we get started, we need to check if you're eligible to join Propagrub.
                    </p>
                    <hr class="col-11">
                    <?= $this->Flash->render() ?>
                </div>
                <div class="row">
                    <h5 class="col-12 font-weight-bold">Which best describes your business?</h5>
                    <div class="col-12">
                        <div class="layers bgc-white p-20 rounded bd my-2">
                            <div class="layer w-100 service-type" data-id="1">
                                <?= $this->Html->image('plus-mix-match.png', ['style' => 'width: 48px; height: 48px;']) ?>
                                <span class="font-weight-bold ml-3">Restaurant - </span><span>including takeaways, cafes and pubs</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="layers bgc-white p-20 rounded bd my-2">
                            <div class="layer w-100 service-type" data-id="2">
                                <?= $this->Html->image('grocery.png', ['style' => 'width: 48px; height: 48px;']) ?>
                                <span class="font-weight-bold ml-3">Grocery - </span><span>including convenience and alcohol shops</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="stage-2" style="display: none;">
                <div class="text-center my-3">
                    <?= $this->Html->image('rider-bicycle.png') ?>
                </div>
                <div class="row text-center">
                    <h3 class="col-12">Do you want Propagrub to deliver your orders?</h3>
                    <hr class="col-11">
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="layers bgc-white p-20 rounded bd my-2">
                            <div class="layer w-100 own-driver" data-id="1">
                                <?= $this->Html->image('rider-moped.png', ['style' => 'width: 48px; height: 48px;']) ?>
                                <span class="font-weight-bold ml-3">Yes - </span><span>I want hassle-free delivery by Propagrub </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="layers bgc-white p-20 rounded bd my-2">
                            <div class="layer w-100 own-driver" data-id="0">
                                <?= $this->Html->image('rider-moped-basic.png', ['style' => 'width: 48px; height: 48px;']) ?>
                                <span class="font-weight-bold ml-3">No - </span><span>I have my own delivery drivers</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="stage-2-1" style="display: none;">
                <div class="text-center my-3">
                    <?= $this->Html->image('coinjar-2.png') ?>
                </div>
                <div class="row text-center">
                    <h3 class="col-12">What's your base delivery fee?</h3>
                    <p class="col-12 text-center">
                        To begin with, we'll set your delivery distance to 5 kilometres and your minimum order to £10. You can change these (as well as your delivery fees) once you've joined Propagrub.
                    </p>
                    <hr class="col-11">
                </div>
                <div class="row">
                    <div class="col-12 price-input-product">
                        <div class="input-group-text bgc-white bd">£</div>
                        <?= $this->Form->control('delivery_fee', ['label' => 'Base delivery fee', 'placeholder' => '0.00', 'required' => true, 'type' => 'number', 'step' => '0.01']) ?>
                    </div>
                    <div class="col-12">
                        <?= $this->Html->link('Next', 'javascript:void(0);', ['class' => 'btn btn-primary w-100', 'id' => 'delivery-fee-btn', 'web' => true]) ?>
                    </div>
                </div>
            </div>
            <div id="stage-3" style="display: none;">
                <div class="text-center my-3">
                    <?= $this->Html->image('collection.png') ?>
                </div>
                <div class="row text-center">
                    <h3 class="col-12">Can customers pick up orders from you?</h3>
                    <hr class="col-11">
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="layers bgc-white p-20 rounded bd my-2">
                            <div class="layer w-100 pickup-available" data-id="1">
                                <?= $this->Html->image('rider-walking.png', ['style' => 'width: 48px; height: 48px;']) ?>
                                <span class="font-weight-bold ml-3">Yes - </span><span>I'm happy for customers to pick up orders</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="layers bgc-white p-20 rounded bd my-2">
                            <div class="layer w-100 pickup-available" data-id="0">
                                <?= $this->Html->image('rider-tandem.png', ['style' => 'width: 48px; height: 48px;']) ?>
                                <span class="font-weight-bold ml-3">No - </span><span>I just want delivery</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="stage-4" style="display: none;">
                <div class="my-3">
                    <?= $this->Html->image('business.png', ['class' => 'w-100']) ?>
                </div>
                <div class="row">
                    <h5 class="col-12 font-weight-bold">How is your business structured?</h5>
                    <div class="col-12">
                        <?= $this->Form->control('sole_trader', ['type' => 'radio', 'options' => ['Limited company', 'Sole trader'], 'default' => 0, 'label' => false]) ?>
                    </div>
                    <h5 class="col-12 font-weight-bold">Are you VAT registered?</h5>
                    <div class="col-12">
                        <?= $this->Form->control('vat_registered', ['type' => 'radio', 'options' => [1 => 'Yes', 0 => 'No'], 'default' => 1, 'label' => false]) ?>
                        <div id="company-name-div">
                            <?= $this->Form->control('company_name', ['label' => 'What\'s your registered company name?', 'placeholder' => 'E.g. Grub Limited', 'required' => true]) ?>
                            <p style="margin-top: -20px;">This is what you filed at <?= $this->Html->link('Companies House', 'https://www.gov.uk/government/organisations/companies-house', ['target' => '_BLANK']) ?>.</p>
                        </div>
                        <div id="vat-number-div">
                            <?= $this->Form->control('vat_number', ['label' => 'What\'s your VAT number?', 'placeholder' => 'E.g. GB 123456789', 'required' => true]) ?>
                            <p style="margin-top: -20px;">This is what you received from <?= $this->Html->link('HM Revenue and Customs (HMRC)', 'https://www.gov.uk/government/organisations/hm-revenue-customs', ['target' => '_BLANK']) ?></p>
                        </div>
                    </div>
                    <div class="col-12">
                        <?= $this->Html->link('Next', 'javascript:void(0);', ['class' => 'btn btn-primary w-100', 'id' => 'bussiness-deatil-btn', 'web' => true]) ?>
                    </div>
                </div>
            </div>
            <div id="stage-5" style="display: none;">
                <div class="my-3">
                    <?= $this->Html->image('restaurant.png', ['class' => 'w-100']) ?>
                </div>
                <div class="row">
                    <div class="col-12">
                        <?= $this->Form->control('resturant_name', ['label' => 'What\'s your restaurant called?', 'placeholder' => 'E.g. Brasserie Les Halles', 'required' => true]) ?>
                        <div class="row">
                            <div class="col-9">
                                <?= $this->Form->control('mobile_number', ['label' => 'What\'s your phone number (to reach you directly)?', 'placeholder' => 'E.g. +447632960093', 'required' => true, 'id' => 'mobile']) ?>
                            </div>
                            <div class="col-3 mt-4 pt-1">
                                <?= $this->Html->link('Send OTP', 'javascript:sendOTP()', ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="col-12 verify-otp-div" style="display: none;">
                                <?= $this->Form->control('otp', ['label' => 'Enter your OTP?', 'placeholder' => 'E.g. 123456', 'required' => true, 'id' => 'otp']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 required">
                        <label for="chef-cuisines" class="form">Select cuisines</label>
                        <?= $this->Form->control('chef_cuisines_ids', ['label' => false, 'options' => $chef_cuisines, 'id' => 'chef-cuisines', 'multiple' => true, 'required' => true]) ?>
                    </div>
                    <div class="col-12 required">
                        <label class="form">Select your store Address</label>
                        <?= $this->Form->hidden('lat', ['id' => 'lat']) ?>
                        <?= $this->Form->hidden('lng', ['id' => 'lng']) ?>
                        <?= $this->Form->control('address', ['label' => false]) ?>
                        <div id="map"></div>
                    </div>
                    <div class="col-12 mt-3">
                        <?= $this->Html->link('Next', 'javascript:void(0);', ['class' => 'btn btn-primary w-100', 'id' => 'resturent-detail-btn', 'web' => true]) ?>
                    </div>
                </div>
            </div>
            <div id="stage-6" style="display: none;">
                <div class="text-center my-3">
                    <?= $this->Html->image('collection.png') ?>
                </div>
                <div class="row text-center">
                    <h3 class="col-12">We need more information</h3>
                    <p class="col-12 text-center">
                        We weren't able to find all our details. Mind telling us a little bit more?
                    </p>
                    <hr class="col-11">
                </div>
                <div class="row">
                    <h5 class="col-12 font-weight-bold required">Do you have a hygiene rating?</h5>
                    <div class="col-12">
                        <?= $this->Form->control('is_hygiene', ['type' => 'radio', 'options' => [1 => 'Yes', 0 => 'No'], 'default' => 1, 'label' => false]) ?>
                    </div>
                    <div class="col-12" id="rating-div">
                        <?= $this->Form->control('rating', ['label' => 'What\'s your hygiene rating ID?', 'placeholder' => 'E.g. 514344', 'required' => true]) ?>
                        <p style="margin-top: -20px;">This is given to you by Food Standards Agency (FSA). Don't know it? <?= $this->Html->link('Here where you can find it', 'https://ratings.food.gov.uk/', ['target' => '_BLANK']) ?></p>
                    </div>
                    <div class="col-12 mt-3">
                        <?= $this->Html->link('Next', 'javascript:void(0);', ['class' => 'btn btn-primary w-100', 'id' => 'hygiene-detail-btn', 'web' => true]) ?>
                    </div>
                </div>
            </div>
            <div id="stage-7" style="display: none;">
                <div class="row text-center">
                    <h3 class="col-12">Confirm your details</h3>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label class="form w-100">Resturant name</label>
                        <p id="resturent-name-text" class="w-100">Propagrub LTD</p>
                        <?= $this->Html->link('<i class="ti-pencil"></i>', 'javascript:editResturent()', ['escape' => false, 'style' => 'position: absolute; right: 0; top: 0;']) ?>
                    </div>
                    <div class="col-12">
                        <label class="form w-100">Address</label>
                        <p id="address-text" class="w-100">20-22 Wenlock Road</p>
                        <?= $this->Html->link('<i class="ti-pencil"></i>', 'javascript:editResturent()', ['escape' => false, 'style' => 'position: absolute; right: 0; top: 0;']) ?>
                    </div>
                    <div class="col-12">
                        <label class="form w-100">Email</label>
                        <p class="w-100"><?= $authUser['email'] ?></p>
                    </div>
                    <div class="col-12">
                        <label class="form w-100">Phone</label>
                        <p id="mobile-number-text" class="w-100">+44 7970441122</p>
                        <?= $this->Html->link('<i class="ti-pencil"></i>', 'javascript:editResturent()', ['escape' => false, 'style' => 'position: absolute; right: 0; top: 0;']) ?>
                    </div>
                    <div class="col-12" id="rating-div-text">
                        <label class="form w-100">Hygiene rating ID</label>
                        <p id="rating-text" class="w-100">123456</p>
                        <?= $this->Html->link('<i class="ti-pencil"></i>', 'javascript:editHygiene()', ['escape' => false, 'style' => 'position: absolute; right: 0; top: 0;']) ?>
                    </div>
                    <div class="col-12 mt-3">
                        <?= $this->Form->submit('Next', ['class' => 'btn btn-primary w-100']) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <div class="layout-footer">
            <div class="sign-up-progress">
                <div class="signup-total-progress" style="width: 20%;"></div>
            </div>
        </div>
    </div>
</div>