<style>
    #mainContent {
        position: absolute;
        left: 0;
        top: 0;
    }
</style>

<div class="main-body-register">
    <div class="bg-blur"></div>
    <div class="register-modal-propagrub">
        <div class="layout-heading">
            <?= $this->Html->image('login_logo.png', ['style' => 'height: 80%;']) ?>
        </div>
        <div class="layout-body">
            <div class="text-center my-3">
                <?= $this->Html->image('problem-2.png') ?>
            </div>
            <p class="text-center">
                Propagrub can help you reach new customers, promote your brand and get useful insights from data. We're much more than a delivery company - we're your partner in growth.
            </p>
            <p class="text-center my-3">
                Already have a Propagrub account? <?= $this->Html->link('Log in to continue', '/', ['web' => true]) ?>
            </p>
            <hr class="col-11">
            <?= $this->Form->create() ?>
            <?= $this->Flash->render() ?>
            <div class="row">
                <div class="col-12 text-center mb-4">
                    <label for="image" style="cursor: pointer;">
                        <?= $this->Html->image('profile-upload.png', ['style' => 'height: 100px; width: 100px; object-fit: cover; border-radius: 100%;', 'id' => 'thumbnail-image', 'default-image' => $this->Url->image('profile-upload.png')]) ?>
                    </label>
                </div>
                <div class="d-none">
                    <?= $this->Form->control('image', ['required' => false, 'type' => 'file', 'label' => false, 'accept' => 'image/*']) ?>
                </div>
                <div class="col-12">
                    <label for="first-name" class="form required">What's your first and last name?</label>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('first_name', ['label' => false, 'placeholder' => 'E.g. Anthony', 'required' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('last_name', ['label' => false, 'placeholder' => 'E.g. Bourdain', 'required' => true]) ?>
                </div>
                <div class="col-12">
                    <?= $this->Form->control('username', ['label' => 'What\'s your username?', 'placeholder' => 'E.g. @Charlie', 'required' => true, 'onkeyup' => "this.value = this.value.replace(/[^a-zA-Z0-9_.]/g, '')"]) ?>
                </div>
                <div class="col-12">
                    <?= $this->Form->control('email', ['label' => 'What\'s your email address?', 'placeholder' => 'E.g. your.name@company.com', 'required' => true]) ?>
                </div>
                <div class="col-12">
                    <?= $this->Form->control('password', ['label' => 'Create a password', 'placeholder' => 'E.g. ********************', 'required' => true]) ?>
                    <p style="margin-top: -20px;">Minimum 10 characters</p>
                </div>
                <hr class="col-11">
                <div class="col-12 my-3">
                    <?= $this->Form->submit('Next', ['class' => 'btn btn-primary w-100']) ?>
                </div>
                <p class="col-12">For details on our processing of your personal information please see our <?= $this->Html->link('Privacy Policy', 'https://propagrub.com') ?>.</p>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>