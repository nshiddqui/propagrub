<?php $companyName = 'Propa'; ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= $this->fetch('meta') ?>
    <title>
        <?= $companyName ?>:
        <?= $this->fetch('title') ?>
    </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Google Font -->
    <?= $this->Html->css('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap') ?>
    <?= $this->Html->css('https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css') ?>
    <?= $this->Html->css('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css') ?>
    <?= $this->Html->css('login') ?>
    <!-- AdminLTE App -->
    <?= $this->Html->script('https://code.jquery.com/jquery-3.4.1.min.js') ?>
    <?= $this->Html->script('https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js') ?>
    <?= $this->Html->script('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js') ?>
    <?= $this->Html->script('page-transition') ?>
    <script>
        var IP_ADDRESS = '<?= $ip_address ?>';
        $(document).ready(function() {

            pageTransition({
                target: document.querySelector('.page'),
                delay: 0,
                duration: 500,
                classIn: 'fadeIn',
                classOut: 'fadeOut',
                classActive: 'animated',
                conditions: function(event, link) {
                    return link && !/(\#|callto:|tel:|mailto:|:\/\/)/.test(link) && !event.currentTarget.hasAttribute('data-lightgallery');
                },
                onTransitionStart: function(options) {
                    setTimeout(function() {
                        $('.preloader').removeClass('loaded');
                    }, options.duration * .75);
                },
                onReady: function() {
                    $('.preloader').addClass('loaded');
                    windowReady = true;
                }
            });
            $('form').on('submit', function() {
                setTimeout(function() {
                    $('.preloader').removeClass('loaded');
                }, 30);
            });
        });
    </script>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?php
    if (file_exists(WWW_ROOT . 'css/' . strtolower($params['controller']) . '.css')) {
        echo $this->Html->css(strtolower($params['controller']));
    }
    if (file_exists(WWW_ROOT . 'js/' . strtolower($params['controller'])) . '.js') {
        echo $this->Html->script(strtolower($params['controller']));
    }
    ?>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->

<body>
    <div class="preloader">
        <div class="preloader-body">
            <div class="cssload-container">
                <div class="cssload-speeding-wheel"></div>
            </div>
            <p>Loading...</p>
        </div>
    </div>
    <video autoplay muted loop id="background-video">
        <source src="https://res.cloudinary.com/dyxmzm398/video/upload/v1632980811/propa_intro.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
    <main class="d-flex align-items-center min-vh-100 py-3 py-md-0 page">
        <div class="container">
            <?= $this->fetch('content') ?>
        </div>
    </main>
</body>

</html>