<?php $companyName = 'PropaGrub'; ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= $this->fetch('meta') ?>
    <title>
        <?= $companyName ?>:
        <?= $this->fetch('title') ?>
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <style>
        #loader {
            transition: all 0.3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000;
        }

        #loader.fadeOut {
            opacity: 0;
            visibility: hidden;
        }

        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1.0s infinite ease-in-out;
            animation: sk-scaleout 1.0s infinite ease-in-out;
        }

        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }

            100% {
                -webkit-transform: scale(1.0);
                opacity: 0;
            }
        }

        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }

            100% {
                -webkit-transform: scale(1.0);
                transform: scale(1.0);
                opacity: 0;
            }
        }
    </style>

    <!-- Main CSS-->
    <?= $this->Html->css('https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css') ?>
    <?= $this->Html->component('fancybox/dist/jquery.fancybox.min', 'css') ?>
    <?= $this->Html->css('adminator/style') ?>
    <?= $this->Html->css('style') ?>
    <?= $this->Html->css('https://unpkg.com/@codolog/form@1.0.0/dist/form.min.css') ?>
    <?= $this->fetch('css') ?>
    <?php
    if (file_exists(WWW_ROOT . 'css/' . strtolower($params['controller']) . '.css')) {
        echo $this->Html->css(strtolower($params['controller']));
    }
    ?>

    <script>
        var IP_ADDRESS = '<?= $ip_address ?>';
    </script>
    <style>
        body {
            color: #212529 !important;
        }
    </style>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->

<body class="app">
    <!-- @TOC -->
    <!-- =================================================== -->
    <!--
      + @Page Loader
      + @App Content
          - #Left Sidebar
              > $Sidebar Header
              > $Sidebar Menu
          - #Main
              > $Topbar
              > $App Screen Content
    -->

    <!-- @Page Loader -->
    <!-- =================================================== -->
    <div id="loader">
        <div class="spinner"></div>
    </div>

    <script>
        window.addEventListener('load', function load() {
            const loader = document.getElementById('loader');
            setTimeout(function() {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>

    <!-- @App Content -->
    <!-- =================================================== -->
    <div>
        <!-- #Left Sidebar ==================== -->
        <div class="sidebar">
            <div class="sidebar-inner">
                <!-- ### $Sidebar Header ### -->
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="#">
                                <div class="peers ai-c fxw-nw">
                                    <?= $this->Html->image('login_logo.png', ['class' => 'w-75 m-auto py-3']) ?>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle">
                                <a href="" class="td-n">
                                    <i class="ti-arrow-circle-left"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ### $Sidebar Menu ### -->
                <ul class="sidebar-menu scrollable pos-r">
                    <?= $this->Html->image('bg-nav.png', ['class' => 'w-100', 'style' => 'margin-top: -75px;']) ?>
                </ul>
            </div>
        </div>

        <!-- #Main ============================ -->
        <div class="page-container">
            <?= $this->element('header') ?>
            <!-- ### $App Screen Content ### -->
            <main class="main-content bgc-grey-100" style="background-image: url('<?= $this->Url->build('/img/bg-cards.svg') ?>');">
                <div id="mainContent">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </main>
            <?= $this->element('footer') ?>
        </div>
        <?= $this->Html->script('https://code.jquery.com/jquery-3.6.0.min.js') ?>
        <?= $this->Html->script('https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js') ?>
        <?= $this->Html->script('adminator/vendor') ?>
        <?= $this->Html->script('adminator/bundle') ?>
        <?= $this->Html->component('fancybox/dist/jquery.fancybox.min', 'script') ?>
        <?= $this->Html->script('initialization') ?>
        <?php
        if (file_exists(WWW_ROOT . 'js/' . strtolower($params['controller']) . '.js')) {
            echo $this->Html->script(strtolower($params['controller']));
        }
        ?>

        <!-- Specific Page JS goes HERE -->
        <?= $this->fetch('script') ?>
</body>

</html>