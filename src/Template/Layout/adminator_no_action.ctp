<?php $companyName = 'PropaGrub'; ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= $this->fetch('meta') ?>
    <title>
        <?= $companyName ?>:
        <?= $this->fetch('title') ?>
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <style>
        #loader {
            transition: all 0.3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000;
        }

        #loader.fadeOut {
            opacity: 0;
            visibility: hidden;
        }

        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1.0s infinite ease-in-out;
            animation: sk-scaleout 1.0s infinite ease-in-out;
        }

        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }

            100% {
                -webkit-transform: scale(1.0);
                opacity: 0;
            }
        }

        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }

            100% {
                -webkit-transform: scale(1.0);
                transform: scale(1.0);
                opacity: 0;
            }
        }
    </style>

    <!-- Main CSS-->
    <?= $this->Html->css('https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css') ?>
    <?= $this->Html->component('fancybox/dist/jquery.fancybox.min', 'css') ?>
    <?= $this->Html->css('adminator/style') ?>
    <?= $this->Html->css('style') ?>
    <?= $this->Html->css('https://unpkg.com/@codolog/form@1.0.0/dist/form.min.css') ?>
    <?= $this->fetch('css') ?>
    <?php
    if (file_exists(WWW_ROOT . 'css/' . strtolower($params['controller']) . '.css')) {
        echo $this->Html->css(strtolower($params['controller']));
    }
    ?>

    <script>
        var IP_ADDRESS = '<?= $ip_address ?>';
    </script>
    <style>
        body {
            color: #212529 !important;
        }
    </style>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->

<body class="app">
    <!-- @TOC -->
    <!-- =================================================== -->
    <!--
      + @Page Loader
      + @App Content
          - #Left Sidebar
              > $Sidebar Header
              > $Sidebar Menu
          - #Main
              > $Topbar
              > $App Screen Content
    -->

    <!-- @Page Loader -->
    <!-- =================================================== -->
    <div id="loader">
        <div class="spinner"></div>
    </div>

    <script>
        window.addEventListener('load', function load() {
            const loader = document.getElementById('loader');
            setTimeout(function() {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>

    <!-- @App Content -->
    <!-- =================================================== -->
    <div>
        <!-- #Left Sidebar ==================== -->
        <div class="sidebar">
            <div class="sidebar-inner">
                <!-- ### $Sidebar Header ### -->
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="#">
                                <div class="peers ai-c fxw-nw">
                                    <?= $this->Html->image('login_logo.png', ['class' => 'w-75 m-auto py-3']) ?>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle">
                                <a href="" class="td-n">
                                    <i class="ti-arrow-circle-left"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ### $Sidebar Menu ### -->
                <ul class="sidebar-menu scrollable pos-r">
                    <!-- #Left Sidebar ==================== -->
                    <div class="sidebar">
                        <div class="sidebar-inner">
                            <!-- ### $Sidebar Header ### -->
                            <div class="sidebar-logo">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer peer-greed">
                                        <a class="sidebar-link td-n" href="#">
                                            <div class="peers ai-c fxw-nw">
                                                <?= $this->Html->image('login_logo.png', ['class' => 'w-75 m-auto py-3']) ?>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="peer">
                                        <div class="mobile-toggle sidebar-toggle">
                                            <a href="" class="td-n">
                                                <i class="ti-arrow-circle-left"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- ### $Sidebar Menu ### -->
                            <ul class="sidebar-menu scrollable pos-r">
                                <li class="nav-item mT-30 actived">
                                    <?= $this->Html->link('<span class="icon-holder"> <i class="c-blue-500 menu-home"></i> </span> <span class="title">Home</span>', 'javascript:void(0)', ['escape' => false, 'class' => 'sidebar-link']) ?>
                                </li>
                                <li class="nav-item">
                                    <?= $this->Html->link('<span class="icon-holder"> <i class="c-brown-500 menu-rating-disable"></i> </span> <span class="title">Ratings and reviews</span>' . $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']),  'javascript:void(0)', ['escape' => false, 'class' => 'sidebar-link']) ?>
                                </li>
                                <li class="nav-item">
                                    <a class="sidebar-link" href="#">
                                        <span class="icon-holder">
                                            <i class="c-blue-500 menu-sales-disable"></i>
                                        </span>
                                        <span class="title">Sales</span>
                                        <?= $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']) ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="sidebar-link" href="#">
                                        <span class="icon-holder">
                                            <i class="c-deep-orange-500 menu-live-orders-disable"></i>
                                        </span>
                                        <span class="title">Live orders</span>
                                        <?= $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']) ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <?= $this->Html->link('<span class="icon-holder"> <i class="c-deep-purple-500 menu-menus-disable"></i> </span> <span class="title">Menus</span>' . $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']), 'javascript:void(0)', ['escape' => false, 'class' => 'sidebar-link']) ?>
                                </li>
                                <li class="nav-item">
                                    <?= $this->Html->link('<span class="icon-holder"> <i class="c-deep-purple-500 menu-invoice-disable"></i> </span> <span class="title">Delievery / Collection</span>' . $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']), 'javascript:void(0)', ['escape' => false, 'class' => 'sidebar-link']) ?>
                                </li>
                                <li class="nav-item">
                                    <a class="sidebar-link" href="#">
                                        <span class="icon-holder">
                                            <i class="c-light-blue-500 menu-invoice-disable"></i>
                                        </span>
                                        <span class="title">Invoices</span>
                                        <?= $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']) ?>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="sidebar-link" href="javascript:void(0);">
                                        <span class="icon-holder">
                                            <i class="c-orange-500 menu-marketer-disable"></i>
                                        </span>
                                        <span class="title">Marketer</span>
                                        <?= $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']) ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <?= $this->Html->link('<span class="icon-holder"> <i class="c-pink-500 menu-feedback-disable"></i> </span> <span class="title">Feedback</span>' . $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']), 'javascript:void(0)', ['escape' => false, 'class' => 'sidebar-link']) ?>
                                </li>
                                <li class="nav-item profile-menu">
                                    <?php
                                    if ($authUser['image'] && !empty($authUser['image'])) {
                                        $userImage = $this->Html->image($authUser['image'], ['style' => 'height:35px;width:35px;border-radius:100%;', 'profile' => WEB_PROFILE_IMAGE]);
                                    } else {
                                        $userImage = '<i class="c-pink-500 menu-user"></i>';
                                    }
                                    ?>
                                    <?= $this->Html->link('<span class="icon-holder"> ' . $userImage . '</i> </span> <span class="title">' . $authUser['first_name'] . ' ' . $authUser['last_name'] . '</span>' . $this->Html->image('icon_lock.png', ['style' => 'height:18px;margin-left:5px;']), 'javascript:void(0)', ['escape' => false, 'class' => 'sidebar-link']) ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </ul>
            </div>
        </div>

        <!-- #Main ============================ -->
        <div class="page-container">
            <?= $this->element('header') ?>
            <!-- ### $App Screen Content ### -->
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </main>
            <?= $this->element('footer') ?>
        </div>
        <?= $this->Html->script('https://code.jquery.com/jquery-3.6.0.min.js') ?>
        <?= $this->Html->script('https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js') ?>
        <?= $this->Html->script('adminator/vendor') ?>
        <?= $this->Html->script('adminator/bundle') ?>
        <?= $this->Html->component('fancybox/dist/jquery.fancybox.min', 'script') ?>
        <?= $this->Html->script('initialization') ?>
        <?php
        if (file_exists(WWW_ROOT . 'js/' . strtolower($params['controller']) . '.js')) {
            echo $this->Html->script(strtolower($params['controller']));
        }
        ?>

        <!-- Specific Page JS goes HERE -->
        <?= $this->fetch('script') ?>
</body>

</html>