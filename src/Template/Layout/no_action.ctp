<?php $companyName = 'Propa'; ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= $this->fetch('meta') ?>
    <title>
        <?= $companyName ?>:
        <?= $this->fetch('title') ?>
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <?= $this->Html->component('bootstrap/dist/css/bootstrap.min', 'css') ?>
    <!-- Font Awesome -->
    <?= $this->Html->component('font-awesome/css/font-awesome.min', 'css') ?>
    <!-- Ionicons -->
    <?= $this->Html->component('Ionicons/css/ionicons.min', 'css') ?>
    <!-- Theme style -->
    <?= $this->Html->css('AdminLTE.min') ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    <!-- Google Font -->
    <?= $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') ?>
    <style>
        .modal-dialog {
            top: 50% !important;
            transform: translate(0, -50%) !important;
            -ms-transform: translate(0, -50%) !important;
            -webkit-transform: translate(0, -50%) !important;
        }
    </style>
</head>

<body class="hold-transition login-page" style="background: #D60876">
    <?= $this->Form->postLink('Logout <i class="fa fa-sign-out"></i>', ['controller' => 'users', 'action' => 'logout'], ['escape' => false, 'confirm' => 'Are you sure want to logout?', 'class' => 'btn btn-primary', 'style' => 'position:fixed;right:25px;top:5px;z-index:9999;']) ?>
    <div class="modal fade" id="payment-box" tabindex="-1" role="dialog" aria-hidden="true" style="background: #D60876">
        <div class="modal-dialog" style="width: 420px;">
            <div class="modal-content" style="border-radius: 15px;">
                <div class="text-center">
                    <?= $this->Html->image('login_logo.png', ['alt' => 'logo', 'class' => 'logo', 'style' => 'height: 45px;margin: 10px auto']) ?>
                </div>
                <?= $this->fetch('content') ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- jQuery 3 -->
    <?= $this->Html->component('jquery/dist/jquery.min', 'script') ?>
    <!-- Bootstrap 3.3.7 -->
    <?= $this->Html->component('bootstrap/dist/js/bootstrap.min', 'script') ?>
    <script>
        $('#payment-box').modal({
            backdrop: 'static',
            keyboard: false
        })
    </script>
</body>

</html>