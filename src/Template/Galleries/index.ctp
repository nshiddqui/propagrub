<?php
$this->assign('title', 'Galleries');
?>
<div class="card">
    <div class="card-header">
        <h3>Upload Image</h3>
    </div>
    <div class="card-body">
        <?= $this->Form->create($gallery, ['id' => 'upload-gallery', 'type' => 'file']) ?>
        <div class="dropzone">
            <img src="http://100dayscss.com/codepen/upload.svg" class="upload-icon" />
            <?= $this->Form->control('image', ['type' => 'file', 'label' => false, 'class' => 'upload-input', 'accept' => 'image/*']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="card-body">
        <div class="row">
            <?php foreach ($galleries as $gallery): ?>
                <div class="image-box">
                    <a href="<?= WEB_GALLERY . $gallery->image ?>" fancybox>
                        <?= $this->Html->image($gallery->image, ['profile' => WEB_GALLERY, 'class' => 'img-gallery']) ?>
                    </a>
                    <?= $this->Form->postLink('X', ['action' => 'delete', $gallery->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gallery->id), 'class' => 'image-cross']) ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if ($galleries->count()) { ?>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        <?php } ?>
    </div>
</div>