<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h($result->offer_title),
        h($result->offer_value),
        $offer_type[$result->offer_type],
        h($result->min_amount),
        $result->offer_exp_date,
        h($result->promo_code),
        $this->Html->link('<i class="fa fa-pencil fa-lg"></i>', ['action' => 'index', $result->id], ['escape' => false, 'class' => 'text-black']) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->client_name), 'escape' => false, 'class' => 'text-black'])
    ]);
}
echo $this->DataTables->response();
