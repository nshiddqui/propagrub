<?php
$this->assign('title', 'Chef Offers Details');
?>
<?= $this->Html->component('moment/moment', 'script', ['block' => true]) ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js', ['block' => true]) ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css', ['block' => true]) ?>
<div class="card">
    <div class="card-header">
        <h3>Add Chef Offers Details</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <?= $this->DataTables->render('ChefOffers') ?>
            </div>
            <div class="col-md-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= !empty($chef_offer->id) ? 'Edit' : 'Add New' ?> Chef Offers</h3>                    
                            <?= $this->Form->create($chef_offer) ?>
                            <?= $this->Form->control('offer_type', ['options' => $offer_type]) ?>
                            <?= $this->Form->control('offer_title') ?>
                            <?= $this->Form->control('offer_value') ?>
                            <?= $this->Form->control('min_amount') ?>
                            <?= $this->Form->control('offer_exp_date', ['type' => 'text', 'value' => !empty($chef_offer->offer_exp_date) ? date('Y-m-d h:i:s', strtotime($chef_offer->offer_exp_date)) : '']) ?>
                            <div class="row">
                                <div class="col-md-8">
                                    <?= $this->Form->control('promo_code', ['value' => !empty($chef_offer->promo_code) ? $chef_offer->promo_code : 'PB']) ?>
                                </div>
                                <div class="col-md-4">
                                    <label>&nbsp;</label>
                                    <button type="button" class="btn btn-info" id="generate-code">Generate</button>
                                </div>
                            </div>

                            <div class="text-center">
                                <?= $this->Form->submit('Save') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>