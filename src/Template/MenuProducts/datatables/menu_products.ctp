<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        $this->Html->image((!empty($result['menu_products_images']) ? $result['menu_products_images'][0]->image : 'product_placeholder.png'), ['profile' => WEB_PRODUCT, 'style' => 'height: 45px; width: 45px', 'class' => 'img-circle', 'onerror' => '/img/product_placeholder.png']),
        h($result->product_name),
        !empty($result['product_ingredients']) ? $result['product_ingredients'][0]->count : '0',
        '£ ' . $result->product_price,
        h($result['menu']->menu_name),
        h($result['menu_category']->category_name),
        $this->Html->link('<i class="fa fa-pencil fa-lg"></i>', ['action' => 'index', $result->id], ['escape' => false, 'class' => 'text-black']) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->client_name), 'escape' => false, 'class' => 'text-black'])
    ]);
}
echo $this->DataTables->response();
