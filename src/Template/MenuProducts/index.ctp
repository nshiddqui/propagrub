<?php
$this->assign('title', 'Manage Menu Products');
?>
<div class="card">
    <div class="card-header">
        <h3>Add/ View Menu Products</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <?= $this->Form->control('menu_filter', ['label' => 'Select Menu', 'value' => $menu_filter, 'options' => $menu, 'empty' => 'All Menu', 'label' => false]) ?>  
                    </div>
                </div>
                <?= $this->DataTables->render('MenuProducts') ?>
            </div>
            <div class="col-md-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= !empty($menu_products->id) ? 'Edit' : 'Add New' ?> Menu Products</h3>
                            <?= $this->Form->create($menu_products, ['type' => 'file']) ?>
                            <label for="menu-products-images-0-image" class="border-black">
                                <?= $this->Html->image((!empty($menu_products['menu_products_images']) ? $menu_products['menu_products_images'][0]->image : 'product_placeholder.png'), ['class' => 'thumbnail-image', 'profile' => WEB_PRODUCT, 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'product_placeholder.png', 'onerror' => '/img/product_placeholder.png'])]) ?>
                            </label>
                            <div class="hidden">
                                <?= $this->Form->control('menu_products_images.0.id') ?>
                                <?= $this->Form->control('menu_products_images.0.image', ['type' => 'file', 'label' => false, 'accept' => 'image/*', 'required' => false, 'need' => empty($menu_products['menu_products_images']) ? '1' : '0']) ?>
                            </div>
                            <?= $this->Form->control('menu_id', ['options' => $menu]) ?>
                            <?= $this->Form->control('category_id', ['options' => $menu_category]) ?>
                            <?= $this->Form->control('product_name') ?>
                            <div class="usd-parent">
                                <i class="usd icon">£</i>
                                <?= $this->Form->control('product_price', ['type' => 'number']) ?>
                            </div>
                            <?= $this->Form->control('product_detail', ['type' => 'textarea']) ?>
                            <div class="text-center">
                                <?= $this->Form->submit('Save') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>