<?= $this->Form->create($menu_products, ['id' => 'modal-form', 'type' => 'file']) ?>
<div class="layers bd bgc-white p-10 rounded">
    <div class="layer w-100">
        <div class="row">
            <div class="col-md-7">
                <div class="image-placeholder text-center" id="product-image-empty">
                    <?= $this->Html->image('placeholder_camera.png') ?>
                    <h4 class="text-dark mb-0">No photo</h4>
                    <p class="text-dark">1200 x 800 pixels (.jpg - 18MB)</p>
                    <label for="menu-products-images-0-image" class="btn btn-default bgc-white bd text-primary">Upload photo</label>
                    <div class="d-none">
                        <?= $this->Form->control('menu_products_images.0.image', ['type' => 'file', 'accept' => 'image/*', 'required' => false]) ?>
                    </div>
                </div>
                <label style="display: none;" class="image-placeholder" for="menu-products-images-0-image" id="product-image">

                </label>
            </div>
            <div class="col-md-5">
                <div class="my-4 py-2 pl-2">
                    <p class="h4 font-weight-normal">Make sure item is:</p>
                    <p class="mb-0"><?= $this->Html->image('grey_menus.png', ['class' => 'menu-menus-image']) ?>Ready to server</p>
                    <p class="mb-0"><?= $this->Html->image('grey_light.png', ['class' => 'menu-light-image']) ?>Brightly lit</p>
                    <p><?= $this->Html->image('menu_grey_full_screen.png', ['class' => 'menu-screen-image']) ?>Large and cnetered</p>
                    <a href="javascript:void(0)">See examples</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-12">
        <?= $this->Form->control('product_name', ['placeholder' => 'E.g. Blueberry pancakes', 'label' => 'Name']) ?>
        <div class="text-option">
            <?= $this->Form->control('internal_name', ['placeholder' => 'E.g. Carol C Special']) ?>
            <p>Customers won't see this in the app. It's just for you and will be printed on the ticket.</p>
        </div>
        <div class="text-option">
            <?= $this->Form->control('product_detail', ['type' => 'textarea', 'placeholder' => 'E.g. We use real buttermilk to give these American-style pancakes lots of fluffiness', 'label' => 'Description']) ?>
            <p>This should give customers all the information they need to understand the dish.</p>
        </div>
        <div class="text-option" style="position: relative;">
            <?= $this->Form->control('estimated_preparation_time', ['placeholder' => 'E.g. 20 Minutes', 'type' => 'number']) ?>
            <div style="position: absolute;top: 25px;right: 25px;">Minutes</div>
        </div>
        <?php if ($menu_id  == 0) { ?>
            <div class="mt-3 price-input-product" style="position: relative;">
                <div class="input-group-text bgc-white bd" style="left: 0;">£</div>
                <?= $this->Form->control('shipping_charges', ['placeholder' => '0.00', 'label' => 'Shipping Charges', 'type' => 'number']) ?>
                <?= $this->Form->hidden('is_shippable', ['value' => 1]) ?>
            </div>
        <?php } ?>
        <?= $this->Form->control('allergen_id', ['options' => $allergens, 'multiple' => true]) ?>
        <?= $this->Form->control('is_allergens', ['type' => 'checkbox', 'label' => 'This item contains no known allergens']) ?>
        <div class="row">
            <div class="col-md-6 price-input-product">
                <div class="input-group-text bgc-white bd">£</div>
                <?= $this->Form->control('product_price', ['placeholder' => '0.00', 'label' => 'Price', 'type' => 'number']) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('product_tax', ['options' => [0 => '0%', 5 => '5%', 12.5 => '12.5%', 20 => '20%'], 'label' => 'Tax rate']) ?>
            </div>
        </div>
        <?= $this->Form->control('is_age_restricted', ['type' => 'radio', 'options' => ['No', 'Yes'], 'default' => 0, 'label' => 'Is this an age-restricted product, like alcohol or tobacco? You must let us know, so we can verify the customer\'s age.']) ?>
    </div>
</div>
<?= $this->Form->end() ?>