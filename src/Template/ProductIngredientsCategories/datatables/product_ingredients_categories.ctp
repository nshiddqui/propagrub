<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h($result->ingredients_category_name),
        $this->Html->link('<i class="fa fa-pencil fa-lg"></i>', ['action' => 'index', $result->id], ['escape' => false, 'class' => 'text-black']) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->ingredients_category_name), 'escape' => false, 'class' => 'text-black'])
    ]);
}
echo $this->DataTables->response();
