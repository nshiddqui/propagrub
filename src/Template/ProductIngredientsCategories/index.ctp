<h5 class="text-dark">Options and upgrades</h5>
<p class="text-dark">Let customers make choices about their item - for example, picking a flavor, adding a topping or upgrading with sides and drinks.</p>
<table class="table w-100 table-bordered">
    <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Options</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($options as $option) { ?>
            <tr>
                <td><?= $option->ingredients_category_name ?></td>
                <td style="white-space: nowrap;max-width: 35vw;" class="text-truncate"><?= @$option->list ?></td>
                <td><a href="javascript:editOption(<?= $option->product_category_id ?>,<?= $option->id ?>)"><i class="ti-pencil"></i></a></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<h5 class="text-dark">Add an Options</h5>
<?= $this->Form->create($ingredients_category, ['id' => 'option-modal-form']) ?>
<?= $this->Form->control('ingredients_category_name', ['label' => 'Name', 'placeholder' => 'Name']) ?>
<?= $this->Form->control('ingredients_category_detail', ['type' => 'textarea', 'placeholder' => 'E.g. We use real buttermilk to give these American-style pancakes lots of fluffiness', 'label' => 'Description']) ?>
<table class="table w-100 table-bordered">
    <thead>
        <tr>
            <th scope="col">Options</th>
            <th scope="col">Price(£)</th>
            <th scope="col">Tax rate</th>
            <?php if (!empty($ingredients_category->id)) { ?>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody id="clone-body">
        <tr class="d-none" id="clone-row">
            <td>
                <?= $this->Form->hidden('product_ingredients_categories.id[]', ['value' => '']) ?>
                <?= $this->Form->control('product_ingredients_categories.name[]', ['placeholder' => 'Name', 'label' => 'Name']) ?></td>
            <td style="width: 30vh;"><?= $this->Form->control('product_ingredients_categories.price[]', ['placeholder' => 'Price', 'type' => 'number', 'label' => 'Price']) ?></td>
            <td style="width: 30vh;"><?= $this->Form->control('product_ingredients_categories.tax[]', ['options' => [0 => '0%', 5 => '5%', 12.5 => '12.5%', 20 => '20%'], 'label' => 'Tax rate', 'class' => 'sumo-select-custom']) ?></td>
            <td style="width: 10vh;"><a href="#" onclick="deleteIngredents(this,0)"><i class="ti-trash"></i></a></td>
        </tr>
        <?php
        foreach ((array)$ingredients_category['ingredients'] as $ingredients) {
        ?>
            <tr>
                <td>
                    <?= $this->Form->hidden('product_ingredients_categories.id[]', ['value' => $ingredients->id]) ?>
                    <?= $this->Form->control('product_ingredients_categories.name[]', ['placeholder' => 'Name', 'value' => $ingredients->name, 'label' => 'Name']) ?></td>
                <td style="width: 30vh;"><?= $this->Form->control('product_ingredients_categories.price[]', ['value' => $ingredients->price, 'placeholder' => 'Price', 'type' => 'number', 'label' => 'Price']) ?></td>
                <td style="width: 30vh;"><?= $this->Form->control('product_ingredients_categories.tax[]', ['value' => $ingredients->tax, 'options' => [0 => '0%', 5 => '5%', 12.5 => '12.5%', 20 => '20%'], 'label' => 'Tax rate']) ?></td>
                <td style="width: 10vh;"><a href="#" onclick="deleteIngredents(this,<?= $ingredients->id ?>)"><i class="ti-trash"></i></a></td>
            </tr>
        <?php
        }
        ?>
        <tr>
            <td>
                <?= $this->Form->hidden('product_ingredients_categories.id[]', ['value' => '']) ?>
                <?= $this->Form->control('product_ingredients_categories.name[]', ['placeholder' => 'Name', 'label' => 'Name']) ?>
            </td>
            <td style="width: 30vh;"><?= $this->Form->control('product_ingredients_categories.price[]', ['placeholder' => 'Price', 'type' => 'number', 'label' => 'Price']) ?></td>
            <td style="width: 30vh;"><?= $this->Form->control('product_ingredients_categories.tax[]', ['options' => [0 => '0%', 5 => '5%', 12.5 => '12.5%', 20 => '20%'], 'label' => 'Tax rate']) ?></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3" onclick="addIngredient()">+ Add another</td>
        </tr>
    </tfoot>
</table>
<?= $this->Form->control('allergen_id', ['options' => $allergens, 'value' => explode(',', $ingredients_category->allergen_id), 'multiple' => true]) ?>
<?= $this->Form->control('is_allergens', ['type' => 'checkbox', 'label' => 'This item contains no known allergens']) ?>
<div class="form-group ">
    <label>How many of these options can customers choose?</label>
    <div class="row px-3">
        <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
            <?= $this->Form->radio('choose_range', ['any' => 'Any Number'], ['hiddenField' => false, 'id' => 'range-1']) ?>
        </div>
        <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
            <div class="row">
                <div class="col-md-8">
                    <?= $this->Form->radio('choose_range', ['custom' => 'Range'], ['hiddenField' => false, 'id' => 'range-2', 'default' => 'custom']) ?>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $this->Form->control('start_range', ['label' => false, 'type' => 'number', 'placeholder' => 'Start', 'class' => 'form number mb-0', 'templates' => ['inputContainer' => '<div class="form-group mb-0 {{required}}">{{content}}</div>',]]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('end_range', ['label' => false, 'type' => 'number', 'placeholder' => 'End', 'class' => 'form number mb-0', 'templates' => ['inputContainer' => '<div class="form-group mb-0 {{required}}">{{content}}</div>',]]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group ">
    <label>Can these options be choosen more than once?</label>
    <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
        <?= $this->Form->radio('more_chosen', ['0' => 'No'], ['hiddenField' => false, 'id' => 'more-chosen-1', 'default' => '0']) ?>
    </div>
    <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
        <?= $this->Form->radio('more_chosen', ['1' => 'Yes'], ['hiddenField' => false, 'id' => 'more-chosen-1']) ?>
    </div>
</div>
<div class="form-group ">
    <label>Are these options required?</label>
    <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
        <?= $this->Form->radio('is_required', ['0' => 'No'], ['hiddenField' => false, 'id' => 'is-required-1', 'default' => '0']) ?>
    </div>
    <div class="col-md-12 layers bd bgc-white p-10 rounded d-block">
        <?= $this->Form->radio('is_required', ['1' => 'Yes'], ['hiddenField' => false, 'id' => 'is-required-1']) ?>
    </div>
</div>
<div class="form-group ">
    <label>Which items in <?= $cat->category_name ?> have these options?</label>
    <div class="layers bd bgc-white p-10 rounded d-block">
        <label for="all-item" class=""><input type="checkbox" class="form " id="all-item"> All items in <?= $cat->category_name ?></label>
        <div class="layers bgc-white p-10 rounded d-block">
            <?php foreach ($products as $key => $product) { ?>
                <?= $this->Form->control("product_ingredients[$key][id]", ['type' => 'hidden', 'value' => @$product['product_ingredients'][0]->id]) ?>
                <?= $this->Form->control("product_ingredients[$key][user_id]", ['type' => 'hidden', 'value' => $authUser['id']]) ?>
                <?= $this->Form->control("product_ingredients[$key][product_id]", ['hiddenField' => false, 'type' => 'checkbox', 'value' => $product->id, 'checked' => !empty($product['product_ingredients']), 'label' => $product->product_name, 'id' => 'product-id-' . $key]) ?>
            <?php } ?>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>