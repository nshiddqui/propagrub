<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="layers bd bgc-white p-10 rounded w-100">
                <div class="layer w-100">
                    <div class="row mt-4">
                        <div class="col-1">
                            <i class="fas fa-quote-left" style="font-size: 2em;"></i>
                        </div>
                        <div class="col-11">
                            <h6 class="w-100"><?= $userReview->review ?></h6>
                            <div style="white-space: nowrap;line-height:.7;display: inline-block;">
                                <?php foreach (range(0, 4) as $start) { ?>
                                    <?php if ($start < $userReview->rating) { ?>
                                        <span class="font-star">&#9733;</span>
                                    <?php } else { ?>
                                        <span class="font-star">&#9734;</span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <p style="display: inline-block;"><?= $userReview->created_at->format('jS M Y') ?></p>
                        </div>
                        <div class="col-12">
                            <h6 class="w-100 p-5"><i class="ti-info-alt"></i> Order #<?= $userReview['user_chef_order']->order_no ?> (£<?= $userReview['user_chef_order']->total_cost ?>)</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>