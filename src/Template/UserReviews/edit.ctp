<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserReview $userReview
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userReview->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userReview->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Reviews'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="userReviews form large-9 medium-8 columns content">
    <?= $this->Form->create($userReview) ?>
    <fieldset>
        <legend><?= __('Edit User Review') ?></legend>
        <?php
            echo $this->Form->control('rating_to');
            echo $this->Form->control('rating_by');
            echo $this->Form->control('rating');
            echo $this->Form->control('review');
            echo $this->Form->control('created_at');
            echo $this->Form->control('modified_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
