<?= $this->Form->create($userReview, ['id' => 'modal-form', 'type' => 'file']) ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="layers bd bgc-white p-10 rounded w-100">
                <div class="layer w-100">
                    <div class="row mt-4">
                        <div class="col-1">
                            <i class="fas fa-quote-left" style="font-size: 2em;"></i>
                        </div>
                        <div class="col-11">
                            <h6 class="w-100"><?= $userReview->review ?></h6>
                            <div style="white-space: nowrap;line-height:.7;display: inline-block;">
                                <?php foreach (range(0, 4) as $start) { ?>
                                    <?php if ($start < $userReview->rating) { ?>
                                        <span class="font-star">&#9733;</span>
                                    <?php } else { ?>
                                        <span class="font-star">&#9734;</span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <p style="display: inline-block;"><?= $userReview->created_at->format('jS M Y') ?></p>
                        </div>
                        <div class="col-12">
                            <h6 class="w-100 p-5"><i class="ti-info-alt"></i> Order #<?= $userReview['user_chef_order']->order_no ?> (£<?= $userReview['user_chef_order']->total_cost ?>)</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-4">
    <div class="row">
        <div class="col-12">
            <?= $this->Form->control('review_reply', ['type' => 'textarea', 'label' => 'Write your reply', 'placeholder' => 'E.g. Thanks for letting us know. Sorry to hear your order wasn/\t good enough, I\'ll pass that feedback to the kitchen team. - Richard, the resturant manager']) ?>
            <?= $this->Form->control('required_credit', ['type' => 'checkbox', 'label' => 'Add credit for their next order']) ?>
            <div style="display:none" id="credit-amount-here">
                <?= $this->Form->control('amount', ['options' => [1.50 => '£1.50', 2.50 => '£2.50', 5.00 => '£5.00', 7.50 => '£7.50', 10.00 => '£10.00'], 'label' => 'Credit amount']) ?>
                <p class="mb-0">We'll apply this credit to their next order from your resturant. You'll cover the cost.</p>
                <?= $this->Html->link('Find out more', '/', ['target' => '_BLANK']) ?>
            </div>
            <p class="text-dark font-weight-bold mb-0">We'll check your reply and email it straight to this customer.</p>
            <p class="mb-0">Other customers won't able to see it. keep you reply honest and professional, with no sear words.</p>
            <p class="mb-0">Not sure what to say? Read <?= $this->Html->link('replying to your customer reviewa', '/', ['target' => '_BLANK']) ?></p>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>