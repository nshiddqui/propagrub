<?php
$this->assign('title', 'Dashboard');
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css', ['block' => true]);
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->css('https://cdn.jsdelivr.net/npm/icheck@1.0.2/skins/square/blue.css', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/jquery.sumoselect.min.js', ['block' => true]);
$this->Html->script('https://cdn.jsdelivr.net/npm/icheck@1.0.2/icheck.min.js', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js', ['block' => true]);
$this->Html->scriptStart(['block' => true]);
echo "var REPLY_URL = '" . $this->Url->build('/user-reviews/send-reply/') . "';";
echo "var VIEW_REVIEW_URL = '" . $this->Url->build('/user-reviews/view/') . "';";
echo "var VIEW_REPLY_REVIEW_URL = '" . $this->Url->build('/user-reviews/view-reply/') . "';";
$this->Html->scriptEnd();
?>
<?= $this->Html->script('//code.jquery.com/ui/1.13.0-rc.3/jquery-ui.min.js', ['block' => true]) ?>
<div class="container">
    <div class="row masonry pos-r" style="position: relative; height: 1825.2px;">
        <div class="masonry-sizer col-md-6"></div>
        <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
            <div class="row menu-container">
                <h1 class="col-12 text-dark font-weight-bold">Rating and reviews</h1>
                <?php if ($menu->count()) { ?>
                    <div class="col-12 my-3">
                        <?= $this->Form->create(null, ['id' => 'filter-form', 'type' => 'get']) ?>
                        <div class="row">
                            <div class="col-md-3">
                                <?= $this->Form->control('test', ['options' => [
                                    'Fired Up One Ltd',
                                    'Hache Burger Connoisseurs',
                                    'Hache Trading Ltd',
                                    'Hache Brasseries Limited'
                                ], 'label' => false, 'disabled' => true]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $this->Form->control('menu_id', ['options' => $menu, 'value' => $menu_id, 'label' => false]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $this->Form->control('duration', ['options' => [
                                    '7' => 'Last 7 days',
                                    '30' => 'Last month',
                                    '180' => 'Last 6 months',
                                ], 'value' => $duration, 'label' => false]) ?>
                            </div>
                        </div>
                        <?= $this->Form->end()  ?>
                    </div>
                    <div class="col-12 my-3">
                        <div class="layers bgc-white p-20 rounded mb-2">
                            <div class="row w-100">
                                <h3 class="col-12 text-dark font-weight-bold">
                                    Your rating is <?= $rating->rating ?> <span class="text-normal font-weight-normal h5">(<?= $total_review ?>+ reviews)</span>
                                </h3>
                                <p class="col-12">Here's how customers rated you.</p>
                                <div class="col-md-6">
                                    <div class="Progress">
                                        <span class="Progress-label" id="Progress-id"><strong>5 &#9733;</strong></span>
                                        <progress max="100" value="<?= $total_5_review ?>" class="Progress-main" aria-labelledby="Progress-id">
                                            <div class="Progress-bar" role="presentation">
                                                <span class="Progress-value" style="width: <?= $total_5_review ?>%;">&nbsp;</span>
                                            </div>
                                        </progress>
                                        <span class="Progress-label" id="Progress-id"><strong><?= $total_5_review ?>%</strong></span>
                                    </div>
                                    <div class="Progress">
                                        <span class="Progress-label" id="Progress-id"><strong>4 &#9733;</strong></span>
                                        <progress max="100" value="<?= $total_4_review ?>" class="Progress-main" aria-labelledby="Progress-id">
                                            <div class="Progress-bar" role="presentation">
                                                <span class="Progress-value" style="width: <?= $total_4_review ?>%;">&nbsp;</span>
                                            </div>
                                        </progress>
                                        <span class="Progress-label" id="Progress-id"><strong><?= $total_4_review ?>%</strong></span>
                                    </div>
                                    <div class="Progress">
                                        <span class="Progress-label" id="Progress-id"><strong>3 &#9733;</strong></span>
                                        <progress max="100" value="<?= $total_3_review ?>" class="Progress-main" aria-labelledby="Progress-id">
                                            <div class="Progress-bar" role="presentation">
                                                <span class="Progress-value" style="width: <?= $total_3_review ?>%;">&nbsp;</span>
                                            </div>
                                        </progress>
                                        <span class="Progress-label" id="Progress-id"><strong><?= $total_3_review ?>%</strong></span>
                                    </div>
                                    <div class="Progress">
                                        <span class="Progress-label" id="Progress-id"><strong>2 &#9733;</strong></span>
                                        <progress max="100" value="<?= $total_2_review ?>" class="Progress-main" aria-labelledby="Progress-id">
                                            <div class="Progress-bar" role="presentation">
                                                <span class="Progress-value" style="width: <?= $total_2_review ?>%;">&nbsp;</span>
                                            </div>
                                        </progress>
                                        <span class="Progress-label" id="Progress-id"><strong><?= $total_2_review ?>%</strong></span>
                                    </div>
                                    <div class="Progress">
                                        <span class="Progress-label" id="Progress-id"><strong>1 &#9733;</strong></span>
                                        <progress max="100" value="<?= $total_1_review ?>" class="Progress-main" aria-labelledby="Progress-id">
                                            <div class="Progress-bar" role="presentation">
                                                <span class="Progress-value" style="width: <?= $total_1_review ?>%;">&nbsp;</span>
                                            </div>
                                        </progress>
                                        <span class="Progress-label" id="Progress-id"><strong><?= $total_1_review ?>%</strong></span>
                                    </div>
                                </div>
                                <!--Progress-->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 my-3">
                        <div class="layers bgc-white p-20 rounded mb-2">
                            <div class="row text-center w-100">
                                <p class="col-12 mb-0">Total reviews.</p>
                                <h3 class="col-12 text-dark font-weight-bold">
                                    <?= $userReviews->count() ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 my-3">
                        <div class="layers bgc-white p-20 rounded mb-2">
                            <div class="row w-100">
                                <h3 class="col-md-4 text-dark font-weight-bold">
                                    Your reviews and replies
                                </h3>
                                <div class="col-md-2">
                                    <?= $this->Form->control('test', ['options' => ['Most Recents'], 'label' => false]) ?>
                                </div>
                                <p class="col-12">Let us know whether you find replying to reviews useful, and if you've has any problems sending replies. Give feedback <?= $this->Html->link($this->Html->image('open_page.png', ['style' => 'height:15px']), '/', ['escape' => false, 'target' => '_BLANK', 'web' => true]) ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 my-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="layers bgc-white p-20 rounded mb-2">
                                    <div class="row w-100">
                                        <?php foreach ($userReviews as $userReview) { ?>
                                            <div class="col-md-4 p-2">
                                                <div style="min-height: 180px;" class="layers bd bgc-white p-10 rounde m-4">
                                                    <div class="row w-100">
                                                        <div class="col-md-6">
                                                            <div style="white-space: nowrap;line-height:.7;">
                                                                <?php foreach (range(0, 4) as $start) { ?>
                                                                    <?php if ($start < $userReview->rating) { ?>
                                                                        <span class="font-star">&#9733;</span>
                                                                    <?php } else { ?>
                                                                        <span class="font-star">&#9734;</span>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                            <p><?= $userReview->created_at->format('jS M Y') ?></p>
                                                        </div>
                                                        <div class="col-md-6 text-right">
                                                            <?= $this->Form->button('Details', ['class' => 'btn btn-default color-primary', 'onclick' => "loadReview({$userReview->id})"]) ?>
                                                            <?php if (empty($userReview->review_reply)) { ?>
                                                                <?= $this->Form->button('Reply', ['class' => 'btn btn-default bd color-primary', 'onclick' => "loadReply({$userReview->id})"]) ?>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col-12">
                                                            <h6><?= $userReview->review ?></h6>
                                                        </div>
                                                        <?php if (!empty($userReview->review_reply)) { ?>
                                                            <div class="col-12">
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-8">
                                                                        <h6><i class="fas fa-redo"></i> Reply being checked</h6>
                                                                        <p>Your replied <?= $userReview->modified_at->format('jS M Y, h:i') ?></p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <?= $this->Form->button('View reply', ['class' => 'btn btn-default color-primary', 'style' => 'white-space: nowrap;display: contents;', 'onclick' => "loadReplyReview({$userReview->id})"]) ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="w-100 text-center my-3">
                        <?= $this->Html->image('no_search_data.png', ['style' => 'height:200px']) ?>
                        <h3>You don't have any reviews yet</h3>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Manage Section -->
<div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="New item" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Reply to a review </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body px-4">

            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="button" class="btn btn-primary" id="save-button">Send via Email</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Manage Section -->
<div class="modal fade" id="review-modal" tabindex="-1" role="dialog" aria-labelledby="New item" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Review Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body px-4">

            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Manage Section -->
<div class="modal fade" id="review-reply-modal" tabindex="-1" role="dialog" aria-labelledby="New item" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Review reply Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body px-4">

            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>