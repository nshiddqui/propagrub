<?php
$this->assign('title', 'Live Orders');
$this->Html->scriptBlock('var url_dispacther = "' . $this->Url->build(['action' => 'dispatcher']) . '";', ['block' => true]);
?>
<?= $this->Html->component('card-tab/css/style', 'css', ['block' => 'css']) ?>
<?= $this->Html->component('card-tab/js/jquery.multipurpose_tabcontent', 'script', ['block' => 'script']) ?>
<?= $this->Html->css('dispatcher', ['block' => 'css']) ?>
<?= $this->Html->script('dispatcher', ['block' => 'script']) ?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h1 class="col-12 text-dark font-weight-bold">Live Orders</h1>
            <div class="col-12">
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <div class="tab_wrapper first_tab">
                            <ul class="tab_list">
                                <?php foreach ($orders as $order_status_type => $order) { ?>
                                    <li class="<?= $order_status_type === $order_status[$status] ? 'active' : '' ?>">
                                        <a href="#" data-tab="#<?= str_replace(' ', '-', $order_status_type) ?>" data-status-id="<?= array_search($order_status_type, $order_status) ?>"><?= $order_status_type ?> (<span id="total-new"><?= $order->count() ?></span>)</a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="content_wrapper">
                                <?php foreach ($orders as $order_status_type => $order_data) { ?>
                                    <div class="tab_content row <?= $order_status_type === $order_status[$status] ? 'active' : '' ?>" id="<?= str_replace(' ', '-', $order_status_type) ?>">
                                        <?php foreach ($order_data as $order) { ?>
                                            <div class="col-md-4 col-sm-6 my-3 pull-left" id="<?= $order->id ?>">
                                                <div class="layers bd bgc-white p-10 rounded">
                                                    <div class="layer w-100 widget-user-2">
                                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                                        <div class="widget-user-header bg-white p-3 d-flex">
                                                            <div class="widget-user-image">
                                                                <?= $this->Html->image($order['customer']->image, ['profile' => WEB_PROFILE_IMAGE, 'class' => 'img-circle']) ?>
                                                            </div>
                                                            <!-- /.widget-user-image -->
                                                            <p class="widget-username"><?= $order['customer']->full_name ?> <span>£ <?= $order->total_cost ?></span></p>
                                                        </div>
                                                        <div class="card-footer p-3">
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col-md-6 my-2 px-0">
                                                                        <h4 class="header-order">Order ID: <span class="color-light"><?= strtoupper($order->order_no) ?></span></h4>
                                                                    </div>
                                                                    <div class="col-md-6  my-2 px-0 text-right">
                                                                        <h4 class="color-light small-text"><?= $this->Time->timeAgoInWords($order->created_at) ?></h4>
                                                                    </div>
                                                                    <div class="col-md-8 px-0">
                                                                        <h4 class="header-order">Order Type: <span class="color-red"><?= $order_type[$order->order_type] ?></span></h4>
                                                                    </div>
                                                                </div>
                                                                <div class="container-bottom">
                                                                    <div class="row text-center justify-content-center">
                                                                        <?= $this->Html->link('View Order', ['action' => 'view', $order->id], ['class' => 'btn btn-primary btn-order mx-1']) ?>
                                                                        <?php if ($order->status == '1') { ?>
                                                                            <?= $this->Form->postLink('Accept', ['action' => 'status', $order->id, '2'], ['class' => 'btn btn-success btn-order mx-1', 'confirm' => __('Are you sure you want to accept # {0}?', $order->order_no)]) ?>
                                                                            <?= $this->Form->postLink('Reject', ['action' => 'status', $order->id, '7'], ['class' => 'btn btn-danger btn-order mx-1', 'confirm' => __('Are you sure you want to reject # {0}?', $order->order_no)]) ?>
                                                                        <?php } else if ($order->status == '2') { ?>
                                                                            <?= $this->Form->postLink('Start Processing', ['action' => 'status', $order->id, '3'], ['class' => 'btn btn-success btn-order mx-1', 'confirm' => __('Are you sure you want to start processing # {0}?', $order->order_no)]) ?>
                                                                        <?php } else if ($order->status == '3') { ?>
                                                                            <?= $this->Form->postLink('Ready to Pick', ['action' => 'status', $order->id, '4'], ['class' => 'btn btn-success btn-order mx-1', 'confirm' => __('Are you sure you want to ready to pick # {0}?', $order->order_no)]) ?>
                                                                        <?php } else if ($order->status == '4') { ?>
                                                                            <?= $this->Form->postLink('Order Picked', ['action' => 'status', $order->id, '5'], ['class' => 'btn btn-success btn-order mx-1', 'confirm' => __('Are you sure you want to ready to order picked # {0}?', $order->order_no)]) ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>