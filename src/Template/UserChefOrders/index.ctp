<?php
$this->assign('title', 'Order List');
?>
<div class="card">
    <div class="card-header">
        <h3>Chef / Store <?= $current_order_status ?> Order Details</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <?= $this->DataTables->render('UserChefOrders') ?>
            </div>
        </div>
    </div>
</div>