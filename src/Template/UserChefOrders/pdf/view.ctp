<style>
    <!--
    /* Font Definitions */
    @font-face
    {font-family:Mangal;
     panose-1:2 4 5 3 5 2 3 3 2 2;}
    @font-face
    {font-family:"Cambria Math";
     panose-1:2 4 5 3 5 4 6 3 2 4;}
    @font-face
    {font-family:Calibri;
     panose-1:2 15 5 2 2 2 4 3 2 4;}
    @font-face
    {font-family:Cambria;
     panose-1:2 4 5 3 5 4 6 3 2 4;}
    /* Style Definitions */
    p.MsoNormal, li.MsoNormal, div.MsoNormal
    {margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:2.0pt;
     margin-left:0cm;
     font-size:10.0pt;
     font-family:"Cambria",serif;
     color:#595959;}
    h1
    {mso-style-link:"Heading 1 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:2.0pt;
     margin-left:0cm;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;
     font-weight:normal;}
    h2
    {mso-style-link:"Heading 2 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:2.0pt;
     margin-left:0cm;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;}
    h2.CxSpFirst
    {mso-style-link:"Heading 2 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;}
    h2.CxSpMiddle
    {mso-style-link:"Heading 2 Char";
     margin:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;}
    h2.CxSpLast
    {mso-style-link:"Heading 2 Char";
     margin-top:0cm;
     margin-right:0cm;
     margin-bottom:2.0pt;
     margin-left:0cm;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;}
    h3
    {mso-style-link:"Heading 3 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#7E97AD;
     text-transform:uppercase;
     font-weight:normal;
     font-style:italic;}
    h4
    {mso-style-link:"Heading 4 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#7E97AD;
     text-transform:uppercase;
     font-style:italic;}
    h5
    {mso-style-link:"Heading 5 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;}
    h6
    {mso-style-link:"Heading 6 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;
     font-weight:normal;
     font-style:italic;}
    p.MsoHeading7, li.MsoHeading7, div.MsoHeading7
    {mso-style-link:"Heading 7 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;}
    p.MsoHeading8, li.MsoHeading8, div.MsoHeading8
    {mso-style-link:"Heading 8 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     font-style:italic;}
    p.MsoHeading9, li.MsoHeading9, div.MsoHeading9
    {mso-style-link:"Heading 9 Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     font-weight:bold;}
    p.MsoHeader, li.MsoHeader, div.MsoHeader
    {mso-style-link:"Header Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:2.0pt;
     margin-left:0cm;
     text-align:right;
     font-size:10.0pt;
     font-family:"Cambria",serif;
     color:#595959;}
    p.MsoFooter, li.MsoFooter, div.MsoFooter
    {mso-style-link:"Footer Char";
     margin-top:2.0pt;
     margin-right:5.05pt;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     border:none;
     padding:0cm;
     font-size:10.0pt;
     font-family:"Cambria",serif;
     color:#595959;}
    p.MsoTitle, li.MsoTitle, div.MsoTitle
    {mso-style-link:"Title Char";
     margin-top:6.0pt;
     margin-right:0cm;
     margin-bottom:6.0pt;
     margin-left:0cm;
     font-size:14.0pt;
     font-family:"Calibri",sans-serif;
     color:white;
     text-transform:uppercase;}
    p.MsoTitleCxSpFirst, li.MsoTitleCxSpFirst, div.MsoTitleCxSpFirst
    {mso-style-link:"Title Char";
     margin-top:6.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     font-size:14.0pt;
     font-family:"Calibri",sans-serif;
     color:white;
     text-transform:uppercase;}
    p.MsoTitleCxSpMiddle, li.MsoTitleCxSpMiddle, div.MsoTitleCxSpMiddle
    {mso-style-link:"Title Char";
     margin:0cm;
     margin-bottom:.0001pt;
     font-size:14.0pt;
     font-family:"Calibri",sans-serif;
     color:white;
     text-transform:uppercase;}
    p.MsoTitleCxSpLast, li.MsoTitleCxSpLast, div.MsoTitleCxSpLast
    {mso-style-link:"Title Char";
     margin-top:0cm;
     margin-right:0cm;
     margin-bottom:6.0pt;
     margin-left:0cm;
     font-size:14.0pt;
     font-family:"Calibri",sans-serif;
     color:white;
     text-transform:uppercase;}
    p.MsoClosing, li.MsoClosing, div.MsoClosing
    {mso-style-link:"Closing Char";
     margin-top:30.0pt;
     margin-right:0cm;
     margin-bottom:4.0pt;
     margin-left:256.3pt;
     font-size:10.0pt;
     font-family:"Cambria",serif;
     color:#595959;}
    p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
    {mso-style-link:"Subtitle Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:8.0pt;
     margin-left:0cm;
     font-size:11.0pt;
     font-family:"Cambria",serif;
     color:#5A5A5A;}
    p.MsoSubtitleCxSpFirst, li.MsoSubtitleCxSpFirst, div.MsoSubtitleCxSpFirst
    {mso-style-link:"Subtitle Char";
     margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:0cm;
     margin-left:0cm;
     margin-bottom:.0001pt;
     font-size:11.0pt;
     font-family:"Cambria",serif;
     color:#5A5A5A;}
    p.MsoSubtitleCxSpMiddle, li.MsoSubtitleCxSpMiddle, div.MsoSubtitleCxSpMiddle
    {mso-style-link:"Subtitle Char";
     margin:0cm;
     margin-bottom:.0001pt;
     font-size:11.0pt;
     font-family:"Cambria",serif;
     color:#5A5A5A;}
    p.MsoSubtitleCxSpLast, li.MsoSubtitleCxSpLast, div.MsoSubtitleCxSpLast
    {mso-style-link:"Subtitle Char";
     margin-top:0cm;
     margin-right:0cm;
     margin-bottom:8.0pt;
     margin-left:0cm;
     font-size:11.0pt;
     font-family:"Cambria",serif;
     color:#5A5A5A;}
    p.MsoDate, li.MsoDate, div.MsoDate
    {mso-style-link:"Date Char";
     margin-top:6.0pt;
     margin-right:0cm;
     margin-bottom:6.0pt;
     margin-left:0cm;
     text-align:right;
     font-size:14.0pt;
     font-family:"Calibri",sans-serif;
     color:white;
     text-transform:uppercase;}
    span.MsoPlaceholderText
    {color:gray;}
    p.MsoQuote, li.MsoQuote, div.MsoQuote
    {mso-style-link:"Quote Char";
     margin-top:10.0pt;
     margin-right:0cm;
     margin-bottom:8.0pt;
     margin-left:0cm;
     text-align:center;
     font-size:10.0pt;
     font-family:"Cambria",serif;
     color:#404040;
     font-style:italic;}
    p.MsoIntenseQuote, li.MsoIntenseQuote, div.MsoIntenseQuote
    {mso-style-link:"Intense Quote Char";
     margin-top:18.0pt;
     margin-right:0cm;
     margin-bottom:18.0pt;
     margin-left:0cm;
     text-align:center;
     border:none;
     padding:0cm;
     font-size:10.0pt;
     font-family:"Cambria",serif;
     color:#7E97AD;
     font-style:italic;}
    span.MsoIntenseReference
    {font-variant:small-caps;
     color:#7E97AD;
     text-transform:none;
     letter-spacing:0pt;
     font-weight:bold;}
    span.MsoBookTitle
    {letter-spacing:0pt;
     font-weight:bold;
     font-style:italic;}
    p.MsoTocHeading, li.MsoTocHeading, div.MsoTocHeading
    {margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:2.0pt;
     margin-left:0cm;
     page-break-after:avoid;
     font-size:10.0pt;
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;}
    span.HeaderChar
    {mso-style-name:"Header Char";
     mso-style-link:Header;}
    span.FooterChar
    {mso-style-name:"Footer Char";
     mso-style-link:Footer;}
    span.TitleChar
    {mso-style-name:"Title Char";
     mso-style-link:Title;
     font-family:"Calibri",sans-serif;
     color:white;
     text-transform:uppercase;}
    span.ClosingChar
    {mso-style-name:"Closing Char";
     mso-style-link:Closing;}
    span.DateChar
    {mso-style-name:"Date Char";
     mso-style-link:Date;
     font-family:"Calibri",sans-serif;
     color:white;
     text-transform:uppercase;}
    span.Heading1Char
    {mso-style-name:"Heading 1 Char";
     mso-style-link:"Heading 1";
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;}
    span.Heading2Char
    {mso-style-name:"Heading 2 Char";
     mso-style-link:"Heading 2";
     font-family:"Calibri",sans-serif;
     color:#D60876;
     text-transform:uppercase;
     font-weight:bold;}
    span.Heading3Char
    {mso-style-name:"Heading 3 Char";
     mso-style-link:"Heading 3";
     font-family:"Calibri",sans-serif;
     color:#7E97AD;
     text-transform:uppercase;
     font-style:italic;}
    span.Heading4Char
    {mso-style-name:"Heading 4 Char";
     mso-style-link:"Heading 4";
     font-family:"Calibri",sans-serif;
     color:#7E97AD;
     text-transform:uppercase;
     font-weight:bold;
     font-style:italic;}
    span.Heading5Char
    {mso-style-name:"Heading 5 Char";
     mso-style-link:"Heading 5";
     font-family:"Calibri",sans-serif;
     color:#D60876;
     font-weight:bold;}
    span.Heading6Char
    {mso-style-name:"Heading 6 Char";
     mso-style-link:"Heading 6";
     font-family:"Calibri",sans-serif;
     color:#D60876;
     font-style:italic;}
    span.Heading7Char
    {mso-style-name:"Heading 7 Char";
     mso-style-link:"Heading 7";
     font-family:"Calibri",sans-serif;
     color:windowtext;}
    span.Heading8Char
    {mso-style-name:"Heading 8 Char";
     mso-style-link:"Heading 8";
     font-family:"Calibri",sans-serif;
     color:windowtext;
     font-style:italic;}
    span.Heading9Char
    {mso-style-name:"Heading 9 Char";
     mso-style-link:"Heading 9";
     font-family:"Calibri",sans-serif;
     color:windowtext;
     font-weight:bold;}
    span.SubtitleChar
    {mso-style-name:"Subtitle Char";
     mso-style-link:Subtitle;
     font-family:"Times New Roman",serif;
     color:#5A5A5A;}
    span.QuoteChar
    {mso-style-name:"Quote Char";
     mso-style-link:Quote;
     color:#404040;
     font-style:italic;}
    span.IntenseQuoteChar
    {mso-style-name:"Intense Quote Char";
     mso-style-link:"Intense Quote";
     color:#7E97AD;
     font-style:italic;}
    .MsoChpDefault
    {font-size:10.0pt;
     font-family:"Cambria",serif;
     color:#595959;}
    .MsoPapDefault
    {margin-top:2.0pt;
     margin-right:0cm;
     margin-bottom:2.0pt;
     margin-left:0cm;}
    /* Page Definitions */
    @page WordSection1
    {size:612.0pt 792.0pt;
     margin:136.8pt 54.0pt 54.0pt 54.0pt;}
    div.WordSection1
    {page:WordSection1;}
    -->
</style>
<div class=WordSection1>
    <h1 style="text-align:center;font-size: 20pt"><img src="<?= $this->Url->build('/img/login_logo.png', true) ?>"></h1>
    <table class=MsoTable15Grid4Accent5 border=1 cellspacing=0 cellpadding=0
           summary="First table is for invoice number and date, second table is for billing and shipping info, third table is the main invoice table, fourth table is for subtotals and totals"
           width="100%" style='width:100.0%;border-collapse:collapse;border:none'>
        <tr>
            <td width=354 valign=top style='width:252.0pt;border:none;background:#D60876;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoTitle><b>Order ID:</b> <?= strtoupper($order->order_no) ?>.</p>
            </td>
            <td width=354 valign=top style='width:252.0pt;border:none;background:#D60876;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoDate>Order Date: <?= $order->created_at ?></p>
            </td>
        </tr>
    </table>

    <table class=MsoTable15Plain2 border=0 cellspacing=0 cellpadding=0
           summary="First table is for invoice number and date, second table is for billing and shipping info, third table is the main invoice table, fourth table is for subtotals and totals"
           width="100%" style='width:100.0%;border-collapse:collapse;border:none'>
        <tr>
            <td width=177 valign=top style='width:126.0pt;border:none;border-bottom:solid #7E97AD 1.0pt;
                padding:16.55pt 5.75pt 0cm 5.75pt'>
                <h1 style='margin-bottom:2.0pt'>Customer Details</h1>
            </td>
            <td width=177 valign=top style='width:126.0pt;border:none;border-bottom:solid #7E97AD 1.0pt;
                padding:16.55pt 5.75pt 0cm 5.75pt'>
                <h1 style='margin-bottom:2.0pt'>Driver Details</h1>
            </td>
            <td width=354 valign=top style='width:252.0pt;border:none;border-bottom:solid #7E97AD 1.0pt;
                padding:16.55pt 5.75pt 0cm 5.75pt'>
                <h1 style='margin-bottom:2.0pt'>Order Notes</h1>
            </td>
        </tr>
        <tr>
            <td width=177 valign=top style='width:126.0pt;border:none;padding:0cm 5.75pt 15.85pt 5.75pt'>
                <p class=MsoNormal style='margin-bottom:2.0pt'>
                    <strong><?= $order['customer']->full_name ?></strong><br>
                    <?= $order['user_addres']->full_address ?>
                    <br>
                    Phone: <?= $order['customer']->mobile ?><br>
                    Email: <?= $order['customer']->email ?>
                </p>
            </td>
            <td width=177 valign=top style='width:126.0pt;border:none;padding:0cm 5.75pt 15.85pt 5.75pt'>
                <p class=MsoNormal style='margin-bottom:2.0pt'>
                    <?php if ($order['driver']) { ?>
                        <strong><?= $order['driver']->full_name ?></strong><br>
                        <?= $order['driver']->full_address ?>
                        <br>
                        Phone: <?= $order['driver']->mobile ?><br>
                        Email: <?= $order['driver']->email ?>
                    <?php } ?>
                </p>
            </td>
            <td width=354 valign=top style='width:252.0pt;border:none;padding:0cm 5.75pt 15.85pt 5.75pt'>
                <p class=MsoNormal style='margin-bottom:2.0pt'><?= $order->notes ?></p>
            </td>
        </tr>
    </table>

    <table class=InvoiceTable border=1 cellspacing=0 cellpadding=0
           summary="First table is for invoice number and date, second table is for billing and shipping info, third table is the main invoice table, fourth table is for subtotals and totals"
           width="100%" style='width:100.0%;border-collapse:collapse;border:none'>
        <thead>
            <tr>
                <td style='text-align: center;width:45pt;border:none;background:#D60876;
                    padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:
                       4.0pt;margin-left:0cm'><span style='font-family:"Calibri",sans-serif;
                            color:white;text-transform:uppercase'>Quantity</span></p>
                </td>
                <td width=142 style='text-align: center;width:70pt;border:none;background:#D60876;
                    padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:
                       4.0pt;margin-left:0cm'><span style='font-family:"Calibri",sans-serif;
                            color:white;text-transform:uppercase'>Product</span></p>
                </td>
                <td width=283 style='text-align: center;width:201.6pt;border:none;background:#D60876;
                    padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:
                       4.0pt;margin-left:0cm'><span style='font-family:"Calibri",sans-serif;
                            color:white;text-transform:uppercase'>Ad-ons</span></p>
                </td>
                <td width=142 style='width:100.8pt;border:none;background:#D60876;
                    padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:center'><span
                            style='font-family:"Calibri",sans-serif;color:white;text-transform:uppercase'>Description</span></p>
                </td>
                <td width=142 style='width:45pt;border:none;background:#D60876;
                    padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:center'><span
                            style='font-family:"Calibri",sans-serif;color:white;text-transform:uppercase'>Product<br>Price</span></p>
                </td>
            </tr>
        </thead>
        <?php foreach ($order['user_cart'] as $user_cart) { ?>
            <tr>
                <td style='text-align: center;width:45pt;border-top:solid #D9D9D9 1.0pt;border-left:
                    none;border-bottom:solid #D9D9D9 1.0pt;border-right:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:
                       4.0pt;margin-left:0cm'><?= $user_cart->quantity ?></p></td>
                <td width=142 style='text-align: center;width:70pt;border-top:solid #D9D9D9 1.0pt;border-left:
                    none;border-bottom:solid #D9D9D9 1.0pt;border-right:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:
                       4.0pt;margin-left:0cm'><?= $user_cart['menu_product']->product_name ?></p></td>
                <td width=283 style='width:100.8pt;border-top:solid #D9D9D9 1.0pt;border-left:
                    none;border-bottom:solid #D9D9D9 1.0pt;border-right:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:left'>
                       <?php
                       $ingredients = [];
                       if (!empty($user_cart['menu_product']['product_ingredients'])) {
                           foreach ($user_cart['menu_product']['product_ingredients'] as $product_ingredients) {
                               $ingredients[] = $product_ingredients->name;
                           }
                       }
                       echo implode(', ', $ingredients);
                       ?>
                    </p></td>
                <td width=142 style='text-align: center;width:100.8pt;border-top:solid #D9D9D9 1.0pt;border-left:
                    none;border-bottom:solid #D9D9D9 1.0pt;border-right:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:left'><?= $user_cart['menu_product']->product_detail ?></p></td>
                <td width=142 style='width:45pt;border-top:solid #D9D9D9 1.0pt;border-left:
                    none;border-bottom:solid #D9D9D9 1.0pt;border-right:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:center'>£ <?= $user_cart['menu_product']->product_price ?></p></td>
            </tr>
        <?php } ?>
    </table>
    <br><br><br><br>
    <div align="right">

        <table class=MsoTable15Grid1LightAccent1 border=1 cellspacing=0 cellpadding=0
               summary="First table is for invoice number and date, second table is for billing and shipping info, third table is the main invoice table, fourth table is for subtotals and totals"
               width="50%" style='width:50.0%;border-collapse:collapse;border:none'>
            <tr style='height:20.15pt'>
                <td width=212 valign=top style='width:151.2pt;border:none;border-bottom:solid #D9D9D9 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <h1 style='margin-top:4.0pt;margin-right:0cm;margin-bottom:4.0pt;margin-left:
                        0cm'>Service Fees</h1>
                </td>
                <td width=142 valign=top style='width:100.8pt;border:none;border-bottom:solid #D9D9D9 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:right'><span style='color:
                            #D60876'>£ <?= $order->service_fee ?></span></p>
                </td>
            </tr>
            <tr style='height:20.15pt'>
                <td width=212 valign=top style='width:151.2pt;border:none;border-bottom:solid #D9D9D9 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <h1 style='margin-top:4.0pt;margin-right:0cm;margin-bottom:4.0pt;margin-left:
                        0cm'>Delivery Charges</h1>
                </td>
                <td width=142 valign=top style='width:100.8pt;border:none;border-bottom:solid #D9D9D9 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:right'><span style='color:
                            #D60876'>£ <?= $order->delivery_charges ?></span></p>
                </td>
            </tr>
            <tr style='height:20.15pt'>
                <td width=212 valign=top style='width:151.2pt;border:none;border-bottom:solid #A6A6A6 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <h1 style='margin-top:4.0pt;margin-right:0cm;margin-bottom:4.0pt;margin-left:
                        0cm'>Discount</h1>
                </td>
                <td width=142 valign=top style='width:100.8pt;border:none;border-bottom:solid #A6A6A6 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:right'><span style='color:
                            #D60876'>£ <?= $order->discount ?></span></p>
                </td>
            </tr>
            <tr style='height:20.15pt'>
                <td width=212 valign=top style='width:151.2pt;border:none;border-bottom:solid #A6A6A6 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <h2 style='margin-top:4.0pt;margin-right:0cm;margin-bottom:4.0pt;margin-left:
                        0cm'>Total Amount</h2>
                </td>
                <td width=142 valign=top style='width:100.8pt;border:none;border-bottom:solid #A6A6A6 1.0pt;
                    padding:0cm 5.4pt 0cm 5.4pt;height:20.15pt'>
                    <p class=MsoNormal align=right style='margin-top:4.0pt;margin-right:0cm;
                       margin-bottom:4.0pt;margin-left:0cm;text-align:right'><span style='color:
                            #D60876'>£ <?= $order->total_cost ?></span></p>
                </td>
            </tr>
        </table>

    </div>

</div>
