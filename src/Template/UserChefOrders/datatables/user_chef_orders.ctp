<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h(strtoupper($result->order_no)),
        $order_type[$result->order_type],
        h($result['customer']->full_name),
        h($result['user_addres']->full_address),
        h($result['customer']->mobile),
        '£ ' . $result->total_cost,
        $this->Html->link('<i class="fa fa-file-pdf-o fa-2x"></i>', ['action' => 'view', $result->id], ['escape' => false, 'target' => '_BLANK', 'class' => 'text-primary']),
    ]);
}
echo $this->DataTables->response();
