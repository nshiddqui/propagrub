<?php
$this->assign('title', 'Order Details');
?>
<!-- Main content -->
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> PropaGrub
                <small class="pull-right">Order Date: <?= $order->created_at ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Customer Details
            <address>
                <strong><?= $order['customer']->full_name ?></strong><br>
                <?= $order['user_addres']->full_address ?>
                <br>
                Phone: <?= $order['customer']->mobile ?><br>
                Email: <?= $order['customer']->email ?>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <?php if ($order['driver']) { ?>
                Driver Details
                <address>
                    <strong><?= $order['driver']->full_name ?></strong><br>
                    <?= $order['driver']->full_address ?>
                    <br>
                    Phone: <?= $order['driver']->mobile ?><br>
                    Email: <?= $order['driver']->email ?>
                </address>
            <?php } ?>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col text-right">
            <h3 class="no-margin"><b>Order ID:</b> <?= strtoupper($order->order_no) ?></h3>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>Ad-ons</th>
                        <th>Description</th>
                        <th>Product Price</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($order['user_cart'] as $user_cart) { ?>
                        <tr>
                            <td><?= $user_cart->quantity ?></td>
                            <td><?= $user_cart['menu_product']->product_name ?></td>
                            <td>
                                <?php
                                $ingredients = [];
                                if (!empty($user_cart['menu_product']['product_ingredients'])) {
                                    foreach ($user_cart['menu_product']['product_ingredients'] as $product_ingredients) {
                                        $ingredients[] = $product_ingredients->name;
                                    }
                                }
                                echo implode(', ', $ingredients);
                                ?>
                            </td>
                            <td><?= $user_cart['menu_product']->product_detail ?></td>
                            <td>£ <?= $user_cart['menu_product']->product_price ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <!-- /.col -->
        <div class="col-xs-12">
            <p class="lead">Total Amount</p>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Service Fees:</th>
                        <td>£ <?= $order->service_fee ?></td>
                    </tr>
                    <tr>
                        <th>Delivery Charges</th>
                        <td>£ <?= $order->delivery_charges ?></td>
                    </tr>
                    <tr>
                        <th>Discount:</th>
                        <td>£ <?= $order->discount ?></td>
                    </tr>
                    <tr>
                        <th>Promo Code:</th>
                        <td><?= $order->promo_code ?></td>
                    </tr>
                    <tr>
                        <th>Total:</th>
                        <td>£ <?= $order->total_cost ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <button type="button" onclick="window.print();" class="btn btn-primary pull-right">
                <i class="fa fa-print"></i> Print
            </button>
        </div>
    </div>
</section>