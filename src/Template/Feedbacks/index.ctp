<?php
$this->assign('title', 'Feedback');
?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h1 class="col-12 text-dark font-weight-bold">Feedback</h1>
            <div class="col-12 my-3">
                <?= $this->Form->create($feedback) ?>
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <?php
                        echo $this->Form->control('name');
                        echo $this->Form->control('email');
                        echo $this->Form->control('phone_number', ['type' => 'number']);
                        echo $this->Form->control('review');
                        ?>
                        <div class="text-center">
                            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<style>
    .required label:after {
        content: " *";
        color: red;
    }
</style>