<?php
$this->assign('no-title', true);
?>
<div class="card" style="border-radius: 15px;border-top: 0;">
    <div class="card-header text-center;">
        <h4 style="font-weight: bold">Enter your subscriptions details</h4>
        <?php if (!empty($authUser['subscription_expired']) && strtotime($authUser['subscription_expired']) > strtotime('now')) { ?>
            <div class="pull-right">
                <h6>Subscription Expiration Date : <span class="pull-right text-bold"> <?= !empty($authUser['subscription_expired']) ? $authUser['subscription_expired'] : 'No Subscription.' ?></span></h6>
            </div>
        <?php } else { ?> 
            <?= $this->Flash->render() ?>
        <?php } ?>
    </div>
    <?= $this->Form->create($card_detail) ?>
    <div class="card-body text-left">
        <div class="row">
            <div class="col-md-12">
                <?= $this->Form->hidden('user.user_subscription.id') ?>
                <?= $this->Form->control('user.user_subscription.subscrriber_plan', ['options' => $subscript_plan, 'empty' => 'Select Plan', 'required' => true, 'label' => 'Choose Subscription Plan']) ?>
            </div>
            <div class="col-md-12">
                <?= $this->Form->control('card_holder_name') ?>
            </div>
            <div class="col-md-12">
                <?= $this->Form->control('card_number', ['maxlength' => 16]) ?>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">  
                        <?= $this->Form->control('card_expiry_month', ['options' => ['01' => "January", '02' => "February", '03' => "March", '04' => "April", '05' => "May", '06' => "June", '07' => "July", '08' => "August", '09' => "September", '10' => "October", '11' => "November", '12' => "December"], 'empty' => 'Select Month']) ?>
                    </div>
                    <div class="col-md-4">  
                        <?= $this->Form->control('card_expiry_year', ['options' => array_combine(range(date("Y"), date("Y") + 20), range(date("Y"), date("Y") + 20)), 'empty' => 'Select Year']) ?>
                    </div>
                    <div class="col-md-4">  
                        <?= $this->Form->control('card_cvv', ['type' => 'password']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <?= $this->Form->control('user.promo_code.promocode', ['required' => false, 'label' => 'Enter your Promocode']) ?>
            </div>
            <div class="col-md-12">
                <?= $this->Form->checkbox('terms', ['required' => true, 'style' => 'display:inline-block;']) ?>
                I agree to accept all <?= $this->Html->link('Terms & Conditions', '/pages/3', ['web' => true]) ?>
            </div>
            <div class="col-md-12 text-center" style="margin: 20px 0">
                <?= $this->Form->submit('Subscribe', ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="text-center">
                <?= $this->Html->image('powered.png', ['alt' => 'logo', 'class' => 'logo', 'style' => 'height: 37px;margin: 10px auto']) ?>   
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>