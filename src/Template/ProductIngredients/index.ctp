<?php
$this->assign('title', 'Manage Product Ad-ons');
?>
<div class="card">
    <div class="card-header">
        <h3>Add/ View Product Ad-ons</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <?= $this->DataTables->render('ProductIngredients') ?>
            </div>
            <div class="col-md-4">
                <h3><?= !empty($product_ingredient->id) ? 'Edit' : 'Add New' ?> Product Ad-ons</h3>
                <?= $this->Form->create($product_ingredient) ?>
                <?= $this->Form->control('menu_id', ['options' => $menu]) ?>
                <?= $this->Form->control('category_id', ['options' => $menu_category]) ?>
                <?= $this->Form->control('product_id', ['options' => $menu_product]) ?>
                <?= $this->Form->control('ingredients_category_id', ['options' => $ingredients_category, 'required' => true]) ?>
                <?= $this->Form->control('name') ?>
                <div class="usd-parent">
                    <i class="usd icon">£</i>
                    <?= $this->Form->control('price', ['type' => 'number']) ?>
                </div>
                <div class="text-center">
                    <?= $this->Form->submit('Save') ?>
                </div>
            </div>
        </div>
    </div>
</div>