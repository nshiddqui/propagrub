<?php
$this->assign('title', 'Admin Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Admins</h3>
        <?= $this->Html->link('Add Admin', ['action' => 'chnageProfile', 'add'], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Admins') ?>
    </div>
</div>