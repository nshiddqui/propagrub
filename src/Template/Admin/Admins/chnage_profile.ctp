<?php
$this->assign('title', 'Admin Management');
?>
<?= $this->Form->create($admin, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __(($admin->id ? 'Update' : 'Add') . ' Admin Profile') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="image">
                        <?= $this->Html->image($admin->image, ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'user.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Thumbnail Image For Admin</p>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*']);
                    echo $this->Form->control('name');
                    echo $this->Form->control('email');
                    echo $this->Form->control('password', ['value' => '', 'label' => 'Change Password', 'required' => false]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__(($admin->id ? 'Update' : 'Add'))) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>