<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $this->Html->image($result->image, ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle',]),
        h($result->name),
        h($result->email),
        $result->status ? 'Active' : 'Inactive',
        $this->Form->postLink('<i class="fa fa-dot-circle-o fa-lg"></i>', ['action' => 'active', $result->id, ($result->status == '1' ? '0' : '1')], ['confirm' => __('Are you sure you want to ' . ($result->status == '1' ? 'Deactivate' : 'Activate') . ' # {0}?', $result->client_name), 'escape' => false, 'class' => 'text-black', 'title' => 'Change Status']) . '&nbsp;' . '&nbsp;' . '&nbsp;' .
        $this->Html->link(__('<i class="fa fa-pencil fa-lg"></i>'), ['action' => 'chnageProfile', $result->id], ['escape' => false, 'class' => 'text-black', 'title' => 'Edit Profile'])
    ]);
}
echo $this->DataTables->response();
