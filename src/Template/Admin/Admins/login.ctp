<div class="card login-card">
    <div class="row no-gutters">
        <div class="col-md-5">
            <?= $this->Html->image('login.jpg', ['alt' => 'login', 'class' => 'login-card-img']) ?>
            <div class="bg-opacity-login">
            </div>
            <?= $this->Html->image('img_logo.gif', ['alt' => 'login', 'class' => 'absolute-center']) ?>
            <div class="bg-text-login">
                <h5>Overview</h5>
                <p>✔ £1 per day membership to run your own kitchen</p>
                <p>✔ A service fee is calculated as a percentage of each order</p>
                <p>✔ Use Propagrub’s driver network or your own</p>
                <p>✔ Marketing and promotion of your business </p>
                <p>✔ Attract loyal customers, send notifications to your followers</p>
                <p>✔ Watch your orders come in through your door and online</p>

                <h5>To Sign up</h5>
                <p>1. Tell us about your restaurant and business.</p>
                <p>2. Upload your menu.</p>
                <p>3. Start taking orders.</p>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card-body login-box-body">
                <div class="brand-wrapper">
                    <?= $this->Html->image('login_logo.png', ['alt' => 'logo', 'class' => 'logo']) ?>
                </div>
                <p class="login-card-description">Sign into your account</p>
                <?= $this->Form->create(null) ?>
                <?= $this->Flash->render() ?>
                <div class="form-group">
                    <?= $this->Form->control('email', ['placeholder' => 'Email', 'label' => false, 'required' => true]) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('password', ['placeholder' => 'Password', 'label' => false, 'required' => true]) ?>
                </div>
                <?= $this->Form->button('Login', ['class' => 'btn btn-block login-btn mb-4']) ?>
                <?= $this->Form->end() ?>
                <div class="link-extra">
                    <?= $this->Html->link('Privay Policy', 'https://propagrub.com/privacy-policy/', ['target' => '_BLANK']) ?>&nbsp;&nbsp;|&nbsp;
                    <?= $this->Html->link('Terms & Conditions', 'https://propagrub.com/terms-conditions/', ['target' => '_BLANK']) ?>&nbsp;&nbsp;|&nbsp;
                    <?= $this->Html->link('FAQ\'s', 'https://propagrub.com/chefsfaq/', ['target' => '_BLANK']) ?>
                </div>
            </div>
        </div>
    </div>
</div>