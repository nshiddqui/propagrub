<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h($result->doc_type),
        $result['userRoles']->name,
        $result->is_required == '1' ? 'Yes' : 'No',
        $this->Html->link('<i class="fa fa-pencil fa-lg"></i>', ['action' => 'index', $result->id], ['escape' => false, 'class' => 'text-black']) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->doc_type), 'escape' => false, 'class' => 'text-black'])
    ]);
}
echo $this->DataTables->response();
