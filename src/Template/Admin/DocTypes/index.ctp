<?php
$this->assign('title', 'Document Type Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Document Type</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-8">
                <?= $this->DataTables->render('DocTypes') ?>
            </div>
            <div class="col-md-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= !empty($doc_types->id) ? 'Edit' : 'Add New' ?> Document Type</h3>                    
                            <?= $this->Form->create($doc_types) ?>
                            <?= $this->Form->control('user_role', ['options' => $user_role]) ?>
                            <?= $this->Form->control('doc_type') ?>
                            <?= $this->Form->control('is_required', ['type' => 'checkbox', 'required' => false]) ?>
                            <div class="text-center">
                                <?= $this->Form->submit('Save') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>