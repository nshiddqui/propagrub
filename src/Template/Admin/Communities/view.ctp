<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Community $community
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Community'), ['action' => 'edit', $community->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Community'), ['action' => 'delete', $community->id], ['confirm' => __('Are you sure you want to delete # {0}?', $community->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Communities'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Member'), ['controller' => 'CommunityMember', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Member'), ['controller' => 'CommunityMember', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Post Comment'), ['controller' => 'CommunityPostComment', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Post Comment'), ['controller' => 'CommunityPostComment', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Posts'), ['controller' => 'CommunityPosts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Post'), ['controller' => 'CommunityPosts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="communities view large-9 medium-8 columns content">
    <h3><?= h($community->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Community Name') ?></th>
            <td><?= h($community->community_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Community Image') ?></th>
            <td><?= h($community->community_image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= h($community->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($community->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Verified') ?></th>
            <td><?= $this->Number->format($community->is_verified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($community->created_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Community Member') ?></h4>
        <?php if (!empty($community->community_member)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_member as $communityMember): ?>
            <tr>
                <td><?= h($communityMember->id) ?></td>
                <td><?= h($communityMember->community_id) ?></td>
                <td><?= h($communityMember->user_id) ?></td>
                <td><?= h($communityMember->created_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityMember', 'action' => 'view', $communityMember->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityMember', 'action' => 'edit', $communityMember->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityMember', 'action' => 'delete', $communityMember->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityMember->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Post Comment') ?></h4>
        <?php if (!empty($community->community_post_comment)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Posted By') ?></th>
                <th scope="col"><?= __('Post Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Modified At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_post_comment as $communityPostComment): ?>
            <tr>
                <td><?= h($communityPostComment->id) ?></td>
                <td><?= h($communityPostComment->comment) ?></td>
                <td><?= h($communityPostComment->posted_by) ?></td>
                <td><?= h($communityPostComment->post_id) ?></td>
                <td><?= h($communityPostComment->community_id) ?></td>
                <td><?= h($communityPostComment->created_at) ?></td>
                <td><?= h($communityPostComment->modified_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityPostComment', 'action' => 'view', $communityPostComment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityPostComment', 'action' => 'edit', $communityPostComment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityPostComment', 'action' => 'delete', $communityPostComment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityPostComment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Posts') ?></h4>
        <?php if (!empty($community->community_posts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Post Title') ?></th>
                <th scope="col"><?= __('Post Detail') ?></th>
                <th scope="col"><?= __('Post Image') ?></th>
                <th scope="col"><?= __('Posted By') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Modified At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_posts as $communityPosts): ?>
            <tr>
                <td><?= h($communityPosts->id) ?></td>
                <td><?= h($communityPosts->community_id) ?></td>
                <td><?= h($communityPosts->post_title) ?></td>
                <td><?= h($communityPosts->post_detail) ?></td>
                <td><?= h($communityPosts->post_image) ?></td>
                <td><?= h($communityPosts->posted_by) ?></td>
                <td><?= h($communityPosts->created_at) ?></td>
                <td><?= h($communityPosts->modified_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityPosts', 'action' => 'view', $communityPosts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityPosts', 'action' => 'edit', $communityPosts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityPosts', 'action' => 'delete', $communityPosts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityPosts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
