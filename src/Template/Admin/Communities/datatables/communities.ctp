<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $this->Html->image($result->community_image, ['style' => 'height: 45px; width: 45px', 'profile' => WEB_COMMUNITIES, 'class' => 'img-circle',]),
        h($result->community_name),
        ($result->is_verified == '1' ? 'Yes' : 'No'),
        (!empty($result['community_member']) ? $this->Html->link($result['community_member'][0]->count, ['controller' => 'CommunityMember', 'action' => 'index', '?' => ['id' => $result->id]]) : 0),
        (!empty($result['user']) ? $result['user']->full_name : 'Admin'),
        $this->Html->link(__('<i class="fa fa-pencil fa-lg"></i>'), ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'text-black', 'title' => 'Edit Profile']) . '&nbsp;' . '&nbsp;' . '&nbsp;' .
            $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->community_name), 'escape' => false, 'class' => 'text-black', 'title' => 'Change Status'])
    ]);
}
echo $this->DataTables->response();
