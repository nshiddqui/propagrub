<?php
$this->assign('title', 'Communities Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Communities</h3>
        <?= $this->Html->link('Add Communitiy', ['action' => 'edit'], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Communities') ?>
    </div>
</div>