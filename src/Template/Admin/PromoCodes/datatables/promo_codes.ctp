<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h($result->promocode),
        $result->start_date,
        $result->end_date,
        $result->total_user,
        (!empty($result['chef_promocodes']) ? $result['chef_promocodes'][0]->count : 0),
        $result->created
    ]);
}
echo $this->DataTables->response();
