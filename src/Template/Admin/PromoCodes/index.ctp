<?php
$this->assign('title', 'Promocodes Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Promocodes</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-8">
                <?= $this->DataTables->render('PromoCodes') ?>
            </div>
            <div class="col-md-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= !empty($promoCode->id) ? 'Edit' : 'Add New' ?> Promocodes</h3>                    
                            <?= $this->Form->create($promoCode) ?>
                            <div class="row">
                                <div class="col-md-8">
                                    <?= $this->Form->control('promocode', ['value' => !empty($promoCode->promocode) ? $promoCode->promocode : 'PB']) ?>
                                </div>
                                <div class="col-md-4">
                                    <label>&nbsp;</label>
                                    <button type="button" class="btn btn-info" id="generate-code">Generate</button>
                                </div>
                            </div>
                            <?= $this->Form->control('start_date', ['type' => 'text', 'datepicker' => true]) ?>
                            <?= $this->Form->control('end_date', ['type' => 'text', 'datepicker' => true]) ?>
                            <?= $this->Form->control('total_user') ?>
                            <?= $this->Form->control('total_days', ['disabled' => true]) ?>
                            <div class="text-center">
                                <?= $this->Form->submit('Save') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>