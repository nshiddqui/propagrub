<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        $this->Html->image((!empty($result->cuisine_image) ? $result->cuisine_image : 'not-found.png'), ['profile' => WEB_CUISINES, 'style' => 'height: 45px; width: 45px', 'class' => 'img-circle', 'onerror' => '/img/not-found.png']),
        h($result->cuisine_name),
        $this->Html->link('<i class="fa fa-pencil fa-lg"></i>', ['action' => 'index', $result->id], ['escape' => false, 'class' => 'text-black']) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->cuisine_name), 'escape' => false, 'class' => 'text-black'])
    ]);
}
echo $this->DataTables->response();
