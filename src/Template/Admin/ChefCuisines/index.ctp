<?php
$this->assign('title', 'Cuisines Management');
?>
<div class="box">
    <div class="box-header">
        <h3>List Cuisines</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8">
                <?= $this->DataTables->render('ChefCuisines') ?>
            </div>
            <div class="col-md-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= !empty($chef_cuisines->id) ? 'Edit' : 'Add New' ?> Cuisines</h3>                    
                            <?= $this->Form->create($chef_cuisines, ['type' => 'file']) ?>
                            <label for="cuisine-image" class="border-black">
                                <?= $this->Html->image((!empty($chef_cuisines->cuisine_image) ? $chef_cuisines->cuisine_image : 'not-found.png'), ['class' => 'thumbnail-image', 'profile' => WEB_CUISINES, 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png']), 'onerror' => '/img/not-found.png']) ?>
                            </label>
                            <div class="hidden">
                                <?= $this->Form->control('cuisine_image', ['type' => 'file', 'label' => false, 'accept' => 'image/*', 'required' => false, 'need' => empty($chef_cuisines->cuisine_image) ? '1' : '0']) ?>
                            </div>
                            <?= $this->Form->control('cuisine_name') ?>
                            <div class="text-center">
                                <?= $this->Form->submit('Save') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>