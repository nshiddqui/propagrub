<?php
$this->assign('title', ' Bank Details');
?>
<?= $this->Html->css('https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css', ['block' => 'css']) ?>
<?= $this->Html->script('https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js', ['block' => 'script']) ?>
<?= $this->Html->css('bank-details', ['block' => 'css']) ?>
<?= $this->Form->create($bank_detail, ['type' => 'file']) ?>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <h4>Bank Details</h4>
                <span class="text-bold" style="position: absolute;top: 10px;left: 37%;">Current Status : (<?= ($bank_detail->status == '1' ? 'Approved' : ($bank_detail->status == '2' ? 'Declined<br>(' . $bank_detail->message . ')' : 'Pending')) ?>)</span>
                <span class="pull-right" style="margin-top: -30px">
                    <?= $this->Html->link('<i class="fa fa-check"></i>', ['action' => 'documents', $bank_detail->id, '?' => ['status' => '1']], ['escape' => false, 'class' => 'btn btn-success', 'title' => 'Approved']) ?>
                    <?= $this->Html->link('<i class="fa fa-times"></i>', '#modal-default', ['escape' => false, 'class' => 'btn btn-danger', 'title' => 'Declined', 'web' => true, 'data-toggle' => 'modal']) ?>
                </span>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->Form->hidden('id') ?>
                        <?= $this->Form->control('holder_name', ['value' => (!empty($bank_detail->holder_name) ? $bank_detail->holder_name : $authUser['first_name'] . ' ' . $authUser['last_name']), 'required' => true]) ?>
                        <?= $this->Form->control('sort_code', ['value' => $bank_detail->routing_number, 'required' => true]) ?>
                        <?= $this->Form->control('phone', ['label' => 'Phone Number', 'type' => 'text', 'required' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('account_number', ['required' => true]) ?>
                        <?= $this->Form->control('payment_email', ['required' => true]) ?>
                        <?= $this->Form->control('date_of_birth', ['datepicker' => true, 'value' => (!empty($bank_detail->dob) ? date('Y-m-d', strtotime($bank_detail->dob)) : ''), 'required' => true]) ?>
                    </div>
                </div>
                <h4>Address Info</h4>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->control('address', ['label' => 'Address Line1', 'required' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('city', ['required' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('post_code', ['label' => 'Postal Code', 'required' => true]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create($bank_detail, ['type' => 'get']) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Please Enter Reason</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->hidden('id') ?>
                <?= $this->Form->hidden('status', ['value' => '2']) ?>
                <?= $this->Form->control('message', ['type' => 'textarea', 'required' => true]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
    if (jQuery().intlTelInput) {
        setTimeout(function () {
            $("#phone").intlTelInput({
                initialCountry: "auto",
                separateDialCode: true,
                onlyCountries: ['gb', 'in'],
                geoIpLookup: function (callback) {
                    $.get('https://ipinfo.io/' + IP_ADDRESS, {
                        token: '101bda4458f672'
                    }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/7.0.1/lib/libphonenumber/build/utils.js" // just for formatting/placeholders etc
            }).then(function () {
                var el = $('#phone');
                el.val(el.val().replace(' ', ''));
            });
        }, 30);
    }
</script>