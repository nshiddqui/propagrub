<?php
$this->assign('title', 'Driver Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Driver</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Driver') ?>
    </div>
</div>