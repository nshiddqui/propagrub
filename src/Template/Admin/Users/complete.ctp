<?php
$this->assign('title', $user->full_name . ' Chef');
?>
<?= $this->Html->css('https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css', ['block' => 'css']) ?>
<?= $this->Html->script('https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js', ['block' => 'script']) ?>
<?= $this->Html->script('https://maps.googleapis.com/maps/api/js?key=' . GMAP_API . '&libraries=places', ['block' => 'script']); ?>
<?= $this->Html->css('user-complete', ['block' => 'css']) ?>
<?= $this->Html->script('user-complete', ['block' => 'script']) ?>
<?= $this->Form->create($user, ['type' => 'file']) ?>
<div class="box">
    <div class="box-body">
        <div class="content">
            <?= $this->Html->link('<i class="fa fa-bank fa-2x"></i> Bank Details', ['action' => 'bank_details', $user->id], ['escape' => false, 'class' => 'text-black pull-right', 'title' => 'Check Chef Bank Details']) ?>
            <h3>Services</h3>
            <hr>
            <section>
                <div class="row">
                    <?php foreach ($service_type as $service_id => $service) { ?>
                        <div class="col-md-3">
                            <?= $this->Form->control('chef_detail.service_type', ['type' => 'radio', 'options' => [$service_id => $service], 'label' => false, 'hiddenField' => false]) ?>
                        </div>
                    <?php } ?>
                </div>
            </section>
            <h3>Contact Person Details</h3>
            <hr>
            <section>
                <div class="row">
                    <?php if ($user->is_complete == 1) { ?>
                        <div class="col-md-9">
                            <div class="row">
                            <?php } ?>
                            <div class="col-md-4"><label for="first-name">Contact Person Name *</label></div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $this->Form->control('first_name', ['label' => false]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $this->Form->control('last_name', ['label' => false]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4"><label for="email">Email Address *</label></div>
                            <div class="col-md-8"><?= $this->Form->control('email', ['label' => false, 'readonly' => !empty($user->email), 'required' => true]) ?></div>
                            <div class="col-md-4"><label for="mobile">Contact Number *</label></div>
                            <div class="col-md-8">
                                <?= $this->Form->control('mobile', ['label' => false, 'readonly' => !empty($user->mobile), 'required' => true]) ?>
                            </div>
                            <div class="col-md-4"><label>Gender *</label></div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-2">
                                        <?= $this->Form->control('gender', ['type' => 'radio', 'options' => [1 => 'Male'], 'label' => false, 'hiddenField' => false]) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $this->Form->control('gender', ['type' => 'radio', 'options' => [2 => 'Female'], 'label' => false, 'hiddenField' => false]) ?>
                                    </div>
                                </div>
                            </div>
                            <?php if ($user->is_complete == 1) { ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Profile Picture</label>
                                    <label for="image" class="border-black">
                                        <?= $this->Html->image((!empty($user->image) ? $user->image . '?' . microtime(true) : 'not-found.png'), ['class' => 'thumbnail-image', 'profile' => WEB_PROFILE_IMAGE, 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                                    </label>
                                    <div class="hidden">
                                        <?= $this->Form->control('image', ['type' => 'file', 'label' => false, 'accept' => 'image/*', 'required' => false]) ?>
                                    </div>
                                    <p class="thumbnail-paragraph">[Note: Image dimension between 250x250 and 512x512]</p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
            <h3>Chef Store Details</h3>
            <hr>
            <section>
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="resturant-name">Chef / Restaurant Name *</label>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->control('chef_detail.id') ?>
                                <?= $this->Form->control('chef_detail.resturant_name', ['label' => false]) ?>
                            </div>
                            <div class="col-md-4">
                                <label for="resturant-name">Select Cuisines</label>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->control('chef_detail.chef_cuisines_ids', ['label' => false, 'options' => $chef_cuisines, 'value' => explode(',', $user['chef_detail'] ? $user['chef_detail']->chef_cuisines_ids : ''), 'multiple' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <label for="order-min-amount">Order Min Amount *</label>
                            </div>
                            <div class="col-md-8">
                                <i class="usd icon">£</i>
                                <?= $this->Form->control('chef_detail.order_min_amount', ['label' => false, 'type' => 'number']) ?>
                            </div>
                            <div class="col-md-4">
                                <label for="short-description">Short Description *</label>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->control('chef_detail.short_description', ['label' => false, 'type' => 'textarea']) ?>
                            </div>
                            <div class="col-md-12">
                                <label for="address">Store Timings</label>
                            </div>
                            <div class="col-md-12">
                                <?php
                                $weekKey = 0;
                                foreach ($weeks as $week_id => $week) {
                                    ?>
                                    <div class="row single-day">
                                        <div class="col-md-4"><?= $week ?></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <?= $this->Form->hidden("chef_timings.{$weekKey}.id") ?>
                                                    <?= $this->Form->hidden("chef_timings.{$weekKey}.week_id", ['value' => $week_id]) ?>
                                                    <?= $this->Form->control("chef_timings.{$weekKey}.start_time", ['label' => false, 'type' => 'time""', 'start-time' => true]) ?>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>To</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <?= $this->Form->control("chef_timings.{$weekKey}.end_time", ['label' => false, 'type' => 'time""', 'end-time' => true]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $weekKey++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Store Banner</label>
                                <label class="border-black">
                                    <?= $this->Html->image((!empty($user['chef_detail']->image) ? $user['chef_detail']->image : 'not-found.png'), ['class' => 'thumbnail-image', 'profile' => WEB_PROFILE_IMAGE, 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                                </label>
                            </div>
                            <div class="col-md-12">
                                <label>Chef Store Address *</label>
                                <?= $this->Form->hidden('chef_detail.lat', ['id' => 'lat']) ?>
                                <?= $this->Form->hidden('chef_detail.lng', ['id' => 'lng']) ?>
                                <?= $this->Form->control('chef_detail.address', ['label' => false]) ?>
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <h3>Chef Documents</h3>
            <hr>
            <section>
                <div class="row">
                    <div class="col-md-4">
                        <label>Chef Company Name *</label>
                    </div>
                    <div class="col-md-8">
                        <?= $this->Form->control('chef_detail.company_name', ['label' => false, 'required' => true, 'placeholder' => 'Chef Company Name']) ?>
                    </div>
                    <div class="col-md-4">
                        <label>Chef Company Number *</label>
                    </div>
                    <div class="col-md-8">
                        <?= $this->Form->control('chef_detail.company_number', ['label' => false, 'required' => true, 'placeholder' => 'Chef Company Number']) ?>
                    </div>
                    <div class="col-md-4">
                        <label>Chef Council Registration Document *</label>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->control('chef_detail.council_registration', ['label' => false, 'type' => 'file', 'required' => empty($user['chef_detail']->council_registration)]) ?>
                    </div>
                    <div class="col-md-1">
                        <?php
                        if (!empty($user['chef_detail']->council_registration)) {
                            echo $this->Html->link('<i class="fa fa-download fa-2x"></i>', $user['chef_detail']->council_registration, ['escape' => false]);
                        }
                        ?>
                    </div>
                    <div class="col-md-4">
                        <label>Chef Level 2 Certificate *</label>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->control('chef_detail.level_certificate', ['label' => false, 'type' => 'file', 'required' => empty($user['chef_detail']->level_certificate)]) ?>
                    </div>
                    <div class="col-md-1">
                        <?php
                        if (!empty($user['chef_detail']->level_certificate)) {
                            echo $this->Html->link('<i class="fa fa-download fa-2x"></i>', $user['chef_detail']->level_certificate, ['escape' => false]);
                        }
                        ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>