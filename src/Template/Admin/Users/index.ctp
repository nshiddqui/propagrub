<?php
$this->assign('title', 'Chef Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Chef</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Users') ?>
    </div>
</div>