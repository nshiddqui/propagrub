<?php
$this->assign('title', 'Dashboard Summary');
?>
<?= $this->Html->component('bootstrap-daterangepicker/daterangepicker', 'css', ['block' => 'css']) ?>
<?= $this->Html->component('moment/moment', 'script', ['block' => 'script']) ?>
<?= $this->Html->component('bootstrap-daterangepicker/daterangepicker', 'script', ['block' => 'script']) ?>
<?= $this->Html->script('dashboard', ['block' => 'script']) ?>
