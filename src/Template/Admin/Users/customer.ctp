<?php
$this->assign('title', 'Customer Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Customer</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Customer') ?>
    </div>
</div>