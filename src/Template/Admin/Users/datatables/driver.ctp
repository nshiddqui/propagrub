<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $this->Html->image((!empty($result->image) ? $result->image . '?' . microtime(true) : 'not-found.png'), ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle', 'profile' => WEB_PROFILE_IMAGE]),
        h($result->full_name),
        h($result->email),
        h($result->mobile),
        !empty($result['users_document']->id) ? $this->Html->link('View', ['action' => 'documents', $result->id]) : 'No Documents',
        $result->status ? 'Active' : 'Inactive',
        $this->Form->postLink('<i class="fa fa' . ($result->status ? '-dot' : '') . '-circle-o fa-lg"></i>', ['action' => 'active', $result->id, ($result->status == '1' ? '0' : '1')], ['confirm' => __('Are you sure you want to ' . ($result->status == '1' ? 'Deactivate' : 'Activate') . ' # {0}?', $result->client_name), 'escape' => false, 'class' => 'text-black', 'title' => 'Change Driver Status']) . '&nbsp;&nbsp;&nbsp;' .
        (!empty($result['bank_detail']) ? $this->Html->link('<i class="fa fa-bank fa-2x"></i>', ['action' => 'bank_details', $result->id], ['escape' => false, 'class' => 'text-black', 'title' => 'Check Driver Bank Details']) : '')
    ]);
}
echo $this->DataTables->response();
