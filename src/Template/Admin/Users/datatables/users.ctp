<?php

foreach ($results as $result) {
    $percentage = 0;
    /* Profile Section Start (20)*/
    if (!empty($result->image)) {
        $percentage += 10;
    }
    if (!empty($result->first_name)) {
        $percentage += 2;
    }
    if (!empty($result->email)) {
        $percentage += 2;
    }
    if (!empty($result->mobile)) {
        $percentage += 2;
    }
    if (!empty($result->last_name)) {
        $percentage += 2;
    }
    if (!empty($result->gender)) {
        $percentage += 2;
    }
    /* Profile Section End */
    /* Detail Section Start (40)*/
    if (!empty($result['chef_detail']->image)) {
        $percentage += 10;
    }
    if (!empty($result['chef_detail']->resturant_name)) {
        $percentage += 2.5;
    }
    if (!empty($result['chef_detail']->chef_cuisines_ids)) {
        $percentage += 2.5;
    }
    if (!empty($result['chef_detail']->order_min_amount)) {
        $percentage += 2.5;
    }
    if (!empty($result['chef_detail']->short_description)) {
        $percentage += 2.5;
    }
    if (!empty($result['chef_detail']->address)) {
        $percentage += 10;
    }
    if (!empty($result['ChefTimings']['id'])) {
        $percentage += 10;
    }
    /* Detail Section End */
    /* Document Section Start (40)*/
    if ($result['chef_detail']->sole_trader) {
        if (!empty($result['chef_detail']->sole_trader_name)) {
            $percentage += 20;
        }
    } else {
        if (!empty($result['chef_detail']->company_name)) {
            $percentage += 10;
        }
        if (!empty($result['chef_detail']->company_number)) {
            $percentage += 10;
        }
    }
    if (!empty($result['chef_detail']->level_certificate)) {
        $percentage += 10;
    }
    if (!empty($result['chef_detail']->council_registration)) {
        $percentage += 10;
    }
    /* Document Section End */
    $this->DataTables->prepareData([
        $this->Html->image((!empty($result->image) ? $result->image . '?' . microtime(true) : 'not-found.png'), ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle', 'profile' => WEB_PROFILE_IMAGE]),
        h($result->full_name),
        h($result->email),
        h($result->mobile),
        $percentage . '%',
        $result->status ? 'Active' : 'Inactive',
        $this->Form->postLink('<i class="fa fa' . ($result->status ? '-dot' : '') . '-circle-o fa-lg"></i>', ['action' => 'active', $result->id, ($result->status == '1' ? '0' : '1')], ['confirm' => __('Are you sure you want to ' . ($result->status == '1' ? 'Deactivate' : 'Activate') . ' # {0}?', $result->client_name), 'escape' => false, 'class' => 'text-black', 'title' => 'Change Chef Status']) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .
            $this->Html->link(__('<i class="fa fa-eye fa-lg"></i>'), ['action' => 'complete', $result->id], ['escape' => false, 'class' => 'text-black', 'title' => 'View Chef Details'])
    ]);
}
echo $this->DataTables->response();
