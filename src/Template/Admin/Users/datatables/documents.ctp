<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        '<i class="fa fa-file fa-3x"></i>"',
        h($result->doc_type),
        h($result->doc_number),
        h($result->doc_name),
        $this->Html->link('<i class="fa fa-download fa-2x"></i>', $result->file, ['escape' => false, 'download' => $result->file, 'target' => '_BLANK']),
        ($result->status == '1' ? 'Approved' : ($result->status == '2' ? 'Declined<br>(' . $result->message . ')' : 'Pending')),
        $this->Html->link('<i class="fa fa-check"></i>', ['action' => 'documents', $result->user_id, '?' => ['id' => $result->id, 'status' => '1']], ['escape' => false, 'class' => 'btn btn-success', 'title' => 'Approved']) . '&nbsp;&nbsp;&nbsp;' .
        $this->Html->link('<i class="fa fa-times"></i>', '#', ['escape' => false, 'class' => 'btn btn-danger', 'title' => 'Declined', 'web' => true, 'onclick' => 'reason(' . $result->id . ')'])
    ]);
}
echo $this->DataTables->response();
