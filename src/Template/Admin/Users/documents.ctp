<?php
$this->assign('title', 'Document Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Document</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Documents') ?>
    </div>
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create(null, ['type' => 'get']) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Please Enter Reason</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->hidden('id', ['id' => 'id']) ?>
                <?= $this->Form->hidden('status', ['value' => '2']) ?>
                <?= $this->Form->control('message', ['type' => 'textarea', 'required' => true]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
    function reason(id) {
        $('#id').val(id);
        $('#modal-default').modal('show');
    }
</script>