<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        (!empty($result['user']) ? $result['user']->full_name : (array_key_exists($result->to_user_id, $send_by) ? $send_by[$result->to_user_id] : 'Not Fund')),
        h($result->notification_title),
        h($result->notification_text),
        $result->created_at,
        $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->notification_title), 'escape' => false, 'class' => 'text-black'])
    ]);
}
echo $this->DataTables->response();
