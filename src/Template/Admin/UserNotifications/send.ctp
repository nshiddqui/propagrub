<?php
$this->assign('title', 'Notification Management');
?>
<?= $this->Form->create($notification) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Send Notification') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('user_type', ['options' => $send_by]) ?>
                    <?= $this->Form->hidden('to_user_id[]') ?>
                    <div id="UserType" style="display: none">
                        <div class="form-group">
                            <label for="select-data-user">Select User</label>
                            <select id="select-data-user" name="to_user_id[]" multiple=""></select>
                        </div>
                    </div>
                    <?php
                    echo $this->Form->control('notification_title');
                    echo $this->Form->control('notification_text', ['type' => 'textarea']);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Send')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>