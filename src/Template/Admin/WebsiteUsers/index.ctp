<?php
$this->assign('title', 'Website Users Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Website Users</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?= $this->DataTables->render('WebsiteUsers') ?>
            </div>
        </div>
    </div>
</div>