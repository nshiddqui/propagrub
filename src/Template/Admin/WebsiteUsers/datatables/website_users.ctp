<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h($result->first_name),
        h($result->last_name),
        h($result->email),
        h($result->phone),
        h($result->User_type),
        h($result->created),
    ]);
}
echo $this->DataTables->response();
