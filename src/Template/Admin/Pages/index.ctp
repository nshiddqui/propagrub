<?php
$this->assign('title', 'Manage Page');
?>
<?= $this->Html->css('floara', ['block' => true]) ?>
<?= $this->Html->script('floara', ['block' => true]) ?>
<div class="box" style="border-radius: 15px;">
    <div class="box-header">
        <h3>Update <?= $page->page_name ?> Page</h3>
    </div>
    <?= $this->Form->create($page) ?>
    <div class="box-body text-left">
        <div class="row">
            <div class="col-md-12">
                <?= $this->Form->control('text', ['label' => false]); ?>
            </div>
            <div class="col-md-12 text-center">
                <?= $this->Form->submit('Update Page', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
<script>
    new FroalaEditor('#text', {
        height: 900,
        imageUploadURL: '<?= $this->Url->build(['action' => 'upload_image']) ?>'
    });
</script>