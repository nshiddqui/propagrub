<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result['user']->full_name,
        $this->Html->image($result['community']->community_image, ['style' => 'height: 45px; width: 45px', 'profile' => WEB_COMMUNITIES, 'class' => 'img-circle',]),
        h($result['community']->community_name),
        h($result->reported_reason),
        h($result->comments),
        $result->created_at,
        $this->Form->postLink('Delete Community', ['controller' => 'Communities', 'action' => 'delete', $result['community']->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result['community']->community_name), 'escape' => false, 'class' => 'btn btn-danger', 'title' => 'Change Status'])
    ]);
}
echo $this->DataTables->response();
