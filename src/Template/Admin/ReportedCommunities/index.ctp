<?php
$this->assign('title', 'Reported Communities');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Reported Communities</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('ReportedCommunities') ?>
    </div>
</div>