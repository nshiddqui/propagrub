<?php
$this->assign('title', 'List ' . $this->Html->link($community->community_name, ['controller' => 'communities', 'action' => 'edit', $community->id]) . ' Communities Members');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('CommunityMember') ?>
    </div>
</div>