<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $this->Html->image((!empty($result['user']->image) ? $result['user']->image . '?' . microtime(true) : 'not-found.png'), ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle', 'profile' => WEB_PROFILE_IMAGE]),
        h($result['user']->full_name),
        h($result['user']->email),
        h($result['user']->mobile),
        $result['user']->status ? 'Active' : 'Inactive',
        $this->Form->postLink('<i class="fa fa-trash fa-lg"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete member # {0}?', $result['user']->full_name), 'escape' => false, 'class' => 'text-black', 'title' => 'Delete Member'])
    ]);
}
echo $this->DataTables->response();
