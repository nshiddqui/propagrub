<?php
$this->assign('title', 'Communities Management');
?>
<?= $this->Form->create($community, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __(($community->id ? 'Update' : 'Add') . ' Community') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="community-image">
                        <?= $this->Html->image($community->community_image, ['class' => 'thumbnail-image', 'profile' => WEB_COMMUNITIES, 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'user.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Thumbnail Image For Community</p>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('community_image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*']);
                    echo $this->Form->control('community_name');
                    echo $this->Form->control('is_verified', ['options' => ['No', 'Yes']]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__(($community->id ? 'Update' : 'Add'))) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>