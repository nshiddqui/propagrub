<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result['reported_by_user']->full_name,
        $this->Html->image($result['reported_by_user']->image, ['style' => 'height: 45px; width: 45px', 'profile' => WEB_PROFILE_IMAGE, 'class' => 'img-circle',]),
        h($result['community_post']->post_title),
        h($result['community_post']->post_detail),
        h($result->reported_reason),
        h($result->comments),
        $result->created_at,
        $this->Form->postLink('Delete Feed', ['action' => 'delete', $result['community_post']->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result['community_post']->post_title), 'escape' => false, 'class' => 'btn btn-danger', 'title' => 'Change Status'])
    ]);
}
echo $this->DataTables->response();
