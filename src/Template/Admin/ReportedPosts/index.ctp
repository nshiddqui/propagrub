<?php
$this->assign('title', 'Reported Feeds');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Reported Feeds</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('ReportedPosts') ?>
    </div>
</div>