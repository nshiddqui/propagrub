<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result['reported_by_user']->full_name,
        $result['reported_to_user']->full_name,
        $this->Html->image($result['reported_to_user']->image, ['style' => 'height: 45px; width: 45px', 'profile' => WEB_PROFILE_IMAGE, 'class' => 'img-circle',]),
        h($result->reported_options),
        h($result->reported_detail),
        $result->created_at,
        $this->Form->postLink('Delete Chef', ['action' => 'delete', $result['reported_to_user']->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result['reported_to_user']->full_name), 'escape' => false, 'class' => 'btn btn-danger', 'title' => 'Change Status'])
    ]);
}
echo $this->DataTables->response();
