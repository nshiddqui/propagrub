<?php
$this->assign('title', 'Menus');
?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h1 class="col-12 text-dark font-weight-bold">Menus</h1>
            <div class="col-12 my-3">
                <?= $this->Html->image('menu-bg.png', ['class' => 'w-100']) ?>
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <h3 class="text-dark font-weight-bold">Add your menu</h3>
                        <p class="text-dark mb-0">Upload a JPEG, CSV, PDF, PNG, Excel or Word file. Or if your menu is available online, you can give us the link to it.</p>
                        <p class="text-dark">Once you've signed your agreement, we'll create your menu for you - it usually takes us a few days.</p>
                        <?= $this->Form->create(null, ['type' => 'file', 'id' => 'form-menu-upload']) ?>
                        <div class="text-option">
                            <?= $this->Form->control('menu_file', ['label' => 'Upload', 'accept' => '.pdf', 'type' => 'file', 'class' => 'form mt-3']) ?>
                            <p>We accept JPEG, CSV, PDF, PNG, Excel or Word files.</p>
                        </div>
                        <?= $this->Form->control('menu_file_url', ['label' => 'Or add a link to your menu', 'type' => 'url']) ?>
                        <h3 class="text-dark font-weight-bold">Upload your Alcohol Licence</h3>
                        <p class="text-dark">In the UK, selling alchohol without a license is against the law. You must upload your license if you're selling alchohol on Propagrub.</p>
                        <div class="text-option">
                            <?= $this->Form->control('alchohol_license_file', ['label' => 'Upload a photo or PDF of your premises license', 'accept' => '.pdf', 'type' => 'file', 'class' => 'form mt-3']) ?>
                            <p>We accept JPEG, PDF, PNG files.</p>
                        </div>
                        <?= $this->Form->submit('Add menu', ['class' => 'btn btn-primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>