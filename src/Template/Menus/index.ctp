<?php
$this->assign('title', 'Menu Details');
$this->Html->scriptStart(['block' => true]);
echo "var CHANGE_MENU_NAME_URL = '" . $this->Url->build('/api/v1/menus/change-name/') . "';";
echo "var MENU_STATUS_URL = '" . $this->Url->build('/menus/change-status/') . "';";
$this->Html->scriptEnd();
?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h1 class="col-12 text-dark font-weight-bold">Menu
                <a href="javascript:addMenu(addMenu)" class="mr-3 h5 pull-right" style="color:#d51b81;">
                    <?= $this->Html->image('noun-menu-1378672.png', ['class' => 'mr-2', 'style' => 'height: 20px;']) ?></i>Add menu
                </a>
            </h1>
            <!-- #Menu Item ==================== -->
            <?php if ($menus->count()) { ?>
                <?php foreach ($menus as $menu) { ?>
                    <div class="col-md-4 col-sm-6 my-3">
                        <div class="layers bgc-white p-20 rounded">
                            <div class="layer w-100 p-5">
                                <div style="position: relative;">
                                    <?php
                                    if ($menu->status == 1) {
                                        echo $this->Html->image('check.png', ['style' => 'position: absolute;height: 20px;right: 3px;top: 3px;']);
                                    }
                                    ?>
                                    <a href="#" onclick="$(this).attr('href',$(this).find('img').attr('src'));" fancybox><?= $this->Html->image($menu->image, ['class' => 'menu-image mb-3', 'profile' => WEB_MENU, 'onerror' => $this->Url->build('/img/menu_placeholder.png')]) ?></a>
                                    <label for="menu-image-upload" style="position: absolute;bottom: 15px;right: 10px;cursor: pointer;">
                                        <?= $this->Html->image('edit_image.png', ['style' => 'height: 25px;']) ?>
                                    </label>
                                    <?= $this->Form->create(null, ['url' => ['controller' => 'Menus', 'action' => 'uploadMenu', $menu->id], 'type' => 'file']) ?>
                                    <?= $this->Form->control('menu_image_upload', ['type' => 'file', 'class' => 'menu-upload-custom d-none', 'label' => false, 'accept' => 'image/*']) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                                <h3 id="menu-name-<?= $menu->id ?>" class="text-dark font-weight-bold"><span><?= $menu->menu_name ?></span><a href="javascript:changeMenuName(<?= $menu->id ?>)">
                                        <?= $this->Html->image('edit.png', ['class' => 'menu-edit-icon propa-pencil']) ?>
                                    </a></h3>
                                <p class="text-dark h5"><i class="ti-home"></i> <?= count($menu['menu_categories']) ?> Products</p>
                                <hr class="my-4">
                                <?= $this->Html->link('Edit menu', ['controller' => 'Menus', 'action' =>  $menu->id], ['class' => 'btn btn-primary menu-edit-button font-weight-bold']) ?>
                                <a href="javascript:void(0)" class="btn btn-default menu-more-button font-weight-bold" data-delay="400" data-placement="bottom" id="<?= $menu->id ?>" data-toggle="popover" data-trigger="focus" data-html="true" data-content='<a href="javascript:void(0)" class="menu-delete-action">Delete Menu?</a><?= ($menu->status == 0 ? '<br><a href="javascript:void(0)" class="menu-status-action">Set as active?</a>' : '') ?>'>
                                    <?= $this->Html->image('more.png', ['style' => 'height: 34px;']) ?>
                                </a>
                                <div id="dropdown-propover" class="d-none">
                                    <?= $this->Form->postLink('Delete Menu?', ['action' => 'delete', $menu->id], ['id' => 'menu-delete-' . $menu->id]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="w-100 text-center">
                    <?= $this->Html->image('no_search_data.png', ['style' => 'height:200px']) ?>
                    <h3>No Menu Found</h3>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- Manage Option -->
<div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="Options" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Add New Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <?= $this->Form->create($new_menu, ['id' => 'modal-form', 'type' => 'file']) ?>
            <div class="modal-body">
                <div class="layers bd bgc-white p-10 rounded">
                    <div class="layer w-100">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="image-placeholder text-center" id="product-image-empty">
                                    <?= $this->Html->image('placeholder_camera.png') ?>
                                    <h4 class="text-dark mb-0">No photo</h4>
                                    <p class="text-dark mb-0">1200 x 800 pixels (.jpg - 18MB)</p>
                                    <p class="text-dark">this will be your main cover photo on your chef profile</p>
                                    <label for="image" class="btn btn-default bgc-white bd text-primary">Upload photo</label>
                                    <div class="d-none">
                                        <?= $this->Form->control('image', ['type' => 'file', 'accept' => 'image/*', 'required' => false]) ?>
                                    </div>
                                </div>
                                <label style="display: none;" class="image-placeholder" for="image" id="product-image">

                                </label>
                            </div>
                            <div class="col-md-5">
                                <div class="my-4 py-2 pl-2">
                                    <p class="h4 font-weight-normal">Make sure menu is:</p>
                                    <p class="mb-0"><?= $this->Html->image('grey_menus.png', ['class' => 'menu-menus-image']) ?>Ready to server</p>
                                    <p class="mb-0"><?= $this->Html->image('grey_light.png', ['class' => 'menu-light-image']) ?>Brightly lit</p>
                                    <p><?= $this->Html->image('menu_grey_full_screen.png', ['class' => 'menu-screen-image']) ?>Large and cnetered</p>
                                    <a href="javascript:void(0)">See examples</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <?= $this->Form->control('menu_name', ['placeholder' => 'Enter menu name', 'label' => 'Name']) ?>
                        <div class="text-option">
                            <?= $this->Form->control('menu_description', ['type' => 'textarea', 'placeholder' => 'Menu Bio...', 'label' => 'Description']) ?>
                            <p>This should give customers all the information they need to understand the menu.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="monu-delete-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Delete Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body text-dark">
                Are you sure to delete this menu?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="action-delete-menu-confirmed">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="monu-status-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Status Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body text-dark">
                Are you sure to set as active this menu?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="action-status-menu-confirmed">Active</button>
            </div>
        </div>
    </div>
</div>