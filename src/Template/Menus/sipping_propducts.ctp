<?php
$this->assign('title', $menu_categories->category_name);
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->css('https://cdn.jsdelivr.net/npm/icheck@1.0.2/skins/square/blue.css', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/jquery.sumoselect.min.js', ['block' => true]);
$this->Html->script('https://cdn.jsdelivr.net/npm/icheck@1.0.2/icheck.min.js', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js', ['block' => true]);
$this->Html->scriptStart(['block' => true]);
echo "var CHANGE_MENU_DESCRIPTION_URL = '" . $this->Url->build('/api/v1/menus/change-description/') . "';";
echo "var CHANGE_CATEGORY_NAME_URL = '" . $this->Url->build('/api/v1/menu-categories/change-description/') . "';";
echo "var CHANGE_CATEGORY_DESCRIPTION_URL = '" . $this->Url->build('/api/v1/menu-categories/change-description/') . "';";
echo "var SORTING_PRODUCT_URL = '" . $this->Url->build('/api/v1/menu-products/sorting/') . "';";
echo "var ADD_PRODUCT_URL = '" . $this->Url->build('/menu-products/add/0/') . "';";
echo "var EDIT_PRODUCT_URL = '" . $this->Url->build('/menu-products/edit/') . "';";
echo "var MANAGE_OPTION_URL = '" . $this->Url->build('/product-ingredients-categories/') . "';";
echo "var DELETE_OPTION_URL = '" . $this->Url->build('/api/v1/product-ingredients-categories/delete/') . "';";
$this->Html->scriptEnd();
?>
<?= $this->Html->script('//code.jquery.com/ui/1.13.0-rc.3/jquery-ui.min.js', ['block' => true]) ?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <div class="col-12 my-3">
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <div class="row">
                            <div class="col-md-9">
                                <h3 class="col-12 text-dark font-weight-bold"><span><?= $menu_categories->category_name ?></span></h3>
                            </div>
                            <div class="col-md-3 text-right">
                                <a href="javascript:manageOptions(<?= $menu_categories->id ?>,'<?= $menu_categories->category_name ?>')" class="mr-3 h5" style="color:#d51b81;">
                                    <?= $this->Html->image('noun-menu-1378672.png', ['class' => 'mr-2', 'style' => 'height: 20px;']) ?>Add options
                                </a>
                            </div>
                        </div>
                        <div class="row mt-3 menu-sortable">
                            <?php foreach ($menu_categories['menu_products'] as $menu_products) { ?>
                                <div class="col-md-4 col-sm-6 my-2 menu-product-item-info" product-id="<?= $menu_products->id ?>">
                                    <div class="layers bd bgc-white p-10 rounded">
                                        <div class="layer w-100 row">
                                            <div class="col-md-3 px-0">
                                                <div class="square" style="background-image: url('<?= WEB_PRODUCT . @$menu_products['menu_products_images'][0]->image ?>'), url('<?= $this->Url->build('/img/product_image_placeholder.png') ?>');"></div>
                                            </div>
                                            <div class="col-md-7" onclick="editProduct(<?= $menu_products->id ?>)">
                                                <p class="mb-0 text-truncate text-dark menu-product-info"><?= $menu_products->product_name ?></p>
                                                <p class="mb-0 text-truncate menu-product-info"><?= $menu_products->product_detail ?></p>
                                                <p class="mb-0 text-truncate menu-product-info">£ <?= $menu_products->product_price ?></p>
                                            </div>
                                            <div class="col-md-1">
                                                <i class="drag-option d-md-block d-none"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-md-4 col-sm-6 my-2">
                                <div class="layers bgc-white p-10 rounded text-center new-item-buttom">
                                    <a href="javascript:addProduct(<?= $menu_categories->id ?>)" class="layer w-100 my-4 py-2">+ New item</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Manage Option -->
<div class="modal fade" id="manage-options-modal" tabindex="-1" role="dialog" aria-labelledby="Options" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Options for <span id="option-title">category</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="button" id="save-action-option-button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Products -->
<div class="modal fade" id="menu-modal" tabindex="-1" role="dialog" aria-labelledby="New item" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Add new item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body px-4">

            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="button" class="btn btn-primary" id="action-modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>