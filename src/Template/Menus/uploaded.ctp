<?php
$this->assign('title', 'Menus');
?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h1 class="col-12 text-dark font-weight-bold">Menus</h1>
            <div class="col-12 my-3">
                <?= $this->Html->image('cooking.png', ['class' => 'w-100']) ?>
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <h3 class="text-dark font-weight-bold">We're creating your menu</h3>
                        <p class="text-dark mb-0">You'll receive an email once it's complete. After that, you can:</p>
                        <ul class="pl-3">
                            <li>Edit menu items and create new ones</li>
                            <li>Enhance your menu with options and upsells</li>
                            <li>Upload your own photos</li>
                            <li>Publish your changes instantly</li>
                        </ul>
                        <?= $this->Html->link(' Back to Home', ['controller' => 'Users', 'action' => 'onboarding'], ['class' => 'btn btn-primary mt-4']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>