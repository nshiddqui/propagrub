<?php
$this->assign('title', $menu->menu_name);
$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/sumoselect.min.css', ['block' => true]);
$this->Html->css('https://cdn.jsdelivr.net/npm/icheck@1.0.2/skins/square/blue.css', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.3.30/jquery.sumoselect.min.js', ['block' => true]);
$this->Html->script('https://cdn.jsdelivr.net/npm/icheck@1.0.2/icheck.min.js', ['block' => true]);
$this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js', ['block' => true]);
$this->Html->scriptStart(['block' => true]);
echo "var CHANGE_MENU_DESCRIPTION_URL = '" . $this->Url->build('/api/v1/menus/change-description/') . "';";
echo "var CHANGE_CATEGORY_NAME_URL = '" . $this->Url->build('/api/v1/menu-categories/change-description/') . "';";
echo "var CHANGE_CATEGORY_DESCRIPTION_URL = '" . $this->Url->build('/api/v1/menu-categories/change-description/') . "';";
echo "var SORTING_PRODUCT_URL = '" . $this->Url->build('/api/v1/menu-products/sorting/') . "';";
echo "var ADD_PRODUCT_URL = '" . $this->Url->build('/menu-products/add/' . $menu->id . '/') . "';";
echo "var EDIT_PRODUCT_URL = '" . $this->Url->build('/menu-products/edit/') . "';";
echo "var MANAGE_OPTION_URL = '" . $this->Url->build('/product-ingredients-categories/') . "';";
echo "var DELETE_OPTION_URL = '" . $this->Url->build('/api/v1/product-ingredients-categories/delete/') . "';";
$this->Html->scriptEnd();
?>
<?= $this->Html->script('//code.jquery.com/ui/1.13.0-rc.3/jquery-ui.min.js', ['block' => true]) ?>
<div class="row gap-20 masonry pos-r" style="position: relative; height: 1825.2px;">
    <div class="masonry-sizer col-md-6"></div>
    <div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
        <div class="row gap-30 menu-container">
            <h5 class="col-12 text-primary"><?= $this->Html->link($this->Html->image('btn_back.png', ['style' => 'height: 15px;']) . ' Menu', ['controller' => 'Menus', 'action' => 'index'], ['style' => 'color: #da2485;', 'escape' => false]) ?></h5>
            <h1 class="col-12 text-dark font-weight-bold"><?= $menu->menu_name ?></h1>
            <p style="padding-top: 0px !important;">What do you think of Menus? <?= $this->Html->link($this->Html->image('open_page.png', ['style' => 'height:15px']), '//propagrub.com', ['escape' => false, 'target' => '_BLANK', 'web' => true]) ?></p>
            <div class="col-12 my-3">
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div style="position: relative;">
                                    <a href="#" onclick="$(this).attr('href',$(this).find('img').attr('src'));" fancybox><?= $this->Html->image($menu->image, ['class' => 'menu-image mb-3', 'profile' => WEB_MENU, 'onerror' => $this->Url->build('/img/menu_placeholder.png')]) ?></a>
                                    <label for="menu-image-upload" style="position: absolute;bottom: 15px;right: 10px;cursor: pointer;">
                                        <?= $this->Html->image('edit_image.png', ['style' => 'height: 25px;']) ?>
                                    </label>
                                    <?= $this->Form->create(null, ['url' => ['controller' => 'Menus', 'action' => 'uploadMenu', $menu->id], 'type' => 'file']) ?>
                                    <?= $this->Form->control('menu_image_upload', ['type' => 'file', 'class' => 'menu-upload-custom d-none', 'label' => false, 'accept' => 'image/*']) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <p id="menu-description-<?= $menu->id ?>" class="text-dark"><span><?= $menu->menu_description ?></span><a href="javascript:changeMenuDescription(<?= $menu->id ?>)">
                                        <?= $this->Html->image('edit.png', ['class' => 'menu-edit-icon propa-pencil']) ?></a></p>
                            </div>
                            <div class="col-md-1 text-right">
                                <a href="javascript:void(0)" class="btn btn-default menu-descript-more-button font-weight-bold" data-delay="400" data-placement="bottom" id="<?= $menu->id ?>" data-toggle="popover" data-trigger="focus" data-html="true" data-content='<a href="javascript:void(0)" class="menu-delete-action">Delete Menu?</a>'>
                                    <?= $this->Html->image('more.png', ['style' => 'height: 20px; width: 19.83px;']) ?>
                                </a>
                                <div id="dropdown-propover" class="d-none">
                                    <?= $this->Form->postLink('Delete Menu?', ['action' => 'delete', $menu->id], ['id' => 'menu-delete-' . $menu->id]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php foreach ($menu['menu_categories'] as $menu_categories) { ?>
                <div class="col-12 my-3">
                    <div class="layers bgc-white p-20 rounded">
                        <div class="layer w-100 p-5">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 id="category-name-<?= $menu_categories->id ?>" class="col-12 text-dark font-weight-bold"><span><?= $menu_categories->category_name ?></span> <a href="javascript:changeCategoryName(<?= $menu_categories->id ?>)"><?= $this->Html->image('edit.png', ['class' => 'menu-edit-icon propa-pencil']) ?></a></h3>
                                    <p id="category-description-<?= $menu_categories->id ?>" class="col-12 text-dark"><span><?= $menu_categories->category_description ?></span> <a href="javascript:changeCategoryDescription(<?= $menu_categories->id ?>)"><?= $this->Html->image('edit.png', ['class' => 'menu-edit-icon propa-pencil']) ?></a></p>
                                </div>
                                <div class="col-md-3 text-right">
                                    <a href="javascript:manageOptions(<?= $menu_categories->id ?>,'<?= $menu_categories->category_name ?>')" class="mr-3 h5" style="color:#d51b81;">
                                        <?= $this->Html->image('noun-menu-1378672.png', ['class' => 'mr-2', 'style' => 'height: 20px;']) ?>Add options
                                    </a>
                                    <a href="javascript:void(0)" class="btn btn-default menu-descript-more-button font-weight-bold">
                                        <div class="dropdown dropleft" style="display: initial;">
                                            <a href="javascript:void(0)" class="btn btn-default menu-descript-more-button font-weight-bold" data-delay="400" data-placement="bottom" id="<?= $menu_categories->id ?>" category-toggle="popover" data-trigger="focus" data-html="true" data-content='<a href="javascript:void(0)" class="change-position">Change position</a><br><br><a href="javascript:void(0)" class="category-delete-action">Delete Category?</a>'>
                                                <?= $this->Html->image('more.png', ['style' => 'height: 20px; width: 19.83px;']) ?>
                                            </a>
                                            <div class="dropdown-menu" class="d-none">
                                                <?= $this->Form->postLink('Delete', ['controller' => 'MenuCategories', 'action' => 'delete', $menu_categories->id], ['id' => 'category-delete-' . $menu_categories->id]) ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row mt-3 menu-sortable">
                                <?php foreach ($menu_categories['menu_products'] as $menu_products) { ?>
                                    <div class="col-md-4 col-sm-6 my-2 menu-product-item-info" product-id="<?= $menu_products->id ?>">
                                        <div class="layers bd bgc-white p-10 rounded">
                                            <div class="layer w-100 row">
                                                <div class="col-md-3 px-0">
                                                    <div class="square" style="background-image: url('<?= WEB_PRODUCT . @$menu_products['menu_products_images'][0]->image ?>'), url('<?= $this->Url->build('/img/product_image_placeholder.png') ?>');"></div>
                                                </div>
                                                <div class="col-md-7" onclick="editProduct(<?= $menu_products->id ?>)">
                                                    <p class="mb-0 text-truncate text-dark menu-product-info"><?= $menu_products->product_name ?></p>
                                                    <p class="mb-0 text-truncate menu-product-info"><?= $menu_products->product_detail ?></p>
                                                    <p class="mb-0 text-truncate menu-product-info">£ <?= $menu_products->product_price ?></p>
                                                </div>
                                                <div class="col-md-1">
                                                    <i class="drag-option d-md-block d-none"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-4 col-sm-6 my-2">
                                    <div class="layers bgc-white p-10 rounded text-center new-item-buttom">
                                        <a href="javascript:addProduct(<?= $menu_categories->id ?>)" class="layer w-100 my-4 py-2">+ New item</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-12 my-2">
                <div class="layers bgc-white p-20 rounded">
                    <div class="layer w-100 p-5">
                        <div class="row">
                            <div class="col-12 text-center">
                                <a href="javascript:addCategory(<?= $menu->id ?>)" class="new-item-buttom new-category-buttom py-5">+ New category</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Category -->
<div class="modal fade" id="add-category-modal" tabindex="-1" role="dialog" aria-labelledby="New category" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark"> Add New Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <?= $this->Form->create($menu_category, ['id' => 'modal-form']) ?>
            <div class="modal-body">
                <div class="row mt-4">
                    <div class="col-12">
                        <?= $this->Form->control('category_name', ['placeholder' => 'e.g. Lunch, Snacks, Diner', 'label' => 'Name']) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<!-- Manage Option -->
<div class="modal fade" id="manage-options-modal" tabindex="-1" role="dialog" aria-labelledby="Options" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Options for <span id="option-title">category</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="button" id="save-action-option-button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Products -->
<div class="modal fade" id="menu-modal" tabindex="-1" role="dialog" aria-labelledby="New item" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Add new item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body px-4">

            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="button" class="btn btn-primary" id="action-modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="monu-delete-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Delete Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body text-dark">
                Are you sure to delete this menu?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="action-delete-menu-confirmed">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="category-delete-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark">Delete Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <div class="modal-body text-dark">
                Are you sure to delete this category?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="action-delete-category-confirmed">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- Add Category -->
<div class="modal fade" id="category-position-modal" tabindex="-1" role="dialog" aria-labelledby="New category" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center text-dark"> Change position</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <?= $this->Html->image('noun-cross-1776308.png', ['style' => 'height: 15px;']) ?>
                </button>
            </div>
            <?= $this->Form->create(null, ['url' => ['controller' => 'MenuCategories', 'action' => 'sorting']]) ?>
            <div class="modal-body">
                <div class="row mt-4">
                    <div class="col-12">
                        <ul id="sortable" class="pl-0">
                            <?php foreach ($menu['menu_categories'] as $menu_categories) { ?>
                                <li class="ui-state-default layers bd bgc-white p-10 rounded">
                                    <div class="layer w-100">
                                        <?= $this->Form->hidden('category_id.', ['value' => $menu_categories->id]) ?>
                                        <?= $menu_categories->category_name ?>
                                        <i class="drag-option pull-right" style="position: relative;"></i>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row w-100">
                    <div class="col-6 text-left">
                        <button type="button" class="btn-cancle" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>