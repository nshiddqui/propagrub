<?php
$this->assign('title', 'Manage Menu Categories');
?>
<div class="card">
    <div class="card-header">
        <h3>Add/ View Menu Categories</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <?= $this->Form->control('menu_filter', ['label' => 'Select Menu', 'value' => $menu_filter, 'options' => $menu, 'empty' => 'All Menu', 'label' => false]) ?>  
                    </div>
                </div>
                <?= $this->DataTables->render('MenuCategories') ?>
            </div>
            <div class="col-md-4">
                <h3><?= !empty($menu_category->id) ? 'Edit' : 'Add New' ?> Menu Categories</h3>
                <?= $this->Form->create($menu_category) ?>
                <?= $this->Form->control('menu_id', ['label' => 'Select Menu', 'options' => $menu]) ?>
                <?= $this->Form->control('category_name') ?>
                <div class="text-center">
                    <?= $this->Form->submit('Save') ?>
                </div>
            </div>
        </div>
    </div>
</div>