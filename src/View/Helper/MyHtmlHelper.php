<?php

namespace App\View\Helper;

use Cake\View\Helper\HtmlHelper;
use Cake\Core\Configure;

class MyHtmlHelper extends HtmlHelper
{

    public function component($path, $type = 'css', array $options = array())
    {
        $path = '/bower_components/' . $path;
        return parent::{$type}($path, $options);
    }

    public function image($path, array $options = array())
    {
        $image = $this->Url->build('/img/not-found.png');
        if (array_key_exists('onerror', $options)) {
            $image = $options['onerror'];
        }
        $options['onerror'] = "this.onerror=null;this.src='{$image}';";
        if (strpos($path, '://') === false) {
            if (file_exists(WWW_ROOT . Configure::read('App.imageBaseUrl') . $path) === false) {
                if (array_key_exists('profile', $options)) {
                    $path = $options['profile'] . $path;
                    unset($options['profile']);
                } else {
                    $path = WEB_DOC . $path;
                }
            }
        }
        return parent::image($path, $options);
    }

    public function link($title, $url = null, array $options = array())
    {
        if (is_string($url) && !isset($options['web']) && (strpos($url, 'javascript:') === false)) {
            if (strpos($url, '://') === false) {
                $url = WEB_DOC . $url;
            }
        }
        return parent::link($title, $url, $options);
    }
}
